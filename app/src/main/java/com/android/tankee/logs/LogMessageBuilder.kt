package com.android.tankee.logs


import java.lang.StringBuilder


class LogMessageBuilder {

    lateinit var message: String
    var stepCount: Int? = null
    var logStatus: LogStatus? = null

    fun build(): String {
        val messageBuilder = StringBuilder()

        stepCount?.let { messageBuilder.append("\t").append(buildStepCount()) }
        logStatus?.let { messageBuilder.append("\t").append(buildStatusMessage()) }
        messageBuilder.append("\t").append(message)

        return messageBuilder.toString()
    }

    private fun buildStepCount(): String {
        return "Step_$stepCount."
    }

    private fun buildStatusMessage(): String {
        return "Status: ${logStatus?.value}."
    }

}

enum class LogStatus(val value: String) {

    EXECUTING("Executing"),
    SUCCESS("Success"),
    FAILED("Failed")

}