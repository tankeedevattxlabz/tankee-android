package com.android.tankee.logs


import android.util.Log
import com.android.tankee.TANKEE


object AppLogger {

    fun logInfo(tag: String, message: String, stepCount: Int? = null, logStatus: LogStatus? = null) {
        Log.i(buildLogTag(tag), buildLogMessage(message, stepCount, logStatus))
    }

    fun logDebug(tag: String, message: String, stepCount: Int? = null, logStatus: LogStatus? = null) {
        Log.d(buildLogTag(tag), buildLogMessage(message, stepCount, logStatus))
    }

    fun logDebug(tag: String, subtag: String, message: String?, stepCount: Int? = null, logStatus: LogStatus? = null) {
        Log.d(buildLogTag(tag), buildLogMessage("$subtag:\t$message", stepCount, logStatus))
    }

    fun logError(tag: String, message: String, stepCount: Int? = null, logStatus: LogStatus? = null) {
        Log.e(buildLogTag(tag), buildLogMessage(message, stepCount, logStatus))
    }

    private fun buildLogTag(tag: String): String {
        return "${TANKEE}_$tag"
    }

    private fun buildLogMessage(message: String, stepCount: Int? = null, logStatus: LogStatus? = null): String {
        val logBuilder = LogMessageBuilder()

        logBuilder.message = message
        stepCount?.let { logBuilder.stepCount = it }
        logStatus?.let { logBuilder.logStatus = it }

        return logBuilder.build()
    }

}
