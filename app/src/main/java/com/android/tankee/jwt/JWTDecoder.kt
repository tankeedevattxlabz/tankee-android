package com.android.tankee.jwt


import com.android.tankee.ui.authorization.KWS_USER_ID
import com.auth0.android.jwt.JWT


object JWTDecoder {

    fun decodeAccessToken(accessToken: String): String? {
        val parsedJWT = JWT(accessToken)
        val subscriptionMetaData = parsedJWT.getClaim(KWS_USER_ID)

        return subscriptionMetaData.asString()
    }
}