package com.android.tankee.extentions

import android.view.View
import org.jetbrains.anko.runOnUiThread

fun View.visibility(visible: Boolean) {
    if (visible) {
        show()
    } else {
        gone()
    }
}

fun View.show() {
    context?.runOnUiThread {
        visibility = View.VISIBLE
    }
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    context?.runOnUiThread {
        visibility = View.GONE
    }
}

fun View.enable() {
    isEnabled = true
}

fun View.disable() {
    isEnabled = false
}

fun View.enableClick() {
    isEnabled = true
    isClickable = true
}

fun View.finishClick() {
    isEnabled = false
    isClickable = false
}

fun View.disableClick() {
    isEnabled = false
    isClickable = false
}

