package com.android.tankee.extentions


import androidx.recyclerview.widget.RecyclerView


fun RecyclerView.layoutManagerWidth(): Int {
    return layoutManager?.width!!
}