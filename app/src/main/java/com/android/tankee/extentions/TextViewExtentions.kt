package com.android.tankee.extentions

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.widget.TextView

fun TextView.bold(boldString: String) {
    val string = this.text
    if (string.isNotEmpty()) {
        val sb = SpannableStringBuilder(string)
        val bss = StyleSpan(Typeface.BOLD)

        if (string.contains(boldString)) {
            val startIndex = string.indexOf(boldString)
            val endIndex = startIndex + boldString.length
            sb.setSpan(bss, startIndex, endIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            this.text = sb
        }
    }
}
