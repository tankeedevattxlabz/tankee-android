package com.android.tankee

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class GlideAppModule : AppGlideModule() {

//    override fun applyOptions(context: Context, builder: GlideBuilder) {
//        val memoryCacheSizeBytes = 1024 * 1024 * 20 // 20mb
//        builder.setMemoryCache(LruResourceCache(memoryCacheSizeBytes.toLong()))
//        builder.setDiskCache(InternalCacheDiskCacheFactory(context, memoryCacheSizeBytes.toLong()))
//        builder.setDefaultRequestOptions(gameRequestOptions(context))
////        builder.createPlayerFragment(context)
//    }
//
//    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
//        val client = OkHttpClient.Builder().eventListener(TankeeApp.connection.connectionStateListener)
//            .readTimeout(20, TimeUnit.SECONDS)
//            .connectTimeout(20, TimeUnit.SECONDS)
//            .createPlayerFragment()
//
//        val factory = OkHttpUrlLoader.Factory(client)
//
//        glide.registry.replace(GlideUrl::class.java, InputStream::class.java, factory)
//    }
//
//    private fun gameRequestOptions(context: Context): RequestOptions {
//        return RequestOptions()
//            .signature(
//                ObjectKey(
//                    System.currentTimeMillis() / (24 * 60 * 60 * 1000)
//                )
//            )
////            .override(400, 400)
//            .encodeFormat(bg_index_page.CompressFormat.PNG)
//            .encodeQuality(100)
//            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//            .skipMemoryCache(false)
//    }
}