package com.android.tankee.crashlytics


import com.android.tankee.BuildConfig
import com.android.tankee.utils.log
import com.google.firebase.crashlytics.FirebaseCrashlytics





/**
 * Created by Sergey Kulyk on 2019-10-24.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
object CrashlyticsLogSender {

    fun sendLog(message: String) {
        if (!BuildConfig.DEBUG) {
            val crashlytics = FirebaseCrashlytics.getInstance()
            crashlytics.log(message)
        }
    }

    fun sendLog(priority: Int, tag: String, message: String) {
        if (!BuildConfig.DEBUG) {
            val crashlytics = FirebaseCrashlytics.getInstance()
            crashlytics.log(tag, message)
        }
    }

}