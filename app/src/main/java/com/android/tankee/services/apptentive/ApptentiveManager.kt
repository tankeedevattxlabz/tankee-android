package com.android.tankee.services.apptentive


import android.util.Log
import com.android.tankee.BuildConfig
import com.android.tankee.getAppContext
import com.apptentive.android.sdk.Apptentive


object ApptentiveManager {

    fun seeAllAnotherSection() {
        engage(ApptentiveEvents.SEE_ALL_ANOTHER_SECTION.event)
    }

    // TODO need to add later
    fun saveClip() {
        engage(ApptentiveEvents.SAVE_CLIP.event)
    }

    fun replayVideo() {
        engage(ApptentiveEvents.REPLAY_VIDEO.event)
    }

    fun openVideoItem() {
        engage(ApptentiveEvents.OPEN_VIDEO_ITEM.event)
    }

    fun openVideo() {
        engage(ApptentiveEvents.OPEN_VIDEO.event)
    }

    // Doesn't use it now
    fun openSettingsItem() {
        engage(ApptentiveEvents.OPEN_SETTINGS_ITEM.event)
    }

    fun openNotificationItem() {
        engage(ApptentiveEvents.OPEN_NOTIFICATION_ITEM.event)
    }

    // TODO need to add later
    fun openMyStuffItem() {
        engage(ApptentiveEvents.OPEN_MY_STUFF_ITEM.event)
    }

    fun openMoreVideos() {
        engage(ApptentiveEvents.OPEN_MORE_VIDEOS.event)
    }

    // TODO need to add later
    fun openGamesItem() {
        engage(ApptentiveEvents.OPEN_GAMES_ITEM.event)
    }

    fun openGamerFromVideoScreen() {
        engage(ApptentiveEvents.OPEN_GAMER_FROM_VIDEO_SCREEN.event)
    }

    fun openGameTypes() {
        engage(ApptentiveEvents.OPEN_GAME_TYPES.event)
    }

    fun openGameFromVideoScreen() {
        engage(ApptentiveEvents.OPEN_GAME_FROM_VIDEO_SCREEN.event)
    }

    // TODO need to add later
    fun openGameFromGamesScreen() {
        engage(ApptentiveEvents.OPEN_GAME_FROM_GAMES_SCREEN.event)
    }

    // TODO need to add later
    fun openDownloadItem() {
        engage(ApptentiveEvents.OPEN_DOWNLOAD_ITEM.event)
    }

    // TODO need to add later
    fun changeAvatar() {
        engage(ApptentiveEvents.CHANGE_AVATAR.event)
    }

    // TODO need to add later
    fun addVideoToFavorite() {
        engage(ApptentiveEvents.ADD_VIDEO_TO_FAVORITE.event)
    }

    fun addSticker() {
        engage(ApptentiveEvents.ADD_STICKER.event)
    }

    fun openAbout() {
        engage(ApptentiveEvents.OPEN_ABOUT.event)
    }

    // TODO need to add later
    fun seeAllFavorites() {
        engage(ApptentiveEvents.SEE_ALL_FAVORITE.event)
    }

    fun openAppVideoScreen() {
        engage(ApptentiveEvents.OPEN_APP_VIDEO_SCREEN.event)
    }

    fun openApp() {
        engage(ApptentiveEvents.OPEN_APP.event)
    }

    private fun engage(event: String) {
        Apptentive.engage(getAppContext(), event)
    }

}

enum class ApptentiveEvents(val event: String) {

    SEE_ALL_ANOTHER_SECTION("see_all_another_section"),
    // TODO need to add later
    SAVE_CLIP("save_clip"),
    REPLAY_VIDEO("replay_video"),
    OPEN_VIDEO_ITEM("open_video_item"),
    OPEN_VIDEO("open_video"),
    // Doesn't use it now
    OPEN_SETTINGS_ITEM("open_settings_item"),
    OPEN_NOTIFICATION_ITEM("open_notification_item"),
    // TODO need to add later
    OPEN_MY_STUFF_ITEM("open_my_stuff_item"),
    OPEN_MORE_VIDEOS("open_more_videos"),
    OPEN_GAMES_ITEM("open_games_item"),
    OPEN_GAMER_FROM_VIDEO_SCREEN("open_gamer_from_video_screen"),
    OPEN_GAME_TYPES("open_game_types"),
    OPEN_GAME_FROM_VIDEO_SCREEN("open_game_from_video_screen"),
    // TODO need to add later
    OPEN_GAME_FROM_GAMES_SCREEN("open_game_from_games_screen"),
    // TODO need to add later
    OPEN_DOWNLOAD_ITEM("open_download_item"),
    // TODO need to add later
    CHANGE_AVATAR("change_avatar"),
    // TODO need to add later
    ADD_VIDEO_TO_FAVORITE("add_video_to_favorite"),
    ADD_STICKER("add_sticker"),
    OPEN_ABOUT("open_about"),
    // TODO need to add later
    SEE_ALL_FAVORITE("see_all_favorite"),
    OPEN_APP_VIDEO_SCREEN("open_app_video_screen"),
    OPEN_APP("open_app"),

}