package com.android.tankee.services.apptentive


import android.app.Application
import com.apptentive.android.sdk.Apptentive
import com.apptentive.android.sdk.ApptentiveConfiguration


object ApptentiveController {

    private const val APPTENTIVE_APP_KEY = "ANDROID-TANKEE-35813c35dd8c"
    private const val APPTENTIVE_APP_SIGNATURE = "b3bddb1dd9d28912615cb53b7264a092"

    fun registerApptentive(application: Application) {
        val configuration = ApptentiveConfiguration(APPTENTIVE_APP_KEY, APPTENTIVE_APP_SIGNATURE)
        Apptentive.register(application, configuration)

    }

}