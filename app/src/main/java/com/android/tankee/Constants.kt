package com.android.tankee

const val TANKEE = "tankee"

const val BASE_THUMBNAIL_URL = "http://18.222.78.220/v4"
const val BASE_THUMBNAIL_URL_IMAGE = "http://api.tankee.com"
const val TANKEE_PRICACY = "https://www.tankee.com/policy"

const val HTML_BR = "<br>"

const val HTTP = "http://"
const val HTTPS = "https://"
const val GAME_TYPES_COUNT = 5

const val EMPTY_STRING = ""
const val NULL_STRING = "null"
const val AT_SIGN = "@"
const val SPACE = " "

const val OS_ANDROID = "Android"
