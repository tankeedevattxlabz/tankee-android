package com.android.tankee.web

enum class HtmlTags(val tag: String) {
    Div("div")
}

enum class HtmlTagsAttrs(val attr: String) {
    Id("id")

}