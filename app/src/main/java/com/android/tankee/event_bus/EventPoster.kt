package com.android.tankee.event_bus

import org.greenrobot.eventbus.EventBus

class EventPoster {

    companion object {
        @JvmStatic
        fun <T> postEvent(event: T) {
            EventBus.getDefault().post(event)
        }
    }
}