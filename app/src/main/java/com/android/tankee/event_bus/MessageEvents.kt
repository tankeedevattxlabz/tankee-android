package com.android.tankee.event_bus

import com.android.tankee.notifications.model.NotificationModel

class CloseEvent
class CloseCreateAccountPopupEvent
class OnHomeScreenNavigationEvent
class OnHomeScreenNavigationEventAfterDeleting
class OnRegistrationSettingsNavigationEvent
class CloseDialogEvent
class CloseDeleteDialogEvent
class ConnectionEvent
class ItemCheckedEvent
class OpenCreateAccountDialog
class ChangeToolbarAvatar

data class OnFavoriteGamesSelectedAction(val hasSelectedGames: Boolean = false)
data class OnFavoriteGamersSelectedAction(val hasSelectedGamers: Boolean = false)
class UpdateGeneratedUserNameEvent

/**
 * Notifications Event
 */
class ShowNotificationsMarkerEvent
data class NewNotificationEvent(val notificationModel: NotificationModel)
