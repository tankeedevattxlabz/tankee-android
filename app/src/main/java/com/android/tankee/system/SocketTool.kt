package com.android.tankee.system

import android.net.TrafficStats
import android.os.Handler
import android.os.HandlerThread
import androidx.annotation.WorkerThread
import java.net.*
import java.security.SecureRandom

internal class SocketTool(private val connectionStateListener: ConnectionStateListener) {
    private val random = SecureRandom()
    private val handler: Handler
    private val runnable = Runnable { startSocketTool() }

    private val host: String
        get() {
            val hosts = arrayOf("1.1.1.1", "google.com", "www.baidu.com")
            for (host in hosts) {
                try {
                    if (InetAddress.getAllByName(host).isNotEmpty()) {
                        return host
                    }
                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
            throw IllegalStateException("Unable to Resolve hostname")
        }

    init {
        val handlerThread = HandlerThread("SocketToolThread")
        handlerThread.start()
        val looper = handlerThread.looper
        handler = Handler(looper)
    }

    fun cancelConnectionCheckQuery() {
        handler.removeCallbacks(runnable)
    }

    fun updateConnectionCheckQuery() {
        cancelConnectionCheckQuery()
        handler.postDelayed(runnable, 1000)
    }

    private fun getProxyForSocket(uri: URI): Proxy {
        var prx = Proxy.NO_PROXY
        val proxyList = ProxySelector.getDefault().select(uri)
        if (Proxy.NO_PROXY != proxyList[0])
            prx = proxyList[0]

        return prx
    }

    @WorkerThread
    private fun startSocketTool() {
        try {
            var mTrafficTag = TrafficStats.getThreadStatsTag()
            if (mTrafficTag <= 0) {
                mTrafficTag = Math.abs(random.nextInt(Integer.MAX_VALUE))
                TrafficStats.setThreadStatsTag(mTrafficTag)
            }
            val uri = URI("socket://$host:80")
            val socket = Socket(getProxyForSocket(uri))
            socket.connect(InetSocketAddress(InetAddress.getByName(uri.host).hostAddress, uri.port))
            socket.close()
            connectionStateListener.setState(true)
        } catch (ignored: Throwable) {
            connectionStateListener.setState(false)
        }
    }
}