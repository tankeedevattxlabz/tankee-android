package com.android.tankee.system

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.*
import android.net.wifi.WifiManager
import android.os.Build
import com.android.tankee.event_bus.ConnectionEvent
import com.android.tankee.event_bus.EventPoster
import okhttp3.Call
import okhttp3.EventListener
import okhttp3.Protocol
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Proxy
import java.util.concurrent.atomic.AtomicBoolean

class ConnectionStateListener internal constructor(private val context: Application) : EventListener() {
    private val isConnectionOk = AtomicBoolean(false)
    private val socketTool: SocketTool
    private val connectivityManager: ConnectivityManager?

    private var networkCallback: ConnectivityManager.NetworkCallback? = null
    private var receiverTypeConnection: BroadcastReceiver? = null

    val isConnected: Boolean
        get() {
            updateConnectionCheckQuery()
            return isConnectionOk.get()
        }

    init {
        socketTool = SocketTool(this)
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkCallback = object : ConnectivityManager.NetworkCallback() {
                override fun onCapabilitiesChanged(
                    network: Network,
                    networkCapabilities: NetworkCapabilities
                ) {
                    EventPoster.postEvent<Any>(ConnectionEvent())
                }

                override fun onLinkPropertiesChanged(network: Network, linkProperties: LinkProperties) {
                    EventPoster.postEvent<Any>(ConnectionEvent())
                }

                override fun onAvailable(network: Network) {
                    EventPoster.postEvent<Any>(ConnectionEvent())
                }

                override fun onLost(network: Network) {
                    EventPoster.postEvent<Any>(ConnectionEvent())
                }
            }
        } else {
            //pre-Lollipop devices
            receiverTypeConnection = object : BroadcastReceiver() {
                override fun onReceive(ctx: Context, intent: Intent) {
                    EventPoster.postEvent<Any>(ConnectionEvent())
                }
            }
        }
    }

    fun startListeners() {

        //Looking for the network changes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && connectivityManager != null) {
            connectivityManager.registerNetworkCallback(NetworkRequest.Builder().build(), networkCallback)
        } else {
            //NOTE: almost all intent actions deprecated for latest Android OS versions (05/2019)
            val intentFilter = IntentFilter()
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)
            intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)
            intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
            context.registerReceiver(receiverTypeConnection, intentFilter)
        }
    }

    fun stopListeners() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && connectivityManager != null) {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } else {
            context.unregisterReceiver(receiverTypeConnection)
        }
    }

    //OkHttp3 finished OK
    override fun connectEnd(
        call: Call?, inetSocketAddress: InetSocketAddress?, proxy: Proxy?,
        protocol: Protocol?
    ) {
        super.connectEnd(call, inetSocketAddress, proxy, protocol)
        setState(true)
    }

    override fun callEnd(call: Call?) {
        super.callEnd(call)
        setState(true)
    }

    //OkHttp3 failed
    override fun connectFailed(
        call: Call?,
        inetSocketAddress: InetSocketAddress?,
        proxy: Proxy?,
        protocol: Protocol?,
        ioe: IOException?
    ) {
        super.connectFailed(call, inetSocketAddress, proxy, protocol, ioe)
        if (ioe!!.toString().contains("Network is unreachable")) {
            setState(false)
        }
    }

    override fun callFailed(call: Call?, ioe: IOException?) {
        super.callFailed(call, ioe)
        if (ioe!!.toString().contains("Network is unreachable")) {
            setState(false)
        }
    }

    internal fun setState(newState: Boolean) {
        //cancel  SocketCheck if it planned
        socketTool.cancelConnectionCheckQuery()
        if (newState != isConnectionOk.get()) {
            isConnectionOk.set(newState)
            EventPoster.postEvent<Any>(ConnectionEvent())
        }
    }

    private fun updateConnectionCheckQuery() {
        socketTool.updateConnectionCheckQuery()
    }
}
