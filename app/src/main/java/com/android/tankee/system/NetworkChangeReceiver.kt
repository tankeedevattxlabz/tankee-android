package com.android.tankee.system

import com.android.tankee.utils.isNetworkAvailable

class NetworkChangeReceiver : Connection.NetworkListener {

    var onNetworkAvailable: (() -> Unit?)? = null
    var onNetworkDisable: (() -> Unit?)? = null

    override fun networkChanged() {
        if (isNetworkAvailable()) {
            onNetworkAvailable?.invoke()
        } else {
            onNetworkDisable?.invoke()
        }
    }
}