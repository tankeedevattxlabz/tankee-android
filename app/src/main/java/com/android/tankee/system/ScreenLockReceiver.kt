package com.android.tankee.system

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.android.tankee.event_bus.ConnectionEvent
import com.android.tankee.event_bus.EventPoster

import java.util.concurrent.atomic.AtomicBoolean

class ScreenLockReceiver : BroadcastReceiver() {
    private val screen_on = AtomicBoolean(true)

    val isScreenOn: Boolean
        get() = screen_on.get()

    override fun onReceive(context: Context, intent: Intent) {

        screen_on.set(Intent.ACTION_SCREEN_ON == intent.action)

        EventPoster.postEvent<Any>(ConnectionEvent())
    }
}