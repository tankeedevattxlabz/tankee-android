package com.android.tankee.system

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Handler
import android.os.Looper
import android.telephony.TelephonyManager
import com.android.tankee.event_bus.ConnectionEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.net.NetworkInterface
import java.util.*
import java.util.concurrent.TimeUnit

class Connection(private val context: Application) {
    private val connectivityManager: ConnectivityManager?
    private val netlistLis = ArrayList<NetworkListener>()
    private val notifyNetworkChangedTask = Runnable {
        for (i in netlistLis.indices) {
            val l = netlistLis[i]
            l.networkChanged()
        }
    }
    private val screenLockReceiver = ScreenLockReceiver()
    var connectionStateListener: ConnectionStateListener
    private val mainThreadHandler = Handler(Looper.getMainLooper())
    private var lastActiveNetworkInfo: NetworkInfo? = null
    private var lastKnownConnection = false

    val isConnection: Boolean
        get() = connectionStateListener.isConnected

    val isWiFi: Boolean
        get() {
            val activeNetworkInfo = networkInfo
            return activeNetworkInfo != null && activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
        }

    val isConnectedMobile: Boolean
        get() {
            val activeNetworkInfo = networkInfo
            return activeNetworkInfo != null && activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE
        }

    /**
     * Check if there is fast connectivity
     */
    val isConnectedFast: Boolean
        get() {
            val activeNetworkInfo = networkInfo
            return activeNetworkInfo != null && isConnectionFast(activeNetworkInfo.type, activeNetworkInfo.subtype)
        }

    //TelephonyManager.NETWORK_TYPE_LTE_CA:
    //default: return "UNKNOWN";
    val networkTypeName: String
        get() {
            val networkInfo = networkInfo ?: return "UNKNOWN"

            val type = networkInfo.type
            val subType = networkInfo.subtype

            if (type == ConnectivityManager.TYPE_WIFI) {
                return "WiFi"
            } else if (type == ConnectivityManager.TYPE_MOBILE) {
                when (subType) {
                    TelephonyManager.NETWORK_TYPE_GPRS -> return "GPRS"
                    TelephonyManager.NETWORK_TYPE_EDGE -> return "EDGE"
                    TelephonyManager.NETWORK_TYPE_UMTS -> return "UMTS"
                    TelephonyManager.NETWORK_TYPE_HSDPA -> return "HSDPA"
                    TelephonyManager.NETWORK_TYPE_HSUPA -> return "HSUPA"
                    TelephonyManager.NETWORK_TYPE_HSPA -> return "HSPA"
                    TelephonyManager.NETWORK_TYPE_CDMA -> return "CDMA"
                    TelephonyManager.NETWORK_TYPE_EVDO_0 -> return "CDMA - EvDo rev. 0"
                    TelephonyManager.NETWORK_TYPE_EVDO_A -> return "CDMA - EvDo rev. A"
                    TelephonyManager.NETWORK_TYPE_EVDO_B -> return "CDMA - EvDo rev. B"
                    TelephonyManager.NETWORK_TYPE_1xRTT -> return "CDMA - 1xRTT"
                    TelephonyManager.NETWORK_TYPE_LTE -> return "LTE"
                    TelephonyManager.NETWORK_TYPE_EHRPD -> return "CDMA - eHRPD"
                    TelephonyManager.NETWORK_TYPE_IDEN -> return "iDEN"
                    TelephonyManager.NETWORK_TYPE_HSPAP -> return "HSPA+"
                    TelephonyManager.NETWORK_TYPE_GSM -> return "GSM"
                    TelephonyManager.NETWORK_TYPE_TD_SCDMA -> return "TD_SCDMA"
                    TelephonyManager.NETWORK_TYPE_IWLAN -> return "IWLAN"
                    19 -> return "LTE_CA"
                }

                return "UNKNOWN MOBILE NETWORK"
            }
            try {
                val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (intf in interfaces) {
                    if (intf.displayName.startsWith("usb"))
                        return "USB TETHERING"
                    else if (intf.displayName.startsWith("bt"))
                        return "BLUETOOTH TETHERING"
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return "UNKNOWN"
        }

    val networkInfo: NetworkInfo?
        get() = connectivityManager?.activeNetworkInfo

    init {
        connectionStateListener = ConnectionStateListener(context)
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        initConnectionReceivers()
    }

    /**
     * Check if the connection is fast
     */
    private fun isConnectionFast(type: Int, subType: Int): Boolean {
        return when (type) {
            ConnectivityManager.TYPE_WIFI -> true
            ConnectivityManager.TYPE_MOBILE -> when (subType) {
                TelephonyManager.NETWORK_TYPE_1xRTT -> false // ~ 50-100 kbps
                TelephonyManager.NETWORK_TYPE_CDMA -> false // ~ 14-64 kbps
                TelephonyManager.NETWORK_TYPE_EDGE -> false // ~ 50-100 kbps
                TelephonyManager.NETWORK_TYPE_EVDO_0 -> true // ~ 400-1000 kbps
                TelephonyManager.NETWORK_TYPE_EVDO_A -> true // ~ 600-1400 kbps
                TelephonyManager.NETWORK_TYPE_GPRS -> false // ~ 100 kbps
                TelephonyManager.NETWORK_TYPE_HSDPA -> true // ~ 2-14 Mbps
                TelephonyManager.NETWORK_TYPE_HSPA -> true // ~ 700-1700 kbps
                TelephonyManager.NETWORK_TYPE_HSUPA -> true // ~ 1-23 Mbps
                TelephonyManager.NETWORK_TYPE_UMTS -> true // ~ 400-7000 kbps
                /*
                     * Above API level 7, make sure to set android:targetSdkVersion
                     * to appropriate level to use these
                     */
                TelephonyManager.NETWORK_TYPE_EHRPD // API level 11
                -> true // ~ 1-2 Mbps
                TelephonyManager.NETWORK_TYPE_EVDO_B // API level 9
                -> true // ~ 5 Mbps
                TelephonyManager.NETWORK_TYPE_HSPAP // API level 13
                -> true // ~ 10-20 Mbps
                TelephonyManager.NETWORK_TYPE_IDEN // API level 8
                -> false // ~25 kbps
                TelephonyManager.NETWORK_TYPE_LTE // API level 11
                -> true // ~ 10+ Mbps
                // Unknown
                TelephonyManager.NETWORK_TYPE_UNKNOWN -> false
                else -> false
            }
            else -> false
        }
    }

    private fun checkConnection() {
        try {
            val activeNetworkInfo = networkInfo
            val isConnected = isConnection
            val isNetworkChanged = activeNetworkInfo != null && activeNetworkInfo != lastActiveNetworkInfo
            if (screenLockReceiver.isScreenOn && (isNetworkChanged || isConnected != lastKnownConnection)) {
                lastActiveNetworkInfo = activeNetworkInfo
                lastKnownConnection = isConnected
                notifyNetworkChange()
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun initConnectionReceivers() {

        //looking for the Screen ON/OFF
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_SCREEN_ON)
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
        context.registerReceiver(screenLockReceiver, intentFilter)
        connectionStateListener.startListeners()
        EventBus.getDefault().register(this)
    }

    fun close() {
        context.unregisterReceiver(screenLockReceiver)
        connectionStateListener.stopListeners()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ConnectionEvent) {
        checkConnection()
    }

    fun addNetworkListener(listener: NetworkListener) {
        netlistLis.add(listener)
    }

    fun removeNetworkListener(listener: NetworkListener) {
        netlistLis.remove(listener)
    }

    private fun notifyNetworkChange() {
        mainThreadHandler.removeCallbacks(notifyNetworkChangedTask)
        mainThreadHandler.postDelayed(notifyNetworkChangedTask, TimeUnit.SECONDS.toMillis(1))
    }

    interface NetworkListener {
        fun networkChanged()
    }
}