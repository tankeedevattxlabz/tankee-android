package com.android.tankee.repo.base

import com.android.tankee.ui.base.BaseView

/**
 * Created by Sergey Kulyk on 2019-09-07.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */

abstract class BaseRepo(open val baseView: BaseView)