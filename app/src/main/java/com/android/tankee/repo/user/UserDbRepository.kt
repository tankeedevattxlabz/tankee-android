package com.android.tankee.repo.user

import com.android.tankee.database.AppDatabase
import com.android.tankee.database.dao.UsersDao
import com.android.tankee.rest.responses.users.UserModel

object UserDbRepository {

    fun getUserModel(): UserModel? {
        return userDao().getUser()
    }

    fun updateUser(userModel: UserModel) {
        userDao().updateUser(userModel)
    }

    fun getAvatar(): String? {
        return userDao().getUser()?.avatar
    }

    fun getUserName(): String? {
        return userDao().getUser()?.fullName
    }

    private fun userDao(): UsersDao {
        return AppDatabase.getAppDatabase().userDao()
    }



}