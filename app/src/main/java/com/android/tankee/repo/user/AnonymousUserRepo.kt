package com.android.tankee.repo.user


class AnonymousUserRepo {

    fun isUserAnonymous(): Boolean {
        val user = UserDbRepository.getUserModel()
        return user == null
    }

    fun isNotUserAnonymous(): Boolean {
        return !isUserAnonymous()
    }

}