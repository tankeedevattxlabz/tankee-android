package com.android.tankee.repo.avatars


import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.database.AppDatabase
import com.android.tankee.ui.my_stuff.pic.pic_avatar.AvatarModel
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.getString
import com.android.tankee.utils.getStringArray


object AvatarsRepository {

    private const val AVATARS_FREE_START_INDEX = 0
    private const val AVATARS_FREE_END_INDEX = 12
    const val DEFAULT_AVATAR_ID = "0"

    /**
     * Get avatar content name for Analytics.
     */
    fun getAnalyticsAvatarName(): String {
        var avatarId = DEFAULT_AVATAR_ID.toInt()
        try {
            avatarId = getAvatarId().toInt()
        } catch (e: NumberFormatException) {
            // Handle this bug if there is no exist valid into avatarId.
        }

        val avatarNamesList = getStringArray(R.array.avatar_name_list)

        return if (avatarId in 0 until avatarNamesList.size) {
            avatarNamesList[avatarId]
        } else {
            getString(R.string.unknown_avatar)
        }
    }

    fun getAvatar(): Int {
        return getAvatarById(getAvatarId())
    }

    fun getAvatarId(): String {
        val userModel = AppDatabase.getAppDatabase().userDao().getUser()
        var userAvatar = DEFAULT_AVATAR_ID

        if (userModel != null) {
            if (userModel.avatar != null) {
                userAvatar = userModel.avatar!!.replace("avatar", EMPTY_STRING)
            }
        }

        return userAvatar
    }

    fun getAvatars(): List<AvatarModel> {
        val avatars = mutableListOf<AvatarModel>()

        for (i in AVATARS_FREE_START_INDEX..AVATARS_FREE_END_INDEX) {
            val avatarModel = buildAvatarModel("$i")
            avatars.add(avatarModel)
        }

        return avatars
    }

    fun getAvatarById(avatarId: String): Int {
        val avatarDrawerId = getAvatarDrawableId(buildAvatarDrawableName(avatarId))
        return if (avatarDrawerId != 0) {
            avatarDrawerId
        } else {
            getDefaultAvatar()
        }
    }

    fun getDefaultAvatar(): Int {
        return R.drawable.avatar_0
    }

    private fun buildAvatarModel(avatarId: String): AvatarModel {
        val drawableStringName = buildAvatarDrawableName(avatarId)
        val drawableId = getAvatarDrawableId(drawableStringName)

        return AvatarModel(avatarId, drawableId)
    }

    private fun getAvatarDrawableId(avatarId: String): Int {
        return ResUtils.getDrawableIdByString(avatarId)
    }

    private fun buildAvatarDrawableName(avatarId: String): String {
        return "avatar_$avatarId"
    }

    fun buildAvatarModel(avatarId: String?, isSelected: Boolean = false): AvatarModel {
        return AvatarModel(
                avatarId ?: DEFAULT_AVATAR_ID,
                if (avatarId != null) getAvatarById(avatarId) else getDefaultAvatar(),
                isSelected
        )
    }

}


/**
 * Repository for getting user's selected background.
 *
 * If user is vendor than get default background.
 * Else get background by avatar id from UserModel.
 */
class UserBackgroundRepo {

    fun getBackground(): Int {
        val avatarId = AvatarsRepository.getAvatarId()

        val backgroundDrawableName = buildBackgroundDrawableName(avatarId)
        val drawableBackgroundId = getBackgroundDrawableId(backgroundDrawableName)
        if (drawableBackgroundId == 0) {
            return getDefaultBackground()
        }
        return drawableBackgroundId
    }

    private fun buildBackgroundDrawableName(backgroundId: String): String {
        return "background_$backgroundId"
    }

    private fun getBackgroundDrawableId(backgroundId: String): Int {
        return ResUtils.getDrawableIdByString(backgroundId)
    }

    private fun getDefaultBackground(): Int {
        return DEFAULT_BACKGROUND
    }

    companion object {
        const val DEFAULT_BACKGROUND = R.drawable.background_0
    }

}