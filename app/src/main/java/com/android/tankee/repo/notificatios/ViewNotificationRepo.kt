package com.android.tankee.repo.notificatios

import com.android.tankee.BuildConfig
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.repo.base.BaseRepo
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.log
import com.google.firebase.crashlytics.FirebaseCrashlytics


/**
 * Created by Sergey Kulyk on 2019-09-07.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
class ViewNotificationRepo(override val baseView: BaseView) : BaseRepo(baseView) {

    fun viewNotifications(list: List<NotificationModel>) {
        list.forEach {
            it.id?.let { id ->
                viewNotification(id)
            }
        }
    }

    fun viewNotification(id: Int) {
        TankeeRest.api.viewNotification(id).enqueue(
            baseView.createCallBack(
                {
                    AppLogger.logDebug(
                        TAG,
                        "View notification id=$id.",
                        logStatus = LogStatus.SUCCESS
                    )
                },
                {
                    AppLogger.logDebug(
                        TAG,
                        "View notification id=$id.",
                        logStatus = LogStatus.FAILED
                    )

                    if (!BuildConfig.DEBUG) {
                        val crashlytics = FirebaseCrashlytics.getInstance()
                        crashlytics.log(
                            TAG,
                            "Notification update:\tNotification with id=$id has error while updating."
                        )
                    }
                }
            )
        )
    }

    companion object {
        var TAG: String = ViewNotificationRepo::class.java.simpleName
    }
}