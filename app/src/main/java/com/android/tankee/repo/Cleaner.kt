package com.android.tankee.repo

import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.database.AppDatabase

object TankeeCleaner {

    fun clearPrefs() {
        TankeePrefs.clearAll()
    }

    fun cleanDb() {
        AppDatabase.getAppDatabase().clearAllTables()
    }

    fun cleanAllData() {
        clearPrefs()
        cleanDb()
    }
}