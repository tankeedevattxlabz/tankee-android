package com.android.tankee.repo.user


import com.android.tankee.EMPTY_STRING
import com.android.tankee.extentions.isNotNull
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.notifications.TankeeMessagingService.Companion.FCM_TAG
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.base.BaseView
import com.google.firebase.iid.FirebaseInstanceId
import java.util.*


class FcmTokenRepo(private val view: BaseView) {

    var onUpdateAction: (() -> Unit)? = null
    var onErrorAction: (() -> Unit)? = null

    fun updateFcmTokenAndTimeZone(
    ) {
        val fcmToken = FirebaseInstanceId.getInstance().token
        val timeZone = Calendar.getInstance().timeZone.id ?: EMPTY_STRING

        AppLogger.logDebug(FCM_TAG, "$fcmToken")
        AppLogger.logDebug(FCM_TAG, "TimeZone", timeZone)

        if (fcmToken.isNotNull()) {
            TankeeRest.api.updateFcmTokenAndTimeZone(fcmToken!!, timeZone).enqueue(
                view.createCallBack(
                    {
                        AppLogger.logDebug(FCM_TAG, "Update fcm token and timezone", logStatus = LogStatus.SUCCESS)
                        onUpdateAction?.invoke()
                    },
                    {
                        AppLogger.logDebug(FCM_TAG, "Update fcm token and timezone", logStatus = LogStatus.FAILED)
                        onUpdateAction?.invoke()
                    }
                )
            )
        } else {
            onUpdateAction?.invoke()
        }
    }

    fun clearFcmTokenAndTimeZone() {
        TankeeRest.api.updateFcmTokenAndTimeZone(EMPTY_STRING, EMPTY_STRING).enqueue(
            view.createCallBack(
                {
                    AppLogger.logDebug(FCM_TAG, "FCM token was cleared", logStatus = LogStatus.SUCCESS)
                    onUpdateAction?.invoke()
                },
                {
                    AppLogger.logDebug(FCM_TAG, "FCM token was not cleared", logStatus = LogStatus.FAILED)
                    onErrorAction?.invoke()
                }
            )
        )
    }

}