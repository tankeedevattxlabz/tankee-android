package com.android.tankee.ui.gif


import com.android.tankee.getAppRecourse
import pl.droidsonroids.gif.GifDrawable
import pl.droidsonroids.gif.GifImageView


class GifImageViewHelper(private val gifImageView: GifImageView) {

    private var gifDrawable: GifDrawable? = null

    fun setAnimation(animationId: Int): GifImageViewHelper {
        gifDrawable = GifDrawable(getAppRecourse(), animationId)
        gifImageView.setImageDrawable(gifDrawable)
        gifDrawable?.reset()

        return this
    }

    fun setSpeed(speed: Float = 1f): GifImageViewHelper {
        gifDrawable?.setSpeed(speed)
        return this
    }

    fun setOnFinishAction(action: (() -> Unit)): GifImageViewHelper {
        gifDrawable?.duration?.toLong()?.let { duration ->
            gifImageView.postDelayed({ action.invoke() }, duration)
        }
        return this
    }

    fun reset() {
        gifDrawable?.reset()
    }

    fun start() {
        gifDrawable?.start()
    }
}