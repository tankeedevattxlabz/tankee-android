package com.android.tankee.ui.my_stuff.pic.pic_favorites_games.choose_favorite_games


import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.extentions.finishClick
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.enableClick
import com.android.tankee.extentions.show
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.OnRecyclerItemClickListener
import com.android.tankee.ui.gif.GifImageViewHelper
import com.android.tankee.ui.request_options.gameRequestOptions
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.ResWrapper
import com.android.tankee.utils.isTable
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_fav_game.view.*
import org.jetbrains.anko.textColor


class FavoriteGamesAdapter : BaseRecyclerAdapter<
        Game,
        OnRecyclerItemClickListener<Game>,
        FavoriteGamesAdapter.ChooseFavoriteGameViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseFavoriteGameViewHolder {
        return ChooseFavoriteGameViewHolder(inflate(R.layout.item_fav_game, parent, false))
    }

    fun setFavoriteOrUnfavorableGame(item: Game) {
        for (i in 0 until itemCount) {
            val game = items[i]
            if (item.id == game.id) {
                game.like?.let { game.like = !game.like!! }
                notifyItemChanged(i)
                break
            }
        }
    }

    class ChooseFavoriteGameViewHolder(itemView: View) :
            BaseVideoHolder<Game, OnRecyclerItemClickListener<Game>>(itemView) {

        override fun onBind(item: Game, listener: OnRecyclerItemClickListener<Game>?) {
            Glide.with(getAppContext())
                    .load(item.getValidImageUrl())
                    .apply(gameRequestOptions)
                    .into(itemView.favGameImage)

            itemView.favGameName.text = item.name

            if (item.like != null && item.like!!) {
                setFavoriteGame()
            } else {
                setUnfavorableGame()
            }

            itemView.setOnClickListener {
                onFavGameClick(item, listener)
            }
        }

        private fun setFavoriteGame() {
            itemView.heartImage.show()
            itemView.blueMask.show()
            setFavoriteGameNameColor()
            setGameNameMaskBackground(true)
        }

        private fun setUnfavorableGame() {
            itemView.heartImage.gone()
            itemView.blueMask.gone()
            setUnfavoredGameNameColor()
            setGameNameMaskBackground(false)
        }

        private fun setFavoriteGameNameColor() {
            itemView.favGameName.textColor = if (isTable()) {
                ResUtils.getColor(R.color.colorWhite)
            } else {
                ResUtils.getColor(R.color.colorFavGameNameTextActive)
            }
        }

        private fun setUnfavoredGameNameColor() {
            itemView.favGameName.textColor = ResUtils.getColor(R.color.colorFavGameNameTextInactive)
        }

        private fun onFavGameClick(item: Game, listener: OnRecyclerItemClickListener<Game>?) {
            itemView.finishClick()
            // Add delay on isFavorite / unfavorable game click for preventing spanning server.
            itemView.postDelayed(
                    { itemView.enableClick() },
                    ResUtils.getInteger(R.integer.favorite_game_click_delay).toLong()
            )

            if (item.isNotFavoriteGame()) {
                setFavoriteGameAnimation { listener?.onItemClicked(item) }
                setFavoriteGameNameColor()
                TransitionManager.beginDelayedTransition(itemView.favoriteGameRoot)
                itemView.blueMask.show()
                setGameNameMaskBackground(true)
                setFavoriteHeartScaling()
            } else {
                setUnfavorableHeartScalingUp()
                listener?.onItemClicked(item)
            }
        }

        private fun setGameNameMaskBackground(favorite: Boolean) {
            if (isTable()) {
                if (favorite) {
                    itemView.gameNameMask.show()
                } else {
                    itemView.gameNameMask.gone()
                }
            }
        }

        private fun setFavoriteGameAnimation(onFinishAction: () -> Unit?) {
            GifImageViewHelper(itemView.favoriteAnimation)
                    .setAnimation(R.drawable.favorite_animation)
                    .setOnFinishAction {
                        itemView.favoriteAnimation.gone()
                        onFinishAction.invoke()
                    }
                    .start()

            itemView.favoriteAnimation.show()
        }

        private fun setFavoriteHeartScaling() {
            itemView.heartImage.show()

            itemView.heartImage.scaleX = 0.0f
            itemView.heartImage.scaleY = 0.0f

            itemView.heartImage.animate().scaleX(1f).duration = ResWrapper.getHeartImageScaleDuration()
            itemView.heartImage.animate().scaleY(1f).duration = ResWrapper.getHeartImageScaleDuration()
        }

        private fun setUnfavorableHeartScalingUp() {
            itemView.heartImage.scaleX = 1f
            itemView.heartImage.scaleY = 1f

            itemView.heartImage.animate().scaleX(0f).duration = ResWrapper.getHeartImageScaleDuration()
            itemView.heartImage.animate().scaleY(0f).duration = ResWrapper.getHeartImageScaleDuration()
        }
    }
}