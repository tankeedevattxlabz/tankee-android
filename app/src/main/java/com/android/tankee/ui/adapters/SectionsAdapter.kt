package com.android.tankee.ui.adapters

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.NUM_RECORDS
import com.android.tankee.ui.sections.SectionModel
import com.android.tankee.ui.sections.SectionType
import com.android.tankee.ui.sections.VideoSectionModel
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllBuilder
import com.android.tankee.ui.views.see_all.SeeAllStyle
import com.android.tankee.ui.views.see_all.SeeAllType
import com.android.tankee.utils.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.view_section.view.*
import kotlinx.android.synthetic.main.view_top_games_sections.view.*

class SectionsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var sections: List<SectionModel> = listOf()
    var sectionItemClickListener: SectionItemClickListener? = null
    var scrollToGamesSectionListener: ScrollToGamesSectionListener? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var layoutId = R.layout.view_section

        if (viewType == 1) {
            layoutId = R.layout.view_top_games_sections
        }

        val itemView = LayoutInflater.from(parent.context)
            .inflate(layoutId, parent, false)
        var holder: RecyclerView.ViewHolder = AnotherSectionHolder(itemView)
        when (viewType) {
            SectionType.TOP.value -> holder = TopViewHolder(itemView)
            SectionType.GAMES.value -> holder = GamesSectionHolder(itemView)
            SectionType.GAMERS.value -> holder = GamersSectionsHolder(itemView)
            SectionType.ANOTHER.value -> holder = AnotherSectionHolder(itemView)
            SectionType.GAME_TYPES.value -> holder = GameTypesSectionHolder(itemView)
        }
        return holder
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    override fun getItemViewType(position: Int): Int {
        return sections[position].sectionType.value
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val section = sections[position]
        when (section.sectionType) {
            SectionType.TOP -> (holder as TopViewHolder).onBind(section)
            SectionType.GAMES -> (holder as GamesSectionHolder).onBind(section)
            SectionType.GAMERS -> (holder as GamersSectionsHolder).onBind(section)
            SectionType.ANOTHER -> (holder as AnotherSectionHolder).onBind(section as VideoSectionModel)
            SectionType.GAME_TYPES -> (holder as GameTypesSectionHolder).onBind(section)
        }
    }

    inner class TopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(section: SectionModel) {
            Glide.with(getAppContext())
                .load(section.background)
                .apply(requestOptionsTop())
                .into(itemView.justForYouBackground)
            itemView.justForYouTitle.text = section.title

            // Init layout manager for "Just For you" section.
            // If device is table than use grid for 2 lines videos.
            val adapter: SectionAdapter
            val layoutManager: RecyclerView.LayoutManager
            if (isTable()) {
                layoutManager =
                    GridLayoutManager(
                        getAppContext(),
                        2,
                        GridLayoutManager.HORIZONTAL,
                        false
                    )
                adapter = VideoItemsAdapter()
            } else {
                layoutManager = createHorizontalLayoutManager()
                adapter = VideoItemsAdapter()
            }

            itemView.justForYouRecycler.layoutManager = layoutManager
            itemView.justForYouRecycler.adapter = adapter

            adapter.items = section.items
            adapter.notifyDataSetChanged()

            adapter.listener = object : SectionAdapter.ItemClickListener {
                override fun onClick(item: SectionItemMarker) {
                    sectionItemClickListener?.onClick(item, section.title)
                }
            }

            val bt = itemView.findViewById<ImageView>(R.id.scrollToGamesSectionButton)

            scrollToGamesSectionListener?.let { listener ->
                itemView.scrollToGamesSection.setOnClickListener {
                    val step1 = ObjectAnimator.ofFloat(bt, "translationY", 11.dp().toFloat())
                    val step2 = ObjectAnimator.ofFloat(bt, "translationY", 0.dp().toFloat())
                    step2.duration = 200
                    val set = AnimatorSet().apply {
                        play(step2).after(step1)
                        addListener(object : Animator.AnimatorListener {
                            override fun onAnimationRepeat(p0: Animator?) {
                            }

                            override fun onAnimationEnd(p0: Animator?) {
                                listener.scroll(it.translationY.toInt())
                            }

                            override fun onAnimationCancel(p0: Animator?) {
                            }

                            override fun onAnimationStart(p0: Animator?) {
                            }
                        })
                    }

                    set.start()
                }
            }
        }
    }

    inner class AnotherSectionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(section: VideoSectionModel) {
            Glide.with(getAppContext())
                .asBitmap()
                .load(buildUrl(section.backgroundUrl))
                .apply(requestOptions())
                .into(itemView.sectionBackground)
            itemView.sectionTitle.text = section.title

            val itemsAdapter: SectionAdapter = VideoItemsAdapter()
            itemView.itemsRecycler.layoutManager = createHorizontalLayoutManager()
            itemView.itemsRecycler.adapter = itemsAdapter

            val items = mutableListOf<SectionItemMarker>()
            items.addAll(section.items)

            // Add see all item.
            if (section.totalItemCount > NUM_RECORDS && section.items.isNotEmpty()) {
                val seeAllItem = SeeAllBuilder()
                    .setType(SeeAllType.SECTION)
                    .setStyle(SeeAllStyle.RED)
                    .setSectionId(section.id)
                    .setSectionName(section.title)
                    .build()
                items.let { items.add(seeAllItem) }
            }

            itemsAdapter.items = items
            itemsAdapter.notifyDataSetChanged()

            itemsAdapter.listener = object : SectionAdapter.ItemClickListener {
                override fun onClick(item: SectionItemMarker) {
                    sectionItemClickListener?.onClick(item, section.title)
                }
            }
        }
    }

    inner class GamesSectionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(section: SectionModel) {
            Glide.with(getAppContext())
                .load(section.background)
                .apply(requestOptions())
                .into(itemView.sectionBackground)

            // Set titles
            itemView.sectionTitle.text = section.title

            // Init layout manager for "Just For you" section.
            // If device is table than use grid for 2 lines videos.

            itemView.itemsRecycler.layoutManager = createHorizontalLayoutManager()

            val gamesAdapter = GameItemsAdapter()
            val gameItems = mutableListOf<SectionItemMarker>()
            gameItems.addAll(section.items)
            val seeAll = SeeAllBuilder()
                .setType(SeeAllType.GAMES)
                .setStyle(SeeAllStyle.BLUE)
                .build()

            gameItems.add(seeAll)
            gamesAdapter.items = gameItems
            gamesAdapter.notifyDataSetChanged()
            itemView.itemsRecycler.adapter = gamesAdapter

            // Init listeners
            gamesAdapter.listener = object : SectionAdapter.ItemClickListener {
                override fun onClick(item: SectionItemMarker) {
                    sectionItemClickListener?.onClick(item, section.title)
                }
            }
        }
    }

    inner class GamersSectionsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(section: SectionModel) {
            Glide.with(getAppContext())
                .load(section.background)
                .apply(requestOptions())
                .into(itemView.sectionBackground)

            // Set titles
            itemView.sectionTitle.text = section.title
            itemView.itemsRecycler.layoutManager = createHorizontalLayoutManager()

            val gamesAdapter = GamersAdapter()
            val gameItems = mutableListOf<SectionItemMarker>()
            gameItems.addAll(section.items)
            val seeAll = SeeAllBuilder()
                .setType(SeeAllType.GAMERS)
                .setStyle(SeeAllStyle.ORANGE)
                .build()

            gameItems.add(seeAll)
            gamesAdapter.items = gameItems
            gamesAdapter.notifyDataSetChanged()
            itemView.itemsRecycler.adapter = gamesAdapter

            // Init listeners
            gamesAdapter.listener = object : SectionAdapter.ItemClickListener {
                override fun onClick(item: SectionItemMarker) {
                    sectionItemClickListener?.onClick(item, section.title)
                }
            }
        }
    }

    inner class GameTypesSectionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(section: SectionModel) {
            if (!isTable()) {
                itemView.layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    getAppContext().resources.getDimension(R.dimen.game_type_section).toInt().px()
                )
            }
            Glide.with(getAppContext())
                .load(section.background)
                .apply(requestOptions())
                .into(itemView.sectionBackground)
            itemView.sectionTitle.text = section.title

            itemView.itemsRecycler.layoutManager = createHorizontalLayoutManager()

            val gamesAdapter = GameTypesAdapter()
            gamesAdapter.items = section.items
            gamesAdapter.notifyDataSetChanged()
            itemView.itemsRecycler.adapter = gamesAdapter

            gamesAdapter.listener = object : SectionAdapter.ItemClickListener {
                override fun onClick(item: SectionItemMarker) {
                    sectionItemClickListener?.onClick(item, section.title)
                }
            }
        }
    }

    private fun requestOptions(): RequestOptions {
        return RequestOptions()
            .centerCrop()
            .override(1200, getAppContext().resources.getDimension(R.dimen.home_section_height).toInt().px())
            .encodeFormat(Bitmap.CompressFormat.PNG)
            .encodeQuality(100)
            .skipMemoryCache(false)
    }


    private fun requestOptionsTop(): RequestOptions {
        return RequestOptions()
            .centerCrop()
            .encodeFormat(Bitmap.CompressFormat.PNG)
            .encodeQuality(100)
            .skipMemoryCache(false)
    }

    interface SectionItemClickListener {
        fun onClick(item: SectionItemMarker, title: String? = null)
    }

    interface ScrollToGamesSectionListener {
        fun scroll(positionY: Int)
    }

    fun onDestroy() {
        scrollToGamesSectionListener = null
        sectionItemClickListener = null
    }
}