package com.android.tankee.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.ui.request_options.gameRequestOptions
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildUrl
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_game.view.*

class GameItemsAdapter : SectionAdapter() {
//    init {
//        setHasStableIds(true)
//    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSectionViewHolder {
        var layoutId = R.layout.item_game

        if (viewType == 0) {
            layoutId = R.layout.item_see_all
        }

        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

        return when (viewType) {
            1 -> GameItemsViewHolder(itemView)
            else -> SeeAllViewHolder(itemView)
        }
    }

    inner class GameItemsViewHolder(itemView: View) : SectionViewHolder(itemView) {
        override fun onBind(item: SectionItemMarker) {
            if (item is Game) {
                val image: String? = if (item.imageUrl != null) {
                    item.imageUrl
                } else {
                    buildUrl(item.avatar?.url.toString())
                }

                Glide.with(getAppContext())
                        .load(image)
                        .apply(gameRequestOptions)
                        .into(itemView.gameTypeImage)
                item.name?.let { itemView.gameTypeName.text = it }
                listener?.let { itemView.setOnClickListener { iv -> it.onClick(item) } }
            }
        }
    }
}