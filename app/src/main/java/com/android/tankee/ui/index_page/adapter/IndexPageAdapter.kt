package com.android.tankee.ui.index_page.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.index_page.adapter.holders.IndexPageDeleteViewHolder
import com.android.tankee.ui.index_page.adapter.holders.IndexPageHeaderViewHolder
import com.android.tankee.ui.index_page.adapter.holders.IndexPageTagsViewHolder
import com.android.tankee.ui.index_page.adapter.holders.items_holder.IndexPageItemsViewHolder
import com.android.tankee.ui.index_page.adapter.models.*
import com.android.tankee.ui.index_page.tags.TagModel
import com.android.tankee.ui.views.see_all.SectionItemMarker

class IndexPageAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var loadTagItemsListener: LoadTagItemsListener? = null
    var backClickListener: BackClickListener? = null
    var deleteClickListener: DeleteClickListener? = null
    var items = mutableListOf<IndexPageHolderModel>()
    var itemClickListener: SectionAdapter.ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutId = when (viewType) {
            IndexPageViewHolderType.HEADER.value -> R.layout.item_index_page_header
            IndexPageViewHolderType.TAGS.value -> R.layout.item_index_page_tags
            IndexPageViewHolderType.DELETE.value -> R.layout.item_index_page_delete
            else -> R.layout.item_index_page_horizontal_items
        }

        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

        return when (viewType) {
            IndexPageViewHolderType.HEADER.value -> IndexPageHeaderViewHolder(itemView)
            IndexPageViewHolderType.TAGS.value -> IndexPageTagsViewHolder(itemView)
            IndexPageViewHolderType.DELETE.value -> IndexPageDeleteViewHolder(itemView)
            else -> IndexPageItemsViewHolder(itemView)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]

        when (holder) {
            is IndexPageHeaderViewHolder -> {
                holder.onBind(item as IndexPageHeaderHolderModel, backClickListener)
            }
            is IndexPageTagsViewHolder -> {
                holder.onBind(item as TagsHolderModel, loadTagItemsListener)
            }
            is IndexPageDeleteViewHolder -> {
                holder.onBind(item as DeleteHolderModel, deleteClickListener)
            }
            is IndexPageItemsViewHolder -> {
                holder.onBind(item as IndexPageItemsHolderModel, itemClickListener)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item) {
            is IndexPageHeaderHolderModel -> IndexPageViewHolderType.HEADER.value
            is TagsHolderModel -> IndexPageViewHolderType.TAGS.value
            is DeleteHolderModel -> IndexPageViewHolderType.DELETE.value
            else -> IndexPageViewHolderType.ITEMS.value
        }
    }

    fun insertIndexPageModels(indexPageHolderModels: List<IndexPageHolderModel>) {
        val lastItemPos = itemCount
        items.addAll(indexPageHolderModels)
        notifyItemRangeChanged(lastItemPos, lastItemPos + indexPageHolderModels.size)
    }

    fun removeItems() {
        val newIndexPageItems = mutableListOf<Int>()

        for (i in 0 until itemCount) {
            if (items[i] is IndexPageItemsHolderModel) {
                newIndexPageItems.add(i)
            }
        }
        newIndexPageItems.reverse()
        newIndexPageItems.forEach {
            items.removeAt(it)
            notifyItemRemoved(it)
        }
    }

    fun setIndexPageModels(indexPageHolderModels: List<IndexPageHolderModel>) {
        items = indexPageHolderModels as MutableList<IndexPageHolderModel>
        notifyDataSetChanged()
    }

    enum class IndexPageViewHolderType(val value: Int) {
        HEADER(1),
        TAGS(2),
        ITEMS(3),
        DELETE(4)
    }

    interface LoadTagItemsListener {
        fun loadTagItems(tagModel: TagModel)
    }

    interface DeleteClickListener {
        fun onDelete(items: List<SectionItemMarker>)
        fun onDeleteStateChanged(isDeleteMode: Boolean)
        fun onDeleteFinished()
    }
    interface BackClickListener {
        fun onBackClickListener()
    }
}