package com.android.tankee.ui.search

import android.os.Handler
import com.android.tankee.EMPTY_STRING
import com.android.tankee.database.AppDatabase
import com.android.tankee.database.models.SuggestionModel
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.base.lazy_loading.LazyLoadingBaseModel
import java.util.*

class SearchViewModel(private val searchView: SearchView) {

    private val db = AppDatabase.getAppDatabase()
    private val lazyLoadingModel = SearchLazyLoadingModel()

    private var queryHandler: Handler = Handler()
    private var queryRunnable: Runnable? = null

    fun initSearchedVideos() {
        val searchHeaderModel = SearchHeaderModel()

        searchHeaderModel.title = lazyLoadingModel.title
        searchHeaderModel.totalVideosCount = lazyLoadingModel.items.size

        searchView.setHeaderItem(searchHeaderModel)
        searchView.setVideos(lazyLoadingModel.items, lazyLoadingModel.title, lazyLoadingModel.totalResult)
    }

    fun loadMoreItems() {
//        addLocalSuggestion(lazyLoadingModel.title)

        if (!lazyLoadingModel.isLoading && !lazyLoadingModel.isFinished) {
            lazyLoadingModel.startLoading()
            lazyLoadingModel.let {
                TankeeRest.api.getSearchVideos(lazyLoadingModel.title, lazyLoadingModel.page)
                    .enqueue(searchView.createCallBack({ response ->
                        response.body()?.let { body ->
                            body.items?.let { videos ->
                                if (videos.size < LazyLoadingBaseModel.DEFAULT_LAZY_LOADING_ITEMS) {
                                    lazyLoadingModel.finishLazyLoading()
                                }

                                lazyLoadingModel.totalResult = body.totalResults!!
                                searchView.addMoreItems(videos, lazyLoadingModel.title, lazyLoadingModel.totalResult)
                                lazyLoadingModel.items.addAll(videos)
                            }

                            lazyLoadingModel.stopLoading()
                            lazyLoadingModel.incrementPage()
                        }
                    }, {
                        lazyLoadingModel.stopLoading()
                    }))
            }
        }
    }

    fun getQuerySuggestions(query: String = "") {
        stopQuayHandler()
        queryRunnable = Runnable { getQuerySuggestionsPost(query) }
        queryHandler.postDelayed(queryRunnable, SUGGESTIONS_DELAY)
    }

    fun stopQuayHandler() {
        queryHandler.removeCallbacks(queryRunnable)
    }

    private fun getQuerySuggestionsPost(query: String) {
        TankeeRest.api.getSearchVideosSuggestions(query)
            .enqueue(searchView.createCallBack({ response ->
                response.body()?.let { body ->
                    searchView.setSuggestions(body.items!!, query)
                }
            }))
    }

    fun addLocalSuggestion(title: String = "", video: Video? = null) {
        if (isContainsTitleSuggestion(title)) return

        val suggestionModels = db.suggestionsDao().getAll()
        val suggestionModel = SuggestionModel()
        if (video != null) {
            video.id?.let { suggestionModel.id = it }
            suggestionModel.title = video.title
            suggestionModel.jwKey = video.jwKey
            suggestionModel.playlistKey = video.playlistKey
            suggestionModel.createdAt = Calendar.getInstance().timeInMillis
        } else {
            suggestionModel.title = title
        }

        if (suggestionModels.size >= MAX_SUGGESTION_SIZE) {
            db.suggestionsDao().delete(suggestionModels[0])
        }

        AppDatabase.getAppDatabase().suggestionsDao().insert(suggestionModel)
    }

    private fun isContainsTitleSuggestion(title: String): Boolean {
        val suggestionModels = db.suggestionsDao().getAll()
        suggestionModels.forEach {
            if (it.title == title) {
                updateSuggestionCreateAt(it)
                return true
            }
        }

        return false
    }

    private fun updateSuggestionCreateAt(suggestionModel: SuggestionModel) {
        suggestionModel.createdAt = Calendar.getInstance().timeInMillis
        db.suggestionsDao().update(suggestionModel)
    }

    fun setLocalSuggestions() {
        Handler().postDelayed({
            val suggestionModels = db.suggestionsDao().getAll()
            Collections.sort(suggestionModels, SuggestionsComparator())
            val suggestions = convertSuggestionsModelToVideos(suggestionModels)
            if (suggestions.isEmpty()) {
                getQuerySuggestions()
            } else {
                searchView.setSuggestions(suggestions)
            }
        }, 100)
    }

    private fun convertSuggestionsModelToVideos(suggestionModels: List<SuggestionModel>): MutableList<Video> {
        val videos = mutableListOf<Video>()
        suggestionModels.forEach {
            videos.add(convertSuggestionModelToVideo(it))
        }

        return videos
    }

    private fun convertSuggestionModelToVideo(suggestionModel: SuggestionModel): Video {
        val video = Video()
        video.id = suggestionModel.id
        video.title = suggestionModel.title
        suggestionModel.jwKey?.let { video.jwKey = it }
        suggestionModel.playlistKey?.let { video.playlistKey = it }

        return video
    }

    fun destroy() {
        AppDatabase.destroyInstance()
    }

    fun isEnableLazyLoading(): Boolean {
        return !lazyLoadingModel.isFirstPage() && !lazyLoadingModel.isLoading
    }

    fun checkIsSearchedQuery(query: String): Boolean {
        return lazyLoadingModel.title == query
    }

    fun updateTitle(query: String) {
        lazyLoadingModel.reset()
        lazyLoadingModel.title = query
    }

    fun isLoadNewItems(): Boolean {
        return lazyLoadingModel.isLoadNewItems()
    }

    interface SearchView : BaseView {
        fun addMoreItems(videos: List<Video>, title: String, totalResult: Int)
        fun setSuggestions(suggestions: List<Video>, title: String = EMPTY_STRING)
        fun setHeaderItem(headerModel: SearchHeaderModel)
        fun setVideos(videos: List<Video>, title: String, totalResult: Int)
    }

    companion object {
        const val MAX_SUGGESTION_SIZE = 5
        const val SUGGESTIONS_DELAY = 300L
    }
}

class SuggestionsComparator : Comparator<SuggestionModel> {
    override fun compare(o1: SuggestionModel?, o2: SuggestionModel?): Int {
        return if (o1?.createdAt!! < o2?.createdAt!!) {
            1
        } else {
            -1
        }
    }
}