package com.android.tankee.ui.home


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.dynamic_links.DynamicLinks.*
import com.android.tankee.event_bus.OpenCreateAccountDialog
import com.android.tankee.extentions.show
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.ui.authorization.create_account_dialog.CreateTankeeAccountDialog
import com.android.tankee.ui.home.toolbar.TankeeToolbar
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.SHOW_HIDE_TOOLBAR_ACTION
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.SHOW_TOOLBAR_ARG
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.UPDATE_TOOLBAR_ARG
import com.android.tankee.utils.getLastIndex
import com.android.tankee.utils.getLastItem
import kotlinx.android.synthetic.main.fragment_home_screen.*
import kotlinx.android.synthetic.main.fragment_home_screen.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class HomeScreen : Fragment() {

    private val set: ConstraintSet = ConstraintSet()
    private val homeNavigationStack = HomeNavigationStack()

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_home_screen, container, false)
        try {
            set.clone(inflate as ConstraintLayout)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbarSelectionListener()
        initNavControllerDestinationChangedListener()
        registerToolbarReceiver()
        setDefaultScreen()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            toolbar?.destroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            unregisterToolbarReceiver()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        toolbarReceiver = null
    }

    fun showToolbar() {
        toolbar?.show()
        view?.toolbar?.animate()?.translationY(0f)
        TankeeToolbar.isVisible = true
    }

    fun hideToolbar() {
        val h: Float = -(view?.toolbar?.height?.toFloat() ?: -0f)
        toolbar?.animate()?.translationY(h)
        TankeeToolbar.isVisible = false
    }

    private fun setDefaultScreen() {
        SessionDataHolder.dynamicLink?.let { path ->
            when (path.getPath()) {
                VideosTab.path -> toolbar.setSelectedItem(R.id.homeListFragment)
                MyStuffPage.path -> setSelectedItem(R.id.myStuffFragment)

                AvatarSelectionPage.path,
                FavoriteGamesPage.path,
                FavoriteGamersPage.path,
                FavoriteVideosPage.path -> {
                    setSelectedItem(R.id.myStuffFragment)
                    if (AnonymousUserRepo().isNotUserAnonymous()) {
                        return
                    }
                }

                NotificationsPage.path -> setSelectedItem(R.id.notificationsFragment)
                // TODO: Add downloads page.
                DownloadsPage.path -> setSelectedItem(R.id.homeListFragment)
                // TODO: Add downloads page.
                HeroPassSubscriptionPage.path -> setSelectedItem(R.id.homeListFragment)
                SigncaseupLoginScreen.path -> {
                    setSelectedItem(R.id.homeListFragment)
                    fragmentManager?.let { fm ->
                        CreateTankeeAccountDialog.openCreateTankeeDialog(fm, false)
                    }
                }
                TankeeAboutPage.path -> {
                    setSelectedItem(R.id.settingsFragment)
                    return
                }
                else -> {
                    setSelectedItem(R.id.homeListFragment)
                    return
                }
            }

            SessionDataHolder.clearDynamicLink()
        }

    }

    private fun setSelectedItem(fragmentId: Int) {
        toolbar.setSelectedItem(fragmentId)
    }

    private fun initToolbarSelectionListener() {
        toolbar.selectionListener = object : TankeeToolbar.ToolbarSelectionListener {
            override fun onDestinationSelected(selectedDest: Int) {

                val builder = NavOptions.Builder()
                builder.setPopUpTo(selectedDest, true)
                navController()?.navigate(
                    selectedDest,
                    null,
                    NavOptions.Builder().setPopUpTo(selectedDest, true).build()
                )
            }
        }
    }

    private fun initNavControllerDestinationChangedListener() {
        navController()?.addOnDestinationChangedListener { controller, destination, arguments ->
            toolbar.setSelectedItem(destination.id, false)
            Log.d(TAG, "onNavigate to " + destination.label)
        }
    }

    private fun navController() = activity?.findNavController(R.id.home_nav_host_fragment)

    private var toolbarReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            val show = intent?.extras?.getBoolean(SHOW_TOOLBAR_ARG, true)
            val update = intent?.extras?.getBoolean(UPDATE_TOOLBAR_ARG, false)

            if (update == true) {
                navController()?.currentDestination?.id?.let {
                    //                    navController()?.navigateUp()
                    view?.toolbar?.setSelectedItem(
                        it,
                        changeDest = false,
                        onBackClick = true
                    )
                }
            } else {
                show?.let {
                    if (show) {
                        showToolbar()
                    } else {
                        hideToolbar()
                    }
                }
            }
        }
    }

    private fun registerToolbarReceiver() {
        val filter = IntentFilter(SHOW_HIDE_TOOLBAR_ACTION)
        activity?.registerReceiver(toolbarReceiver, filter)
    }

    private fun unregisterToolbarReceiver() {
        activity?.unregisterReceiver(toolbarReceiver)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun openCreateAccountDialog(event: OpenCreateAccountDialog) {
        fragmentManager?.let { CreateTankeeAccountDialog.openCreateTankeeDialog(it, true) }
    }

    companion object {
        var TAG: String = HomeScreen::class.java.simpleName
    }

}

class HomeNavigationStack {

    private val stack = mutableListOf<Int>()

    fun add(destinationId: Int) {
        stack.add(destinationId)
    }

    fun remove() {
        if (stack.isNotEmpty()) {
            stack.remove(stack.getLastIndex())
        }
    }

    fun lastItem(): Int? {
        return if (stack.isNotEmpty()) {
            stack.getLastItem()
        } else {
            null
        }
    }

    fun hasDestination(destinationId: Int): Boolean {
        return stack.contains(destinationId)
    }

    fun removeTo(destinationId: Int) {
        if (stack.isNotEmpty()) {
            if (destinationId != stack.getLastIndex()) {
                stack.remove(destinationId)
                removeTo(destinationId)
            }
        }
    }

}