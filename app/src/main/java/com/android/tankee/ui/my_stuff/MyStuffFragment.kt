package com.android.tankee.ui.my_stuff


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.OpenCreateAccountDialog
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_my_stuff.*


class MyStuffFragment : BaseFragment(), MyStuffView {

    private var isToolBarHide: Boolean = false
    private var oldScroll: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.android.tankee.R.layout.fragment_my_stuff, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setOnScrollListener()

        if (AnonymousUserRepo().isUserAnonymous()) {
            view.postDelayed({
                EventPoster.postEvent(OpenCreateAccountDialog())
            }, 1)
        }
    }

    private fun setOnScrollListener() {
        myStuffRoot.viewTreeObserver.addOnScrollChangedListener {
            try {
                val scrollY = myStuffRoot.scrollY
                val diff = scrollY - oldScroll
                if (diff > 50) {
                    if (!isToolBarHide) {
                        hideToolbar()
                        isToolBarHide = !isToolBarHide
                    }
                } else if (diff < -15) {
                    if (isToolBarHide) {
                        showToolbar()
                        isToolBarHide = !isToolBarHide
                    }
                }
                oldScroll = scrollY
            } catch (e: Exception) {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        showToolbar()
    }
}
