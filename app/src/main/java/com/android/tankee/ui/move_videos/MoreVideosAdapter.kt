package com.android.tankee.ui.move_videos

import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.utils.millisToMinutes
import com.android.tankee.utils.millisToSeconds
import com.android.tankee.utils.showView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_more_video.view.*

class MoreVideosAdapter : BaseRecyclerAdapter<
        Video,
        BaseRecyclerAdapter.OnRecyclerItemClickListener<Video>,
        MoreVideosAdapter.MoreVideosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreVideosViewHolder {
        return MoreVideosViewHolder(inflate(R.layout.item_more_video, parent, false))
    }

    class MoreVideosViewHolder(itemView: View
    ) : BaseVideoHolder<Video, BaseRecyclerAdapter.OnRecyclerItemClickListener<Video>>(itemView) {
        override fun onBind(item: Video, listener: OnRecyclerItemClickListener<Video>?) {
            item.thumbnailUrl?.let {
                Glide.with(getAppContext())
                        .load(item.thumbnailUrl)
                        .into(itemView.moreVideoThumbnail)
            }
            item.title?.let { itemView.moreVideoTitle.text = it }
            item.duration?.let {
                showView(itemView.moreVideoDuration)
                itemView.moreVideoDuration.text = millisToMinutes(it.toFloat().toInt())
            }
            itemView.setOnClickListener { listener?.onItemClicked(item) }
        }
    }
}