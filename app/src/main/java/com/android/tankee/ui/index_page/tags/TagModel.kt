package com.android.tankee.ui.index_page.tags

data class TagModel(val tagName: String, var selected: Boolean)