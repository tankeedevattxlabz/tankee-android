package com.android.tankee.ui.settings.debug_info

import com.android.tankee.ui.base.BaseView

/**
 * Created by Sergey Kulyk on 2019-09-11.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
class DebugInfoPresenter(private val debugInfoView: DebugInfoView) {

    fun initDebugInfo() {
        debugInfoView.setDebugInfo(DebugInfoBuilder().build())
    }

}

interface DebugInfoView: BaseView {
    fun setDebugInfo(list: List<DebugInfoModel>)
}