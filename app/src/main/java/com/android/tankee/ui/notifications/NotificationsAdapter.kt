package com.android.tankee.ui.notifications


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.extentions.bold
import com.android.tankee.getAppContext
import com.android.tankee.notifications.model.BaseNotificationModel
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.notifications.model.NotificationPlaceHolderModel
import com.android.tankee.notifications.model.NotificationType
import com.android.tankee.ui.request_options.GlideRequestOptions
import com.android.tankee.ui.request_options.circleRequestOptions
import com.android.tankee.ui.request_options.roundRequestOptions
import com.android.tankee.utils.buildThumbnailUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_notification.view.*
import kotlinx.android.synthetic.main.item_notification_ph.view.*


class NotificationsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = mutableListOf<BaseNotificationModel>()
    var onItemClick: ((NotificationModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(viewType, parent, false)

        return getViewHolder(itemView, viewType)
    }

    private fun getViewHolder(itemView: View, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_notification -> NotificationViewHolder(itemView)
            else -> NotificationPlaceHolderViewHolder(itemView)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = items[position]) {
            is NotificationModel -> (holder as NotificationViewHolder).onBind(item)
            is NotificationPlaceHolderModel -> (holder as NotificationPlaceHolderViewHolder).onBind(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if (item is NotificationModel) {
            R.layout.item_notification
        } else {
            R.layout.item_notification_ph
        }
    }

    fun addItem(item: NotificationModel) {
        items.add(0, item)
        notifyDataSetChanged()
    }

    fun addItems(items: MutableList<BaseNotificationModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(model: NotificationModel) {
            val requestOptions = buildRequestOptions(model.type)
            Glide.with(getAppContext())
                    .load(buildThumbnailUrl(model.imageUrl))
                    .apply(requestOptions)
                    .into(itemView.notificationItemImage)
            itemView.notificationItemText.text = model.text
            model.bold?.let { itemView.notificationItemText.bold(it) }

            itemView.setOnClickListener { onItemClick?.invoke(model) }
        }

        private fun buildRequestOptions(type: String?): RequestOptions {
            return GlideRequestOptions.getNotificationRequestOptions(
                    NotificationType.getType(type
                            ?: EMPTY_STRING)
            )
        }
    }

    inner class NotificationPlaceHolderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun onBind(model: NotificationPlaceHolderModel) {
            itemView.notificationPlaceHolderImage.setImageResource(model.imageId)
        }

    }
}


