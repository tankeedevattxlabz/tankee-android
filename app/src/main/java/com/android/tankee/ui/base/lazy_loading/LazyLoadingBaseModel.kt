package com.android.tankee.ui.base.lazy_loading

open class LazyLoadingBaseModel(
        var page: Int = DEFAULT_START_PAGE,
        var isFinished: Boolean = false,
        var isLoading: Boolean = false
) {
    open fun reset() {
        page = DEFAULT_START_PAGE
        isFinished = false
        isLoading = false
    }

    fun isFirstPage(): Boolean {
        return page == DEFAULT_START_PAGE
    }

    fun incrementPage() {
        page++
    }

    fun isDefaultPage(): Boolean {
        return page == DEFAULT_START_PAGE
    }
    fun isLoadNewItems(): Boolean {
        return page == 1
    }

    fun startLoading() {
        isLoading = true
    }

    fun stopLoading() {
        isLoading = false
    }

    fun finishLazyLoading() {
        isFinished = true
    }

    companion object {
        const val DEFAULT_START_PAGE = 1
        const val DEFAULT_LAZY_LOADING_ITEMS = 36
    }
}