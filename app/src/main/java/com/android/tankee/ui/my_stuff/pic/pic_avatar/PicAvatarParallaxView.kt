package com.android.tankee.ui.my_stuff.pic.pic_avatar


import android.content.Context
import android.graphics.Rect
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.ui.home.videos.CenterLayoutManager
import com.android.tankee.ui.parallax.ParallaxController
import com.android.tankee.ui.parallax.ParallaxTransition
import kotlinx.android.synthetic.main.pic_avatar_parallax_view.view.*


class PicAvatarParallaxView(context: Context, attributeSet: AttributeSet) : FrameLayout(context, attributeSet) {

    private var parallaxController: ParallaxController? = null
    var onAvatarSelected: ((AvatarModel) -> Unit)? = null

    init {
        inflate(context, R.layout.pic_avatar_parallax_view, this)
    }

    public override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        var ss: SavedState? = null
        superState?.let {
            ss = SavedState(superState)
        }
//        Log.d("paralaxTransition", parallaxController?.parallaxTransition.toString())
        ss?.parallaxTransition = parallaxController?.parallaxTransition
        return ss
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        setCustomState(ss.parallaxTransition)
    }

    fun initParallaxItems(items: List<AvatarModel>) {
        val layoutManager = getCenterLayoutManager()

        avatarsRecycler.layoutManager = layoutManager
        avatarsRecycler.adapter = getAdapter(items)

        val marginItemDecoration = MarginItemDecoration(resources.getDimension(R.dimen.pic_avatar_list_margin).toInt())
        avatarsRecycler.addItemDecoration(marginItemDecoration)
        parallaxController = ParallaxController(avatarsRecycler, parallaxBackground)
        parallaxController?.itemDecoratorSize = resources.getDimension(R.dimen.pic_avatar_list_margin).toInt()
    }

    private fun getCenterLayoutManager(): CenterLayoutManager {
        val layoutManager = CenterLayoutManager(context)

        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        layoutManager.millisecondsPerInch = 50f

        return layoutManager
    }

    private fun getAdapter(items: List<AvatarModel>): RecyclerView.Adapter<*>? {
        val adapter = PicAvatarAdapter()
        adapter.items.addAll(items)
        adapter.listener = { avatarModel ->
            if (!avatarModel.isSelected) {
                onAvatarSelected?.invoke(avatarModel)
                setSelectedAvatar(avatarModel)
            }
        }
//        adapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<AvatarModel> {
//            override fun onItemClicked(item: AvatarModel) {
//                if (!item.isSelected) {
//                    onAvatarSelected?.invoke(item)
//                    adapter.selectItem(item)
//                }
//            }
//        }
        return adapter
    }

    fun setSelectedAvatar(avatarModel: AvatarModel) {
        (avatarsRecycler.adapter as PicAvatarAdapter).selectItem(avatarModel)
    }

    private fun setCustomState(parallaxTransition: ParallaxTransition?) {
        parallaxController?.setParallaxViewTranslation(parallaxTransition)
        parallaxController?.updateViewTransition()
    }

    internal class SavedState(superState: Parcelable) : BaseSavedState(superState), Parcelable {

        var parallaxTransition: ParallaxTransition? = null

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelable(parallaxTransition, flags)
        }
    }

    fun getSelectedAvatar(): AvatarModel? {
        return (avatarsRecycler.adapter as PicAvatarAdapter).getSelectedItem()
    }

    private fun getSelectedItemPosition(): Int {
        return (avatarsRecycler.adapter as PicAvatarAdapter).getSelectedItemPosition()
    }

    fun scrollToSelectedItem() {

        avatarsRecycler.postDelayed(
                { avatarsRecycler.smoothScrollToPosition(getSelectedItemPosition()) },
                SCROLL_TO_SELECTED_ITEM_DELAY
        )
    }

    companion object {
        const val SCROLL_TO_SELECTED_ITEM_DELAY = 400L
    }
}


class MarginItemDecoration(val spaceHorizontal: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) != 0) {
                left = spaceHorizontal
            }
        }
    }
}