package com.android.tankee.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.VideoView

/**
 * {@FullScreenVideoView} class scales video width and height
 * for correct fullscreen video.
 */
class FullScreenVideoView(context: Context?, attrs: AttributeSet?) : VideoView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            getDefaultSize(suggestedMinimumWidth, widthMeasureSpec),
            getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        )
    }
}