package com.android.tankee.ui.authorization.create_account_dialog


import android.app.Dialog
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.android.tankee.R
import com.android.tankee.event_bus.CloseCreateAccountPopupEvent
import com.android.tankee.event_bus.CloseEvent
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.ui.authorization.AuthorizationDialog
import com.android.tankee.utils.getColor
import kotlinx.android.synthetic.main.dialog_create_tankee_account.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class CreateTankeeAccountDialog : DialogFragment() {

    var moveToPreviousScreenIfClickBack = false

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)

        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(getColor(R.color.colorDialogBackground)))

        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_create_tankee_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createTankeeAccountCloseButton.setOnClickListener {
            if (AnonymousUserRepo().isUserAnonymous()) {
                if (moveToPreviousScreenIfClickBack) {
                    activity?.onBackPressed()
                }
            }
            dismiss()
        }
        joinTankeeButton.onClickAction = {
            AuthorizationDialog.openAuthorizationDialog(fragmentManager, R.id.homeScreen)
        }
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: CloseCreateAccountPopupEvent) {
        dismiss()
    }

    override fun onCancel(dialog: DialogInterface) {
        if (moveToPreviousScreenIfClickBack) {
            activity?.onBackPressed()
        }
    }

    companion object {
        var TAG: String = CreateTankeeAccountDialog::class.java.simpleName

        fun openCreateTankeeDialog(fragmentManager: FragmentManager, moveToPreviousScreenIfClickBack: Boolean = false) {
            val createTankeeAccountDialog = CreateTankeeAccountDialog()
            createTankeeAccountDialog.moveToPreviousScreenIfClickBack = moveToPreviousScreenIfClickBack
            createTankeeAccountDialog.show(fragmentManager, TAG)
        }
    }
}