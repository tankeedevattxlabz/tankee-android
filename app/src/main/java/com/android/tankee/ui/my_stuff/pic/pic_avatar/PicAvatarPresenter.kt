package com.android.tankee.ui.my_stuff.pic.pic_avatar


import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.database.AppDatabase
import com.android.tankee.event_bus.ChangeToolbarAvatar
import com.android.tankee.event_bus.EventPoster
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.repo.avatars.AvatarsRepository
import com.android.tankee.repo.user.UserDbRepository
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.tankee.UserRequestBodyHelper
import com.android.tankee.ui.base.BaseView


class PicAvatarPresenter(private val picAvatarView: PicAvatarView) {

    fun initAvatars() {
        picAvatarView.setAvatars(AvatarsRepository.getAvatars())
    }

    fun initSelectedAvatar() {
        val userModel = AppDatabase.getAppDatabase().userDao().getUser()
        AppLogger.logDebug(PIC_AVATAR_FLOW, "Init selected avatar.")
        AppLogger.logDebug(PIC_AVATAR_FLOW, userModel.toString())

        val avatarModel = if (userModel != null) {
            AvatarsRepository.buildAvatarModel(AvatarsRepository.getAvatarId(), true)
        } else {
            AvatarsRepository.buildAvatarModel(AvatarsRepository.DEFAULT_AVATAR_ID, true)
        }

        AppLogger.logDebug(PIC_AVATAR_FLOW, avatarModel.toString())
        AppLogger.logDebug(PIC_AVATAR_FLOW, "Selected avatar id is:\t${avatarModel.id}")

        picAvatarView.setSelectedAvatar(avatarModel)
    }

    fun saveSelectedAvatar(newAvatarModel: AvatarModel, onSaveUserAvatarNavigation: (() -> Unit)) {
        val userAvatar = UserDbRepository.getAvatar()
        if (userAvatar == null || newAvatarModel.id != userAvatar) {
            updateUserInfo(newAvatarModel) { onSaveUserAvatarNavigation() }
        } else {
            AppLogger.logDebug(PIC_AVATAR_FLOW, "Avatar has been chosen.")
            onSaveUserAvatarNavigation()
        }
    }

    private fun updateUserInfo(avatarModel: AvatarModel, onSaveUserAvatarNavigation: (() -> Unit)) {
        val updateAvatarRequestBody =
                UserRequestBodyHelper.getUpdateAvatarRequestBody("avatar${avatarModel.id}")

        picAvatarView.showLoading()
        AppLogger.logDebug(
                PIC_AVATAR_FLOW,
                "Creating request body for saving avatar.\t$updateAvatarRequestBody",
                1
        )
        AppLogger.logDebug(PIC_AVATAR_FLOW, "Saving new avatar.", 2)

        TankeeRest.api.updateUserInfo(updateAvatarRequestBody)
                .enqueue(picAvatarView.createCallBack(
                        { response ->
                            AnalyticsManager.pickAvatarEvent(AvatarsRepository.getAnalyticsAvatarName())
                            AppLogger.logDebug(
                                    PIC_AVATAR_FLOW,
                                    "New avatar was saved.",
                                    3,
                                    LogStatus.SUCCESS
                            )

                            response.body()?.let { body ->
                                body.items?.isNotEmpty().let {
                                    body.items?.get(0)?.let { userModel -> UserDbRepository.updateUser(userModel) }
                                }
                            }
                            EventPoster.postEvent(ChangeToolbarAvatar())
                            onSaveUserAvatarNavigation()
                            picAvatarView.hideLoading()
                        },
                        {
                            AppLogger.logDebug(
                                    PIC_AVATAR_FLOW,
                                    "New avatar was no saved.",
                                    3,
                                    LogStatus.FAILED
                            )
                        }
                ))
    }

    companion object {
        const val PIC_AVATAR_FLOW = "PicAvatarFlow"
    }
}

interface PicAvatarView : BaseView {

    fun setAvatars(avatars: List<AvatarModel>)
    fun setSelectedAvatar(avatarModel: AvatarModel)
    fun onBackButtonClick()
}