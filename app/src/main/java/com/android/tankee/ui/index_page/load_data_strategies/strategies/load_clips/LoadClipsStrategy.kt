package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_clips

import com.android.tankee.R
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.clip.ClipInfo
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.ResUtils

class LoadClipsStrategy : IndexPageLoadStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getClips().enqueue(indexPageView.createCallBack(
            { response ->
                response.body()?.let { body ->
                    indexPageBuilder.title = ResUtils.getString(R.string.saved_сlips)
                    indexPageBuilder.tags = listOf()
                    body.items?.let {
                            items -> indexPageBuilder.newItems =
                        applyStartedAt(items).toMutableList()
                    }


                    updateMainInfo()
                }
            }, {
                indexPageBuilder.title = ResUtils.getString(R.string.saved_сlips)
                indexPageBuilder.tags = listOf()
                hideLoading()
            })
        )
    }

    fun applyStartedAt(clipInfos: List<ClipInfo>): List<SectionItemMarker> {
        val videos = ArrayList<SectionItemMarker>()
        for (clip in clipInfos) {
            clip.video?.let {

                it.clipInfo = clip
                it.startedAt = clip.time
                it.duration = null

                videos.add(it)
            }
        }

        return videos
    }
}