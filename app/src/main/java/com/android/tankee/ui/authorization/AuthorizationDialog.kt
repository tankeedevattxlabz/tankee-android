package com.android.tankee.ui.authorization


import android.app.Dialog
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.event_bus.*
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.navigation.FragmentAnimationStore
import com.android.tankee.ui.authorization.navigation.Navigator
import com.android.tankee.ui.authorization.sign_in.SignInFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import com.android.tankee.utils.getColor
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class AuthorizationDialog : DialogFragment() {

    private var toHomeSceenNavigationId: Int? = null

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        EventBus.getDefault().register(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(getColor(R.color.colorDialogBackground)))

        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_authorization, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Navigator.instance.fragmentManager = childFragmentManager
        Navigator.instance.countId = R.id.authorizationContainer
        Navigator.instance.replaceFragment(
            SignInFragment(),
            SignInFragment.TAG,
            FragmentAnimationStore.donToUpAnimation
        )
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)

        Navigator.instance.removeFromBackStack()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: CloseEvent) {
        dismiss()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onHomeScreenNavigationEvent(event: OnHomeScreenNavigationEvent) {
        if (toHomeSceenNavigationId == R.id.homeScreen) {
            val navBuilder = NavOptions.Builder()
                .setPopUpTo(R.id.homeScreen, false)
            Navigation.findNavController(activity!!, R.id.nav_host_fragment)
                .navigate(R.id.homeScreen, null, navBuilder.build())
        } else {
            toHomeSceenNavigationId?.let {
                val navOption = NavOptions.Builder().setPopUpTo(it, true)
                findNavController().navigate(it, null, navOption.build())
            }
        }

        EventPoster.postEvent(CloseCreateAccountPopupEvent())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRegistrationSettingsNavigationEvent(event: OnRegistrationSettingsNavigationEvent) {
        (activity as? MainActivity)?.hideToolbar(true)
        val args = Bundle()
        args.putString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY, PicAvatarNavigation.RegistrationFlow.value)

        Navigation.findNavController(activity!!, R.id.home_nav_host_fragment).navigate(R.id.picAvatarFragment, args)
        EventPoster.postEvent(CloseCreateAccountPopupEvent())
    }

    companion object {
        var TAG: String = AuthorizationDialog::class.java.simpleName

        fun openAuthorizationDialog(fragmentManager: FragmentManager?, toHomeSceneNavigationId: Int) {
            fragmentManager?.let {
                val authorizationDialog = AuthorizationDialog()
                authorizationDialog.toHomeSceenNavigationId = toHomeSceneNavigationId
                authorizationDialog.show(it, TAG)
            }
        }
    }
}