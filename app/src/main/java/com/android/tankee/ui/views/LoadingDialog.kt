package com.android.tankee.ui.views

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window

class LoadingDialog(private val activity: Activity) {
    private var dialog: Dialog? = null

    fun show() {
        dialog = Dialog(activity)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
        dialog?.setContentView(com.android.tankee.R.layout.view_loading_dialog)

        dialog?.show()
    }

    fun hide() {
        dialog?.dismiss()
    }
}