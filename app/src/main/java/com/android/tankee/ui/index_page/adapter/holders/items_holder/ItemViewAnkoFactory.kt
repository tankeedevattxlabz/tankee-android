package com.android.tankee.ui.index_page.adapter.holders.items_holder

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.android.tankee.R
import com.android.tankee.event_bus.ItemCheckedEvent
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.request_options.gameRequestOptions
import com.android.tankee.ui.request_options.gamerRequestOptions
import com.android.tankee.ui.views.see_all.CheckableItem
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildThumbnailUrl
import com.android.tankee.utils.millisToMinutes
import com.android.tankee.utils.setVisibilityInto
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_game.view.*
import kotlinx.android.synthetic.main.item_gamer.view.*
import kotlinx.android.synthetic.main.item_video.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko._FrameLayout
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.view

fun @AnkoViewDslMarker _FrameLayout.createItemView(
    itemMarker: SectionItemMarker,
    itemClickListener: SectionAdapter.ItemClickListener?, isDeleteMode: Boolean = false
) {
    var itemView: View? = null

    when (itemMarker) {
        is Game -> {
            itemView = LayoutInflater.from(context).inflate(
                R.layout.item_game,
                this,
                false
            )
            getGameItemView(
                itemMarker,
                itemView,
                itemClickListener
            )

        }
        is Gamer -> {
            itemView = LayoutInflater.from(context).inflate(
                R.layout.item_gamer,
                this,
                false
            )
            getGamerItemView(
                itemMarker,
                itemView,
                itemClickListener
            )

        }
        is Video -> {
            itemView = LayoutInflater.from(context).inflate(
                R.layout.item_video,
                this,
                false
            )
            getVideoItemView(
                itemMarker,
                itemView,
                itemClickListener,
                isDeleteMode
            )
        }
    }
    itemView?.let {
        (it.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.CENTER_HORIZONTAL
    }
    itemView?.let { addView(it) }
}

private fun getGameItemView(itemMarker: Game, itemView: View, listener: SectionAdapter.ItemClickListener?) {
    val image: String? = if (itemMarker.imageUrl != null && itemMarker.imageUrl!!.isNotEmpty()) {
        itemMarker.imageUrl
    } else {
        itemMarker.avatar?.url
    }
//    Log.v("ImageGame", "${itemMarker.id} ${buildThumbnailUrl(image)}")
    Glide.with(getAppContext())
            .load(buildThumbnailUrl(image))
            .apply(gameRequestOptions)
            .into(itemView.gameTypeImage!!)
    itemMarker.name?.let { itemView.gameTypeName.text = it }
    listener?.let { itemView.setOnClickListener { listener.onClick(itemMarker) } }
}

private fun getGamerItemView(itemMarker: Gamer, itemView: View?, listener: SectionAdapter.ItemClickListener?) {
    val imageUrl = if (itemMarker.imageUrl != null && itemMarker.imageUrl?.url != null) {
        itemMarker.imageUrl?.url
    } else {
        itemMarker.url
    }
    Glide.with(getAppContext())
        .load(buildThumbnailUrl(imageUrl))
        .apply(RequestOptions.circleCropTransform())
        .apply(gamerRequestOptions)
        .into(itemView?.influencerImage!!)

    itemMarker.name?.let { itemView.influencerName?.text = it }
    listener?.let { itemView.setOnClickListener { listener.onClick(itemMarker) } }
}

private fun getVideoItemView(itemMarker: Video, itemView: View, listener: SectionAdapter.ItemClickListener?, isDeleteMode: Boolean) {
    Glide.with(getAppContext()).load(itemMarker.thumbnailUrl).into(itemView.videoThumbnail)
    itemMarker.title?.let { itemView.videoTitle.text = itemMarker.title }
    Glide.with(getAppContext())
        .load(itemMarker.buildGamerImage())
        .apply(RequestOptions.circleCropTransform())
        .into(itemView.gamerImageVideoItem)
    itemMarker.influencerName?.let { itemView.gamerNameVideoItem.text = it }
    itemMarker.duration?.let { itemView.nextVideoDuration.text = millisToMinutes(it.toFloat().toInt()) }
    setVisibilityInto(itemView.originalVideoMarker, itemMarker.isOriginal())
    setVisibilityInto(itemView.originalVideoBorderMarker, itemMarker.isOriginal())

    listener?.let {
        if (isDeleteMode) {
            itemView.delete.visibility = View.VISIBLE
        } else
            itemView.delete.visibility = View.GONE

        itemView.setOnClickListener {

            if (isDeleteMode) {
                if (itemMarker is CheckableItem) {
                    itemMarker.setChecked(!itemMarker.getChecked())
                    itemView.delete.isChecked = itemMarker.getChecked()
                    EventBus.getDefault().post(ItemCheckedEvent())
                }
            } else {
                listener.onClick(itemMarker)
            }
        }
    }

}

fun @AnkoViewDslMarker _LinearLayout.getEmptyView() {
    view {
        layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1f
        )
    }
}
