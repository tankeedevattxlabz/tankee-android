package com.android.tankee.ui.move_videos


import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.TankeeApp
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.analytics.AnalyticsParameter
import com.android.tankee.analytics.EventSender
import com.android.tankee.extentions.disable
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.system.NetworkChangeReceiver
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.move_videos.model.MoreVideoModel
import com.android.tankee.ui.video_player.VIDEO_ARG
import com.android.tankee.ui.video_player.VIdeoPlayerFragment
import com.android.tankee.utils.getDimen
import com.android.tankee.utils.isNetworkAvailable
import com.android.tankee.utils.showView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_more_videos.*
import org.jetbrains.anko.image


class MoreVideosFragment : BaseFragment(), MoreVideosViewModel.MoreVideosView {

    private lateinit var viewModel: MoreVideosViewModel
    private var countDownTimerExecutor: CountDownTimerExecutor? = null
    private val moreVideosAdapter = MoreVideosAdapter()

    private var isFirstStart = true
    private var showLines = true
    private var isNextVideoLoaded = false
    private var isReplayClicked = false
    private var isRepeatButtonAnimationFinish = false
    private var isAddReviewToRepeatVideo = false
    private var isAnotherVideoSelected = false

    private var sectionName: String? = null

    private lateinit var networkChangeReceiver: NetworkChangeReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = MoreVideosViewModel(this)
        arguments?.let {
            if (it.containsKey(VIDEO_ARG)) {
                viewModel.video = it.getParcelable(VIDEO_ARG) as Video?
            }
            if (it.containsKey(AnalyticsParameter.SECTION_NAME.paramName)) {
                sectionName = it.getString(AnalyticsParameter.SECTION_NAME.paramName)
            }
        }

        if (!isFirstStart) {
            registerNetworkChangeBroadcast()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_more_videos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moreVideosBack.setOnClickListener {
            if (viewModel.video?.isVideoCompleted == true) {
                replayButton.disable()
                nextVideoPlayLayout.disable()
                nextVideoThumbnail.disable()
                isAnotherVideoSelected = true
                viewModel.video?.startedAt = 0
                viewModel.addReviewToVideo(viewModel.video) { navigateToVideo(viewModel.video!!) }
            } else {
                findNavController().popBackStack()
            }
        }
        nextVideoPlayLayout.setOnClickListener { nextVideoPlayClick() }
        replayLayout.setOnClickListener { replayButtonClick() }
        nextVideoThumbnail.setOnClickListener {
            if (isNextVideoLoaded && !isReplayClicked) {
                openNextVideo()
            }
        }


        initCountDownTimerExecutor()
        initAdapter()

        viewModel.loadMoreItems()

        registerNetworkChangeBroadcast()
    }

    private fun nextVideoPlayClick() {
        if (isReplayClicked) return

        val onFinishAddReviewToVideo = {
            if (isNextVideoLoaded) {
                if (isNetworkAvailable()) {
                    countDownTimerExecutor?.executeTimer()
                }
            }
        }
        viewModel.addReviewToVideo(viewModel.nextVideo, onFinishAddReviewToVideo)
    }

    private fun replayButtonClick() {
        if (isReplayClicked) return

        isReplayClicked = true
        animateRepeatLines()

        val onFinishAddReviewToVideo = {
            isAddReviewToRepeatVideo = true
            countDownTimerExecutor?.pauseTimer()
            if (isNetworkAvailable()) {
                viewModel.video?.startedAt = 0
                if (isRepeatButtonAnimationFinish) {
                    viewModel.video?.let { navigateToVideo(it) }
                }
                viewModel.video?.let { AnalyticsManager.replayEvent(it) }
                ApptentiveManager.replayVideo()
            } else {
                showConnectionErrorPopup()
            }
        }
        viewModel.addReviewToVideo(viewModel.video, onFinishAddReviewToVideo)
    }

    override fun onResume() {
        super.onResume()
        if (countDownTimerExecutor != null) {
            if (!isFirstStart) {
                countDownTimerExecutor?.comeBackFromBackground()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (countDownTimerExecutor != null) {
            countDownTimerExecutor?.goBackground()
        }
    }

    override fun onStop() {
        super.onStop()
        TankeeApp.connection.removeNetworkListener(networkChangeReceiver)
    }

    private fun registerNetworkChangeBroadcast() {
        networkChangeReceiver = NetworkChangeReceiver()
        networkChangeReceiver.onNetworkAvailable = {
            hideConnectionPopup()
            if (!isFirstStart) {
                countDownTimerExecutor?.startTimer()
            }
        }
        networkChangeReceiver.onNetworkDisable = {
            countDownTimerExecutor?.pauseTimer()
        }

        TankeeApp.connection.addNetworkListener(networkChangeReceiver)
    }

    private fun initCountDownTimerExecutor() {
        countDownTimerExecutor = CountDownTimerExecutor()
        countDownTimerExecutor?.onUpdateTimerText = { updateTimerText(it) }
        countDownTimerExecutor?.onUpdateTimerState = { updateTimerState(it) }
        countDownTimerExecutor?.onOpenNextVideo = { openNextVideo() }
        countDownTimerExecutor?.updateTimerText()
    }

    private fun navigateToVideo(video: Video) {
        if (isAdded && isVisible && userVisibleHint) {
            sendOpenVideoEvent(video)
            VIdeoPlayerFragment.showVideo(video, findNavController())
        }
    }

    override fun setNextVideo(nextVideo: MoreVideoModel) {
        isFirstStart = false

        nextVideo.thumbnailUrl?.let {
            Glide.with(context!!)
                .load(nextVideo.thumbnailUrl)
                .into(nextVideoThumbnail)
        }
        nextVideo.title?.let { nextVideoTitle.text = it }
        nextVideo.duration?.let {
            showView(nextVideoDuration)
            nextVideoDuration.text = it
        }

        countDownTimerExecutor?.startTimer()
    }

    override fun addMoreVideos(moreVideos: List<Video>) {
        val oldListSize = moreVideosAdapter.items.size
        moreVideosAdapter.items.addAll(moreVideos)
        moreVideosAdapter.notifyItemRangeChanged(oldListSize, moreVideosAdapter.items.size)
    }

    private fun initAdapter() {
        moreVideoRecycler.layoutManager = GridLayoutManager(context, 2)
        moreVideoRecycler.adapter = moreVideosAdapter

        moreVideosAdapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<Video> {
            override fun onItemClicked(item: Video) {
                if (isReplayClicked || isAnotherVideoSelected) return
                isAnotherVideoSelected = true
                if (isNetworkAvailable()) {
                    viewModel.addReviewToVideo(item) { navigateToVideo(item) }
                } else {
                    showConnectionErrorPopup()
                }
            }
        }

        moreVideoRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as LinearLayoutManager

                val totalItems = layoutManager.itemCount
                val currentItems = layoutManager.childCount
                val scrollOutItems = layoutManager.findFirstVisibleItemPosition()

                if (!viewModel.lazyLoadingModel.isLoading && totalItems <= (currentItems + scrollOutItems)) {
                    viewModel.loadMoreItems()
                    viewModel.lazyLoadingModel.startLoading()
                }
            }
        })
    }

    private fun updateTimerText(timeLeftSeconds: String) {
        if (nextVideoTimer != null) {
            nextVideoTimer.text = timeLeftSeconds
        }
    }

    private fun updateTimerState(isTimerRunning: Boolean) {
        if (isTimerRunning) {
            nextVideoTimerStateText?.text = getString(R.string.start_next)
            playButton.image = resources.getDrawable(R.drawable.ic_pause, null)
        } else {
            nextVideoTimerStateText?.text = getString(R.string.timer_paused)
            playButton.image = resources.getDrawable(R.drawable.ic_more_videos_play, null)
        }

        if (isNextVideoLoaded) {
            animateLines()
        } else {
            isNextVideoLoaded = true
        }
    }

    private fun animateLines() {
        showLines = if (!showLines) {
            leftPlayLines.animate().translationX(0f)
            rightPlayLines.animate().translationX(0f)
            true
        } else {
            leftPlayLines.animate().translationX(LINES_ANIM_TRANSLATION_X)
            rightPlayLines.animate().translationX(-LINES_ANIM_TRANSLATION_X)
            false
        }
    }

    private fun animateRepeatLines() {
        val leftRepeatLineAnimation1 = ObjectAnimator
            .ofFloat(leftReplayLines, "translationX", 30f)
            .setDuration(REPLAY_ANIMATION_DURATION_LINES)
        val leftRepeatLineAnimation2 = ObjectAnimator
            .ofFloat(leftReplayLines, "translationX", 0f)
            .setDuration(REPLAY_ANIMATION_DURATION_LINES)

        val rightRepeatLineAnimation1 = ObjectAnimator
            .ofFloat(rightReplayLines, "translationX", -30f)
            .setDuration(REPLAY_ANIMATION_DURATION_LINES)
        val rightRepeatLineAnimation2 = ObjectAnimator
            .ofFloat(rightReplayLines, "translationX", 0f)
            .setDuration(REPLAY_ANIMATION_DURATION_LINES)

        val rotationReplayButton = ObjectAnimator
            .ofFloat(replayButton, "rotation", 0f, -360f)
            .setDuration(REPLAY_ANIMATION_DURATION)

        val replayAnimationSet = AnimatorSet().apply {
            play(rotationReplayButton)
            play(leftRepeatLineAnimation1).before(leftRepeatLineAnimation2)
            play(rightRepeatLineAnimation1).before(rightRepeatLineAnimation2)
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                    // No action
                }

                override fun onAnimationEnd(animation: Animator?) {
                    isRepeatButtonAnimationFinish = true
                    if (isAddReviewToRepeatVideo) {
                        viewModel.video?.let {
                            navigateToVideo(it)
                        }
                    }
                }

                override fun onAnimationCancel(animation: Animator?) {
                    // No action
                }

                override fun onAnimationStart(animation: Animator?) {
                    // No action
                }
            })
        }

        replayAnimationSet.start()
    }

    private fun openNextVideo() {
        viewModel.nextVideo?.let { navigateToVideo(it) }
    }

    private fun sendOpenVideoEvent(video: Video) {
        EventSender.logOpenVideoEvent(
            video,
            sectionName,
            AnalyticsParameter.MORE_VIDEOS_SCREEN.paramName
        )
    }

    companion object {
        var TAG: String = MoreVideosFragment::class.java.simpleName

        var LINES_ANIM_TRANSLATION_X = getDimen(R.dimen.next_video_lines_animation)
        const val REPLAY_ANIMATION_DURATION = 600L
        const val REPLAY_ANIMATION_DURATION_LINES = 300L
    }
}
