package com.android.tankee.ui.custom_tabs

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.android.tankee.R
import com.android.tankee.getAppContext

object CustomTabsFactory {

    fun openUrl(context: Context, url: String) {
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        builder.setToolbarColor(ContextCompat.getColor(getAppContext(), R.color.colorPrimary))
        builder.enableUrlBarHiding()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }

}