package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_gamers

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy

class LoadGamersStrategy : IndexPageLoadStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getGamers().enqueue(indexPageView.createCallBack(
            { response ->
                response.body()?.let { body ->
                    body.sectionTitle?.let { indexPageBuilder.title = body.sectionTitle!! }
                    body.items?.let { indexPageBuilder.newItems = it.toMutableList() }

                    updateMainInfo()
                }
            }, { hideLoading() })
        )
    }
}