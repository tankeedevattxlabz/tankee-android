package com.android.tankee.ui.parallax


import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.adapters.GamersAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import kotlinx.android.synthetic.main.pic_avatar_parallax_view.view.*


class ParallaxViewTest(context: Context, attributeSet: AttributeSet) : FrameLayout(context, attributeSet) {

    private var parallaxController: ParallaxController? = null

    init {
        inflate(context, R.layout.pic_avatar_parallax_view, this)
        parallaxController = ParallaxController(avatarsRecycler, parallaxBackground)
    }

    public override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        var ss: SavedState? = null
        superState?.let {
            ss = SavedState(superState)
        }
        ss?.parallaxTransition = parallaxController?.parallaxTransition
        return ss
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        setCustomState(ss.parallaxTransition)
    }


    fun initParallaxItems(items: List<Gamer>) {
        avatarsRecycler.layoutManager = createHorizontalLayoutManager()
        avatarsRecycler.adapter = getAdapter(items)
    }

    private fun getAdapter(items: List<Gamer>): RecyclerView.Adapter<*>? {
        val adapter = GamersAdapter()
        adapter.items = items
        return adapter
    }

    private fun setCustomState(parallaxTransition: ParallaxTransition?) {
        parallaxController?.setParallaxViewTranslation(parallaxTransition)
        parallaxController?.updateViewTransition()
    }

    internal class SavedState(superState: Parcelable) : BaseSavedState(superState), Parcelable {

        var parallaxTransition: ParallaxTransition? = null

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelable(parallaxTransition, flags)
        }
    }
}

