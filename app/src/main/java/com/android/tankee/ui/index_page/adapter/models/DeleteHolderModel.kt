package com.android.tankee.ui.index_page.adapter.models

import com.android.tankee.ui.views.see_all.SectionItemMarker

data class DeleteHolderModel(val items: List<SectionItemMarker>) : IndexPageHolderModel