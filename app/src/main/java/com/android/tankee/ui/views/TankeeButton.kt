package com.android.tankee.ui.views


import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.android.tankee.R
import com.android.tankee.extentions.disable
import com.android.tankee.extentions.enable
import com.android.tankee.extentions.show
import com.github.florent37.viewanimator.ViewAnimator
import kotlinx.android.synthetic.main.view_tankee_button.view.*


class TankeeButton(context: Context, private val attrs: AttributeSet) : FrameLayout(context, attrs) {

    var onClickAction: (() -> Unit)? = null
    var onPreAnimationAction: (() -> Unit)? = null

    init {
        inflate(context, R.layout.view_tankee_button, this)
        getAttrs()

        tankeeButtonRoot.setOnClickListener {
            tankeeButtonRoot.disable()
            tankeeButtonRoot.postDelayed({ tankeeButtonRoot.enable() }, 500)

            ViewAnimator
                    .animate(it)
                    .scale(1f, 0.9f, 1f)
                    .duration(500)
                    .onStop {
                        onClickAction?.invoke()
                    }
                    .start()
        }
    }

    @SuppressLint("CustomViewStyleable", "Recycle")
    private fun getAttrs() {
        val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.TankeeButtonOptions,
                0,
                0
        )
        val tbTextId = typedArray.getResourceId(R.styleable.TankeeButtonOptions_tb_text, 0)
        val hintIcon = typedArray.getResourceId(R.styleable.TankeeButtonOptions_tb_hintIcon, 0)
        val style = typedArray.getString(R.styleable.TankeeButtonOptions_tb_style)

        if (tbTextId != 0) {
            buttonText.text = resources.getString(tbTextId).toString()
        }

        if (hintIcon != 0) {
            tbHintIcon.show()
            tbHintIcon.setImageDrawable(resources.getDrawable(hintIcon, null))
        }

        setStyle(style)
    }

    private fun setStyle(style: String?) {
        when (style) {
            Style.Dark.name -> {
                setDefaultTheme()
                return
            }
            Style.Light.name -> {
                setLightTheme()
                return
            }
            else -> setDefaultTheme()
        }
    }

    private fun setDefaultTheme() {
        tankeeButtonRoot.setBackgroundResource(R.drawable.bg_authorization_button)
        buttonText.setTextColor(resources.getColor(R.color.lightGreen))
    }

    private fun setLightTheme() {
        tankeeButtonRoot.setBackgroundResource(R.drawable.bg_light)
        buttonText.setTextColor(resources.getColor(R.color.colorBayOfMany))
    }

    private enum class Style {
        Light,
        Dark
    }
}