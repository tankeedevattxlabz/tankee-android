package com.android.tankee.ui.video_player


import android.util.Log
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView


class VideoPlayerPresenter(private val videoPlayerView: VideoPlayerView) {


    fun addReviewToVideo(videoId: Int, time: Int) {
        TankeeRest.api.addReviewsToVideo(videoId, time)
            .enqueue(videoPlayerView.createCallBack(
                {
                    Log.d("add_review_to_video",time.toString())
                },
                {
                }
            ))
    }

    fun getVideoInfo(videoId: Int) {
        TankeeRest.api.getVideoInfo(videoId)
            .enqueue(
                videoPlayerView.createCallBack(
                    { response ->
                        response.body()?.items?.let { items ->
                            if (items.isNotEmpty()) {
                                items[0].let {
                                    videoPlayerView.setupVideo(it)
                                }
                            }
                        }
                    },
                    {
                        videoPlayerView.closePlayerScreen()
//                        videoPlayerView.setupVideo()
                    }
                ))
    }
}


interface VideoPlayerView : BaseView {

    fun onFinishAddingReviewToVideo()

    fun setupVideo(video: Video)

    fun closePlayerScreen()

}