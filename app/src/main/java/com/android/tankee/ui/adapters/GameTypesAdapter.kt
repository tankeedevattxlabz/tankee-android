package com.android.tankee.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.ui.home.videos.GameType
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_game_type.view.*

class GameTypesAdapter : SectionAdapter() {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSectionViewHolder {
        var layoutId = R.layout.item_game_type

        if (viewType == 0) {
            layoutId = R.layout.item_see_all
        }

        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

        return when (viewType) {
            1 -> GameTypesViewHolder(itemView)
            else -> SeeAllViewHolder(itemView)
        }
    }

    inner class GameTypesViewHolder(itemView: View) : SectionViewHolder(itemView) {
        override fun onBind(item: SectionItemMarker) {
            if (item is GameType) {

                Glide.with(getAppContext())
                    .load(item.icon)
                    .into(itemView.gameTypeIcon)
                itemView.gameTypeName.text = item.name
                listener?.let { itemView.setOnClickListener { iv -> it.onClick(item) } }
            }
        }
    }
}