package com.android.tankee.ui.authorization.join.parent_email

import android.util.Log
import com.android.tankee.EMPTY_STRING
import com.android.tankee.database.AppDatabase
import com.android.tankee.database.kws.helper.KwsDbHelper
import com.android.tankee.database.kws.models.KwsUserIdModel
import com.android.tankee.database.kws.utils.KwsDbUtils
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.rest.utils.RequestBodyConverter
import com.android.tankee.ui.authorization.join.UserAccountInfoModel
import com.android.tankee.ui.authorization.join.account_info.AccountInfoViewModel
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.isEmailValid
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.RequestBody
import java.util.*

class ParentEmailViewModel(private val parentEmailView: ParentEmailView) {

    fun checkParentEmail(parentEmail: String) {
        if (!checkIsValidField(parentEmail)) {
            Log.i(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Invalid parent email field.")
            return
        } else {
            Log.i(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Valid parent email field.")
        }

        saveParentEmail(parentEmail)
        createKwsUser()
    }

    private fun createKwsUser() {
        AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Create KWS user.", 3, LogStatus.EXECUTING)

        UserAccountInfoModel.instance.kwsCreateUserRequestBody?.let { kwsCreateUserRequestBody ->

            parentEmailView.showLoading()
            val requestBody = RequestBodyConverter.convertToRequestBody(kwsCreateUserRequestBody)
            KwsRest.api.createUser(requestBody)
                    .enqueue(parentEmailView.createCallBack(
                            { response ->
                                AppLogger.logDebug(
                                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                                        "Kws user was created.",
                                        3,
                                        LogStatus.SUCCESS
                                )

                                parentEmailView.hideLoading()
                                response.body()?.let { kwsCreateUserResponse ->
                                    AppLogger.logDebug(
                                            AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                                            "${kwsCreateUserResponse.id}",
                                            3
                                    )
                                    KwsDbHelper.insertKwsUserId(KwsUserIdModel(kwsCreateUserResponse.id.toString()))
                                    parentEmailView.navigateToGenerateDisplayName()
                                }
                            },
                            {
                                parentEmailView.hideLoading()
                                parentEmailView.setIncorrectEmailFormatError()

                                AppLogger.logDebug(
                                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                                        "KWS user was not created.",
                                        3,
                                        LogStatus.FAILED
                                )
                            }
                    ))
        }
    }

    private fun saveParentEmail(parentEmail: String) {
        UserAccountInfoModel.instance.kwsCreateUserRequestBody?.parentEmail = parentEmail
    }

    private fun checkIsValidField(parentEmail: String): Boolean {

        if (parentEmail.isEmpty()) {
            parentEmailView.setParentEmailEmptyError()
            return false
        }
        if (!isEmailValid(parentEmail)) {
            parentEmailView.setIncorrectEmailFormatError()
            return false
        }

        return true
    }

    fun setTestData() {
        parentEmailView.setTestData("sergioKulyk${Random().nextInt(100000000)}@gmail.com")
    }

    interface ParentEmailView : BaseView {
        fun navigateToGenerateDisplayName()

        fun setParentEmailEmptyError()

        fun setIncorrectEmailFormatError()

        fun setTestData(parentEmailValue: String)
    }
}