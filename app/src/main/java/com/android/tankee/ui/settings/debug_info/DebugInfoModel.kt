package com.android.tankee.ui.settings.debug_info

/**
 * Created by Sergey Kulyk on 2019-09-11.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
data class DebugInfoModel(
    val name: String,
    val value: String?
) {
    override fun toString(): String {
        return "$name = $value"
    }
}