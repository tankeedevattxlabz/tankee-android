package com.android.tankee.ui.home.toolbar.toolbar_item


import android.content.Context
import android.util.AttributeSet
import com.android.tankee.R
import kotlinx.android.synthetic.main.toolbar_util_item.view.*


open class ToolbarUtilItem(context: Context, attrs: AttributeSet?) : BaseToolbarItem(context, attrs) {

    override fun onItemClick() {
    }

    override fun getLayoutId(): Int {
        return R.layout.toolbar_util_item
    }

    override fun setBodyImage() {
        if (imageResourceId > 0) {
            itemImage.setImageResource(imageResourceId)
            itemImage.setBlackWhite(true)
        }
    }

    override fun setBodyImagePadding(padding: Int) {
    }

    override fun setText(textResourceId: Int) {
    }

    override fun initConstraintSets() {
    }

    override fun setHighlighted(selected: Boolean) {
        if (checkIsSelectedItem(selected)) {
            return
        }

        setItemState(selected)
    }

    private fun setItemState(selected: Boolean) {
        itemImage.setBlackWhite(!selected)
        this.itemSelected = selected
    }

    private fun checkIsSelectedItem(selected: Boolean) =
            this.itemSelected == selected
}