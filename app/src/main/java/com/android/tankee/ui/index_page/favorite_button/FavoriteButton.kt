package com.android.tankee.ui.index_page.favorite_button

import android.content.Context
import android.graphics.PorterDuff
import android.transition.TransitionManager
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.android.tankee.R
import com.android.tankee.ui.index_page.GameContent
import com.android.tankee.ui.index_page.GamerContent
import com.android.tankee.ui.index_page.IndexPageContent
import com.android.tankee.utils.ResUtils
import kotlinx.android.synthetic.main.view_index_page_favorite_button.view.*

class FavoriteButton(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs),
        FavoriteButtonView {
    override fun showLoading(showLoading: Boolean) {
    }

    override fun setFavoriteButtonState(isFavorite: Boolean) {
        setFavorite(isFavorite)
    }

    override fun showPopUp(p: DialogFragment, tag: String) {
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    private var isFavorite: Boolean = false
    private var onButtonClick: ((isFavorite: Boolean) -> Unit)? = null
    private var indexPageFavoriteButtonPresenter: IndexPageFavoriteButtonPresenter
    var indexPageContent: IndexPageContent? = null
    var contentId: Int? = null

    init {
        inflate(context, R.layout.view_index_page_favorite_button, this)
        indexPageFavoriteButtonPresenter = IndexPageFavoriteButtonPresenter(this)

        getInfo()

        onButtonClick = {
            when (indexPageContent) {
                is GameContent -> {
                    if (it) {
                        contentId?.let { gameId -> indexPageFavoriteButtonPresenter.setFavoriteGame(gameId) }
                    } else {
                        contentId?.let { gameId -> indexPageFavoriteButtonPresenter.deleteFavoriteGame(gameId) }
                    }
                }
                is GamerContent -> {
                    if (it) {
                        contentId?.let { gamerId -> indexPageFavoriteButtonPresenter.setFavoriteGamer(gamerId) }
                    } else {
                        contentId?.let { gamerId -> indexPageFavoriteButtonPresenter.deleteFavoriteGamer(gamerId) }
                    }
                }
            }
        }

        favoriteButtonRoot.setOnClickListener {
            if (isFavorite) {
                setFavorite()
            } else {
                setUnfavorite()
            }

            // Run action with opposite isFavorite action to change isFavorite state.
            onButtonClick?.invoke(isFavorite)
        }
    }


    private fun setFavorite() {
        isFavorite = false

        favoriteIconState.setImageResource(R.drawable.ic_heart)
        favoriteIconState.setColorFilter(ContextCompat.getColor(context, R.color.colorBlue), PorterDuff.Mode.MULTIPLY)
        favoriteTextState.text = ResUtils.getString(R.string.favorite)
        favoriteTextState.setTextColor(ResUtils.getColor(R.color.colorBlue))
        favoriteButtonRoot.background = ResUtils.getDrawable(R.drawable.bg_tankee_material_button_green)
    }

    private fun setUnfavorite() {
        isFavorite = true

        favoriteIconState.setImageResource(R.drawable.ic_unfavorite)
        favoriteIconState.colorFilter = null
        favoriteTextState.text = ResUtils.getString(R.string.unfavorite)
        favoriteTextState.setTextColor(ResUtils.getColor(R.color.colorGray))
        favoriteButtonRoot.background = ResUtils.getDrawable(R.drawable.bg_tankee_material_button_blue)
    }

    fun setFavorite(isFavorite: Boolean) {
        if (isFavorite) {
            setUnfavorite()
        } else {
            setFavorite()
        }
    }

    fun getInfo() {
        when (indexPageContent) {
            is GameContent -> {
                contentId?.let { indexPageFavoriteButtonPresenter.getGameInfo(it) }
            }
            is GamerContent -> {
                contentId?.let { indexPageFavoriteButtonPresenter.getGamerInfo(it) }
            }
        }
    }


}

