package com.android.tankee.ui.authorization.delete_account


import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.event_bus.CloseDeleteDialogEvent
import com.android.tankee.event_bus.OnHomeScreenNavigationEventAfterDeleting
import com.android.tankee.utils.getColor
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class DeleteAccountDialog : DialogFragment() {

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        EventBus.getDefault().register(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(getColor(R.color.colorDialogBackground)))

        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_delete_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        childFragmentManager.beginTransaction().add(R.id.deleteAccountContainer, DeleteAccountFragment()).commit()
//        Navigator.instance.fragmentManager = childFragmentManager
//        Navigator.instance.countId = R.id.deleteAccountContainer
//        Navigator.instance.replaceFragment(
//                DeleteAccountFragment(),
//                DeleteAccountFragment.TAG,
//                FragmentAnimationStore.donToUpAnimation
//        )
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onHomeScreenNavigationEventAfterDeleting(event: OnHomeScreenNavigationEventAfterDeleting) {
        val navOption = NavOptions.Builder().setPopUpTo(R.id.homeListFragment, true)
        findNavController().navigate(R.id.homeListFragment, null, navOption.build())
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: CloseDeleteDialogEvent) {
        dismiss()
    }

    companion object {
        var TAG: String = DeleteAccountDialog::class.java.simpleName

        fun openDeleteAccountDialog(fragmentManager: FragmentManager?) {
            fragmentManager?.let {
                val deleteAccountDialog = DeleteAccountDialog()
                deleteAccountDialog.show(fragmentManager, TAG)
            }
        }
    }
}