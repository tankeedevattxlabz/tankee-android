package com.android.tankee.ui.my_stuff.my_video_content.saved_clips


import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.clip.ClipInfo
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.ui.my_stuff.my_video_content.base.BaseMyStuffSectionView


/**
 * Presenter for executing saved clips.
 * Now here is only loading saved clips.
 */
class SavedClipsPresenter(private val savedClipsView: SavedClipsView) {

    fun loadSavedClipsVideo() {
        AppLogger.logDebug(TAG, "Get Saved clips.", logStatus = LogStatus.EXECUTING)

        TankeeRest.api.getClips().enqueue(
                savedClipsView.createCallBack(
                        { response ->
                            response.body()?.items?.let {
                                if (it.isNotEmpty()) {
                                    AppLogger.logDebug(TAG, "Saved clips were loaded.", logStatus = LogStatus.FAILED)

                                    val clipVideoItems = mapClipInfoToClipVideos(it)
                                    savedClipsView.setVideos(clipVideoItems)
                                } else {
                                    AppLogger.logDebug(
                                            TAG,
                                            "User don't have Saved clips.\tShowing placeholder",
                                            logStatus = LogStatus.FAILED
                                    )
                                    savedClipsView.showNoVideosPlaceHolder()
                                }
                            } ?: run {
                                AppLogger.logDebug(TAG, "Saved clips list is null.", logStatus = LogStatus.FAILED)
                                savedClipsView.showNoVideosPlaceHolder()
                            }
                        },
                        {
                            AppLogger.logDebug(TAG, "Get Saved clips.", logStatus = LogStatus.FAILED)
                        }
                )
        )
    }

    private fun mapClipInfoToClipVideos(clipInfoList: List<ClipInfo>): List<ClipVideo> {
        val videos = ArrayList<ClipVideo>()
        for (clip in clipInfoList) {
               clip.video?.let {
                it.clipInfo = clip
                it.startedAt = clip.time
                it.duration = null
                videos.add(it)
            }
        }

        return videos
    }

    companion object {
        const val TAG = "SavedClipsFlow"
    }
}


interface SavedClipsView : BaseMyStuffSectionView