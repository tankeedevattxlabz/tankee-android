package com.android.tankee.ui.base

interface LoadingDialogActions {

    fun showLoading()

    fun hideLoading()

    fun showLoading(showLoading: Boolean)

}