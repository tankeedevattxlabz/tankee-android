package com.android.tankee.ui.notifications


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.event_bus.NewNotificationEvent
import com.android.tankee.notifications.model.BaseNotificationModel
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.notifications.model.NotificationType
import com.android.tankee.ui.adapters.createVerticalLayoutManager
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.base.dialog.BaseDialog
import com.android.tankee.ui.home.toolbar.TankeeToolbar
import com.android.tankee.ui.home.videos.IndexPageBundleFactory
import com.android.tankee.ui.notifications.notification_text.NotificationTextFragment
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import kotlinx.android.synthetic.main.fragment_notifications.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * This class reproduce Bell screen for push notification items.
 * This class contains runtime inserting new push notification when it comes (use EventBus).
 */
class NotificationsFragment : BaseFragment(), NotificationsView {

    private lateinit var notificationsPresenter: NotificationsPresenter
    private var adapter: NotificationsAdapter? = null

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notificationsPresenter = NotificationsPresenter(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showToolbar()
        initScreenViews()

        notificationsPresenter.getNotifications()
    }

    private fun initScreenViews() {
        notificationsBackButton.setOnClickListener { findNavController().popBackStack() }
        initRecyclerView()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun setNotifications(notifications: MutableList<BaseNotificationModel>) {
        adapter?.addItems(notifications)
    }

    private fun initRecyclerView() {
        notificationsRecyclerView?.layoutManager = createVerticalLayoutManager()
        notificationsRecyclerView?.addOnScrollListener(getRecyclerViewOnScrollListener())

        initAdapter()
        notificationsRecyclerView?.adapter = adapter
    }

    private fun getRecyclerViewOnScrollListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 50) {
                    if (TankeeToolbar.isVisible) {
                        hideToolbar()
                        TankeeToolbar.isVisible = false
                    }
                } else if (dy < 0) {
                    if (!TankeeToolbar.isVisible) {
                        showToolbar()
                        TankeeToolbar.isVisible = true
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        adapter = NotificationsAdapter()
        adapter!!.onItemClick = { navigateToItem(it) }
    }

    private fun navigateToItem(model: NotificationModel) {
        when (model.getNotificationType()) {
            NotificationType.GAME,
            NotificationType.GAMER -> navigateToIndexPage(model)
            NotificationType.VIDEO -> navigateToVideoPlayer(model)
            NotificationType.TEXT -> {
                openTextNotificationPopup(model)
            }
        }
    }

    private fun navigateToIndexPage(model: NotificationModel) {
        if (model.isValid()) {
            val args = IndexPageBundleFactory().createBundleNotification(model)
            findNavController().navigate(R.id.indexPageFragment, args)
        }
    }

    private fun navigateToVideoPlayer(model: NotificationModel) {
        if (model.isValid()) {
            VideoPlayerController.openVideoPlayer(model.itemId!!, findNavController())
        }
    }

    private fun openTextNotificationPopup(model: NotificationModel) {
        val notificationTextFragment = NotificationTextFragment
                .newInstance(model.title ?: EMPTY_STRING, model.text ?: EMPTY_STRING)
        BaseDialog.openDialog(fragmentManager, notificationTextFragment)
    }

    /**
     * This method use insert new notification runtime into recycler view.
     * Find send event into {@TankeeMessagingService} class.
     */
    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun insertNewNotification(event: NewNotificationEvent) {
        SessionDataHolder.clearNotificationModel()
        val model = event.notificationModel
        notificationsPresenter.addNewNotification(model)
        adapter?.addItem(model)
    }

}
