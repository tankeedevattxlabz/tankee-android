package com.android.tankee.ui.views.see_all

open class SeeAllItem(var colorId: Int, val iconId: Int, var seeAllType: SeeAllType = SeeAllType.SECTION) : SectionItemMarker

enum class SeeAllType {
    GAMES,
    GAMERS,
    SECTION,
    FAVORITE_VIDEOS,
    SAVED_CLIPS
}