package com.android.tankee.ui.my_stuff.pic.selected_avatar


import com.android.tankee.database.AppDatabase
import com.android.tankee.logs.AppLogger
import com.android.tankee.repo.avatars.AvatarsRepository
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.my_stuff.pic.pic_avatar.AvatarModel
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarPresenter


class SelectedAvatarPresenter(private val selectedAvatarView: SelectedAvatarView) {

    fun initSelectedAvatar() {
        val userModel = AppDatabase.getAppDatabase().userDao().getUser()
        AppLogger.logDebug(PicAvatarPresenter.PIC_AVATAR_FLOW, "Init selected avatar.")
        AppLogger.logDebug(PicAvatarPresenter.PIC_AVATAR_FLOW, userModel.toString())

        val avatarModel = if (userModel != null) {
            AvatarsRepository.buildAvatarModel(AvatarsRepository.getAvatarId(), true)
        } else {
            AvatarsRepository.buildAvatarModel(AvatarsRepository.DEFAULT_AVATAR_ID, true)
        }

        AppLogger.logDebug(PicAvatarPresenter.PIC_AVATAR_FLOW, avatarModel.toString())
        AppLogger.logDebug(PicAvatarPresenter.PIC_AVATAR_FLOW, "Selected avatar id is:\t${avatarModel.id}")

        selectedAvatarView.setSelectedAvatar(avatarModel)
    }
}

interface SelectedAvatarView : BaseView {

    fun setSelectedAvatar(avatarModel: AvatarModel)

}