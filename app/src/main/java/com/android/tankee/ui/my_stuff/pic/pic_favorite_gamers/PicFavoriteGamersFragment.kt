package com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.android.tankee.R
import com.android.tankee.event_bus.OnFavoriteGamersSelectedAction
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.ui.my_stuff.pic.base.PickBaseFragment
import com.android.tankee.ui.my_stuff.pic.base.PickBaseView
import com.android.tankee.ui.my_stuff.pic.pic_avatar.AvatarModel
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers.choose_favorite_gamers.ChooseFavoriteGamersFragment
import com.android.tankee.ui.my_stuff.pic.selected_avatar.SelectedAvatarPresenter
import com.android.tankee.ui.my_stuff.pic.selected_avatar.SelectedAvatarView
import kotlinx.android.synthetic.main.fragment_pic_favotite_games.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class PicFavoriteGamersFragment : PickBaseFragment(), PickBaseView, SelectedAvatarView {

    private lateinit var selectedAvatarPresenter: SelectedAvatarPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        selectedAvatarPresenter = SelectedAvatarPresenter(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pic_favorite_gamers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pickFavoriteGamesBack.setOnClickListener { navigateToMySettingsScreen() }
        selectedAvatarPresenter.initSelectedAvatar()

        initChooseFavoriteGamersFragment()
        initActionButton()
    }

    override fun onResume() {
        super.onResume()
        if (isRegistrationFlow()) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (isRegistrationFlow()) {
            EventBus.getDefault().unregister(this)
        }
    }

    private fun initChooseFavoriteGamersFragment() {
        val chooseFavoriteGamersFragment = ChooseFavoriteGamersFragment()
        chooseFavoriteGamersFragment.arguments = arguments
        fragmentManager
                ?.beginTransaction()
                ?.add(
                        R.id.chooseFavGamesContainer,
                        chooseFavoriteGamersFragment,
                        ChooseFavoriteGamersFragment.TAG
                )
                ?.commit()
    }

    private fun initActionButton() {
        arguments?.let {
            when (it.getString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY)) {
                PicAvatarNavigation.MySettingsFlow.value -> {
                    doneButton.onClickAction = { onDoneClick() }

                    doneButton.show()
                    nextButton.gone()
                    skipButton.gone()
                }
                PicAvatarNavigation.RegistrationFlow.value -> {
                    nextButton.onClickAction = { onNextClick() }
                    skipButton.onClickAction = { onSkipClick() }

                    doneButton.gone()
                    nextButton.gone()
                    skipButton.show()
                }
            }
        }
    }

    private fun onDoneClick() {
        (fragmentManager?.findFragmentByTag(
                ChooseFavoriteGamersFragment.TAG
        ) as ChooseFavoriteGamersFragment).saveFavoriteUnfavorableGames?.invoke()
    }

    private fun onNextClick() {
        (fragmentManager?.findFragmentByTag(
                ChooseFavoriteGamersFragment.TAG
        ) as ChooseFavoriteGamersFragment).saveFavoriteUnfavorableGames?.invoke()
    }

    override fun setSelectedAvatar(avatarModel: AvatarModel) {
        selectedAvatar.setImageResource(avatarModel.image)
    }

    private fun onSkipClick() {
        val navOption = NavOptions.Builder().setPopUpTo(R.id.homeListFragment, true)
        findNavController().navigate(R.id.homeListFragment, null, navOption.build())
    }

    private fun showSkipHideNextButton() {
        skipButton.show()
        nextButton.gone()
    }

    private fun hideSkipShowNextButton() {
        skipButton.gone()
        nextButton.show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: OnFavoriteGamersSelectedAction) {
        if (event.hasSelectedGamers) {
            hideSkipShowNextButton()
        } else {
            showSkipHideNextButton()
        }
    }
}
