package com.android.tankee.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.request_options.gamerRequestOptions
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildThumbnailUrl
import com.android.tankee.utils.hideView
import com.android.tankee.utils.showView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_search_header.view.*
import kotlinx.android.synthetic.main.item_search_video.view.*

class SearchVideosAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val items: MutableList<SectionItemMarker> = mutableListOf()

    var onItemClickListener: ((Video) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutId = getLayout(viewType)
        val itemView = inflateView(parent, layoutId)
        return createViewHolder(itemView, viewType)
    }

    private fun inflateView(parent: ViewGroup, layoutId: Int) =
            LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

    private fun createViewHolder(itemView: View, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            SearchVideoHeaderViewHolder(itemView)
        } else {
            SearchVideoViewHolder(itemView)
        }
    }

    private fun getLayout(viewType: Int): Int {
        return if (viewType == 0) {
            R.layout.item_search_header
        } else {
            R.layout.item_search_video
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        if (item is SearchHeaderModel) {
            (holder as SearchVideoHeaderViewHolder).onBind(item)
        } else if (item is Video) {
            (holder as SearchVideoViewHolder).onBind(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if (item is SearchHeaderModel) {
            0
        } else {
            1
        }
    }

    fun addMoreItems(videos: List<SectionItemMarker>, clearItems: Boolean = false) {
        if (clearItems) {
            clearItems()
        }

        items.addAll(videos)
        notifyDataSetChanged()
    }

    private fun clearItems() {
        if (itemCount > 1) {
            val startIndex = 1
            val endIndex = itemCount
            for (i in 1 until itemCount) {
                items.removeAt(startIndex)
            }
            notifyItemRangeRemoved(startIndex, endIndex)
        }
    }

    inner class SearchVideoHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(item: SearchHeaderModel) {

            if (item.totalVideosCount > 0 || item.title.isNotEmpty()) {
                showView(itemView.showingVideosText)
                showView(itemView.searchVideoText)
            } else {
                hideView(itemView.showingVideosText)
                hideView(itemView.searchVideoText)
            }

            itemView.showingVideosText.text = String.format(
                    getAppContext().getString(R.string.search_showing_videos),
                    item.totalVideosCount,
                    item.title
            )
        }
    }

    inner class SearchVideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(item: Video) {
            Glide.with(getAppContext())
                    .load(buildThumbnailUrl(item.thumbnailUrl))
                    .into(itemView.searchVideoThumbnail)
            item.title?.let { itemView.searchVideoTitle.text = it }

            Glide.with(getAppContext())
                    .load(buildThumbnailUrl(item.influencerImage))
                    .apply(gamerRequestOptions)
                    .into(itemView.searchGamerImage)
            item.influencerName?.let { itemView.searchGamerName.text = it }

            itemView.setOnClickListener { onItemClickListener?.invoke(item) }
        }
    }
}

