package com.android.tankee.ui.popups

import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.android.tankee.R
import com.android.tankee.utils.dp
import kotlinx.android.synthetic.main.popup_base.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

class BasePopup : DialogFragment() {
    private var title:String?=null
    private var msg:String?=null
    private var imageRes:Int?=null
    private var buttons:Map<String,(BasePopup)->Unit>?=null
    private var customview: View?=null

    companion object {
        private const val TITLE_ARG = "TITLE_ARG"
        private const val MSG_ARG = "TITLE_ARG"
        private const val IMAGE_ARG = "TITLE_ARG"



        private fun newInstance(title:String? = null,message:String?=null, @IdRes imageRes:Int?=null,buttons:Map<String,()->Unit>?=null){

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.popup_base, container, true)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        if (title!=null){
            view.popup_title.visibility = View.VISIBLE
            view.popup_title.text = title
        } else {
            view.popup_title.visibility = View.GONE
        }
        if (msg!=null){
            view.popup_message.visibility = View.VISIBLE
            view.popup_message.text = msg
        } else {
            view.popup_message.visibility = View.GONE
        }
        if (imageRes!=null){
            view.popup_image.visibility = View.VISIBLE
            view.popup_image.imageResource = imageRes!!
        } else {
            view.popup_image.visibility = View.GONE
        }
        buttons?.let {
            it.forEach { button->
                view.popup_ll.apply {
                    val t = textView {
                        setTypeface(typeface, Typeface.BOLD)
                        text = button.key
                        background = resources.getDrawable(R.drawable.popup_bt_background)
                        padding = 8.dp()
                        gravity = Gravity.CENTER
                        textColor = resources.getColor(R.color.colorBlue)
                        setTextSize(TypedValue.COMPLEX_UNIT_SP,17f)
                        this.setOnClickListener {
                            button.value.invoke(this@BasePopup)
                        }
                    }.updateLayoutParams<LinearLayout.LayoutParams> {
                        setMargins(16.dp(),8.dp(),16.dp(),8.dp())
                    }

                }
            }
        }
        customview?.let {

        }
        return view
    }

    class Builder{
        private val basePopup = com.android.tankee.ui.popups.BasePopup()

        fun title(title: String):Builder{
            basePopup.title = title
            return this
        }

        fun message(msg: String):Builder{
            basePopup.msg =msg
            return this
        }

        fun image(@DrawableRes imageRes:Int):Builder{
            basePopup.imageRes = imageRes
            return this
        }

        fun buttons(buttons:Map<String,(BasePopup)->Unit>):Builder{
            basePopup.buttons = buttons
            return this
        }

        fun customView(view: View){
            basePopup.customview = view
        }

        fun show(fragmentManager:FragmentManager){
            basePopup.show(fragmentManager,"BASE_POPUP")
        }

        fun create():BasePopup{
            return basePopup
        }

        fun cancelable(canClose:Boolean): Builder {
            basePopup.isCancelable = canClose
            return this
        }
        fun hide(){
            this.hide()
        }

    }

}