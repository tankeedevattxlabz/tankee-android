package com.android.tankee.ui.index_page


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.TankeeApp.Companion.display
import com.android.tankee.analytics.AnalyticsParameter
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.analytics.AnalyticsPageName
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.adapters.createVerticalLayoutManager
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.home.videos.GameType
import com.android.tankee.ui.home.videos.IndexPageBundleFactory
import com.android.tankee.ui.index_page.adapter.IndexPageAdapter
import com.android.tankee.ui.index_page.adapter.models.IndexPageHolderModel
import com.android.tankee.ui.index_page.adapter.models.IndexPageItemsHolderModel
import com.android.tankee.ui.index_page.delete_feature.performDelete
import com.android.tankee.ui.index_page.tags.TagModel
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllItem
import com.android.tankee.ui.views.see_all.SeeAllItemVideosSection
import com.android.tankee.utils.log
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.fragment_index_page.*


class IndexPageFragment : BaseFragment(), IndexPageViewModel.IndexPageView {

    private lateinit var viewModel: IndexPageViewModel
    private lateinit var indexPageContent: IndexPageContent

    private var totalItems: Int = 0
    private var currentItems: Int = 0
    private var scrollOutItems: Int = 0

    private var isLoading: Boolean = false
    private var isToolBarHide: Boolean = false
    private var isNewTag: Boolean = false

    private var sectionName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.log()
        viewModel = IndexPageViewModel(this)
        arguments?.let {
            indexPageContent = it.getSerializable(INDEX_PAGE_CONTENT) as IndexPageContent
            if (it.containsKey(AnalyticsParameter.SECTION_NAME.paramName)) {
                sectionName = it.getString(AnalyticsParameter.SECTION_NAME.paramName)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (indexPageContent is FavsContent) {
            viewModel.reset()
        }
        viewModel.initIndexPage(indexPageContent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_index_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadBackground()
        showToolbar()
        initAdapter()
    }

    private fun loadBackground() {
        Glide.with(context!!)
            .load(R.drawable.bg_index_page_1)
            .apply(requestOptions())
            .into(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    indexPageLayout?.background = resource
                }
            })
    }

    private fun requestOptions(): RequestOptions {
        return RequestOptions()
            .override(
                display.width,
                display.height
            )
            .encodeFormat(Bitmap.CompressFormat.PNG)
            .encodeQuality(100)
    }

    override fun updateMainInfo(items: List<IndexPageHolderModel>) {
        disableLoading()
        (indexPageRecycler?.adapter as? IndexPageAdapter)?.setIndexPageModels(items)
        viewModel.loadFirstPartItems = false
    }

    override fun addMoreItems(items: List<IndexPageHolderModel>) {
        disableLoading()
        if (isNewTag) {
            (indexPageRecycler?.adapter as? IndexPageAdapter)?.removeItems()
            isNewTag = false
        }

        if (viewModel.loadFirstPartItems) {
            viewModel.loadFirstPartItems = false
        }

        (indexPageRecycler?.adapter as? IndexPageAdapter)?.insertIndexPageModels(items)
    }

    private fun enableLoading() {
        isLoading = true
    }

    override fun disableLoading() {
        isLoading = false
    }


    private fun getBaseView(): BaseView {
        return this
    }

    override fun onErrorLoading() {
        findNavController().popBackStack()
    }

    private fun initAdapter() {
        indexPageRecycler.layoutManager = createVerticalLayoutManager()
        addScrollAdapter()
        val adapter = IndexPageAdapter()
        val deleteClickListener = object : IndexPageAdapter.DeleteClickListener {
            override fun onDeleteFinished() {
                println("onDeleteFinished")
                viewModel.reset()
                viewModel.initIndexPage(indexPageContent)
            }

            override fun onDeleteStateChanged(isDeleteMode: Boolean) {
                for (item in adapter.items) {
                    if (item is IndexPageItemsHolderModel) {
                        item.isDeleteMode = isDeleteMode
                    }
                }
                adapter.notifyDataSetChanged()
                showToolbar()
            }

            override fun onDelete(items: List<SectionItemMarker>) {
                performDelete(items, getBaseView(), adapter.deleteClickListener)
            }
        }
        val loadTagItemsListener = object : IndexPageAdapter.LoadTagItemsListener {
            override fun loadTagItems(tagModel: TagModel) {
                isNewTag = true
                viewModel.selectNewTag(tagModel)
            }
        }

        val backListener = object : IndexPageAdapter.BackClickListener {
            override fun onBackClickListener() {
                findNavController().popBackStack()
            }
        }

        indexPageRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                try {
                    if (dy > 50) {
                        if (!isToolBarHide) {
                            (activity as? MainActivity)?.hideToolbar()
                            isToolBarHide = !isToolBarHide
                        }
                    } else if (dy < 0) {
                        if (isToolBarHide) {
                            (activity as? MainActivity)?.showToolbar()
                            isToolBarHide = !isToolBarHide
                        }
                    }
                } catch (e: Exception) {
                }
            }
        })


        adapter.itemClickListener = object : SectionAdapter.ItemClickListener {
            override fun onClick(item: SectionItemMarker) {
                if (item is Video) {
                    navigateToVideoPlayer(item)
                } else if (item is SeeAllItemVideosSection ||
                    item is SeeAllItem ||
                    item is Game ||
                    item is Gamer ||
                    item is GameType
                ) {
                    val bundle = IndexPageBundleFactory().createBundle(item)
                    findNavController().navigate(R.id.indexPageFragment, bundle)
                    sendOpenAnalyticsEvent(item)
                }
            }
        }

        adapter.loadTagItemsListener = loadTagItemsListener
        adapter.backClickListener = backListener
        adapter.deleteClickListener = deleteClickListener

        indexPageRecycler.adapter = adapter
    }

    private fun navigateToVideoPlayer(item: Video) {
        VideoPlayerController.openVideoPlayer(item, findNavController())
    }

    private fun addScrollAdapter() {
        indexPageRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                totalItems = layoutManager.itemCount
                currentItems = layoutManager.childCount
                scrollOutItems = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading && totalItems <= (currentItems + scrollOutItems)) {
                    viewModel.loadMoreItems()
                    enableLoading()
                }
            }
        })
    }

    private fun sendOpenAnalyticsEvent(item: SectionItemMarker) {
        when (item) {
            is Game -> AnalyticsManager.openGameEvent(item, AnalyticsPageName.VIDEOS)
            is Gamer -> AnalyticsManager.openGamerEvent(item, AnalyticsPageName.VIDEOS)
            is GameType -> AnalyticsManager.openGameTypeEvent(item, AnalyticsPageName.VIDEOS)
            is SeeAllItemVideosSection -> AnalyticsManager.seeAllEvent(item.sectionName)
            is SeeAllItem -> AnalyticsManager.seeAllEvent(item.seeAllType.name)
        }
    }

    companion object {
        const val INDEX_PAGE_CONTENT = "index_page_content"
    }
}
