package com.android.tankee.ui.index_page.adapter.models

import com.android.tankee.ui.index_page.IndexPageContent
import com.android.tankee.ui.index_page.adapter.holders.AvatarCornersType

data class IndexPageHeaderHolderModel(
        val title: String,
        val image: String?,
        val avatarCornersType: AvatarCornersType,
        val indexPageContent: IndexPageContent
) : IndexPageHolderModel
