package com.android.tankee.ui.my_stuff.pic.pic_favorites_games.choose_favorite_games


import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.OnFavoriteGamersSelectedAction
import com.android.tankee.event_bus.OnFavoriteGamesSelectedAction
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.ui.my_stuff.pic.base.PickBaseView


class ChooseFavoriteGamesPresenter(private val chooseFavoriteGamesView: ChooseFavoriteGamesView) {

    private val favoriteUnfavorableGames = mutableListOf<Game>()

    fun getGames() {
        chooseFavoriteGamesView.showLoading()
        TankeeRest.api.getGames()
                .enqueue(chooseFavoriteGamesView.createCallBack(
                        { response ->
                            chooseFavoriteGamesView.hideLoading()
                            response.body()?.items?.let { items ->
                                // Show next button button.
                                val favoriteItems = items.filter { game -> game.isFavoriteGame() }
                                EventPoster.postEvent(
                                        OnFavoriteGamesSelectedAction(favoriteItems.isNotEmpty())
                                )
                                chooseFavoriteGamesView.setGames(items)
                            }
                        },
                        {
                            chooseFavoriteGamesView.hideLoading()
                        }
                ))
    }

    fun addGameToStore(game: Game) {
        if (isGameNotChanged(game)) {
            favoriteUnfavorableGames.add(game)
        } else {
            favoriteUnfavorableGames.remove(game)
        }
        EventPoster.postEvent(
                OnFavoriteGamesSelectedAction(favoriteUnfavorableGames.isNotEmpty())
        )
    }

    private fun isGameNotChanged(game: Game): Boolean {
        return favoriteUnfavorableGames.find { changedGame -> game.id == changedGame.id } == null
    }

    fun updateGames() {
        chooseFavoriteGamesView.showLoading()

        favoriteUnfavorableGames.forEach { game ->
            game.like?.let { liked ->
                if (liked) {
                    addLikeToGame(game)
                } else {
                    deleteLikeToGame(game)
                }
            }
        }
        chooseFavoriteGamesView.hideLoading()
        chooseFavoriteGamesView.onGamesUpdatesNavigation()
    }

    private fun addLikeToGame(game: Game) {
        game.id?.let {
            TankeeRest.api.addLikeToGame(it)
                    .enqueue(chooseFavoriteGamesView.createCallBack(
                            { response ->
                            },
                            {
                            }
                    )
                    )
        }
    }

    private fun deleteLikeToGame(game: Game) {
        game.id?.let {
            TankeeRest.api.deleteLikeToGame(it)
                    .enqueue(chooseFavoriteGamesView.createCallBack(
                            { response ->
                            },
                            {
                            }
                    )
                    )
        }
    }
}


interface ChooseFavoriteGamesView : PickBaseView {

    fun setGames(games: List<Game>)

    fun onGamesUpdatesNavigation()

}