package com.android.tankee.ui.index_page

import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.index_page.adapter.models.IndexPageHolderModel
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy
import com.android.tankee.ui.index_page.load_data_strategies.base.LazyLoadingStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategy_factory.LoadingStrategyFactory
import com.android.tankee.ui.index_page.tags.TagModel
import com.android.tankee.ui.index_page.tags.TagsBuilder.Companion.TAG_ALL_VIDEOS

class IndexPageViewModel(private val indexPageView: IndexPageView) {

    private var strategy: IndexPageLoadStrategy? = null
    var loadFirstPartItems: Boolean = true

    fun initIndexPage(indexPageContent: IndexPageContent) {
        if (strategy?.indexPageBuilder?.newItems?.isEmpty() != false) {
            strategy = LoadingStrategyFactory()
                .createLoadingStrategy(indexPageContent, indexPageView)
            loadMainInfo()
        } else {
            strategy?.updateMainInfo()
        }
    }

    fun reset(){
        strategy = null
         loadFirstPartItems= true
    }
    private fun loadMainInfo() {
        strategy?.loadMainInfo()
    }

    fun loadMoreItems() {
        if (!loadFirstPartItems) {
            (strategy as? LazyLoadingStrategy)?.loadItems()
        }
    }

    fun selectNewTag(tagModel: TagModel) {
        loadFirstPartItems = true
        val newTag = when (TAG_ALL_VIDEOS) {
            tagModel.tagName -> ""
            else -> tagModel.tagName
        }

        (strategy as? LazyLoadingStrategy)?.selectNewTag(newTag)
    }



    interface IndexPageView : BaseView {
        fun updateMainInfo(items: List<IndexPageHolderModel>)

        fun addMoreItems(items: List<IndexPageHolderModel>)

        fun onErrorLoading()

        fun disableLoading()
    }
}