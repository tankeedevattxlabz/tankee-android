package com.android.tankee.ui.my_stuff.my_video_content.saved_clips


import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.adapters.VideoItemsAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import com.android.tankee.ui.my_stuff.my_video_content.base.BaseMyStuffSectionFragment
import com.android.tankee.ui.my_stuff.my_video_content.base.MyStuffSectionPlaceHolderModel
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.ui.views.see_all.*
import com.android.tankee.utils.ResUtils
import kotlinx.android.synthetic.main.fragment_my_stuff_section.*


/**
 * Class {@SavedClipsFragment} used for separating saved clips block on my stuff screen.
 *
 * This class use {@SavedClipsFragment} for getting 10 saved clips
 * for showing it into section with SEE ALL button if videos less than 10.
 *
 * If user doesn't have videos here is placeholder with cat).
 */
class SavedClipsFragment : BaseMyStuffSectionFragment(), SavedClipsView {

    private lateinit var savedClipsPresenter: SavedClipsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedClipsPresenter = SavedClipsPresenter(this)
    }

    override fun loadVideos() {
        savedClipsPresenter.loadSavedClipsVideo()
    }

    override fun getTitle(): String {
        return ResUtils.getString(R.string.saved_clips)
    }

    override fun getTitleImage(): Drawable? {
        return ResUtils.getDrawable(R.drawable.ic_save_clips_white)
    }

    override fun getBackgroundDrawableId(): Int {
        return R.drawable.bg_saved_clips
    }

    override fun buildSeeAllButton(): SeeAllItem {
        return SeeAllBuilder()
                .setType(SeeAllType.SAVED_CLIPS) // Set SeeAllType for detect index page content.
                .setStyle(SeeAllStyle.RED)
                .setSectionName(getTitle()) // Set section for index page title.
                .build()
    }

    override fun getPlaceHolderModel(): MyStuffSectionPlaceHolderModel {
        return MyStuffSectionPlaceHolderModel(
                R.color.colorBlue,
                getTitleImage(),
                getTitle(),
                ResUtils.getString(R.string.you_have_not_saved_any_videos_yet)
        )
    }

    override fun <T : Video> setVideos(videos: List<T>) {
        videoItemsRecycler.layoutManager = createHorizontalLayoutManager()
        videoItemsRecycler.adapter = getVideoItemsAdapter(videos)
    }

    private fun getVideoItemsAdapter(favoriteVideos: List<Video>): VideoItemsAdapter {
        val adapter = VideoItemsAdapter()
        adapter.items = buildVideoItems(favoriteVideos.toMutableList())
        adapter.listener = object : SectionAdapter.ItemClickListener {
            override fun onClick(item: SectionItemMarker) {
                when (item) {
                    is Video -> VideoPlayerController.openVideoPlayer(item, findNavController())
                    is SeeAllItem -> navigateToIndexPage(item)
                }
            }
        }
        return adapter
    }

}
