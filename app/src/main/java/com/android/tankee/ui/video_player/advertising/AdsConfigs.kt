package com.android.tankee.ui.video_player.advertising

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AdType {

    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("active")
    @Expose
    var active: Boolean? = null
    @SerializedName("break_timing")
    @Expose
    var breakTiming: String? = null

}

class Config {
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("frequency")
    @Expose
    var frequency: Int? = null
    @SerializedName("skippable_seconds")
    @Expose
    var skippableSeconds: Int? = null
    @SerializedName("config")
    @Expose
    var config: List<AdType>? = null
}

class AdsConfigResponse {
    @SerializedName("items")
    @Expose
    var items: List<Config>? = null
    @SerializedName("hasData")
    @Expose
    var hasData: Boolean? = null
    @SerializedName("totalResults")
    @Expose
    var totalResults: Int? = null

    fun getConfig():Config?{
        return items?.firstOrNull()
    }
}


enum class VideoPlayerAdType(val value: String) {
    // Config for show or hide video player banner ads .
    BannerAndroid("banner_android"),
    // Config for show or hide JWP video player ads.
    VideoAndroid("video_android")
}
