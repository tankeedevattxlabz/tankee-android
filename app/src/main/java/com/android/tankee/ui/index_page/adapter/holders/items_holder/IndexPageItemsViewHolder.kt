package com.android.tankee.ui.index_page.adapter.holders.items_holder

import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.index_page.adapter.models.IndexPageItemsHolderModel
import kotlinx.android.synthetic.main.item_index_page_horizontal_items.view.*
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.linearLayout

class IndexPageItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun onBind(itemsModel: IndexPageItemsHolderModel, itemClickListener: SectionAdapter.ItemClickListener?) {

        itemView.container.removeAllViews()
        itemView.container.linearLayout {
            orientation = LinearLayout.HORIZONTAL
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            setItems(itemsModel, itemClickListener)
        }
    }

    private fun @AnkoViewDslMarker _LinearLayout.setItems(
        itemsModel: IndexPageItemsHolderModel,
        itemClickListener: SectionAdapter.ItemClickListener?
    ) {
        val itemsSize = itemsModel.items.size
        for (i in 0 until itemsModel.horizontalCount) {
            if (i < itemsSize) {
                val item = itemsModel.items[i]
                frameLayout {
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        1f
                    )
                    // Put item into frame layout for scaling items by with.
                    createItemView(item, itemClickListener, itemsModel.isDeleteMode)
//                    textView {
//                        text = "Hello"
//                    }
                }
            } else {
                // Set empty view for right scaling items.
                getEmptyView()
            }
        }
    }
}