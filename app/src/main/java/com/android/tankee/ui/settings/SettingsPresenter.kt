package com.android.tankee.ui.settings


import com.android.tankee.BuildConfig
import com.android.tankee.NULL_STRING
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.database.AppDatabase
import com.android.tankee.ui.base.BaseView


class SettingsPresenter(private val settingsView: SettingsView) {

    fun initSettings() {
        settingsView.setSettings(getSettings())
    }

    private fun getSettings(): MutableList<SettingModel> {
        val settings = mutableListOf<SettingModel>()

        val aboutSetting = SettingModel(SettingsId.About, R.string.about, R.drawable.ic_info)
        settings.add(aboutSetting)

        if (AppDatabase.getAppDatabase().userDao().getUser() != null) {
            val switchUser = SettingModel(SettingsId.SwitchUser, R.string.switch_user, R.drawable.ic_account)
            settings.add(switchUser)

            val deleteAccount = SettingModel(SettingsId.DeleteAccount, R.string.delete_account, R.drawable.ic_account)
            settings.add(deleteAccount)

        } else {
            val switchUser = SettingModel(SettingsId.JoinOrSignIn, R.string.about_join_or_sign_in, R.drawable.ic_account)
            settings.add(switchUser)
        }

        if (BuildConfig.DEBUG) {
            val switchUser = SettingModel(SettingsId.DebugInfo, R.string.debug_info, R.drawable.ic_bug_report)
            settings.add(switchUser)
        }

        return settings
    }

    interface SettingsView : BaseView {
        fun setSettings(setting: List<SettingModel>)
    }

}

enum class SettingsId {
    About,
    SwitchUser,
    JoinOrSignIn,
    DeleteAccount,
    DebugInfo
}