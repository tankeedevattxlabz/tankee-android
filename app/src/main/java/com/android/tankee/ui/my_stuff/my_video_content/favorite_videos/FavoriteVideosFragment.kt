package com.android.tankee.ui.my_stuff.my_video_content.favorite_videos


import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.dynamic_links.DynamicLinks
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.SectionAdapter
import com.android.tankee.ui.adapters.VideoItemsAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import com.android.tankee.ui.home.videos.IndexPageBundleFactory
import com.android.tankee.ui.my_stuff.my_video_content.base.BaseMyStuffSectionFragment
import com.android.tankee.ui.my_stuff.my_video_content.base.MyStuffSectionPlaceHolderModel
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.ui.views.see_all.*
import com.android.tankee.utils.ResUtils
import kotlinx.android.synthetic.main.fragment_my_stuff_section.*


/**
 * Class {@FavoriteVideosFragment} used for separating isFavorite videos block on my stuff screen.
 *
 * This class use {@FavoriteVideosPresenter} for getting 10 isFavorite videos
 * for showing it into section with SEE ALL button if videos less than 10.
 *
 * If user doesn't have videos here is placeholder with cat).
 */
class FavoriteVideosFragment : BaseMyStuffSectionFragment(), FavoriteVideoView {

    private lateinit var favoriteVideosPresenter: FavoriteVideosPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        favoriteVideosPresenter = FavoriteVideosPresenter(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDynamicLink()
    }

    override fun loadVideos() {
        favoriteVideosPresenter.loadFavoriteVideo()
    }

    override fun getBackgroundDrawableId(): Int {
        return R.drawable.bg_favorite_videos
    }

    override fun getTitle(): String {
        return ResUtils.getString(R.string.favorite_videos)
    }

    override fun getTitleImage(): Drawable? {
        return ResUtils.getDrawable(R.drawable.ic_heart)
    }

    override fun buildSeeAllButton(): SeeAllItem {
        return SeeAllBuilder()
            .setType(SeeAllType.FAVORITE_VIDEOS) // Set SeeAllType for detect index page content.
            .setStyle(SeeAllStyle.RED)
            .setSectionName(ResUtils.getString(R.string.favorite_videos)) // Set section for index page title.
            .build()
    }

    override fun getPlaceHolderModel(): MyStuffSectionPlaceHolderModel {
        return MyStuffSectionPlaceHolderModel(
            R.color.colorRed,
            getTitleImage(),
            getTitle(),
            ResUtils.getString(R.string.you_have_not_favourite_any_videos_yet)
        )
    }

    override fun <T : Video> setVideos(videos: List<T>) {
        videoItemsRecycler.layoutManager = createHorizontalLayoutManager()
        videoItemsRecycler.adapter = getVideoItemsAdapter(videos)
    }

    private fun getVideoItemsAdapter(favoriteVideos: List<Video>): VideoItemsAdapter {
        val adapter = VideoItemsAdapter()
        adapter.items = buildVideoItems(favoriteVideos.toMutableList())
        adapter.listener = object : SectionAdapter.ItemClickListener {
            override fun onClick(item: SectionItemMarker) {
                when (item) {
                    is Video -> VideoPlayerController.openVideoPlayer(item, findNavController())
                    is SeeAllItem -> navigateToIndexPage(item)
                }
            }
        }
        return adapter
    }

    private fun getDynamicLink() {
        if (AnonymousUserRepo().isNotUserAnonymous()) {
            val linkPath = SessionDataHolder.dynamicLink?.getPath()

            linkPath.let {
                when (it) {
                    DynamicLinks.FavoriteVideosPage.path -> {
                        navigateToIndexPageWithDynamicLink(it)
                    }
                    else -> return
                }
                SessionDataHolder.clearDynamicLink()
            }
        }
    }

    private fun navigateToIndexPageWithDynamicLink(it: String) {
        val indexPageBundle = IndexPageBundleFactory().createBundleDynamicLink(it)
        findNavController().navigate(R.id.indexPageFragment, indexPageBundle)
    }

}
