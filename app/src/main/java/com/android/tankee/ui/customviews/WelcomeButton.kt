package com.android.tankee.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View.OnClickListener
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.android.tankee.R
import kotlinx.android.synthetic.main.welcome_button.view.*

class WelcomeButton : FrameLayout {
    constructor(context: Context?) : super(context!!) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        init(context, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context!!, attrs, defStyleAttr) {
        init(context, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context!!,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(context, attrs)
    }

    fun init(context: Context?, attrs: AttributeSet? = null) {
        val id = R.layout.welcome_button
        val view = LayoutInflater.from(context).inflate(id, this, true)
        val typedArray = context?.theme?.obtainStyledAttributes(attrs, R.styleable.WelcomeButton, 0, 0)
        val imageRes = typedArray?.getResourceId(R.styleable.WelcomeButton_imageRes, -1)
        if (imageRes ?: -1 > 0) {
            imageRes?.let { view.image_iv.setImageResource(it) }
        }
        val textResourceId = typedArray?.getResourceId(R.styleable.WelcomeButton_textRes, -1)
        val string = typedArray?.getString(R.styleable.WelcomeButton_text)
        if (textResourceId ?: -1 > 0) {
            textResourceId?.let { view?.text_tv?.setText(it) }
        } else {
            string?.let { view?.text_tv?.text = string }
        }
        typedArray?.recycle()
        val startset = ConstraintSet()
        startset.clone(constraint_bt)
        val finishSet = ConstraintSet()
        finishSet.clone(startset)
        finishSet.connect(R.id.left_indicator, ConstraintSet.START, constraint_bt.id, ConstraintSet.START)
        finishSet.connect(R.id.right_indicator, ConstraintSet.END, constraint_bt.id, ConstraintSet.END)
        finishSet.applyTo(constraint_bt)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        val newListener: OnClickListener = OnClickListener {
            //            postDelayed({
                l?.onClick(it)
//            }, ANIMATION_DURATION)
//            animateClick()
        }
        super.setOnClickListener(newListener)
    }

    private val ANIMATION_DURATION = 200L

    fun animateClick() {
        val autoTransition = AutoTransition()
        autoTransition.duration = ANIMATION_DURATION
        val startset = ConstraintSet()
        startset.clone(constraint_bt)
        val finishSet = ConstraintSet()
        finishSet.clone(startset)
        finishSet.connect(R.id.left_indicator, ConstraintSet.START, constraint_bt.id, ConstraintSet.START)
        finishSet.connect(R.id.right_indicator, ConstraintSet.END, constraint_bt.id, ConstraintSet.END)
        TransitionManager.beginDelayedTransition(this, autoTransition)
        finishSet.applyTo(constraint_bt)
        postDelayed({
            TransitionManager.beginDelayedTransition(this, autoTransition)
            startset.applyTo(constraint_bt)
        }, 200)
    }
}