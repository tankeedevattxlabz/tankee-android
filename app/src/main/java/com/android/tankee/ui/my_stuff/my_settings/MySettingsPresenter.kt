package com.android.tankee.ui.my_stuff.my_settings


import com.android.tankee.repo.avatars.AvatarsRepository
import com.android.tankee.ui.base.BaseView


class MySettingsPresenter(private val mySettingsView: MySettingsView) {

    fun initAvatar() {
        mySettingsView.setAvatar(AvatarsRepository.getAvatar())
    }

}


interface MySettingsView : BaseView {

    fun setAvatar(avatarDrawerId: Int)

}