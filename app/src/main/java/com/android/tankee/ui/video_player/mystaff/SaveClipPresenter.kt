package com.android.tankee.ui.videoplayer.mystaff

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.database.AppDatabase
import com.android.tankee.extentions.disableClick
import com.android.tankee.extentions.enableClick
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.dp
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.jetbrains.anko.textColor
import java.util.concurrent.TimeUnit
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToLong

class SaveClipPresenter(
    private var rootView: ConstraintLayout?,
    private val video: Video,
    private var savedClipsView: SavedClipsView?
) {
    private val saveClipItem: ViewGroup
    private var fullDuration: Long = 0
    private var saveClipTimestamp: SaveClip? = null
    private val UNBOUNDED = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    private var saveClipInProgress = false

    init {
        video.duration?.let{
            fullDuration = video.duration!!.toDouble().roundToLong()
        }
        saveClipItem = rootView!!.findViewById(R.id.saveClipItem)
        saveClipItem.setOnClickListener {
            if (AppDatabase.getAppDatabase().userDao().getUser() != null) {
                userClick()
            } else {
                savedClipsView?.openCreateAccountDialog()
            }
        }
        showSaveClipForTime(0)
        setClipSavedState(false)
    }

    fun setSavedClipItemEnable() {
        saveClipItem.enableClick()
    }

    fun setSavedClipItemDisable() {
        saveClipItem.disableClick()
    }

    private fun userClick() {
        if (saveClipInProgress) {
            return
        }
        saveClipInProgress = true

        AnalyticsManager.addClipVideo()
        saveClipTimestamp?.let {
            TankeeRest.api.saveClip(it).enqueue(savedClipsView!!.createCallBack(
                { response ->
                    if (response.isSuccessful) {
                        val popup = setClipSavedState(true)
                        if (popup != null) {
                            rootView?.postDelayed({
                                rootView?.removeView(popup)
                                setClipSavedState(false)
                                saveClipInProgress = false
                            }, TimeUnit.SECONDS.toMillis(5))
                        }
                    }

                },
                { t ->
                    t.printStackTrace()
                    saveClipInProgress = false
                }
            )
            )
        }
    }

    private fun animateToTop(view: View?) {
        val array = IntArray(2) { 0 }
        saveClipItem.getLocationInWindow(array)
        val animator = ObjectAnimator.ofFloat(view, "translationY", (array[1] - getViewHeight(view!!) - 4.dp()).toFloat())
        animator.duration = TimeUnit.SECONDS.toMillis(1)
        animator.setAutoCancel(true)
        val fadeoutAnimator = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
        fadeoutAnimator.duration = 300.toLong()
        val animatorSet = AnimatorSet()
        animatorSet.apply {
            play(animator).with(fadeoutAnimator)
            start()
        }
    }

    private fun createPopupView(): View? {
        try {
            var view: View? = null
            rootView?.apply {

                view =
                    LayoutInflater.from(saveClipItem.context)
                        .inflate(R.layout.view_tankee_clip_saved, saveClipItem, false)

                if (view != null) {
                    addView(view)
                    val constraintSet = ConstraintSet()
                    constraintSet.clone(this)
                    val array = IntArray(2) { 0 }
                    saveClipItem.getLocationInWindow(array)

                    val saveClipItemMiddleX = array[0] + saveClipItem.width / 2

                    val x = saveClipItemMiddleX - getViewWidth(view!!) / 2

                    val saveClipItemMiddleY = array[1] + saveClipItem.height / 2

                    val y = saveClipItemMiddleY - getViewHeight(view!!) / 2

                    constraintSet.setTranslationX(view!!.id, x.toFloat())

                    constraintSet.setTranslationY(view!!.id, y.toFloat())

                    constraintSet.applyTo(this)
                }
            }
            return view
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    private fun changeViewState(showWings: Boolean, drawable: Int) {
        val saveClip_bottom_toolbar_element = saveClipItem.findViewById<View>(R.id.saveClip_bottom_toolbar_element)
        val leftWing = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_icon_left)
        val rightWing = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_right)

        val textView = saveClipItem.findViewById<TextView>(R.id.saveClipLabel)



        val icon = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_icon)
        icon.setImageResource(drawable)

        if(showWings){
            textView.text = textView.context.getString(R.string.toolbar_item_save_clip_active)

            textView.textColor = ResUtils.getColor(R.color.colorWhite)
            leftWing.visibility = View.VISIBLE
            rightWing.visibility = View.VISIBLE

        }
        else
        {
            textView.text = textView.context.getString(R.string.toolbar_item_save_clip_inactive)
            textView.textColor = ResUtils.getColor(R.color.colorVideoItemPlayerName)
            leftWing.visibility = View.INVISIBLE
            rightWing.visibility = View.INVISIBLE
        }
    }

    private fun setClipSavedState(showSavedState: Boolean): View? {

        changeViewState(showSavedState, if(showSavedState) R.drawable.ic_clip_saved else R.drawable.ic_save_clip_inactive)

        return if (showSavedState) {
            val fl: View? = createPopupView()
            fl?.elevation = 12f
            animateToTop(fl)
            fl
        } else
            null
    }

    private fun getViewHeight(view: View): Int {
        view.measure(UNBOUNDED, UNBOUNDED)
        return view.measuredHeight
    }

    private fun getViewWidth(view: View): Int {
        view.measure(UNBOUNDED, UNBOUNDED)
        return view.measuredWidth
    }

    fun showSaveClipForTime(position: Long) {
        if(video.id!=null) {
            var start = max(position - 25, 0)
            var end = min(position + 25, fullDuration)

            if (end == fullDuration) {
                start = end - 30
            }

            if (start == 0L) {
                end = start + 30
            }

            saveClipTimestamp =
                SaveClip(video.id!!, start.toInt())
        }
    }

    fun destroy() {
        savedClipsView = null
        rootView = null
    }

    data class SaveClip(
        @SerializedName("video_id")
        @Expose
        val video_id: Int,
        @SerializedName("time")
        @Expose
        val time: Int
    )
}


interface SavedClipsView : BaseView {

    fun openCreateAccountDialog()

}
