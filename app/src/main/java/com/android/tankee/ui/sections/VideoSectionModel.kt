package com.android.tankee.ui.sections

import com.android.tankee.ui.views.see_all.SectionItemMarker

class VideoSectionModel(
    val id: Int,
    override val title: String,
    val backgroundUrl: String,
    override val items: List<SectionItemMarker>,
    override val totalItemCount: Int,
    sectionType: SectionType
) : SectionModel(title, 0, items, totalItemCount, sectionType)