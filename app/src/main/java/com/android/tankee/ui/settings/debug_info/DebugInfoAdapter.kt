package com.android.tankee.ui.settings.debug_info


import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.OnRecyclerItemClickListener
import kotlinx.android.synthetic.main.item_debug_info.view.*


class DebugInfoAdapter : BaseRecyclerAdapter<
        DebugInfoModel,
        OnRecyclerItemClickListener<DebugInfoModel>,
        DebugInfoAdapter.DebugInfoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DebugInfoViewHolder {
        return DebugInfoViewHolder(inflate(R.layout.item_debug_info, parent, false))
    }


    class DebugInfoViewHolder(itemView: View) :
            BaseVideoHolder<DebugInfoModel, OnRecyclerItemClickListener<DebugInfoModel>>(itemView) {

        override fun onBind(item: DebugInfoModel, listener: OnRecyclerItemClickListener<DebugInfoModel>?) {
            itemView.debugInfoName.text = item.name
            itemView.debugInfoValue.text = item.value
            itemView.setOnClickListener { listener?.onItemClicked(item) }
        }
    }
}
