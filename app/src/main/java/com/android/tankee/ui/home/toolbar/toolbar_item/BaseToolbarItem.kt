package com.android.tankee.ui.home.toolbar.toolbar_item

import android.content.Context
import android.content.res.TypedArray
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.analytics.AnalyticsPageName
import com.android.tankee.ui.home.toolbar.TankeeToolbar


/**
 * Class {@BaseToolbarItem} contains base logic for creating view
 * and for getting styleable params and set click action, on click destination, name.
 */
abstract class BaseToolbarItem(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs), ToolbarItemInterface {

    var itemSelected: Boolean = false
    var selectionListener: TankeeToolbar.ToolbarSelectionListener? = null
    var destinationId: Int = -1
    var isEnabledItem: Boolean = true

    protected var imageResourceId: Int = -1
    protected var disabledImageResourceId: Int = -1
    protected var animationsEnabled = true


    init {
        initAttributes(context, attrs)
    }

    private fun initAttributes(context: Context, set: AttributeSet?) {
        val attributes: TypedArray?
        var resourceId: Int = -1
        var textResourceId: Int = -1
        var padding = 0

        try {
            attributes = context.theme.obtainStyledAttributes(set, R.styleable.ToolbarSelectableItem, 0, 0)
            resourceId = attributes.getResourceId(R.styleable.ToolbarSelectableItem_destinationId, -1)
            imageResourceId = attributes.getResourceId(R.styleable.ToolbarSelectableItem_itemImage, -1)
            disabledImageResourceId = attributes.getResourceId(R.styleable.ToolbarSelectableItem_itemImageDisabled, -1)
            textResourceId = attributes.getResourceId(R.styleable.ToolbarSelectableItem_itemImageText, -1)
            animationsEnabled = attributes.getBoolean(R.styleable.ToolbarSelectableItem_animate, true)

            padding = attributes.getDimensionPixelSize(R.styleable.ToolbarSelectableItem_itemPadding, 0)
            destinationId = resourceId
//            attributes.recycle()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        setOnClickListener {
            if (isEnabledItem) {
                onItemClick()
                sendToolbarEvent(resourceId)
                selectionListener?.onDestinationSelected(resourceId)
            }
        }

        initConstraintSets()
        inflate(context, getLayoutId(), this)

        setBodyImage()
        setBodyImagePadding(padding)
        setText(textResourceId)
    }

    abstract fun initConstraintSets()

    abstract fun setBodyImage()

    abstract fun setBodyImagePadding(padding: Int)

    abstract fun setText(textResourceId: Int)

    abstract fun setHighlighted(selected: Boolean)

    abstract fun getLayoutId(): Int

    abstract fun onItemClick()

    fun isItemSelected(): Boolean {
        return itemSelected
    }

   private fun sendToolbarEvent(destinationId: Int) {
        when (destinationId) {
            R.id.homeListFragment -> AnalyticsManager.toolbarItemEvent(AnalyticsPageName.VIDEOS.value)
            R.id.searchFragment -> AnalyticsManager.toolbarItemEvent(AnalyticsPageName.SEARCH.value)
            R.id.settingsFragment -> AnalyticsManager.toolbarItemEvent(AnalyticsPageName.MENU.value)
            R.id.myStuffFragment -> AnalyticsManager.toolbarItemEvent(AnalyticsPageName.MY_STUFF.value)
        }
    }

     fun ImageView.setBlackWhite(bw: Boolean) {
        val matrix = ColorMatrix()
        matrix.setSaturation(if (bw) 0f else 1f)
        val filter = ColorMatrixColorFilter(matrix)
        this.colorFilter = filter
    }
}