package com.android.tankee.ui.authorization.helper


import com.android.tankee.ui.authorization.join.UserAccountInfoModel


object AuthoResetDataHelper {

    fun resetData() {
        UserAccountInfoModel.instance.kwsCreateUserRequestBody = null
    }
}