package com.android.tankee.ui.about

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.android.tankee.HTML_BR
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.utils.setSpanColor
import com.android.tankee.utils.setSpanStringBold
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment : BaseFragment(), AboutViewModel.AboutView {

    private lateinit var viewModel: AboutViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = AboutViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back.setOnClickListener { findNavController().popBackStack() }
        viewModel.initAboutData()
    }

    override fun onResume() {
        super.onResume()
        val mainActivity = activity as? MainActivity
        mainActivity?.hideToolbar()
    }

    /**
     * Set about info: title, video thumbnail, text.
     */
    override fun setAbout(titleAboutText: String?, thumbnailAboutUrl: String?, textAboutText: String?, video: Video?) {
        thumbnailAbout.setOnClickListener {
            video?.let { video ->
                AnalyticsManager.watchForParentEvent(video)
                VideoPlayerController.openVideoPlayer(video, findNavController(), true)
            }
        }

        titleAboutText?.let { titleAbout?.text = it }
        context?.let { i ->
            Glide.with(i)
                .load(thumbnailAboutUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        try {
                            play?.visibility = View.VISIBLE
                        } catch (ignore: Exception) {
                        }
                        return false
                    }
                })
                .into(thumbnailAbout)
        }
        textAboutText?.let { aboutText?.text = styleAboutText(it) }
    }

    /**
     * Set bold typeface for titles,
     * set color sites URL's,
     * delete '^' char,
     * make text HTML.
     */
    private fun styleAboutText(text: String): Spanned? {

        var isOpenTag = true
        var startTitleIndex = 0
        var endTitleIndex: Int
        val titleList = mutableListOf<String>()

        // Find titles.
        for (i in 0 until text.length) {
            if (text[i] == '^') {
                if (isOpenTag) {
                    if (i < text.length) {
                        startTitleIndex = i + 1
                    }
                } else {
                    if (i != 0) {
                        endTitleIndex = i

                        // Get title
                        val title = text.subSequence(startTitleIndex, endTitleIndex) as String

                        if (!title.isEmpty()) {
                            titleList.add(title)
                        }
                    }
                }

                // Set close or open tagName status.
                isOpenTag = !isOpenTag
            }
        }

        // Make new line.
        val htmlText = text
            .replace("\r\n", HTML_BR)
            .replace("^", "")

        val span =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(htmlText)
            } as SpannableStringBuilder

        // Set color for sites.
        setSitesColor(span, span.toString())

        // Set titles bold.
        titleList.forEach {
            setSpanStringBold(span, it, span.toString())
        }

        return span
    }

    private fun setSitesColor(span: SpannableStringBuilder, text: String) {
        val siteList = mutableListOf<String>()

        siteList.add(getString(R.string.about_site_tankee))
        siteList.add(getString(R.string.about_site_policy))
        siteList.add(getString(R.string.about_site_terms))
        siteList.add(getString(R.string.about_site_help))

        for (i in siteList) {
            setSpanColor(span, i, text, resources.getColor(R.color.lightGreen))
        }
    }
}
