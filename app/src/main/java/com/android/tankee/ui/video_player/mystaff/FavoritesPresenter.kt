package com.android.tankee.ui.video_player.mystaff


import android.graphics.Color
import android.graphics.PorterDuff
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.database.AppDatabase
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.ResUtils
import org.jetbrains.anko.textColor


class FavoritesPresenter(
        private val favoriteItem: View,
        private val video: Video,
        private var favoritesView: FavoritesView
) {
    private var itemInFav = false

    init {
        setFavoriteState()

        if (AppDatabase.getAppDatabase().userDao().getUser() != null) {
            updateState()
        }
    }

    fun onFavButtonClick() {
        if (AppDatabase.getAppDatabase().userDao().getUser() != null) {
            userClick()
        } else {
            favoritesView.openCreateAccountDialog()
        }
    }

    private fun userClick() {
        deleteOrAdd()
    }

    private fun deleteOrAdd() {
        val videoId = video.id
        if(videoId == null || video.title.isNullOrEmpty())
            return

        if (itemInFav) {
            AnalyticsManager.deleteFavoriteVideo()
            TankeeRest.api.deleteVideoFromFavorites(videoId).enqueue(
                    favoritesView.createCallBack(
                            {
                                itemInFav = false
                                setFavoriteState()
                            },
                            { t ->
                                t.printStackTrace()
                                updateState()
                            }
                    )
            )
        } else {
            AnalyticsManager.addFavoriteVideo(video.title!!)
            TankeeRest.api.addVideoToFavorites(videoId).enqueue(
                    favoritesView.createCallBack(
                            {
                                itemInFav = true
                                setFavoriteState()
                            },
                            { t ->
                                t.printStackTrace()
                                updateState()
                            }
                    )
            )
        }
    }
    private fun changeViewState(showWings: Boolean, drawable: Int) {
        val saveClip_bottom_toolbar_element = favoriteItem.findViewById<View>(R.id.favorite_bottom_toolbar_element)
        val leftWing = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_icon_left)
        val rightWing = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_right)
        val textView = favoriteItem.findViewById<TextView>(R.id.favoriteLabel)

        leftWing.clearColorFilter()
        leftWing.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        rightWing.clearColorFilter()
        rightWing.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)

        val icon = saveClip_bottom_toolbar_element.findViewById<ImageView>(R.id.bottom_toolbar_icon)
        icon.setImageResource(drawable)

        if(showWings){
            textView.textColor = ResUtils.getColor(R.color.colorWhite)
            leftWing.visibility = View.VISIBLE
            rightWing.visibility = View.VISIBLE

        }
        else
        {
            textView.textColor = ResUtils.getColor(R.color.colorVideoItemPlayerName)
            leftWing.visibility = View.INVISIBLE
            rightWing.visibility = View.INVISIBLE
        }
    }

    private fun setFavoriteState() {
        changeViewState(itemInFav, if(itemInFav) R.drawable.ic_like else R.drawable.ic_favorite_inactive)
    }

    private fun updateState() {
        val videoId = video.id
        if(videoId == null || video.title.isNullOrEmpty())
            return

        TankeeRest.api.getFavoriteVideos().enqueue(favoritesView.createCallBack(
            { response ->
                response.body()?.items?.let {
                        items ->
                    itemInFav = false

                    for (item in items) {
                        if (item.id == videoId) {
                            itemInFav = true
                            break
                        }
                    }

                    setFavoriteState()
                }

            }, {
                it.printStackTrace()
            })
        )
    }

}

interface FavoritesView : BaseView {

    fun openCreateAccountDialog()

}

