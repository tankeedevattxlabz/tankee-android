package com.android.tankee.ui.home.toolbar.toolbar_item


import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.android.tankee.R
import com.android.tankee.event_bus.ChangeToolbarAvatar
import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.OpenCreateAccountDialog
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.repo.avatars.AvatarsRepository
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.utils.ResUtils
import kotlinx.android.synthetic.main.toolbar_selectable_avatar_item.view.*
import kotlinx.android.synthetic.main.toolbar_selectable_avatar_item.view.left_indicator
import kotlinx.android.synthetic.main.toolbar_selectable_avatar_item.view.right_indicator
import kotlinx.android.synthetic.main.toolbar_selectable_avatar_item.view.title
import kotlinx.android.synthetic.main.toolbar_selectable_item_enabled.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ToolbarAvatarItem(context: Context, attrs: AttributeSet?) : BaseToolbarItem(context, attrs) {

    override fun initConstraintSets() {
    }

    override fun onItemClick() {
        // No action
    }

    init {
        setDisableState()
        EventBus.getDefault().register(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.toolbar_selectable_avatar_item
    }

    override fun setBodyImage() {
        imageResourceId = AvatarsRepository.getAvatar()
        disabledImageResourceId = imageResourceId
        avatarIcon.setImageResource(imageResourceId)
    }

    override fun setBodyImagePadding(padding: Int) {
    }

    override fun setText(textResourceId: Int) {
        if (textResourceId > 0) {
            title.setText(textResourceId)
        }
    }

    override fun setHighlighted(selected: Boolean) {
        setToolbarAvatarColorFilter(selected)
        setToolbarAvatarBorder(selected)

        if (checkIsSelectedItem(selected)) {
            return
        }

        setItemState(selected)
    }

    private fun setItemState(selected: Boolean) {
        if (selected) {
            setEnableState()
        } else {
            setDisableState()
        }
        this.itemSelected = selected
    }

    private fun checkIsSelectedItem(selected: Boolean) =
            this.itemSelected == selected || !animationsEnabled

    private fun setToolbarAvatarBorder(selected: Boolean) {
        toolbarAvatarBorder.setImageResource(when {
            selected -> R.drawable.toolbar_avatar_selector_active
            else -> R.drawable.toolbar_avatar_selector_inactive
        })
    }

    private fun setToolbarAvatarColorFilter(selected: Boolean) {
        avatarIcon.setBlackWhite(!selected)
    }


    private fun setEnableState() {
        title.show()
        title.animate().alpha(1f)

        val layoutParams = ConstraintLayout.LayoutParams(
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_enable).toInt(),
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_enable).toInt()
        )
        layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.bottomToTop = R.id.title
        toolbarAvatarBorder.layoutParams = layoutParams

        right_indicator.animate().alpha(1f)
        left_indicator.animate().alpha(1f)

        right_indicator.animate().translationX(0f)
        left_indicator.animate().translationX(0f)
    }

    private fun setDisableState() {
        title.gone()
        title.animate().alpha(0f)

        val layoutParams = ConstraintLayout.LayoutParams(
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_disable).toInt(),
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_disable).toInt()
        )
        layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
        toolbarAvatarBorder.layoutParams = layoutParams

        right_indicator.animate().alpha(0f)
        left_indicator.animate().alpha(0f)

        right_indicator.animate().translationX(-49f)
        left_indicator.animate().translationX(49f)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun changeToolbarAvatar(event: ChangeToolbarAvatar) {
        imageResourceId = AvatarsRepository.getAvatar()
        disabledImageResourceId = imageResourceId
        avatarIcon.setImageResource(imageResourceId)
    }
}