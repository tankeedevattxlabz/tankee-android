package com.android.tankee.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import kotlinx.android.synthetic.main.view_generate_name.view.*

class TankeeGenerateName(context: Context, private val attrs: AttributeSet) : RelativeLayout(context, attrs) {

    private var showError: Boolean = false
    var updateRootContainer: (() -> Unit)? = null
    var regenerateNameAction: (() -> Unit)? = null

    init {
        inflate(context, R.layout.view_generate_name, this)

        getAttrs()
        initViews()
    }

    private fun initViews() {
        regeneratedName.setOnClickListener {
            regeneratedName.isEnabled = false
            regeneratedName.postDelayed({ regeneratedName.isEnabled = true}, 500L)

            it.animate().setDuration(500L).rotationBy(360f)
            regenerateNameAction?.invoke()
        }
    }

    @SuppressLint("CustomViewStyleable", "Recycle")
    private fun getAttrs() {
        val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.TankeeGenerateNameOptions,
                0,
                0
        )

        val icon = typedArray.getResourceId(R.styleable.TankeeGenerateNameOptions_tgn_hintIcon, 0)
        val hintText = typedArray.getResourceId(R.styleable.TankeeGenerateNameOptions_tgn_hintText, 0)

        setAttrsValues(icon, hintText)
    }

    private fun setAttrsValues(icon: Int, hintId: Int) {
        if (icon != 0) {
            hintIcon.setImageDrawable(resources.getDrawable(icon, null))
        }

        if (hintId != 0) {
            generateNameEditText.hint = resources.getString(hintId)
        }
    }

    fun setError(errorText: String = EMPTY_STRING) {
        if (isErrorHide()) {
            updateRootContainer?.invoke()
            helperView.visibility = View.VISIBLE
            setErrorBackground()

            helperText.animate().translationY(60f)
            helperText.text = if (errorText.isNotEmpty()) {
                errorText
            } else {
                EMPTY_STRING
            }
            showError = true
        }
    }

    fun hideError() {
        if (isErrorShow()) {
            helperView.visibility = View.GONE
            setUnselectedBackground()

            updateRootContainer?.invoke()
            helperText.animate().translationY(0f)
            showError = false
        }
    }

    private fun isErrorShow(): Boolean {
        return showError
    }

    private fun isErrorHide(): Boolean {
        return !showError
    }

    private fun setUnselectedBackground() {
        tankeeEdiTextLayout.setBackgroundResource(R.drawable.bg_unselected)
    }

    private fun setErrorBackground() {
        tankeeEdiTextLayout.setBackgroundResource(R.drawable.bg_error)
    }

    fun getText(): String {
        return generateNameEditText.text.toString().trim()
    }

    fun setText(generatedDisplayName: String) {
        generateNameEditText.text = Editable.Factory.getInstance().newEditable(generatedDisplayName)
    }
}