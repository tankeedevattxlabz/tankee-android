package com.android.tankee.ui.authorization.join.generate_name


import com.android.tankee.TANKEE
import com.android.tankee.database.AppDatabase
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.rest.kws.KwsRestUtils
import com.android.tankee.rest.kws.responces.activate_user_account.ActivateUserAccountRequestBody
import com.android.tankee.rest.utils.RequestBodyConverter
import com.android.tankee.ui.authorization.join.SignInLogic
import com.android.tankee.ui.authorization.join.UserAccountInfoModel
import com.android.tankee.ui.authorization.join.account_info.AccountInfoViewModel
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.authorization.sign_in.controller.SignInStepsHandler
import com.android.tankee.ui.authorization.sign_in.controller.SignInFlow
import com.android.tankee.ui.base.BaseView
import okhttp3.RequestBody
import java.util.*


class GenerateNameViewModel(private val generateNameView: GenerateNameView) {

    fun checkGeneratedName(generatedName: String) {
        if (!checkIsValidField(generatedName)) {
            AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Invalid generate name field.")
            return
        } else {
            AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Valid generate name field.")
        }

        getKwsUserToken(generatedName)
    }

    private fun checkIsValidField(generatedName: String): Boolean {
        var isFieldsValid = true

        if (generatedName.isEmpty()) {
            generateNameView.setGeneratedNameEmptyError()
            isFieldsValid = false
        }

        return isFieldsValid
    }

    fun generateDisplayName() {
        AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Generate display name.")

        KwsRest.api.generateDisplayName().enqueue(
            generateNameView.createCallBack(
                { response ->
                    AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "Display name was generated.")

                    response.body()?.let { generatedDisplayName ->
                        generatedDisplayName.isNotEmpty().let {
                            AppLogger.logDebug(
                                AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                                "\tGenerated Display Name:\t$generatedDisplayName"
                            )
                            generateNameView.setGeneratedDisplayName(generatedDisplayName)
                        }
                    }
                },
                {
                    AppLogger.logDebug(
                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                        "Display name was not generated."
                    )
                }
            )
        )
    }

    /**
     * Step_4
     * Get KWS token for session.
     */
    private fun getKwsUserToken(generatedName: String) {
        AppLogger.logDebug(AccountInfoViewModel.TAG_SIGN_UP_FLOW, "tGet tmp KWS user token.", 4, LogStatus.EXECUTING)

        generateNameView.showLoading()
        KwsRest.api.getToken(
            SignInLogic.KWS_GET_USER_TOKEN_HEADER,
            UserAccountInfoModel.instance.kwsCreateUserRequestBody?.username!!,
            UserAccountInfoModel.instance.kwsCreateUserRequestBody?.password!!
        )
            .enqueue(generateNameView.createCallBack(
                { response ->
                    AppLogger.logDebug(
                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                        "Get tmp KWS user token.",
                        4,
                        LogStatus.SUCCESS
                    )

                    response.body()?.accessToken?.let { tmpAccessToken ->
                        // This tmp token need for activating user account.
                        AppLogger.logDebug(
                            AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                            "Tmp KWS access token:\t$tmpAccessToken",
                            4
                        )

                        activateUserAccount(tmpAccessToken, generatedName)
                    }
                }, {
                    generateNameView.hideLoading()

                    AppLogger.logDebug(
                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                        "tGet tmp KWS user token.",
                        4,
                        LogStatus.FAILED
                    )
                })
            )
    }

    /**
     * Step_5
     * Activate User
     */
    private fun activateUserAccount(tmpAccessToken: String, generatedName: String) {
        AppLogger.logDebug(
            AccountInfoViewModel.TAG_SIGN_UP_FLOW,
            "\t${UserAccountInfoModel.instance.kwsCreateUserRequestBody}",
            5
        )
        AppLogger.logDebug(
            AccountInfoViewModel.TAG_SIGN_UP_FLOW,
            "tActivate user account.",
            5,
            LogStatus.EXECUTING
        )

        val userId = AppDatabase.getAppDatabase().kwsUserIdDao().get()
        val requestBody = buildRequestBody(generatedName)

        KwsRest.api.activateUserAccount(
            KwsRestUtils.buildBearerHeader(tmpAccessToken),
            userId,
            requestBody
        )
            .enqueue(generateNameView.createCallBack(
                { response ->
                    AppLogger.logDebug(
                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                        "User account was activated.",
                        5,
                        LogStatus.SUCCESS
                    )

                    if (response.code() == 201) {
                        signIn()
                    } else {
                        generateNameView.hideLoading()
                    }
                },
                {
                    generateNameView.hideLoading()

                    AppLogger.logDebug(
                        AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                        "User account was not activated.",
                        5,
                        LogStatus.FAILED
                    )
                }
            ))
    }

    private fun signIn() {
        val signInController = buildSignInController()

        val signInFlow =
            SignInFlow(generateNameView, signInController)
        signInFlow.getKwsUserToken(
            buildSigInUserModel(
                UserAccountInfoModel.instance.kwsCreateUserRequestBody?.username!!,
                UserAccountInfoModel.instance.kwsCreateUserRequestBody?.password!!
            )
        )
    }

    private fun buildSigInUserModel(userName: String, password: String): UserAccountModel {
        return UserAccountModel(userName, password)
    }

    private fun buildSignInController(): SignInStepsHandler {
        val signInController = SignInStepsHandler()

        signInController.onError = {

            generateNameView.hideLoading()
        }
        signInController.onSuccessSignIn = {
            AppLogger.logDebug(
                AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                "Successful Sign Up.",
                6
            )
            AppLogger.logDebug(
                AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                "Move to HomeScreen.",
                7
            )

            generateNameView.hideLoading()
            generateNameView.navigateToHomeScreen()
        }
        signInController.onFailedSignIn = {
            AppLogger.logDebug(
                AccountInfoViewModel.TAG_SIGN_UP_FLOW,
                "Error Sign Up.",
                6
            )

            generateNameView.hideLoading()
        }

        return signInController
    }

    private fun buildRequestBody(generatedName: String): RequestBody {
        val requestBody = buildActivateUserRequestBody(generatedName)
        return RequestBodyConverter.convertToRequestBody(requestBody)
    }

    private fun buildActivateUserRequestBody(generatedName: String): ActivateUserAccountRequestBody {
        val requestBody = ActivateUserAccountRequestBody()

        requestBody.userName = generatedName
        requestBody.appName = TANKEE
        requestBody.parentEmail = UserAccountInfoModel.instance.kwsCreateUserRequestBody?.parentEmail
        requestBody.dateOfBirth = "1996-01-22"
        requestBody.permissions = mutableListOf()
        requestBody.permissions?.add("sendPushNotification")

        return requestBody
    }

    fun generateTestDisplayName() {
        val testNames = mutableListOf<String>()

        testNames.add("Sergey")
        testNames.add("Vasya")
        testNames.add("Sasha")
        testNames.add("Kristina")
        testNames.add("Valera")
        testNames.add("Genadiy")
        testNames.add("Alex")
        testNames.add("Margo")
        testNames.add("Jack")
        testNames.add("Nasta")

        val generatedDisplayName = testNames[Random().nextInt(testNames.size)]
        generateNameView.setGeneratedDisplayName(generatedDisplayName)
    }

    interface GenerateNameView : BaseView {
        fun setGeneratedDisplayName(generatedDisplayName: String)

        fun navigateToHomeScreen()

        fun setGeneratedNameEmptyError()
    }
}