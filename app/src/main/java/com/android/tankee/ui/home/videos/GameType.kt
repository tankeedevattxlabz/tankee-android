package com.android.tankee.ui.home.videos

import com.android.tankee.ui.views.see_all.SectionItemMarker

data class GameType(val name: String, val icon: Int) : SectionItemMarker