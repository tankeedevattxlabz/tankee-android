package com.android.tankee.ui.index_page.builder

import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.index_page.IndexPageContent
import com.android.tankee.ui.index_page.adapter.holders.AvatarCornersType
import com.android.tankee.ui.index_page.adapter.models.*
import com.android.tankee.ui.index_page.tags.TagsBuilder
import com.android.tankee.ui.views.see_all.CheckableItem
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.chopped

class IndexPageBuilder {

    lateinit var indexPageContent: IndexPageContent
    var title: String = ""
    var image: String? = null
    var avatarCornersType = AvatarCornersType.GAME_AVATAR
    var tags = listOf<String>()

    var newItems: MutableList<SectionItemMarker> = mutableListOf()

    private var header: IndexPageHeaderHolderModel? = null
    private var tagsHolderModel: TagsHolderModel? = null
    private var deleteHolderModel: DeleteHolderModel? = null
    private var indexPageItemsModels: MutableList<IndexPageItemsHolderModel>? = mutableListOf()

    private var horizontalItemsCount: Int = 0

    private var holderModels: MutableList<IndexPageHolderModel> = mutableListOf()

    fun buildIndexPageInfo(): List<IndexPageHolderModel> {
        if (holderModels.isEmpty()) {
            header = buildHeader()
            tagsHolderModel = TagsBuilder().buildTags(tags)
            indexPageItemsModels = buildItems(newItems)

            header?.let { holderModels.add(it) }
            tagsHolderModel?.let {
                if (showTags()) {
                    holderModels.add(it)
                }
            }

            if (showDeleteSection())
                deleteHolderModel = DeleteHolderModel(newItems)
            else
                deleteHolderModel = null

            deleteHolderModel?.let { holderModels.add(it) }

            indexPageItemsModels?.let { holderModels.addAll(it) }
        }
        return holderModels
    }

    private fun showTags(): Boolean {
        if (newItems.size <= MIN_ITEMS_TO_SHOW_TAGS) return false
        newItems.isNotEmpty().let {
            if (newItems[0] is Video) {
                return true
            }
        }

        return false
    }
    private fun showDeleteSection(): Boolean {
        if(newItems.isEmpty())
            return false

        newItems.let {
            if (newItems[0] is CheckableItem) {
                return true
            }
        }

        return false
    }
    fun buildNewItems(): List<IndexPageItemsHolderModel>? {
        if (!isFullLastModel()) {
            val lastItemsModel = indexPageItemsModels?.get(indexPageItemsModels!!.size - 1)
            newItems.addAll(0, lastItemsModel?.items!!)
            indexPageItemsModels?.remove(lastItemsModel)
        }

        val newPageModels = buildItems(newItems)
        indexPageItemsModels?.let {
            if (newPageModels != null) {
                it.addAll(newPageModels)
            }
        }
        return newPageModels?.toList()
    }

    private fun isFullLastModel(): Boolean {
        if (horizontalItemsCount == 0) {
            horizontalItemsCount = getHorizontalItemsCount()
        }
        if (indexPageItemsModels != null && indexPageItemsModels!!.isNotEmpty()) {
            return indexPageItemsModels!![indexPageItemsModels!!.size - 1].items.size == horizontalItemsCount
        }
        return true
    }

    private fun buildHeader(): IndexPageHeaderHolderModel? {
        return IndexPageHeaderHolderModel(title, image, avatarCornersType, indexPageContent)
    }

    private fun buildItems(items: List<SectionItemMarker>): MutableList<IndexPageItemsHolderModel>? {
        if (items.isEmpty()) return mutableListOf()

        if (horizontalItemsCount == 0)  {
            horizontalItemsCount = getHorizontalItemsCount()
        }
        val sectionItemMarkerLists = chopped(items, horizontalItemsCount)

        val res = mutableListOf<IndexPageItemsHolderModel>()
        sectionItemMarkerLists.forEach {
            res.add(IndexPageItemsHolderModel(it, horizontalItemsCount))
        }

        return res
    }

    private fun getHorizontalItemsCount(): Int {
        if (newItems.isEmpty()) return 0
        return HorizontalItemsCountUtils().getHorizontalItemsCount(newItems[0])
    }

    fun reset() {
        title = ""
        image = ""
        tags = listOf()
        newItems = mutableListOf()

        header = null
        tagsHolderModel = null
        deleteHolderModel = null
        indexPageItemsModels = null
    }

    fun resetIndexPageItemsModels() {
        indexPageItemsModels = mutableListOf()
        newItems = mutableListOf()
    }

    companion object {
        const val MIN_ITEMS_TO_SHOW_TAGS = 24
    }
}