package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_favs

import com.android.tankee.R
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy
import com.android.tankee.utils.ResUtils

class LoadFavsStrategy : IndexPageLoadStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getFavoriteVideos().enqueue(indexPageView.createCallBack(
            { response ->
                response.body()?.let { body ->
                    indexPageBuilder.title = ResUtils.getString(R.string.favorite_videos)
                    indexPageBuilder.tags = listOf()
                    body.items?.let { items ->
                        indexPageBuilder.newItems = items.toMutableList()
                    }


                    updateMainInfo()
                }
            }, {
                indexPageBuilder.title = ResUtils.getString(R.string.favorite_videos)
                indexPageBuilder.tags = listOf()
                hideLoading()
            })
        )
    }
}