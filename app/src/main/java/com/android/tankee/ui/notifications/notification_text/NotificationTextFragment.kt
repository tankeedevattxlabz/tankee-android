package com.android.tankee.ui.notifications.notification_text


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.event_bus.CloseDialogEvent
import com.android.tankee.event_bus.EventPoster
import kotlinx.android.synthetic.main.fragment_notification_text.*


private const val ARG_NOTIFICATION_TITLE = "notification_title"
private const val ARG_NOTIFICATION_TEXT = "notification_text"

class NotificationTextFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification_text, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        closeNotificationTextButton.setOnClickListener { EventPoster.postEvent(CloseDialogEvent()) }
        notificationTitle.text = getNotificationTitle()
        notificationText.text = getNotificationText()
    }

    private fun getNotificationTitle(): String {
        return if (arguments?.containsKey(ARG_NOTIFICATION_TITLE) == true) {
            arguments?.getString(ARG_NOTIFICATION_TITLE) ?: EMPTY_STRING
        } else {
            EMPTY_STRING
        }
    }

    private fun getNotificationText(): String {
        return if (arguments?.containsKey(ARG_NOTIFICATION_TEXT) == true) {
            arguments?.getString(ARG_NOTIFICATION_TEXT) ?: EMPTY_STRING
        } else {
            EMPTY_STRING
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(title: String, text: String) =
            NotificationTextFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_NOTIFICATION_TITLE, title)
                    putString(ARG_NOTIFICATION_TEXT, text)
                }
            }
    }
}
