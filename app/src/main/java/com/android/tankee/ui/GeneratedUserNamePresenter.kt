package com.android.tankee.ui


import com.android.tankee.cookies.SessionDataHolder


class GeneratedUserNamePresenter(private val generatedUserNameView: GeneratedUserNameView) {

    fun initGeneratedUserName() {
        val userName = SessionDataHolder.user?.userName
        userName?.let {
            if (it.isNotEmpty()) {
                generatedUserNameView.setGeneratedUserName(it)
            }
        }
    }

}

interface GeneratedUserNameView {

    fun setGeneratedUserName(generatedUserNameValue: String)

}