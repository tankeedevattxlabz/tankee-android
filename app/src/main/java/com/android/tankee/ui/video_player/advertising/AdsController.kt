package com.android.tankee.ui.video_player.advertising

import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.logs.AppLogger
import com.android.tankee.rest.BaseResponse
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.responses.ad.VideoPlayerBannerModel
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.rest.responses.video_ads_configurations.VideoAdsConfigurationsModel
import com.android.tankee.rest.responses.video_ads_configurations.VideosAdsConfigFactory
import com.longtailvideo.jwplayer.configuration.PlayerConfig
import com.longtailvideo.jwplayer.media.ads.AdBreak
import com.longtailvideo.jwplayer.media.ads.AdSource
import com.longtailvideo.jwplayer.media.ads.Advertising
import com.longtailvideo.jwplayer.media.ads.ImaAdvertising
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AdsController(private val video: Video?) {

    fun startVideoPlayerWithAdsIfNeeded(playlist: PlayerConfig.Builder) {
        if (isAdsNeededInSession()) {
            cachedConfig?.let { playlist.configurePlayerWithConfig() }
        } else {
            playlist.disableAds()
        }
        markVideoPlayed()
    }

    private fun markVideoPlayed() {
        --leftToShowAd
        if (leftToShowAd <= 0) {
            leftToShowAd = cachedConfig?.frequency ?: DEFAULT_LEFT_TO_SHOW_AD
        }
    }

    private fun isAdsNeededInSession(): Boolean {
        return leftToShowAd == DEFAULT_LEFT_TO_SHOW_AD
    }

    private fun PlayerConfig.Builder.configurePlayerWithConfig(): Advertising {
        // Create your ad schedule
        val adSchedule = buildAdSchedule()
        // Set your ad schedule to your advertising object
        val advertising = ImaAdvertising(adSchedule)
        this.advertising(advertising)
//        advertising.skipOffset = cachedConfig?.skippableSeconds?:5
        return advertising
    }

    private fun buildAdSchedule(): ArrayList<AdBreak> {
        val adSchedule = ArrayList<AdBreak>()

        if (video?.disableAds == false) {

            val adsConfig = VideosAdsConfigFactory(video.duration?.toFloat()?.toInt() ?: 0)
                .buildVideoAds()

            val startedAd = video.startedAt ?: 0
            adsConfig?.let {
                if (startedAd < 1) {
                    val preroll = cachedConfig?.config?.find { it.type == "preroll" }
                    if (preroll?.active == true) {
                        val adBreakPre = AdBreak(
                            "pre",
                            AdSource.IMA,
                            cachedConfig?.url
                        )

                        repeat((adsConfig.prerollBackToBack ?: 0)) {
                            adSchedule.add(adBreakPre)
                        }
                    }
                }
            }


            val midroll = cachedConfig?.config?.find { it.type == "midroll" }
            video.duration?.toFloat()?.div(2) ?: 0F
            try {
            } catch (e: Exception) {
//            Crashlytics.log(e.message)
            }

            /* Check if base midroll ad is active
         And check video configuration for midroll active.

         You can see than parameter video?.disableAds is not correspond to
         midroll ad config. It's because on server at start this flag disabled all ads.
          But now this flag is used only for disable midroll ad,
        */
            var offsetCounter = (adsConfig?.midrollStartAt ?: 30) * 1.0
            val midrollBackToBack = adsConfig?.midrollBackToBack ?: 1
            var numberOfMidrolls = adsConfig?.numberOfMidrolls ?: 0

            if (numberOfMidrolls == 1) {
                if (midroll?.active == true) {
                    val adBreakMid = AdBreak(
                        "${offsetCounter.toInt()}%",
                        AdSource.IMA,
                        cachedConfig?.url
                    )
                    repeat(midrollBackToBack) {
                        adSchedule.add(adBreakMid)
                    }
                }
            } else if (numberOfMidrolls >= 2) {
                val percentBetweenAds = (100.0 - (adsConfig?.midrollStartAt ?: 30) * 2) / (numberOfMidrolls - 1)
                if (midroll?.active == true) {
                    repeat(midrollBackToBack) {
                        val adBreakMid = AdBreak(
                            "${offsetCounter.toInt()}%",
                            AdSource.IMA,
                            cachedConfig?.url
                        )

                        val adBreakMid1 = AdBreak(
                            "${(100 - offsetCounter).toInt()}%",
                            AdSource.IMA,
                            cachedConfig?.url
                        )

                        adSchedule.add(adBreakMid)
                        adSchedule.add(adBreakMid1)
                    }
                    offsetCounter += percentBetweenAds
                    numberOfMidrolls -= 2
                }

                repeat(numberOfMidrolls) {
                    AppLogger.logDebug("VideoAdsPersent", offsetCounter.toString())
                    repeat(midrollBackToBack) {
                        if (midroll?.active == true) {
                            val adBreakMid = AdBreak(
                                "${Math.round(offsetCounter).toInt()}%",
                                AdSource.IMA,
                                cachedConfig?.url
                            )
                            adSchedule.add(adBreakMid)
                        }
                    }
                    offsetCounter += percentBetweenAds
                }
            }


            val postroll = cachedConfig?.config?.find { it.type == "postroll" }
            if (postroll?.active == true) {
                val adBreakPost = AdBreak(
                    "post",
                    AdSource.IMA,
                    cachedConfig?.url
                )
                repeat((adsConfig?.postrollBackToBack ?: 0)) {
                    adSchedule.add(adBreakPost)
                }
            }
        }
        return adSchedule
    }

    private fun PlayerConfig.Builder.disableAds() {
        this.advertising(null)
    }

    companion object {
        private const val DEFAULT_LEFT_TO_SHOW_AD = 1

        var cachedConfig: Config? = null
        private var leftToShowAd: Int = DEFAULT_LEFT_TO_SHOW_AD

        @JvmStatic
        fun retrieveAndSaveAdsConfig() {
            TankeeRest.api.getAdsConfig().enqueue(object : Callback<AdsConfigResponse> {
                override fun onFailure(call: Call<AdsConfigResponse>, t: Throwable) {
//                    Crashlytics.log(t.message)
                }

                override fun onResponse(call: Call<AdsConfigResponse>, response: Response<AdsConfigResponse>) {
                    val config = response.body()?.getConfig()
                    cachedConfig = config
                    leftToShowAd = config?.frequency ?: DEFAULT_LEFT_TO_SHOW_AD
                }
            })
        }

        fun showVideoPlayerBanner(): Boolean {
            return cachedConfig?.config?.find { it.type == VideoPlayerAdType.BannerAndroid.value }?.active
                ?: true
        }

        fun showJwpVideoPlayerAds(): Boolean {
            return cachedConfig?.config?.find { it.type == VideoPlayerAdType.VideoAndroid.value }?.active
                ?: true
        }

        fun containsAds(): Boolean {
            return cachedConfig?.config?.isNotEmpty() ?: false
        }

        fun getVideoPlayerBanner() {
            TankeeRest.api.getVideoPlayerBanner().enqueue(
                object : Callback<BaseResponse<VideoPlayerBannerModel>> {
                    override fun onFailure(call: Call<BaseResponse<VideoPlayerBannerModel>>, t: Throwable) {
//                    Crashlytics.log(t.message)
                    }

                    override fun onResponse(
                        call: Call<BaseResponse<VideoPlayerBannerModel>>,
                        response: Response<BaseResponse<VideoPlayerBannerModel>>
                    ) {
                        response.body()?.items?.let { items ->
                            if (items.isNotEmpty()) {
                                SessionDataHolder.videoPlayerBannerModel = items[0]
                            }
                        }
                    }
                }
            )
        }

        fun getVideosAdsConfigurations() {
            TankeeRest.api.getVideoAdsConfigurations().enqueue(
                object : Callback<BaseResponse<VideoAdsConfigurationsModel>> {
                    override fun onFailure(call: Call<BaseResponse<VideoAdsConfigurationsModel>>, t: Throwable) {
                    }

                    override fun onResponse(
                        call: Call<BaseResponse<VideoAdsConfigurationsModel>>,
                        response: Response<BaseResponse<VideoAdsConfigurationsModel>>
                    ) {
                        response.body()?.items?.let { items ->
                            if (items.isNotEmpty()) {
                                SessionDataHolder.videoAdsConfigurations = items
                            }
                        }
                    }
                }
            )
        }
    }
}


/**
 * There are 3 types of video ads.
 *
 * @property Preroll is for ads before video start to play.
 * @property Midroll is for ads when video is playing.
 * @property Postroll is for ads after video finish to play.
 */
enum class VideoAdType(val value: String) {
    Preroll("preroll"),
    Midroll("midroll"),
    Postroll("postroll")
}