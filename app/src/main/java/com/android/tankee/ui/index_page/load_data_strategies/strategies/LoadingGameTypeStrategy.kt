package com.android.tankee.ui.index_page.load_data_strategies.strategies

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.load_data_strategies.base.LazyLoadingStrategy

class LoadingGameTypeStrategy(private val gameType: String) : LazyLoadingStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getGameType(gameType)
            .enqueue(indexPageView.createCallBack(
                { response ->
                    response.body()?.let { baseResponse ->
                        baseResponse.items?.let { items ->
                            items.isNotEmpty().let {
                                val gamer = items[0]
                                gamer.name?.let { indexPageBuilder.title = it }
                                gamer.tags?.let { indexPageBuilder.tags = it }
                                loadItems()
                            }
                        }
                    }
                }, { hideLoading()
                    indexPageView.onErrorLoading()
                })
            )
    }

    override fun loadMoreItems() {
        TankeeRest.api.getGameTypeVideos(gameType, lazyLoadingModel.page, lazyLoadingModel.tag)
            .enqueue(
                indexPageView.createCallBack(
                    { response ->
                        response.body()?.let { baseResponse ->
                            baseResponse.items?.let { items ->
                                finishLoadingItems()
                                checkIsLazyLoadingFinished(items.size)

                                items.isNotEmpty().let {
                                    indexPageBuilder.newItems = items.toMutableList()
                                    updateLazyLoadingIndexPage()
                                }.run {
                                    hideLoading()
                                }
                            }
                        }

                    }, {
                        hideLoading()
                        finishLoadingItems()
                    })
            )
    }
}