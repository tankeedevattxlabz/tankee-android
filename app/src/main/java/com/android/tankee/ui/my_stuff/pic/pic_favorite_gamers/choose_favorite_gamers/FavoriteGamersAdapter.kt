package com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers.choose_favorite_gamers


import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.extentions.finishClick
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.enableClick
import com.android.tankee.extentions.show
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.OnRecyclerItemClickListener
import com.android.tankee.ui.gif.GifImageViewHelper
import com.android.tankee.ui.request_options.GlideRequestOptions
import com.android.tankee.ui.request_options.gamerRequestOptions
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.ResWrapper
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_fav_gamer.view.*
import org.jetbrains.anko.textColor


class FavoriteGamersAdapter : BaseRecyclerAdapter<
        Gamer,
        OnRecyclerItemClickListener<Gamer>,
        FavoriteGamersAdapter.ChooseFavoriteGamerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseFavoriteGamerViewHolder {
        return ChooseFavoriteGamerViewHolder(inflate(R.layout.item_fav_gamer, parent, false))
    }

    fun setFavoriteOrUnfavorableGame(item: Gamer) {
        for (i in 0 until itemCount) {
            val gamer = items[i]
            if (item.id == gamer.id) {
                gamer.like?.let { gamer.like = !gamer.like!! }
                notifyItemChanged(i)
                break
            }
        }
    }

    class ChooseFavoriteGamerViewHolder(itemView: View) :
            BaseVideoHolder<Gamer, OnRecyclerItemClickListener<Gamer>>(itemView) {

        override fun onBind(item: Gamer, listener: OnRecyclerItemClickListener<Gamer>?) {
            Glide.with(getAppContext())
                    .load(item.getValidImageUrl())
                    .apply(gamerRequestOptions)
                    .into(itemView.favGamerImage)
            Glide.with(getAppContext())
                    .load(R.drawable.blue_mask)
                    .apply(GlideRequestOptions.getCircleOptions())
                    .into(itemView.blueMask)

            itemView.favGamerName.text = item.name

            if (item.like != null && item.like!!) {
                setFavoriteGame()
            } else {
                setUnfavorableGame()
            }

            itemView.setOnClickListener {
                onFavGameClick(item, listener)
            }
        }

        private fun setFavoriteGame() {
            TransitionManager.beginDelayedTransition(itemView.favGamerRoot)
            itemView.heartImage.show()
            itemView.blueMask.show()
            setFavoriteGameNameColor()
        }

        private fun setUnfavorableGame() {
            itemView.heartImage.gone()
            itemView.blueMask.gone()
            setUnfavoredGameNameColor()
        }

        private fun setFavoriteGameNameColor() {
            itemView.favGamerName.textColor = ResUtils.getColor(R.color.colorFavGameNameTextActive)
        }

        private fun setUnfavoredGameNameColor() {
            itemView.favGamerName.textColor = ResUtils.getColor(R.color.colorFavGameNameTextInactive)
        }

        private fun onFavGameClick(item: Gamer, listener: OnRecyclerItemClickListener<Gamer>?) {
            itemView.finishClick()
            // Add delay on isFavorite / unfavorable game click for preventing spanning server.
            itemView.postDelayed(
                    { itemView.enableClick() },
                    ResUtils.getInteger(R.integer.favorite_game_click_delay).toLong()
            )

            if (item.isNotFavoriteGamer()) {
                setFavoriteGameAnimation { listener?.onItemClicked(item) }
                setFavoriteGameNameColor()
                TransitionManager.beginDelayedTransition(itemView.favGamerRoot)
                itemView.blueMask.show()
                setFavoriteHeartScaling()
            } else {
                setUnfavorableHeartScalingUp()
                listener?.onItemClicked(item)
            }
        }

        private fun setFavoriteGameAnimation(onFinishAction: () -> Unit?) {
            GifImageViewHelper(itemView.favoriteAnimation)
                    .setAnimation(R.drawable.favorite_animation)
                    .setOnFinishAction {
                        itemView.favoriteAnimation.gone()
                        onFinishAction.invoke()
                    }
                    .start()

            itemView.favoriteAnimation.show()
        }

        private fun setFavoriteHeartScaling() {
            itemView.heartImage.show()

            itemView.heartImage.scaleX = 0.0f
            itemView.heartImage.scaleY = 0.0f

            itemView.heartImage.animate().scaleX(1f).duration = ResWrapper.getHeartImageScaleDuration()
            itemView.heartImage.animate().scaleY(1f).duration = ResWrapper.getHeartImageScaleDuration()
        }

        private fun setUnfavorableHeartScalingUp() {
            itemView.heartImage.scaleX = 1f
            itemView.heartImage.scaleY = 1f

            itemView.heartImage.animate().scaleX(0f).duration = ResWrapper.getHeartImageScaleDuration()
            itemView.heartImage.animate().scaleY(0f).duration = ResWrapper.getHeartImageScaleDuration()
        }
    }
}