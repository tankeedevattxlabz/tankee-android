package com.android.tankee.ui.views


import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.PasswordTransformationMethod
import android.transition.TransitionManager
import android.util.AttributeSet
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.extentions.show
import com.android.tankee.utils.dp
import com.android.tankee.utils.showKeyboard
import kotlinx.android.synthetic.main.view_tankee_edit_text.view.*
import android.text.InputFilter


class TankeeEditText(context: Context, private val attrs: AttributeSet) : FrameLayout(context, attrs) {

    private var isPasswordVisible: Boolean = false
    private val helperTextHelper: HelperTextHelper = HelperTextHelper()
    var updateRootContainer: (() -> Unit)? = null

    // This id need for edit text view for save instate state for view.
    private var editTextView: Int = 0

    init {
        inflate(context, R.layout.view_tankee_edit_text, this)
        getAttrs()
        initViews()
    }

    private fun initViews() {

        getEditText().setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                setSelectedBackground()
                hideHelperText()
            } else {
                setUnselectedBackground()
            }
        }
        getEditText().addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                // No actions
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No actions
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (helperTextHelper.isHelperTextShow()) {
                    hideHelperText()
                }
            }
        })

        tankeeEdiTextLayout.setOnClickListener {
            getEditText().requestFocus()
            showKeyboard(getEditText())
        }
        showPassword.setOnClickListener { onPasswordClick() }
    }

    private fun onPasswordClick() {
        getEditText().requestFocus()
        isPasswordVisible = !isPasswordVisible

        val cursorPosition = getEditText().selectionEnd
        TransitionManager.beginDelayedTransition(tankeeEdiTextLayout)
        if (isPasswordVisible) {
            getEditText().transformationMethod = null
            showPassword.setImageDrawable(resources.getDrawable(R.drawable.ic_visibility, null))
        } else {
            getEditText().transformationMethod = PasswordTransformationMethod.getInstance()
            showPassword.setImageDrawable(resources.getDrawable(R.drawable.ic_visibility_off, null))
        }
        getEditText().setSelection(cursorPosition)
    }

    @SuppressLint("CustomViewStyleable", "Recycle")
    private fun getAttrs() {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.TankeeEditTextOptions,
            0,
            0
        )

        val icon = typedArray.getResourceId(R.styleable.TankeeEditTextOptions_hintIcon, 0)
        val hintText = typedArray.getResourceId(R.styleable.TankeeEditTextOptions_hintText, 0)
        val inputType = typedArray.getString(R.styleable.TankeeEditTextOptions_tet_inputType)
        val containsSpecialCharacters =
            typedArray.getBoolean(R.styleable.TankeeEditTextOptions_tet_contains_special_characters, false)

        editTextView = typedArray.getResourceId(R.styleable.TankeeEditTextOptions_edId, 0)

        for (i in 0 until tankeeEdiTextLayoutViews.childCount) {
            val v = tankeeEdiTextLayoutViews.getChildAt(i)
            if (v is EditText) {
                v.id = editTextView
            }
        }
        setAttrsValues(icon, hintText, inputType, containsSpecialCharacters)
    }

    private fun setAttrsValues(
        icon: Int,
        hintId: Int?,
        inputType: String?,
        containsSpecialCharacters: Boolean?
    ) {
        if (icon != 0) {
            hintIcon.setImageDrawable(resources.getDrawable(icon, null))
        }

        hintId?.let { getEditText().hint = resources.getString(hintId) }

        if (inputType == "password") {
            showPassword.show()
            getEditText().transformationMethod = PasswordTransformationMethod.getInstance()
        } else if (inputType == "textEmailAddress") {
            getEditText().inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        }

        containsSpecialCharacters?.let {
            if (it) {
                addSpecialCharactersFilter()
            }
        }
    }

    /**
     * Allow characters:
     * 0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz-_
     */
    private fun addSpecialCharactersFilter() {
        val filter = InputFilter { source, start, end, _, _, _ ->
            for (i in start until end) {
                if (!Character.isLetterOrDigit(source[i]) &&
                    Character.toString(source[i]) != "_" &&
                    Character.toString(source[i]) != "-"
                ) {
                    return@InputFilter ""
                }
            }
            null
        }

        getEditText().filters = arrayOf(filter)
    }

    fun setErrorText(errorText: String = EMPTY_STRING) {
        if (errorText.isEmpty()) {
            return
        }

        if (helperTextHelper.isSuccessfulShow()) {
            hideHelperText { setError(errorText) }
        } else {
            setError(errorText)
        }
    }

    private fun setError(errorText: String = EMPTY_STRING) {
        if (errorText.isEmpty()) {
            return
        }

        if (helperTextHelper.isSuccessfulShow()) {
            hideHelperText { setErrorMessage(errorText) }
        } else {
            setErrorMessage(errorText)
        }
    }

    fun setSuccessfulText(successfulText: String = EMPTY_STRING) {
        if (successfulText.isEmpty()) {
            return
        }

        if (helperTextHelper.isErrorShow()) {
            hideHelperText { setSuccessful(successfulText) }
        } else {
            setSuccessful(successfulText)
        }
    }

    private fun setSuccessful(successfulMessage: String) {
        showHelperText(successfulMessage)
        helperTextHelper.showSuccessfulText()
        helperText.setTextColor(resources.getColor(R.color.lightGreen))
    }

    private fun setErrorMessage(errorMessage: String) {
        showHelperText(errorMessage)
        helperTextHelper.showErrorText()
        helperText.setTextColor(resources.getColor(R.color.colorError))
    }

    private fun showHelperText(text: String) {
        helperText.text = text

        postDelayed({
            startRootAnimation()
            setVisibleHelperTextLayoutParams()
        }, 100)
    }

    private fun setVisibleHelperTextLayoutParams() {
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(4.dp(), 0.dp(), 4.dp(), 0.dp())
        params.addRule(RelativeLayout.BELOW, R.id.tankeeEdiTextLayout)
        helperText.layoutParams = params
    }

    fun hideHelperText(onFinishAction: (() -> Unit)? = null) {
        if (helperTextHelper.isHelperTextShow()) {
            helperTextHelper.hideText()

            postDelayed({
                startRootAnimation()
                setHideHelperTextLayoutParamsParams()
                onFinishAction?.invoke()
            }, 100)

        }
    }

    private fun setHideHelperTextLayoutParamsParams() {
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(4.dp(), 0.dp(), 4.dp(), 0.dp())
        params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.tankeeEdiTextLayout)
        helperText.layoutParams = params
    }

    private fun startRootAnimation() {
        updateRootContainer?.invoke()
    }

    private fun setSelectedBackground() {
        tankeeEdiTextLayout.setBackgroundResource(R.drawable.bg_selected)
    }

    private fun setUnselectedBackground() {
        tankeeEdiTextLayout.setBackgroundResource(R.drawable.bg_unselected)
    }

    fun setText(text: String) {
        getEditText().text = Editable.Factory.getInstance().newEditable(text)
    }

    fun getText(): String {
        return getEditText()?.text?.toString()?.trim() ?: EMPTY_STRING
    }

    fun isEmpty(): Boolean {
        return getText().isEmpty()
    }

    fun isNotEmpty(): Boolean {
        return getText().isNotEmpty()
    }

    private fun getEditText() = findViewById<EditText>(editTextView)

    private enum class HelperTextType {
        Successful,
        Error,
        Non;

        fun isSuccessful(): Boolean {
            return this == Successful
        }

        fun isError(): Boolean {
            return this == Error
        }

        fun isNon(): Boolean {
            return this == Non
        }
    }

    private inner class HelperTextHelper {

        var showHelperText: Boolean = false
        var helperTextType: HelperTextType = HelperTextType.Non

        private fun showHelperText() {
            showHelperText = true
        }

        private fun hideHelperText() {
            showHelperText = false
        }

        fun isHelperTextShow(): Boolean {
            return showHelperText && !helperTextType.isNon()
        }

        fun isSuccessfulShow(): Boolean {
            return showHelperText && helperTextType.isSuccessful()
        }

        fun isErrorShow(): Boolean {
            return showHelperText && helperTextType.isError()
        }

        fun showSuccessfulText() {
            showHelperText()
            helperTextType = HelperTextType.Successful
        }

        fun showErrorText() {
            showHelperText()
            helperTextType = HelperTextType.Error
        }

        fun hideText() {
            hideHelperText()
            helperTextType = HelperTextType.Non
        }
    }
}
