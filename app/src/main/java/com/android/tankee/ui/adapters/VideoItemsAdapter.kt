package com.android.tankee.ui.adapters

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.millisToMinutes
import com.android.tankee.utils.setVisibilityInto
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_video.view.*

class VideoItemsAdapter : SectionAdapter() {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSectionViewHolder {
        var layoutId = R.layout.item_video

        if (viewType == 0) {
            layoutId = R.layout.item_see_all
        }

        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

        return when (viewType) {
            1 -> VideoItemsViewHolder(itemView)
            else -> SeeAllViewHolder(itemView)
        }
    }

    inner class VideoItemsViewHolder(itemView: View) : SectionViewHolder(itemView) {
        override fun onBind(item: SectionItemMarker) {
            if (item is Video) {
                Glide.with(getAppContext())
                        .load(item.thumbnailUrl)
                        .apply(requestOptions())
                        .into(itemView.videoThumbnail)
                item.title?.let { itemView.videoTitle.text = item.title }
                Glide.with(getAppContext())
                        .load(item.buildGamerImage())
                        .apply(RequestOptions.circleCropTransform())
                        .into(itemView.gamerImageVideoItem)
                item.influencerName.let { itemView.gamerNameVideoItem.text = it }

                if (item is ClipVideo) {
                    // Hide duration for saved clip item.
                    itemView.nextVideoDuration.gone()
                } else {
                    item.duration.let { itemView.nextVideoDuration.text = millisToMinutes(it!!.toFloat().toInt()) }
                }
                setVisibilityInto(itemView.originalVideoMarker, item.isOriginal())
                setVisibilityInto(itemView.originalVideoBorderMarker, item.isOriginal())
                listener?.let { itemView.setOnClickListener { iv -> it.onClick(item) } }
            }
        }
    }

    private fun requestOptions(): RequestOptions {
        return RequestOptions()
//                .placeholder(R.drawable.gamepad)
//                .override(
//                        getAppContext().resources.getDimension(R.dimen.section_item_with).toInt().px(),
//                        getAppContext().resources.getDimension(R.dimen.section_item_height).toInt().px())
                .encodeFormat(Bitmap.CompressFormat.PNG)
                .encodeQuality(50)
                .skipMemoryCache(false)
    }
}