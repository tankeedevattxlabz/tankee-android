package com.android.tankee.ui.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.getAppContext
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllItem
import kotlinx.android.synthetic.main.item_see_all.view.*

abstract class SectionAdapter : RecyclerView.Adapter<SectionAdapter.BaseSectionViewHolder>() {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    var items: List<SectionItemMarker> = listOf()

    var listener: ItemClickListener? = null

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseSectionViewHolder, position: Int) {
        val item = items[position]
        if (holder is SectionViewHolder) {
            holder.onBind(item)
        } else if (holder is SeeAllViewHolder) {
            holder.onBind(item as SeeAllItem)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position] !is SeeAllItem) {
            1
        } else {
            0
        }
    }

    abstract class BaseSectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    abstract class SectionViewHolder(itemView: View) : BaseSectionViewHolder(itemView) {
        abstract fun onBind(item: SectionItemMarker)
    }

    inner class SeeAllViewHolder(itemView: View) : BaseSectionViewHolder(itemView) {
        fun onBind(item: SeeAllItem) {
            itemView.seeMoreRoot.setBackgroundColor(getAppContext().resources.getColor(item.colorId))
            itemView.seeMoreIcon.setImageResource(item.iconId)
            listener?.let { itemView.setOnClickListener { iv -> it.onClick(item) } }
        }
    }

    interface ItemClickListener {
        fun onClick(item: SectionItemMarker)
    }

}