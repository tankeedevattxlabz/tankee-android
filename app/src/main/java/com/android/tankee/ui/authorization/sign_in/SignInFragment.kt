package com.android.tankee.ui.authorization.sign_in


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.isNotNull
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.forgot_password.ForgotPasswordFragment
import com.android.tankee.ui.authorization.join.account_info.AccountInfoFragment
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.authorization.navigation.Navigator
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.views.TankeeButton
import com.android.tankee.utils.isTable
import kotlinx.android.synthetic.main.fragment_account_info.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.view_header_authorization.*


class SignInFragment : BaseFragment(), SignInViewModel.SignInView {

    private lateinit var sigInViewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sigInViewModel = SignInViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authorizationBack.gone()
        authorizationClose.setOnClickListener { AuthoEventHelper.closeEvent() }

        signInButton.onClickAction = {
            AnalyticsManager.tankeeSignInButtonEvent()
            executeSigIn()
        }

        signInButton.onPreAnimationAction = {
            userName.hideHelperText()
            password.hideHelperText()
        }

        userName.updateRootContainer = {
            startRootAnimation()
        }
        password.updateRootContainer = {
            startRootAnimation()
        }

        forgotPasswordButton.setOnClickListener {
            AnalyticsManager.tankeeSignInForgotPasswordButtonEvent()
            Navigator.instance.replaceFragment(
                ForgotPasswordFragment(),
                ForgotPasswordFragment.TAG
            )
        }

        if (isTable()) {
            (joinHereButton as? TankeeButton)?.onClickAction = {
                AnalyticsManager.tankeeSignInJoinButtonEvent()
                navigateToAccountInfo()
            }
        } else {
            joinHereButton.setOnClickListener {
                AnalyticsManager.tankeeSignInJoinButtonEvent()
                navigateToAccountInfo()
            }
        }
//        sigInViewModel.setTestData()
//        executeSigIn()
    }

    override fun navigateToHomeScreen() {
        AuthoEventHelper.onHomeScreenNavigationEvent()
        AuthoEventHelper.closeEvent()
    }

    override fun setUserNameEmptyError() {
        startRootAnimation()
        userName?.setErrorText(getString(R.string.error_user_name_required))
    }

    override fun setUserNameFirstDigitalError() {
        startRootAnimation()
        userName?.setErrorText(getString(R.string.error_user_first_digital))
    }

    override fun setPasswordEmptyError() {
        startRootAnimation()
        password?.setErrorText(getString(R.string.password_error_required))
    }

    override fun setUserAccountError() {
        startRootAnimation()
        password?.setErrorText(getString(R.string.error_invalid_credentials))
    }

    override fun setTestSignInData(testUserName: String, testPassword: String) {
        userName?.setText(testUserName)
        password?.setText(testPassword)
    }

    private fun executeSigIn() {
        if (userName.isNotNull() && password.isNotNull()) {
            sigInViewModel.sigIn(buildSignInUserModel())
        }
    }

    private fun buildSignInUserModel(): UserAccountModel {
        return UserAccountModel(
            userName.getText(),
            password.getText()
        )
    }

    private fun startRootAnimation() {
        signInRoot?.let { TransitionManager.beginDelayedTransition(it) }
    }

    private fun navigateToAccountInfo() {
        Navigator.instance.replaceStartFragment(
            AccountInfoFragment(),
            AccountInfoFragment.TAG
        )
    }

    companion object {
        var TAG: String = SignInFragment::class.java.simpleName
    }

}
