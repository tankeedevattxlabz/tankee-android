package com.android.tankee.ui.home.videos


import android.os.Bundle
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsParameter
import com.android.tankee.dynamic_links.DynamicLinks
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.index_page.*
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllItem
import com.android.tankee.ui.views.see_all.SeeAllItemVideosSection
import com.android.tankee.ui.views.see_all.SeeAllType
import com.android.tankee.utils.getString
import com.android.tankee.dynamic_links.DynamicLinks.*
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.notifications.model.NotificationType

class IndexPageBundleFactory {

    fun createBundle(sectionItemMarker: SectionItemMarker): Bundle {
        return when (sectionItemMarker) {
            is SeeAllItemVideosSection -> bundleForSeeAllItemsVideoSection(sectionItemMarker)
            is SeeAllItem -> bundleSeeAllSection(sectionItemMarker)
            is Game -> bundleForGame(sectionItemMarker)
            is Gamer -> bundleForGamer(sectionItemMarker)
            is GameType -> bundleForGameType(sectionItemMarker)
            else -> Bundle()
        }
    }

    private fun bundleForSeeAllItemsVideoSection(item: SeeAllItemVideosSection?) = Bundle().apply {
        item?.let {
            val indexPageContent =
                SectionContent(it.sectionId)
            putSerializable(
                IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent
            )
            putString(AnalyticsParameter.SECTION_NAME.paramName, item.seeAllType.name)
        }
    }

    private fun bundleSeeAllSection(item: SeeAllItem) = Bundle().apply {
        val indexPageContent: IndexPageContent?
        when (item.seeAllType) {
            SeeAllType.GAMES -> indexPageContent = GamesContent()
            SeeAllType.GAMERS -> indexPageContent = GamersContent()
            SeeAllType.FAVORITE_VIDEOS -> indexPageContent = FavsContent()
            else -> indexPageContent = ClipsContent()
        }

        putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
        putString(AnalyticsParameter.SECTION_NAME.paramName, item.seeAllType.name)
    }

    private fun bundleForGame(item: Game) = Bundle().apply {
        item.id?.let {
            val indexPageContent = GameContent(it, item.isFavoriteGame())
            putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
            putString(AnalyticsParameter.SECTION_NAME.paramName, item.name)
        }
    }

    private fun bundleForGamer(item: Gamer) = Bundle().apply {
        item.id?.let {
            val indexPageContent = GamerContent(it, item.isFavoriteGamer())
            putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
            putString(AnalyticsParameter.SECTION_NAME.paramName, item.name)
        }
    }

    private fun bundleForGameType(item: GameType) = Bundle().apply {
        item.let {
            val indexPageContent = GameTypeContent(item.name)
            putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
            putString(AnalyticsParameter.SECTION_NAME.paramName, item.name)
        }
    }


    /*******************************************************************************
     *
     * For dynamic Links
     *
     ****************************************************************************/

    fun createBundleDynamicLink(linkPath: String) = Bundle().apply {
        val indexPageContent: IndexPageContent?
        var sectionName: String? = null

        when (linkPath) {
            DynamicLinks.GamesIndexPageAllGames.path -> {
                indexPageContent = GamesContent()
                sectionName = getString(R.string.section_games)
            }
            DynamicLinks.GamersIndexPageAllGamers.path -> {
                indexPageContent = GamersContent()
                sectionName = getString(R.string.section_gamers)
            }
            DynamicLinks.FavoriteVideosPage.path -> {
                indexPageContent = FavsContent()
                sectionName = getString(R.string.favorite_videos)
            }
            else -> indexPageContent = ClipsContent()
        }

        putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
        putString(AnalyticsParameter.SECTION_NAME.paramName, sectionName)
    }

    fun createBundleDynamicLink(linkPath: String, id: String) = Bundle().apply {
        val indexPageContent: IndexPageContent? = when (linkPath) {
            GamesIndexPageIndividualGames.path -> GameContent(id.toInt(), false)
            GamersIndexPageIndividualGamers.path -> GamerContent(id.toInt(), false)
            SectionIndexPageSeeAll.path -> SectionContent(id.toInt())
            GameTypeIndexPage.path -> GameTypeContent(id)
            else -> null
        }

        putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
    }

    fun createBundleNotification(model: NotificationModel) = Bundle().apply {
        if (model.isNotValid()) return@apply
        val indexPageContent: IndexPageContent? = when (model.getNotificationType()) {
            NotificationType.GAME -> GameContent(model.itemId!!.toInt(), false)
            NotificationType.GAMER -> GamerContent(model.itemId!!.toInt(), false)
            else -> null
        }
        putSerializable(IndexPageFragment.INDEX_PAGE_CONTENT, indexPageContent)
    }
}