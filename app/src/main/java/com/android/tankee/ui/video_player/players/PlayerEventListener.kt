package com.android.tankee.ui.video_player.players


interface PlayerEventListener {

    fun onFullScreen(isFullscreen: Boolean)

    fun onPlayerTime(position: Long)

    fun resetState()

    fun onCompleteVideo()

    fun onControlBarVisibilityEvent(isVisible: Boolean)

    fun onAdPlay()

    fun onAdComplete()

    fun onAdError()

    fun onFullscreen(fullscreen: Boolean)

}