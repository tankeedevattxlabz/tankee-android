package com.android.tankee.ui.authorization.forgot_password


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.extentions.isNotNull
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.navigation.Navigator
import com.android.tankee.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import kotlinx.android.synthetic.main.view_header_authorization.*


class ForgotPasswordFragment : BaseFragment(), ForgotPasswordViewModel.ForgotPasswordView {

    private lateinit var forgotPasswordViewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        forgotPasswordViewModel = ForgotPasswordViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authorizationBack.setOnClickListener { Navigator.instance.removeFromBackStack() }
        authorizationClose.setOnClickListener { AuthoEventHelper.closeEvent() }

        userName.updateRootContainer = {
            startRootAnimation()
        }

        resetPasswordButton.onClickAction = { executeForgotPassword() }
    }

    override fun setSuccessfulForgotPasswordText() {
        userName?.setSuccessfulText(
            getString(R.string.helper_text_success_forgot_password)
        )
    }

    override fun setUsernameEmptyError() {
        userName?.setErrorText(
            getString(R.string.error_user_name_required)
        )
    }

    private fun executeForgotPassword() {
        if (userName.isNotNull()) {
            forgotPasswordViewModel.resetPassword(userName.getText().trim())
        }
    }

    private fun startRootAnimation() {
        forgotPasswordRoot?.let { TransitionManager.beginDelayedTransition(it) }
    }

    companion object {
        val TAG: String = ForgotPasswordFragment::class.java.simpleName
    }
}

