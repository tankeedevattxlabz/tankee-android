package com.android.tankee.ui.request_options


import android.graphics.Bitmap
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.notifications.model.NotificationType
import com.android.tankee.utils.getDimen
import com.android.tankee.utils.px
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions


val circleRequestOptions = RequestOptions()
        .placeholder(R.mipmap.ic_launcher_round)
        .circleCrop()
        .encodeFormat(Bitmap.CompressFormat.PNG)
        .encodeQuality(50)
        .error(R.mipmap.ic_launcher_round)
        .skipMemoryCache(false)

val roundRequestOptions = RequestOptions()
        .apply(RequestOptions.bitmapTransform(RoundedCorners(16)))
        .placeholder(R.mipmap.ic_launcher_round)
        .encodeFormat(Bitmap.CompressFormat.PNG)
        .encodeQuality(50)
        .error(R.mipmap.ic_launcher_round)
        .skipMemoryCache(false)

val gameRequestOptions = RequestOptions()
        .placeholder(R.drawable.gamepad)
        .encodeFormat(Bitmap.CompressFormat.PNG)
        .encodeQuality(50)
        .skipMemoryCache(false)

val gamerRequestOptions = RequestOptions()
        .placeholder(R.drawable.user)
        .circleCrop()
        .encodeFormat(Bitmap.CompressFormat.PNG)
        .encodeQuality(50)
        .skipMemoryCache(false)

val avatarsRequestOptions = RequestOptions()
        .encodeFormat(Bitmap.CompressFormat.PNG)
        .encodeQuality(50)
        .override(300, 300)
        .skipMemoryCache(true)

object GlideRequestOptions {

    fun getNotificationRequestOptions(type: NotificationType): RequestOptions {
        return when (type) {
            NotificationType.VIDEO,
            NotificationType.GAME -> roundRequestOptions
            NotificationType.GAMER,
            NotificationType.TEXT -> circleRequestOptions
        }
    }

    fun getCircleOptions(): RequestOptions {
        return RequestOptions().circleCrop()
    }

    fun getSectionBackgroundRequestOptions(): RequestOptions {
        return RequestOptions()
                .encodeFormat(Bitmap.CompressFormat.PNG)
                .encodeQuality(50)
                .skipMemoryCache(false)
    }

}