package com.android.tankee.ui.base.presenter


interface PresenterLifecycler {

    fun onStart()

    fun onDestroy()

}