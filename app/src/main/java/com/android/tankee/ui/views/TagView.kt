package com.android.tankee.ui.views

import android.content.Context
import android.widget.FrameLayout
import com.android.tankee.R
import com.android.tankee.utils.getColor
import kotlinx.android.synthetic.main.item_tag.view.*

class TagView(context: Context) : FrameLayout(context) {

    private var tagNameValue: String = ""
        set(value) {
            field = value
            tagName.text = value
        }

    var selectedTag: Boolean = true
        set(value) {
            field = value
            setTagState(value)
        }

    init {
        inflate(context, R.layout.item_tag, this)
        tagName.text = tagNameValue
    }

    private fun setTagState(selected: Boolean) {
        if (selected) {
            tagLayout.background = context.getDrawable(R.drawable.bg_tag_unselected)
            tagName.setTextColor(getColor(R.color.colorTagSelected))
        } else {
            tagLayout.background = context.getDrawable(R.drawable.bg_tag_unselected)
            tagName.setTextColor(getColor(R.color.colorTagUnselected))
        }
    }
}