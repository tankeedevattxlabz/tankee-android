package com.android.tankee.ui.my_stuff.my_video_content.favorite_videos


import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.my_stuff.my_video_content.base.BaseMyStuffSectionView


/**
 * Presenter for executing isFavorite videos.
 * Now here is only loading isFavorite videos.
 */
class FavoriteVideosPresenter(private val favoriteVideoView: FavoriteVideoView) {

    fun loadFavoriteVideo() {
        AppLogger.logDebug(TAG, "Get isFavorite videos.", logStatus = LogStatus.EXECUTING)

        favoriteVideoView.showLoading()
        TankeeRest.api.getFavoriteVideos().enqueue(
                favoriteVideoView.createCallBack(
                        { response ->
                            favoriteVideoView.hideLoading()
                            response.body()?.items?.let {
                                if (it.isNotEmpty()) {
                                    AppLogger.logDebug(TAG, "Favorite videos were loaded.", logStatus = LogStatus.FAILED)
                                    favoriteVideoView.setVideos(it)
                                } else {
                                    AppLogger.logDebug(
                                            TAG,
                                            "User don't have isFavorite video.\tShowing placeholder",
                                            logStatus = LogStatus.FAILED
                                    )
                                    favoriteVideoView.showNoVideosPlaceHolder()
                                }
                            } ?: run {
                                AppLogger.logDebug(TAG, "Favorite videos list is null.", logStatus = LogStatus.FAILED)
                                favoriteVideoView.showNoVideosPlaceHolder()
                            }
                        },
                        {
                            favoriteVideoView.showNoVideosPlaceHolder()
                            favoriteVideoView.hideLoading()
                            AppLogger.logDebug(TAG, "Get isFavorite videos.", logStatus = LogStatus.FAILED)
                        }
                )
        )
    }

    companion object {
        const val TAG = "FavoriteVideosFlow"
    }
}


interface FavoriteVideoView : BaseMyStuffSectionView