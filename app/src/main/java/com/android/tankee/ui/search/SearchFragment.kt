package com.android.tankee.ui.search

import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.LayoutParams
import androidx.appcompat.widget.SearchView
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createVerticalLayoutManager
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.utils.dp
import com.android.tankee.utils.hideView
import com.android.tankee.utils.showView
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment(), SearchViewModel.SearchView {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var searchVideosAdapter: SearchVideosAdapter
    private lateinit var suggestionsAdapter: SuggestionsAdapter
    private var keyboardHeight: Float = 0.0f

    private var totalItems = 0
    private var currentItems = 0
    private var scrollOutItems = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchViewModel = SearchViewModel(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchClose.setOnClickListener {
            searchView.clearFocus()
            it.findNavController().popBackStack()
        }

        hideToolbar()
        initSearch()
        initLazyLoading()
        initSuggestionRecycler()
        initKeyboardHeight()
        searchViewModel.initSearchedVideos()
    }

    private fun initLazyLoading() {
        searchRecycler?.layoutManager = createVerticalLayoutManager()

        searchVideosAdapter = SearchVideosAdapter()
        searchVideosAdapter.onItemClickListener = { video -> navigateToVideo(video) }
        searchRecycler?.adapter = searchVideosAdapter

        searchRecycler?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager

                totalItems = layoutManager.itemCount
                currentItems = layoutManager.childCount
                scrollOutItems = layoutManager.findFirstVisibleItemPosition()

                if (searchViewModel.isEnableLazyLoading()) {
                    if (totalItems <= (currentItems + scrollOutItems)) {
                        searchViewModel.loadMoreItems()
                    }
                }
            }
        })
    }

    private fun initKeyboardHeight() {
        val mRootWindow = activity?.window
        root.viewTreeObserver.addOnGlobalLayoutListener {
            if (root != null) {
                val screenHeight = root.rootView.height
                val r = Rect()
                val view = mRootWindow?.decorView
                view?.getWindowVisibleDisplayFrame(r)

                keyboardHeight = (screenHeight - r.bottom).toFloat()
                setSuggestionsHeight()
            }
        }
    }

    private fun isKeyboardVisible(): Boolean {
        return keyboardHeight > 0
    }

    private fun setSuggestionsHeight() {
        if (keyboardHeight > 0) {
            val getSuggestionHeight = 77
            val searchLayoutHeight = 16.dp() + getSuggestionHeight * suggestionsAdapter.items.size + searchView.height

            val fl = root.rootView.height - keyboardHeight
            if (searchLayoutHeight > fl) {
                searchLayout.layoutParams.height = keyboardHeight.toInt() - 16.dp()
            } else {
                searchLayout.layoutParams.height = LayoutParams.WRAP_CONTENT
            }
            isSuggestionsHide = false
        } else {
            if (!isSuggestionsHide) {
                setSuggestionsVisibility(false)
                isSuggestionsHide = true
            }
        }
    }

    private fun initSuggestionRecycler() {
        suggestionsRecycler.layoutManager = createVerticalLayoutManager()

        suggestionsAdapter = SuggestionsAdapter()
        suggestionsAdapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<Video> {
            override fun onItemClicked(item: Video) {
//                item.title?.let { searchViewModel.addLocalSuggestion(it, item) }
                isSuggestionsHide = true
                searchView.clearFocus()
                searchView.setQuery(item.title, true)
                item.jwKey?.let { navigateToVideo(item) }
            }
        }
        suggestionsRecycler.adapter = suggestionsAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        searchViewModel.destroy()
        isSuggestionsHide = true
        isOpenVideo = false
    }

    private fun initSearch() {
        searchView.isActivated = true
        searchView.onActionViewExpanded()
        searchView.queryHint = getAppContext().getString(R.string.search)
        searchView.clearFocus()
        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            //            if (hasFocus && searchView.query.isEmpty()) {
                // Hide load suggestions history.
                // searchViewModel.setLocalSuggestions()
//            }

            if (!hasFocus) {
                setSuggestionsVisibility(false)
            }
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    val queryValue = query.trim()
                    if (!searchViewModel.checkIsSearchedQuery(queryValue)) {
                        searchViewModel.updateTitle(query)
                        searchViewModel.loadMoreItems()
                        isSuggestionsHide = false
                    }
                }
                setSuggestionsVisibility(false)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (!isSuggestionsHide && !isOpenVideo) {
                    if (newText.isNotEmpty()) {
                        searchViewModel.getQuerySuggestions(newText)
                    } else {
                        searchViewModel.stopQuayHandler()
                        // Hide load suggestions history.
                        // searchViewModel.setLocalSuggestions()
                        setSuggestionsVisibility(false)
                    }
                }

                isOpenVideo = false

                return false
            }
        })
    }

    override fun setHeaderItem(headerModel: SearchHeaderModel) {
        searchVideosAdapter.addMoreItems(listOf(headerModel))
    }

    override fun setVideos(videos: List<Video>, title: String, totalResult: Int) {
        searchVideosAdapter.addMoreItems(videos, true)
        setShowingInfo(title, true, totalResult)
    }

    override fun addMoreItems(videos: List<Video>, title: String, totalResult: Int) {
        searchVideosAdapter.addMoreItems(videos, searchViewModel.isLoadNewItems())
        setShowingInfo(title, false, totalResult)

        if (searchViewModel.isLoadNewItems()) {
            Handler().postDelayed({
                if (searchRecycler != null) {
                    searchRecycler.scrollToPosition(0)
                }
            }, 100)
        }
    }

    private fun setShowingInfo(title: String, restoreVideos: Boolean = false, totalResult: Int = -1) {
        if (searchVideosAdapter.itemCount > 0) {
            val searchHeaderModel = searchVideosAdapter.items[0]
            if (searchHeaderModel is SearchHeaderModel) {
                searchHeaderModel.title = title
                if (searchViewModel.isLoadNewItems() || restoreVideos) {
                    searchHeaderModel.totalVideosCount = totalResult
                    searchVideosAdapter.notifyItemChanged(0)
                }
            }
        }
    }

    override fun setSuggestions(suggestions: List<Video>, title: String) {
        suggestionsAdapter.query = title
        suggestionsAdapter.clear()
        suggestionsAdapter.items = suggestions.toMutableList()
        suggestionsAdapter.notifyDataSetChanged()

        setSuggestionsHeight()
        Handler().postDelayed({
            setSuggestionsVisibility(suggestions.isNotEmpty())
        }, 50)
    }

    private fun setSuggestionsVisibility(show: Boolean) {
        try {
            when {
                show && isKeyboardVisible() -> {
                    showView(searchDivider)
                    TransitionManager.beginDelayedTransition(searchLayout)
                    showView(suggestionsRecycler)
                }
                else -> {
                    hideView(searchDivider)
                    TransitionManager.beginDelayedTransition(searchLayout)
                    hideView(suggestionsRecycler)
                    try {
                        suggestionsAdapter.clear()
                    } catch (ignore: Exception) {
                    }
                }
            }
        } catch (ignore: Exception) {
        }
    }

    private fun navigateToVideo(video: Video) {
        searchView.clearFocus()
        isSuggestionsHide = true
        isOpenVideo = true
        VideoPlayerController.openVideoPlayer(video, findNavController())
    }

    companion object {
        private var isSuggestionsHide = true
        private var isOpenVideo = false
    }
}
