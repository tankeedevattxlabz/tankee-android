package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_gamers

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.adapter.holders.AvatarCornersType
import com.android.tankee.ui.index_page.load_data_strategies.base.LazyLoadingStrategy
import com.android.tankee.utils.buildUrl

class LoadingGamerVideosStrategy(private val gamerId: Int) : LazyLoadingStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getGamer(gamerId)
            .enqueue(indexPageView.createCallBack({ response ->
                response.body()?.let { baseResponse ->
                    baseResponse.items?.let { items ->
                        items.isNotEmpty().let {
                            val gamer = items[0]
                            var image: String? =
                                if (gamer.imageUrl != null
                                    && gamer.imageUrl!!.url != null
                                    && gamer.imageUrl!!.url!!.isNotEmpty()
                                ) {
                                    gamer.imageUrl!!.url
                                } else {
                                    gamer.url.toString()
                                }
                            image = buildUrl(image)

                            gamer.name?.let { indexPageBuilder.title = it }
                            gamer.imageUrl?.let { indexPageBuilder.image = image }
                            indexPageBuilder.avatarCornersType = AvatarCornersType.GAMER_AVATAR
                            gamer.tags?.let { indexPageBuilder.tags = it }

                            loadItems()
                        }
                    }
                }
            }, {
                hideLoading()
                indexPageView.onErrorLoading()
            }))
    }

    override fun loadMoreItems() {
        TankeeRest.api.getGamerVideos(gamerId, lazyLoadingModel.page, lazyLoadingModel.tag)
            .enqueue(
                indexPageView.createCallBack({ response ->
                    response.body()?.let { baseResponse ->
                        baseResponse.items?.let { items ->
                            finishLoadingItems()
                            checkIsLazyLoadingFinished(items.size)

                            items.isNotEmpty().let {
                                indexPageBuilder.newItems = items.toMutableList()
                                updateLazyLoadingIndexPage()
                            }.run {
                                hideLoading()
                            }
                        }
                    }
                }, {
                    hideLoading()
                    finishLoadingItems()
                })
            )
    }
}
