package com.android.tankee.ui.video_player.ads.banner.super_awesome

class BannerAdHtmlBuilder {

    fun build(jsAdTag: String): String {

        return "<!DOCTYPE html>" +
                "<html>" +
                "<head>" +
                "<title>Super Awesome Ad</title>" +
                "</head>" +
                "<body style=\"width: 320px; height: 50px; margin: 0;background: #05223A;\">" +
                jsAdTag +
                "</body>" +
                "</html>"
    }

    companion object {
        const val SUPER_AWESOME_AD_DIV_ID = "aa_display_ad"
    }

}