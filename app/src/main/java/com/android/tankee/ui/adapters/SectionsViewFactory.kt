package com.android.tankee.ui.adapters
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.view.animation.AnimationUtils
//import android.widget.RelativeLayout
//import androidx.recyclerview.widget.GridLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.android.tankee.R
//import com.android.tankee.getAppContext
//import com.android.tankee.rest.NUM_RECORDS
//import com.android.tankee.rest.responses.influencer.Gamer
//import com.android.tankee.ui.home.videos.GameType
//import com.android.tankee.ui.sections.VideoSectionModel
//import com.android.tankee.ui.sections.JustForYouAndGamesSections
//import com.android.tankee.ui.sections.SectionInterface
//import com.android.tankee.ui.sections.SectionModel
//import com.android.tankee.ui.views.see_all.SectionItemMarker
//import com.android.tankee.ui.views.see_all.SeeAllBuilder
//import com.android.tankee.ui.views.see_all.SeeAllStyle
//import com.android.tankee.ui.views.see_all.SeeAllType
//import com.android.tankee.utils.*
//import com.bumptech.glide.Glide
//import kotlinx.android.synthetic.main.view_move_to_games.view.*
//import kotlinx.android.synthetic.main.view_section.view.*
//import kotlinx.android.synthetic.main.view_top_games_sections.view.*
//
//class SectionsViewFactory {
//
//    var sections: List<SectionInterface> = listOf()
//    var sectionItemClickListener: SectionItemClickListener? = null
//    var scrollToGamesSectionListener: ScrollToGamesSectionListener? = null
//    var isItemDecoratorInit: Boolean = false
//    var isGamesItemDecoratorInit: Boolean = false
//    var isGamersItemDecoratorInit: Boolean = false
//
//
//    fun getItemViewType(sectionMarker: SectionInterface): Int {
//        return if (sectionMarker is JustForYouAndGamesSections) {
//            1
//        } else {
//            0
//        }
//    }
//
//    fun createView(parent: ViewGroup, section: SectionInterface): RecyclerView.ViewHolder? {
//        var layoutId = R.layout.view_section
//        if (getItemViewType(section) == 1) {
//            layoutId = R.layout.view_top_games_sections
//        }
//        val itemView = LayoutInflater.from(parent.context)
//            .inflate(layoutId, parent, false)
//        val holder: RecyclerView.ViewHolder?
//        if (getItemViewType(section) == 0) {
//            holder = SectionViewHolder(itemView)
//            holder.onBind(section)
//        } else {
//            holder = TopViewHolder(itemView)
//            holder.onBind(section)
//        }
//        return holder
//    }
//
//    inner class SectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        fun onBind(section: SectionInterface) {
//            val seeAllBuilder = SeeAllBuilder()
//
//            // Set home screen "Just For You" and "Games" sections.
//
//            itemView.layoutParams = RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                getAppContext().resources.getDimension(R.dimen.home_section_height).toInt()
//            )
//
//            val itemsRecycler = itemView.findViewById<RecyclerView>(R.id.itemsRecycler)
//            if (section is SectionModel) {
//                // Init background from usr or from resources.
//                if (section is VideoSectionModel) {
//                    Glide.with(getAppContext()).load(buildUrl(section.backgroundUrl))
//                        .into(itemView.sectionBackground)
//                } else {
//                    Glide.with(getAppContext()).load(section.background).into(itemView.sectionBackground)
//                }
//                itemView.sectionTitle.text = section.title
//
//                var itemsAdapter: SectionAdapter = VideoItemsAdapter()
//                if (section.getItem() is Gamer) {
//                    itemsAdapter = GamersAdapter()
//
//                } else if (section.getItem() is GameType) {
//                    itemsAdapter = GameTypesAdapter()
//                    if (isNotTable()) {
//                        itemView.layoutParams = RelativeLayout.LayoutParams(
//                            RelativeLayout.LayoutParams.MATCH_PARENT,
//                            getAppContext().resources.getDimension(R.dimen.game_type_section).toInt()
//                        )
//                    }
//                }
//
//                itemsRecycler.layoutManager = createHorizontalLayoutManager()
//                itemsRecycler.adapter = itemsAdapter
//
//                val items = mutableListOf<SectionItemMarker>()
//                items.addAll(section.items)
//
//                // Add see all item.
//                if (section.totalItemCount > NUM_RECORDS) {
//                    val seeAllItem = if (section.items.isNotEmpty()) {
//                        when {
//                            section.getItem() is Gamer -> {
//                                seeAllBuilder
//                                    .setType(SeeAllType.GAMERS)
//                                    .setStyle(SeeAllStyle.ORANGE)
//                                    .build()
//                            }
//                            else -> {
//                                val sectionId = (section as VideoSectionModel).id
//                                seeAllBuilder
//                                    .setType(SeeAllType.GAMERS)
//                                    .setStyle(SeeAllStyle.RED)
//                                    .setSectionId(sectionId)
//                                    .build()
//                            }
//                        }
//                    } else {
//                        seeAllBuilder.build()
//                    }
//                    seeAllItem.let { items.add(it) }
//                }
//
//                itemsAdapter.items = items
//                itemsAdapter.notifyDataSetChanged()
//
//                itemsAdapter.listener = object : SectionAdapter.ItemClickListener {
//                    override fun onClick(item: SectionItemMarker) {
//                        sectionItemClickListener?.let {
//                            it.onClick(item)
//                        }
//                    }
//                }
//            }
//
//        }
//    }
//
//    inner class TopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        fun onBind(section: SectionInterface) {
//            val seeAllBuilder = SeeAllBuilder()
//
//            if (section is JustForYouAndGamesSections) {
//                itemView.layoutParams = RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.MATCH_PARENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT
//                )
//                // Init sections
//                val justForYouSection = section.justForYouSection
//                val gamesSection = section.gamesSection
//
//                // Set backgrounds.
//                Glide.with(getAppContext()).load(justForYouSection.background).into(itemView.justForYouBackground)
//                Glide.with(getAppContext()).load(gamesSection.background).into(itemView.gamesBackground)
//
//                // Set titles
//                itemView.justForYouTitle.text = justForYouSection.title
//                itemView.gamesTitle.text = gamesSection.title
//
//                // Init layout manager for "Just For you" section.
//                // If device is table than use grid for 2 lines videos.
//                itemView.justForYouRecycler.layoutManager = if (isTable()) {
//                    GridLayoutManager(getAppContext(), getInteger(R.integer.just_for_you_section_videos_grid_count))
//                } else {
//                    createHorizontalLayoutManager()
//                }
//
//                val justForYouAdapter: SectionAdapter
//                if (isTable()) {
//
//                    justForYouAdapter = TopVideoItemsAdapter()
//                    justForYouAdapter.items = justForYouSection.items
//                    justForYouAdapter.notifyDataSetChanged()
//                    itemView.justForYouRecycler.adapter = justForYouAdapter
//                } else {
//                    justForYouAdapter = VideoItemsAdapter()
//                    justForYouAdapter.items = justForYouSection.items
//                    justForYouAdapter.notifyDataSetChanged()
//                    itemView.justForYouRecycler.adapter = justForYouAdapter
//                }
//
//                // TODO: refactor code
//                itemView.gamesItemsRecycler.layoutManager = createHorizontalLayoutManager()
//
//                val gamesAdapter = GameItemsAdapter()
//                val gameItems = mutableListOf<SectionItemMarker>()
//                gameItems.addAll(gamesSection.items)
//                val seeAll = seeAllBuilder
//                    .setType(SeeAllType.GAMES)
//                    .setStyle(SeeAllStyle.BLUE)
//                    .build()
//
//                gameItems.add(seeAll)
//                gamesAdapter.items = gameItems
//                gamesAdapter.notifyDataSetChanged()
//                itemView.gamesItemsRecycler.adapter = gamesAdapter
//
//                // Init listeners
//                itemView.scrollToGamesSection.setOnClickListener {
//                    scrollToGamesSectionListener?.scroll(
//                        itemView.gamesSection.y.toInt(),
//                        getAppContext().resources.getDimension(R.dimen.home_section_height).toInt().px()
//                    )
//                    val animation = AnimationUtils.loadAnimation(getAppContext(), R.anim.scroll_to_games_button_anim)
//                    itemView.scrollToGamesSectionButton.startAnimation(animation)
//                }
//                justForYouAdapter.listener = object : SectionAdapter.ItemClickListener {
//                    override fun onClick(item: SectionItemMarker) {
//                        sectionItemClickListener?.onClick(item)
//                    }
//                }
//                gamesAdapter.listener = object : SectionAdapter.ItemClickListener {
//                    override fun onClick(item: SectionItemMarker) {
//                        sectionItemClickListener?.onClick(item)
//                    }
//                }
//            }
//        }
//    }
//
//    interface SectionItemClickListener {
//        fun onClick(item: SectionItemMarker)
//    }
//
//    interface ScrollToGamesSectionListener {
//        fun scroll(positionY: Int, gameSectionHeight: Int)
//    }
//}