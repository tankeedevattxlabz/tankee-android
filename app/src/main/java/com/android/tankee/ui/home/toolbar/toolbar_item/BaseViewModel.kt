package com.android.tankee.ui.home.toolbar.toolbar_item


import androidx.lifecycle.ViewModel
import com.android.tankee.ui.base.BaseView


open class BaseViewModel: ViewModel() {

    lateinit var baseView: BaseView

}