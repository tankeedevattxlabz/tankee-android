package com.android.tankee.ui.video_player.players


import android.os.Bundle
import android.view.View
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.FragmentArgumentsUtils


/**
 * Base fragment for helper players fragment.
 * Execute base configuration like initializePlayer, get video object for future playing video.
 */
abstract class BasePlayerFragment : BaseFragment(),
        BaseView,
        PlayerInterface {

    protected var video: Video? = null
    protected var showControls: Boolean = true
    open var playerEventListener: PlayerEventListener? = null
    var isVisibleControls: Boolean = true

    private var position = 0L
    var playbackPosition: Long
        set(value) {
            position = value
        }
        get() {
            return position
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVideoFromArguments()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlayer()
    }

    protected abstract fun initPlayer()

    fun showControls(show: Boolean) {
        showControls = show
        setControls()
    }
    abstract fun setControls()

    private fun initVideoFromArguments() {
        video = FragmentArgumentsUtils.getVideoFromArgs(arguments)
    }

    abstract fun seekToPosition(position: Int, onSeekListener: (() -> Unit)?)
}