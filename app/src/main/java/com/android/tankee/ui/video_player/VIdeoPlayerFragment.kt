package com.android.tankee.ui.video_player


import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.updateLayoutParams
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.transition.ChangeBounds
import androidx.transition.TransitionInflater
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsEvent
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.analytics.AnalyticsParameter
import com.android.tankee.analytics.EventSender
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.extentions.*
import com.android.tankee.js.ReceiveJavaScriptInterface
import com.android.tankee.logs.AppLogger
import com.android.tankee.rest.responses.ad.BannerAdSource
import com.android.tankee.rest.responses.ad.VideoPlayerBannerModel.Companion.DEFAULT_REFRESH_AD_SECONDS
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.ui.ads.adbutler.AdButlerHelper
import com.android.tankee.ui.ads.kidoz.KidozAdsHelper
import com.android.tankee.ui.authorization.create_account_dialog.CreateTankeeAccountDialog
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.video_player.ads.banner.super_awesome.BannerAdHtmlBuilder
import com.android.tankee.ui.video_player.ads.banner.super_awesome.BannerAdHtmlBuilder.Companion.SUPER_AWESOME_AD_DIV_ID
import com.android.tankee.ui.video_player.advertising.AdsController
import com.android.tankee.ui.video_player.mystaff.FavoritesPresenter
import com.android.tankee.ui.video_player.mystaff.FavoritesView
import com.android.tankee.ui.video_player.players.BasePlayerFragment
import com.android.tankee.ui.video_player.players.PlayerEventListener
import com.android.tankee.ui.video_player.stickers.StickersPresenter
import com.android.tankee.ui.videoplayer.mystaff.SaveClipPresenter
import com.android.tankee.ui.videoplayer.mystaff.SavedClipsView
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.bold
import com.android.tankee.utils.isNetworkAvailable
import com.android.tankee.utils.isNotTable
import com.android.tankee.web.HtmlTags
import com.android.tankee.web.HtmlTagsAttrs
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.favorite_item.*
import kotlinx.android.synthetic.main.fragment_video_player.*
import org.jetbrains.anko.imageResource
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.sql.Time


const val VIDEO_ARG:String= "com.android.tankee.ui.home.VIDEO_ARG"
const val VIDEO_ID_ARG = "com.android.tankee.ui.home.VIDEO_ID_ARG"


/**
 * Now here is hidden next functionality:
 *  1. Saved clips.
 *  2. Toolbar close button.
 *  3. Swipe listener for showing and hiding toolbar.
 */
class VIdeoPlayerFragment : BaseFragment(), VideoPlayerView, PlayerEventListener, FavoritesView, SavedClipsView {

    private var video: Video? = null
    private var videoId: Int? = null

    private var screenOpenedTime: Long = 0
    private var isFullscreen: Boolean = false
    private var toolbarHidden: Boolean = false

    private var playerFragment: BasePlayerFragment? = null

    private lateinit var videoPlayerPresenter: VideoPlayerPresenter
    private var stickersPresenter: StickersPresenter? = null
    private var saveClipPresenter: SaveClipPresenter? = null
    private var favoritesPresenter: FavoritesPresenter? = null

    private var isPaused = false

    private var refreshBannerAdHandler: Handler? = null
    private var refreshBannerAdRunnable: Runnable? = null

    private var currentPosition = 0L
    private var oldPosition = 0L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        initVideoFromArguments()
        videoPlayerPresenter = VideoPlayerPresenter(this)

        if (video != null) {
            if (video !is ClipVideo) {
                video?.id?.let { videoPlayerPresenter.getVideoInfo(it) }
            }
        } else {
            videoId?.let { videoPlayerPresenter.getVideoInfo(it) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var numberOfVideos:Int= TankeePrefs.numberOfVideosWatched
        if(numberOfVideos < 5){
            numberOfVideos += 1
            TankeePrefs.numberOfVideosWatched = numberOfVideos;
            if(numberOfVideos == 5) {
                EventSender.logFiveVideosOpen()
            }
        }

        screenOpenedTime = System.currentTimeMillis()
        hideToolbar()
        view.postDelayed({
            hideToolbar()
        }, 400)

        videoBackButton.setOnClickListener { onVideoPlayerBackButtonClick() }
        // Hide functionality
//        closeToolbarButton?.setOnClickListener { onCloseButtonClick() }
        moreVideosButton.setOnClickListener { onMoreVideosButtonClick() }

        checkIsHideMoreVideosButton()
        // Hide functionality for some time.
//        initSwipeListener()
        video?.let {
            initPlayerFragment()
            initVideoDependency()
        }
    }
    private fun initVideoDependency() {
        setTitle()
        showVideoPlayerToolbar()
        initStickers()
        initBannerAd()
    }

    override fun onStart() {
        super.onStart()
        playerFragment?.onStart()

        if (refreshBannerAdHandler != null) {
            initUpdateBannerAdHandler()
        }
    }

    override fun onResume() {
        super.onResume()
        playerFragment?.onResume()
        if (isPaused && video is ClipVideo) {
            playerFragment?.moveToPosition(currentPosition.toInt())
        }
        isPaused = false

        hideToolbar()
    }

    override fun onPause() {
        super.onPause()
        playerFragment?.onPause()
        isPaused = true
        video?.startedAt = playerFragment?.getPlayerPosition()?.toInt()

        removeCallbackRefreshAds()
    }

    override fun onStop() {
        super.onStop()
        playerFragment?.stopPlayer()
        video?.startedAt = playerFragment?.getPlayerPosition()?.toInt()
    }

    override fun onDestroy() {
        super.onDestroy()
        video = null
        stickersPresenter = null
        stickersPresenter?.destroy()
        stickersPresenter = null
        hideMoreVideos = false

        // Hide Saved Clips functionality
//        saveClipPresenter?.destroy()
        saveClipPresenter = null
        favoritesPresenter = null
    }

    override fun onFullScreen(isFullscreen: Boolean) {
        this.isFullscreen = isFullscreen
        if (isFullscreen) {
            setFullscreen()
        } else {
            setNotFullscreen()
            AnalyticsManager.videoOpenToolbarEvent()
        }
    }

    override fun onPlayerTime(position: Long) {
        currentPosition = position
        video?.let { video ->
            if (video is ClipVideo) {
                val clipEnd = video.clipDuration
                if (position > clipEnd!!.toLong()) {
                    playerFragment?.moveToPosition(video.clipStartPosition!!)
                }
            }
        }
        stickersPresenter?.showStickersForTime(position, isFullscreen || toolbarHidden)
        var tenMinuteTime:Int= TankeePrefs.videoWatchTime
        if (tenMinuteTime < 600) {
            tenMinuteTime++
            oldPosition=currentPosition
            TankeePrefs.videoWatchTime = tenMinuteTime

            if (tenMinuteTime == 600) {
                video?.let {
                    EventSender.logVideoTimeTenMin(video!!)
                }
            }
        }

        // Hide Saved Clips functionality
//        saveClipPresenter?.showSaveClipForTime(position)
    }


    override fun resetState() {
        stickersPresenter?.resetStickerList()
    }

    override fun onCompleteVideo() {
        navigateToMoreVideos(true)
    }

    override fun onControlBarVisibilityEvent(isVisible: Boolean) {
        if (isVisible && !isFullscreen) {
            if (!hideMoreVideos) {
                showMoreVideosButton()
            }

            if (isNotTable()) {
                videoTitle?.show()
                titleMask?.show()
            }

        } else {
            if (isNotTable()) {
                videoTitle?.gone()
                titleMask?.gone()
            }
        }

    }

    override fun onFinishAddingReviewToVideo() {
        activity?.onBackPressed()
    }

    override fun setupVideo(video: Video) {

        if (videoId != null) {
            this.video = video
            initPlayerFragment()
            initVideoDependency()
        }

        if (video is ClipVideo) {
            hideMoreVideos = true
            playerFragment?.showControls(false)
            playerFragment?.moveToPosition(video.clipStartPosition!!)
            closeToolbarButton.gone()
            toolbarToolbarBottom.gone()
        } else {
            favoritesPresenter = FavoritesPresenter(favoriteItem, video, this)
            favoriteLayout.setOnClickListener { favoritesPresenter?.onFavButtonClick() }
            // Hide Saved Clips functionality
            //                saveClipPresenter = SaveClipPresenter(view as ConstraintLayout, video, this)
            // Check if ads contains ads then set disable favorite and saved clips buttons.

        }
    }

    private fun initVideoFromArguments() {
        if (arguments?.containsKey(VIDEO_ARG) != false) {
            video = arguments?.getParcelable(VIDEO_ARG)
            return
        }

        if (arguments?.containsKey(VIDEO_ID_ARG) != false) {
            videoId = arguments?.getInt(VIDEO_ID_ARG)
            return
        }
    }

    override fun onAdPlay() {
        favoriteLayout?.disableClick()
    }

    override fun onAdComplete() {
        favoriteLayout?.enableClick()
    }

    override fun onAdError() {
        favoriteLayout?.enableClick()
    }

    override fun onFullscreen(fullscreen: Boolean) {
        if (!fullscreen && playerFragment.isNotNull() && playerFragment?.isVisibleControls!!) {
            showVideoPlayerToolbar()
        }
    }

    override fun closePlayerScreen() {
        view?.postDelayed({
            findNavController().popBackStack()
        }, 500)
    }

    private fun initPlayerFragment() {
        if (playerFragment == null) {
            playerFragment = video?.let { PlayerFragmentFactory(it, this).createPlayerFragment() }!!
            playerFragment?.arguments = Bundle().apply { putParcelable(VIDEO_ARG, video) }
            playerFragment.let {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.playerFragmentContainer, it!!)
                    .commit()
            }
        }
    }

    private fun onVideoPlayerBackButtonClick() {
        addReviewToVideo()
        findNavController().popBackStack()
    }

    private fun addReviewToVideo() {
        if (video !is ClipVideo) {
            val playerPosition = playerFragment?.getPlayerPosition()?.toInt()
            if (playerPosition != null) {
                if (video?.id != null) {
                    videoPlayerPresenter.addReviewToVideo(
                        video?.id!!,
                        playerPosition.toInt()
                    )
                }
            }
        }
    }

    private fun setTitle() {
        val name = buildVideoGamerName().bold()
        val title = video?.title
        videoTitle?.apply {
            text = name
            append(title)
        }
    }

    private fun buildVideoGamerName(): String {
        val gamerName = video?.influencerName ?: ResUtils.getString(R.string.authour)
        return String.format("${ResUtils.getString(R.string.video_title_format)} ", gamerName)
    }

    private fun checkIsHideMoreVideosButton() {
        if (hideMoreVideos) {
            hideMoreVideos()
        }
    }

    private fun onMoreVideosButtonClick() {
        if (isNetworkAvailable()) {
            ApptentiveManager.openMoreVideos()
            onSaveInstanceState(Bundle())
            navigateToMoreVideos()
        }
    }

    private fun showVideoPlayerToolbar() {
        AnalyticsManager.videoOpenToolbarEvent()

        toolbarHidden = false
        val changeBounds = ChangeBounds()
        changeBounds.duration = SHOW_TOOLBAR_DURATION

        closeToolbarButton?.imageResource = R.drawable.cancel

        val topPadding =
            resources.getDimensionPixelSize(R.dimen.video_hide_bt_bottom_padding) + resources.getDimensionPixelSize(R.dimen.sticker_padding_add)
        val bottomPadding =
            resources.getDimensionPixelSize(R.dimen.video_hide_bt_top_padding) + resources.getDimensionPixelSize(R.dimen.sticker_padding_add)

        closeToolbarButton?.setPadding(0, topPadding, 0, bottomPadding)

        val set = ConstraintSet()
        set.clone(context, R.layout.fragment_video_player)
        set.applyTo(video_fragment_root)
        playerFragment?.getPlayerPosition()?.let { onPlayerTime(it) }
    }

    private fun initStickers() {
        video?.let {
            stickersPresenter = StickersPresenter(view as ConstraintLayout, it, {
                playerFragment?.getPlayerPosition()!!
            }, this)
            stickersPresenter
        }
    }

    private fun setFullscreen() {
        playerFragmentContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
            setMargins(
                0,
                0,
                0,
                0
            )
        }
    }

    private fun setNotFullscreen() {
        try {
            playerFragmentContainer.updateLayoutParams<ConstraintLayout.LayoutParams> {
                setMargins(
                    0,
                    resources.getDimensionPixelOffset(R.dimen.video_player_top_margin),
                    0,
                    resources.getDimensionPixelOffset(R.dimen.video_player_bottom_margin)
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showMoreVideosButton() {
        moreVideosButton?.show()
    }

    private fun hideMoreVideos() {
        moreVideosButton?.gone()
    }

    private fun navigateToMoreVideos(onCompleteVideo: Boolean = false) {
        if (!hideMoreVideos) {
            try {
                video?.startedAt = if (!onCompleteVideo) {
                    video?.isVideoCompleted = false
                    playerFragment?.getPlayerPosition()?.toInt()
                } else {
                    video?.isVideoCompleted = true
                    0
                }
//                playerFragment?.pausePlayer()
                findNavController().navigate(R.id.action_VIdeoPlayerFragment_to_moreVideosFragment, Bundle().apply {
                    putParcelable(VIDEO_ARG, video)
                    putString(AnalyticsParameter.SECTION_NAME.paramName, sectionName)
                })
            } catch (ignore: Exception) {
            }
        }
    }

    override fun onBackPressed(): Boolean {
        super.onBackPressed()
        return if (isFullscreen) {
            playerFragment?.setFullscreen()
            true
        } else {
            val timeOnScreen = System.currentTimeMillis() - screenOpenedTime
            EventSender.logEvent(
                AnalyticsEvent.VIDEO_PLAYER_BACK_BUTTON, androidx.core.os.bundleOf(
                    AnalyticsParameter.USER_NAME.paramName to (SessionDataHolder.user?.fullName
                        ?: "NONAME"),
                    AnalyticsParameter.USER_ID.paramName to (SessionDataHolder.user?.id ?: "NOID"),
                    AnalyticsParameter.DEVICE_ID.paramName to (SessionDataHolder.user?.uid
                        ?: "NODEVICE"),
                    AnalyticsParameter.TIME_ON_SCREEN.paramName to (timeOnScreen / 1000).toString()
                )
            )
            false
        }
    }

    private fun initBannerAd() {
        if (video.isNotNull() && !video?.hideBannerAds!! && AdsController.showVideoPlayerBanner()) {
            MobileAds.initialize(context, getString(R.string.ad_video_player_banner_app_id))
            bannerAdView.gone()
            initUpdateBannerAdHandler()
        } else {
            bannerAdView.gone()
        }
    }

    private fun initUpdateBannerAdHandler() {
        removeCallbackRefreshAds()

        refreshBannerAdHandler = Handler()
        refreshBannerAdRunnable = object : Runnable {
            override fun run() {
                when (SessionDataHolder.videoPlayerBannerModel?.getSource()) {
                    BannerAdSource.AdMob -> loadAdMobAd()
                    BannerAdSource.Kidoz -> loadKidozAds()
                    BannerAdSource.AdButler -> loadAdButlerAds()
                    else -> loadSuperAwesomeBannerAd()
                }
                val refreshAdMillis =
                    (SessionDataHolder.videoPlayerBannerModel?.seconds
                        ?: DEFAULT_REFRESH_AD_SECONDS) * 1000L

                AppLogger.logDebug(TAG, "RefreshBannerAd")
                refreshBannerAdHandler?.postDelayed(this, refreshAdMillis)
            }
        }
        refreshBannerAdHandler?.post(refreshBannerAdRunnable)
    }

    private fun removeCallbackRefreshAds() {
        refreshBannerAdHandler?.removeCallbacks(refreshBannerAdRunnable)
    }

    private fun loadAdMobAd() {
        kidozBannerView?.gone()
        superAwesomeAdWebView?.gone()
        adButlerBannerView?.gone()

        bannerAdView?.show()
        bannerAdView?.loadAd(
            AdRequest.Builder()
                .tagForChildDirectedTreatment(true)
                .build()
        )
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadSuperAwesomeBannerAd() {
        kidozBannerView?.gone()
        bannerAdView?.gone()
        adButlerBannerView?.gone()

        val jsAdTag = SessionDataHolder.videoPlayerBannerModel?.url

        if (jsAdTag.isNull()) return

        superAwesomeAdWebView?.show()
        val superAwesomeAdHtml = jsAdTag?.let {
            BannerAdHtmlBuilder()
                .build(it)
        }

        superAwesomeAdWebView?.settings?.javaScriptEnabled = true
        // Register a new JavaScript interface called HTM_LOUT
        val javaScriptInterface = object : ReceiveJavaScriptInterface() {
            override fun receiveAction(html: String) {
                onLoadBannerAd(html)
            }
        }
        superAwesomeAdWebView?.addJavascriptInterface(javaScriptInterface, ReceiveJavaScriptInterface.HTML_OUT)

        // WebViewClient must be set BEFORE calling loadUrl!
        superAwesomeAdWebView?.webViewClient = getHtmlOut(superAwesomeAdWebView)
        superAwesomeAdWebView?.loadData(superAwesomeAdHtml, "text/html", "UTF-8")
    }

    private fun loadKidozAds() {
        superAwesomeAdWebView?.gone()
        bannerAdView?.gone()
        adButlerBannerView?.gone()

        kidozBannerView.visibility(true)
        if (!SessionDataHolder.kidozInitialized) {
            activity?.let { KidozAdsHelper(it).initializeKidoz() }
            SessionDataHolder.kidozInitialized = true
        }

        activity?.let { KidozAdsHelper(it).showBanner(kidozBannerView) }
    }

    private fun loadAdButlerAds() {
        kidozBannerView?.gone()
        superAwesomeAdWebView?.gone()
        bannerAdView?.gone()
        adButlerBannerView?.show()
        AdButlerHelper().showAdbutlerBanner(context, adButlerBannerView)
    }

    private fun getHtmlOut(webView: WebView): WebViewClient {
        return object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                openBrowser(url)

                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)

                // This call inject JavaScript into the page which just finished loading.
                webView.loadUrl(
                    "javascript:window.HTML_OUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');"
                )
            }
        }
    }

    private fun onLoadBannerAd(html: String) {
        val doc = Jsoup.parse(html, "UTF-8")
        val elements = doc.getElementsByTag(HtmlTags.Div.tag)

        var containsDivWithAd = false
        for (div: Element in elements) {
            val divId = div.attr(HtmlTagsAttrs.Id.attr)
            AppLogger.logDebug(TAG, "${HtmlTags.Div.tag}\t$div")

            if (divId.startsWith(SUPER_AWESOME_AD_DIV_ID)) {
                val hasChildren = div.children().size > 0

                if (hasChildren) {
                    containsDivWithAd = true
                }

                AppLogger.logDebug(TAG, "${HtmlTags.Div.tag} ${HtmlTagsAttrs.Id.attr}\t$divId}")
                AppLogger.logDebug(TAG, "${HtmlTags.Div.tag} has children\t$hasChildren")
            }
        }
        superAwesomeAdWebView?.visibility(containsDivWithAd)
    }

    override fun openCreateAccountDialog() {
        if (TankeePrefs.isNativePlayer) {
            playerFragment?.stopPlayer()
        } else {
            playerFragment?.pausePlayer()
        }
        fragmentManager?.let { CreateTankeeAccountDialog.openCreateTankeeDialog(it) }
    }

    companion object {
        private const val SHOW_TOOLBAR_DURATION = 0L
        private var TAG = VIdeoPlayerFragment::class.java.simpleName
        private var SA_ADS_HTML_PATH = "file:///android_asset/super_awesome_ads.html"

        @JvmStatic
        var sectionName: String? = null
        private var hideMoreVideos: Boolean = false

        fun showVideo(
            video: Video,
            controller: NavController,
            sectionNameValue: String? = null,
            hideMoreVideoBase: Boolean = false
        ) {
            val args = Bundle()
            args.putParcelable(VIDEO_ARG, video)

            EventSender.logOpenVideoEvent(
                video,
                sectionName,
                AnalyticsParameter.PLAYER_SCREEN.paramName
            )
            showVideo(args, controller, sectionNameValue, hideMoreVideoBase)
        }

        fun showVideo(
            videoId: Int,
            controller: NavController,
            hideMoreVideoBase: Boolean = false
        ) {
            val args = Bundle()
            args.putInt(VIDEO_ID_ARG, videoId)

            showVideo(args, controller, hideMoreVideoBase = hideMoreVideoBase)
        }

        private fun showVideo(
            args: Bundle,
            controller: NavController,
            sectionNameValue: String? = null,
            hideMoreVideoBase: Boolean = false
        ) {
            sectionNameValue?.let { sectionName = it }
            val navOptions = NavOptions.Builder().setPopUpTo(R.id.VIdeoPlayerFragment, true)
            controller.navigate(R.id.VIdeoPlayerFragment, args, navOptions.build())

            ApptentiveManager.openVideo()
            hideMoreVideos = hideMoreVideoBase
        }
    }
}
