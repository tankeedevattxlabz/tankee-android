package com.android.tankee.ui.index_page.tags

import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.utils.getColor
import kotlinx.android.synthetic.main.item_tag.view.*

class TagsAdapter(private var selectedTagIndex: Int
) : BaseRecyclerAdapter<TagModel, BaseRecyclerAdapter.OnRecyclerItemClickListener<TagModel>, TagsAdapter.TagsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagsViewHolder {
        return TagsViewHolder(inflate(R.layout.item_tag, parent, false))
    }

    private fun selectTag(index: Int) {
        // Unselected old tag.
        items[selectedTagIndex].selected = false
        notifyItemChanged(selectedTagIndex)

        // Select new tag.
        items[index].selected = true
        notifyItemChanged(index)

        // Update last selected tag.
        selectedTagIndex = index
    }

    inner class TagsViewHolder(itemView: View
    ) : BaseVideoHolder<TagModel, BaseRecyclerAdapter.OnRecyclerItemClickListener<TagModel>>(itemView) {
        override fun onBind(item: TagModel, listener: BaseRecyclerAdapter.OnRecyclerItemClickListener<TagModel>?) {
            itemView.tagName.text = item.tagName
            setTagState(itemView, item.selected)

            listener?.let {
                itemView.tagLayout.setOnClickListener {
                    val oldSelectedTag = items[selectedTagIndex]
                    if (oldSelectedTag.tagName != item.tagName) {
                        val indexItem = items.indexOf(item)
                        selectTag(indexItem)
                        listener.onItemClicked(item)
                    }
                }
            }
        }
    }

    private fun setTagState(itemView: View, selected: Boolean) {
        val background: Drawable?
        val color: Int

        if (selected) {
            background = getAppContext().getDrawable(R.drawable.bg_tag_selected)
            color = getColor(R.color.colorTagSelected)
        } else {
            background = getAppContext().getDrawable(R.drawable.bg_tag_unselected)
            color = getColor(R.color.colorTagUnselected)
        }

        itemView.tagLayout.background = background
        itemView.tagName.setTextColor(color)
    }
}