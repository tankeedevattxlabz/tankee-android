package com.android.tankee.ui.onboarding.splash


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.TankeeApp
import com.android.tankee.system.NetworkChangeReceiver
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.onboarding.OnboardinViewModel
import com.android.tankee.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_splash.*
import kotlinx.android.synthetic.main.item_video.*


class SplashFragment : BaseFragment() {

    private var nextScreenAction: (() -> Unit)? = null
    private var tryCount = 0
    private var stopPosition: Int = 0
    private var videoFinished = false
    private var proceeded = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startVideo()
    }

    override fun onResume() {
        super.onResume()
        startVideo()
    }

    override fun onPause() {
        super.onPause()
        splashVideo.reset()
        videoFinished=false
    }

    private fun decideToLoginOrHome() {
        try {
            OnboardinViewModel(this).checkLogin(
                onUserExist = {
                    moveToHomeScreen()
                },
                onNeedLogin = {
                    OnboardinViewModel(this).loginAndSaveUserCreds {
                        moveToHomeScreen()
                    }

                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun moveToHomeScreen(){
        if(videoFinished){
            proceeded=true
            findNavController().navigate(R.id.action_splashFragment_to_homeScreen)
        }else {
            nextScreenAction = {
                proceeded = true
                findNavController().navigate(R.id.action_splashFragment_to_homeScreen)
            }
        }

    }

    private fun startVideo() {
        decideToLoginOrHome()
        splashVideo.setRawData(R.raw.splash)
        splashVideo.prepare { mp ->
            run {
                mp?.seekTo(stopPosition)
                mp?.start()
            }
        }
        splashVideo.setOnInfoListener { mp, _, _ ->
            stopPosition = mp?.currentPosition!!
            true
        }
        splashVideo.setOnCompletionListener {
            onVideoFinished()
        }
    }

    /**
     * Try to init login info.
     * If tries are less than 3 -> navigate to {@WelcomeFragment} or {@HomeScreen}
     * else stay on splash screen.
     */
    private fun onVideoFinished() {
        videoFinished = true
        if (nextScreenAction != null && !proceeded) {
            nextScreenAction?.invoke()
        }
    }
}
