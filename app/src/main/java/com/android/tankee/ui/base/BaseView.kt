package com.android.tankee.ui.base

import android.app.Activity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import org.jetbrains.anko.toast

interface BaseView : LoadingDialogActions {
    fun showPopUp(p : DialogFragment,tag:String="POPUP")

    fun showDebugToast(msg : String){
        if (this is Fragment){
            this.context?.toast(msg)?.show()
        } else if (this is Activity){
            this.toast(msg).show()
        }
    }
}