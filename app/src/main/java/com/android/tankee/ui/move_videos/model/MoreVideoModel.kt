package com.android.tankee.ui.move_videos.model

class MoreVideoModel {
    var thumbnailUrl: String? = null
    var title: String? = null
    var duration: String? = null
}