package com.android.tankee.ui.video_player.players.jw_player.handlers

import android.app.Activity
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.fullscreen.DefaultFullscreenHandler

class JWPlayerFullscreenHandler(activity: Activity, view: JWPlayerView) : DefaultFullscreenHandler(activity,view) {
    override fun doRotation(fullscreen: Boolean) {

    }
    override fun doRotationListener() {

    }
}