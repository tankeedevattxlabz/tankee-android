package com.android.tankee.ui.index_page.load_data_strategies.strategy_factory

import com.android.tankee.ui.index_page.*
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_gamers.LoadingGamerVideosStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_games.LoadingGameVideosStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_games.LoadGamesContentStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.LoadingGameTypeStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.LoadingSectionVideosStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_clips.LoadClipsStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_favs.LoadFavsStrategy
import com.android.tankee.ui.index_page.load_data_strategies.strategies.load_gamers.LoadGamersStrategy

class LoadingStrategyFactory {

    fun createLoadingStrategy(
        indexPageContent: IndexPageContent,
        indexPageView: IndexPageViewModel.IndexPageView
    ): IndexPageLoadStrategy? {
        val strategy = when (indexPageContent) {
            is GameContent -> LoadingGameVideosStrategy(indexPageContent.gameId)
            is GamesContent -> LoadGamesContentStrategy()
            is GamerContent -> LoadingGamerVideosStrategy(indexPageContent.gamerId)
            is GamersContent -> LoadGamersStrategy()
            is SectionContent -> LoadingSectionVideosStrategy(indexPageContent.sectionId)
            is GameTypeContent -> LoadingGameTypeStrategy(indexPageContent.gameType)
            is ClipsContent -> LoadClipsStrategy()
            is FavsContent -> LoadFavsStrategy()
            else -> null
        }
        strategy?.indexPageView = indexPageView
        strategy?.indexPageBuilder?.indexPageContent = indexPageContent
        return strategy
    }
}