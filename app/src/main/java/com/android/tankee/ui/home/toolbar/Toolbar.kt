package com.android.tankee.ui.home.toolbar


import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.children
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.android.tankee.R
import com.android.tankee.event_bus.UpdateGeneratedUserNameEvent
import com.android.tankee.event_bus.ShowNotificationsMarkerEvent
import com.android.tankee.extentions.show
import com.android.tankee.ui.GeneratedUserNamePresenter
import com.android.tankee.ui.GeneratedUserNameView
import com.android.tankee.ui.home.toolbar.toolbar_item.BaseToolbarItem
import com.android.tankee.utils.getLastIndex
import com.android.tankee.utils.getLastItem
import kotlinx.android.synthetic.main.toolbar_tankee.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class TankeeToolbar : FrameLayout, ToolbarInterface, GeneratedUserNameView {

    var selectionListener: ToolbarSelectionListener? = null
        set(value) {
            field = value
            initListenerForToolbarItems()
        }

    private var lastSelectedDest: Int = -1
    private lateinit var generatedUserNamePresenter: GeneratedUserNamePresenter
    private val toolbarNavigationStack = ToolbarNavigationStack()

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    init {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun setGeneratedUserName(generatedUserNameValue: String) {
        userName.text = generatedUserNameValue
    }

    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.toolbar_tankee, this, true)
        generatedUserNamePresenter = GeneratedUserNamePresenter(this)
        generatedUserNamePresenter.initGeneratedUserName()
    }

    private fun initListenerForToolbarItems() {
        // Get all toolbar item
        // Like videos, search, hamburger menu
        val toolbarItems = items_host?.children
        // Clear local toolbar back stack for new navigation stack
//        selectableItems.clear()
        toolbarNavigationStack.clear()

        toolbarItems?.forEach { it ->
            val item = it as? BaseToolbarItem
            item?.let {
//                selectableItems.add(it)
                toolbarNavigationStack.add(it)
                item.selectionListener = object : ToolbarSelectionListener {
                    override fun onDestinationSelected(selectedDest: Int) {

                        // Disable click action for toolbar items
                        // if any item is selected for delay {ANIMATION_LENGTH}
                        toolbarNavigationStack.stack.forEach { toolBarItem ->
                            toolBarItem.isEnabledItem = false

                            // Enable click action for toolbar items after delay
                            postDelayed({
                                toolBarItem.isEnabledItem = true
                            }, ANIMATION_LENGTH)
                        }
                        setSelectedItem(selectedDest)
                    }
                }
            }
        }
    }

    override fun showToolbar(show: Boolean) {
    }

    override fun setSelectedItem(selectedDest: Int, changeDest: Boolean, onBackClick: Boolean) {

        if (selectedDest == R.id.action_VIdeoPlayerFragment_to_moreVideosFragment || selectedDest == R.id.moreVideosFragment) {
            lastSelectedDest = R.id.VIdeoPlayerFragment
            return
        }

        if (selectedDest == R.id.VIdeoPlayerFragment) {
            lastSelectedDest = selectedDest
            return
        }

        var newDest = selectedDest

        if (newDest == R.id.indexPageFragment && !changeDest) {
            // Gavnokod

            newDest = if (onBackClick) {
                if (lastSelectedDest == R.id.VIdeoPlayerFragment) {
                    lastSelectedDest
                } else {
                    R.id.homeListFragment
                }
            } else {
                lastSelectedDest
            }
        }

        if (isCurrentToolbarItem(newDest)) {
            return
        }
//        "selectedDest: $selectedDest".log()
        lastSelectedDest = newDest
        val res = toolbarNavigationStack.stack.map {
            val item = it as? BaseToolbarItem
            if (item?.destinationId != newDest) {
                item?.setHighlighted(false)
            }
            return@map it
        }.find {
            val item = it as? BaseToolbarItem
            item?.destinationId == newDest
        }

        if (res?.isItemSelected() == false && res.destinationId > 0) {
            if (changeDest) {
                res.destinationId.let { selectionListener?.onDestinationSelected(it) }
            }
            res.setHighlighted(true)
            setBottomHighlight(res.id)
        }
    }

    private fun isCurrentToolbarItem(newDest: Int) = lastSelectedDest == newDest

    private fun setBottomHighlight(@IdRes item: Int) {
        beginTransaction()
        val set = ConstraintSet()
        set.clone(items_host)
        set.connect(R.id.bottom_indicator, ConstraintSet.END, item, ConstraintSet.END)
        set.connect(R.id.bottom_indicator, ConstraintSet.START, item, ConstraintSet.START)
        set.applyTo(items_host)
    }

    private fun beginTransaction() {
        val autoTransition = AutoTransition()
        autoTransition.duration = ANIMATION_LENGTH
        TransitionManager.beginDelayedTransition(this, autoTransition)
    }

    interface ToolbarSelectionListener {
        fun onDestinationSelected(@IdRes selectedDest: Int)
    }

    fun destroy() {
        toolbarNavigationStack.stack.forEach {
            it.selectionListener = null
        }
        toolbarNavigationStack.stack.clear()
        selectionListener = null
    }

    companion object {
        const val ANIMATION_LENGTH = 250L

        var isVisible: Boolean = true

        const val SHOW_HIDE_TOOLBAR_ACTION = "com.android.tankee.ui.home.SHOW_HIDE_TOOLBAR_ACTION"
        const val SHOW_TOOLBAR_ARG = "com.android.tankee.ui.home.SHOW_TOOLBAR_ARG"
        const val UPDATE_TOOLBAR_ARG = "com.android.tankee.ui.home.UPDATE_TOOLBAR_ARG"
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUpdateGeneratedUserName(event: UpdateGeneratedUserNameEvent) {
        generatedUserNamePresenter.initGeneratedUserName()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updateNewNotificationsMarker(event: ShowNotificationsMarkerEvent) {
        if (lastSelectedDest != R.id.notificationsFragment) {
            toolbarNotifications?.showMarker()
        }
    }

}

interface ToolbarInterface {
    // Used on scroll
    fun showToolbar(show: Boolean)

    // Used when back pressed
    fun setSelectedItem(@IdRes selectedDest: Int, changeDest: Boolean = true, onBackClick: Boolean = false)
}


class ToolbarNavigationStack {

    val stack = mutableListOf<BaseToolbarItem>()

    fun add(item: BaseToolbarItem) {
        stack.add(item)
    }

    fun remove() {
        if (getSize() > 0) {
            stack.removeAt(stack.getLastIndex())
        }
    }

    fun remove(item: BaseToolbarItem) {
        if (getSize() > 0) {
            stack.remove(item)
        }
    }

    fun getLastDestination(): BaseToolbarItem? {
        return stack.getLastItem()
    }

    private fun getSize(): Int {
        return stack.size
    }

    fun clear() {
        stack.clear()
    }
}


data class ToolbarNavigationModel(
    val baseToolbarItem: BaseToolbarItem,
    val toolbarNavigationScreenState: ToolbarNavigationScreenState
)

enum class ToolbarNavigationScreenState {
    ActiveCurrent,
    ActivePrevious,
    Inactive
}
