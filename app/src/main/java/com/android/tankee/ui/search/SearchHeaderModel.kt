package com.android.tankee.ui.search

import com.android.tankee.ui.views.see_all.SectionItemMarker

class SearchHeaderModel : SectionItemMarker {
    var totalVideosCount: Int = 0
    var title: String = ""
}