package com.android.tankee.ui.index_page.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.event_bus.ItemCheckedEvent
import com.android.tankee.ui.index_page.adapter.IndexPageAdapter
import com.android.tankee.ui.index_page.adapter.models.DeleteHolderModel
import com.android.tankee.ui.views.see_all.CheckableItem
import com.android.tankee.ui.views.see_all.SectionItemMarker
import kotlinx.android.synthetic.main.item_index_page_delete.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class IndexPageDeleteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val filterSelected: (SectionItemMarker) -> Boolean = { it is CheckableItem && it.getChecked() }
    private var deleteHolderModel: DeleteHolderModel? = null

    init {
        EventBus.getDefault().register(this)
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ItemCheckedEvent) {
        deleteHolderModel?.let {
            val filteredList = deleteHolderModel!!.items.filter(filterSelected)
            if (filteredList.isEmpty()) {
                showSimpleConfirmLayout()
            } else {
                showFillConfirmLayout()
            }
        }
    }

    fun onBind(model: DeleteHolderModel, listener: IndexPageAdapter.DeleteClickListener?) {
        deleteHolderModel = model
        itemView.cancelDeleteTextView.setOnClickListener {
            resetState(listener)
        }
        
        itemView.confirmDeleteLayout.setOnClickListener {
            listener?.let {
                it.onDelete(deleteHolderModel!!.items.filter(filterSelected))
            }
            resetState(listener)
        }
        itemView.startDeleteLayout.setOnClickListener {
                listener?.let {
                    it.onDeleteStateChanged(true)
                    showSimpleConfirmLayout()
                }           
        }

    }

    private fun resetState(listener: IndexPageAdapter.DeleteClickListener?) {
        listener?.let {
            it.onDeleteStateChanged(false)
        }


        itemView.cancelDeleteTextView.visibility = View.GONE
        itemView.confirmDeleteLayoutTextView.visibility = View.GONE
        itemView.startDeleteLayoutTextView.visibility = View.GONE

        itemView.confirmDeleteLayout.visibility = View.GONE
        itemView.startDeleteLayout.visibility = View.VISIBLE

    }
    private fun showSimpleConfirmLayout() {
        itemView.cancelDeleteTextView.visibility = View.VISIBLE
        itemView.startDeleteLayout.visibility = View.VISIBLE
        itemView.startDeleteLayoutTextView.visibility = View.VISIBLE

        itemView.confirmDeleteLayout.visibility = View.GONE
        itemView.confirmDeleteLayoutTextView.visibility = View.GONE

    }
    private fun showFillConfirmLayout() {
        itemView.cancelDeleteTextView.visibility = View.VISIBLE
        itemView.startDeleteLayout.visibility = View.GONE
        itemView.startDeleteLayoutTextView.visibility = View.GONE

        itemView.confirmDeleteLayout.visibility = View.VISIBLE
        itemView.confirmDeleteLayoutTextView.visibility = View.VISIBLE

    }
}