package com.android.tankee.ui.video_player.video_utils

import android.net.Uri

object VideoUrlBuilder {

    fun buildUrlString(jwKey: String?) = "https://content.jwplatform.com/manifests/$jwKey.m3u8"

    fun buildUrl(jwKey: String?): Uri = Uri.parse(buildUrlString(jwKey))

}