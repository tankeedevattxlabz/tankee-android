package com.android.tankee.ui.video_player

import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.video_player.players.BasePlayerFragment
import com.android.tankee.ui.video_player.players.PlayerEventListener
import com.android.tankee.ui.video_player.players.exo_player.ExoPlayerHelperFragment
import com.android.tankee.ui.video_player.players.jw_player.JWPlayerHelperFragment

class PlayerFragmentFactory(
        private val video: Video,
        private val playerEventListener: PlayerEventListener
) {

    fun createPlayerFragment(): BasePlayerFragment {
        val basePlayerFragment = if (TankeePrefs.isNativePlayer) {
            buildExoPlayerHelperFragment()
        } else {
            buildJWPlayerHelperFragment()
        }
        basePlayerFragment.playerEventListener = playerEventListener
        return basePlayerFragment
    }

    private fun buildExoPlayerHelperFragment(): ExoPlayerHelperFragment {
        return ExoPlayerHelperFragment()
                .apply { video to VIDEO_ARG }
    }

    private fun buildJWPlayerHelperFragment(): JWPlayerHelperFragment {
        return JWPlayerHelperFragment()
                .apply { video to VIDEO_ARG }
    }
}