package com.android.tankee.ui.sections

enum class SectionType(val value: Int) {
    TOP(1),
    GAMES(2),
    GAMERS(3),
    ANOTHER(4),
    GAME_TYPES(5)
}