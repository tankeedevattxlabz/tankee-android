package com.android.tankee.ui.index_page.adapter.holders

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.extentions.show
import com.android.tankee.getAppContext
import com.android.tankee.ui.index_page.GameContent
import com.android.tankee.ui.index_page.GamerContent
import com.android.tankee.ui.index_page.adapter.IndexPageAdapter
import com.android.tankee.ui.index_page.adapter.models.IndexPageHeaderHolderModel
import com.android.tankee.ui.request_options.gameRequestOptions
import com.android.tankee.utils.buildThumbnailUrl
import com.android.tankee.utils.isTable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_index_page_header.view.*

class IndexPageHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun onBind(headerModel: IndexPageHeaderHolderModel, backClickListener: IndexPageAdapter.BackClickListener?) {
        backClickListener?.let { itemView.indexPageBack.setOnClickListener { backClickListener.onBackClickListener() } }
        headerModel.image?.let {
            if (isTable()) {
                itemView.indexPageAvatar.visibility = View.VISIBLE

                val requestOptions = getRequestOptionsForAvatar(headerModel.avatarCornersType)
                Glide.with(getAppContext())
                        .load(buildThumbnailUrl(it))
                        .apply(requestOptions)
                        .into(itemView.indexPageAvatar as ImageView)

            }
            setFavoriteButtonForGameOrGamer(headerModel)
        }
        itemView.indexPageTitle.text = headerModel.title
    }

    private fun setFavoriteButtonForGameOrGamer(
            headerModel: IndexPageHeaderHolderModel
    ) {
        if (itemView.favoriteButton != null) {
            when (headerModel.indexPageContent) {
                is GameContent -> {
                    itemView.favoriteButton.show()
                    itemView.favoriteButton.indexPageContent = headerModel.indexPageContent
                    itemView.favoriteButton.setFavorite(headerModel.indexPageContent.isFavorite)
                    itemView.favoriteButton.contentId = headerModel.indexPageContent.gameId
                    itemView.favoriteButton.getInfo()
                }
                is GamerContent -> {
                    itemView.favoriteButton.show()
                    itemView.favoriteButton.indexPageContent = headerModel.indexPageContent
                    itemView.favoriteButton.setFavorite(headerModel.indexPageContent.isFavorite)
                    itemView.favoriteButton.contentId = headerModel.indexPageContent.gamerId
                    itemView.favoriteButton.getInfo()
                }
            }
        }
    }

    private fun getRequestOptionsForAvatar(avatarCornersType: AvatarCornersType): RequestOptions {
        return when (avatarCornersType) {
            AvatarCornersType.GAME_AVATAR -> RequestOptions().apply(gameRequestOptions)
            AvatarCornersType.GAMER_AVATAR -> RequestOptions().apply(gameRequestOptions).optionalCircleCrop()
            AvatarCornersType.NONE -> RequestOptions()
        }
    }
}

enum class AvatarCornersType {
    GAME_AVATAR,
    GAMER_AVATAR,
    NONE
}