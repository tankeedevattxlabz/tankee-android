package com.android.tankee.ui.index_page.adapter.models

import com.android.tankee.ui.views.see_all.SectionItemMarker

data class IndexPageItemsHolderModel(
        val items: List<SectionItemMarker>, val horizontalCount: Int, var isGenerateDecorator: Boolean = false
) : IndexPageHolderModel {

    var isDeleteMode = false

    fun getFirstItem() = if (items.isNotEmpty()) {
        items[0]
    } else {
        null
    }
}