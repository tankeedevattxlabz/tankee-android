package com.android.tankee.ui.authorization.model

data class UserAccountModel(
    val userName: String,
    val password: String
)