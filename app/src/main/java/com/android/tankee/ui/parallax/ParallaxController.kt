package com.android.tankee.ui.parallax


import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.extentions.layoutManagerWidth
import com.android.tankee.utils.getDimen


/**
 * Recycler view are using for getting current scrollX for calculating parallaxView position
 * ParallaxView is view which is moving depend on scrollX position and marginEnd value.
 */
class ParallaxController(
        private val recyclerView: RecyclerView,
        private val parallaxView: View
) {

    var parallaxTransition: ParallaxTransition = ParallaxTransition()
    private val marginEnd = getDimen(R.dimen.parallax_end_margin)
    var itemDecoratorSize: Int = 0
    var recyclerViewItemWith: Int = 0

    init {
        initRecyclerViewParallax()
        initParallaxView()
    }


    private fun initRecyclerViewParallax() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (recyclerViewItemWith == 0) {
                    initViewHolderItemWith(recyclerView)
                }
                updateTransition(dx)
            }
        })
    }

    private fun initViewHolderItemWith(recyclerView: RecyclerView) {
        val firstVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        recyclerViewItemWith = recyclerView.layoutManager?.getChildAt(firstVisiblePosition)?.width ?: 1
    }

    /**
     * Set margin end for parallax effect.
     * When we getUser scrollX, parallaxView will be moving depend on scrollX value.
     */
    private fun setParallaxViewMargin() {
        when (val layoutParams = recyclerView.layoutManager) {
            is ConstraintLayout.LayoutParams -> layoutParams.setMargins(0, 0, (-marginEnd).toInt(), 0)
            is RelativeLayout.LayoutParams -> layoutParams.setMargins(0, 0, (-marginEnd).toInt(), 0)
            is LinearLayout.LayoutParams -> layoutParams.setMargins(0, 0, (-marginEnd).toInt(), 0)
            is FrameLayout.LayoutParams -> layoutParams.setMargins(0, 0, (-marginEnd).toInt(), 0)
        }
    }

    private fun initParallaxView() {
//        setParallaxViewMargin()
//        setScaleType()
    }

    /**
     * If view is Image View need to set up scale type
     * for right image scaling.
     */
    private fun setScaleType() {
        if (parallaxView is ImageView) {
            parallaxView.scaleType = ImageView.ScaleType.FIT_XY
        }
    }

    private fun updateTransition(dx: Int) {
        updateParallaxTransition(dx)
        updateViewTransition()
    }

    private fun updateParallaxTransition(dx: Int) {
        parallaxTransition.translationX -= dx
    }

    fun updateViewTransition() {
        parallaxView.translationX = -getParallaxPosition(recyclerView)
    }

    /**
     * Parallax position calculate:
     * marginEnd / (layoutManagerWith + allSpaceFromItemDecorator) * parallaxTransition.translationX
     */
    private fun getParallaxPosition(recyclerView: RecyclerView): Float {
        val totalItemDecorationValue = ((recyclerView.layoutManager?.itemCount?.plus(1))
                ?: 0).times(itemDecoratorSize)

        return Math.floor(
            (marginEnd / (recyclerViewItemWith * recyclerView.adapter?.itemCount!! + totalItemDecorationValue) *
                    parallaxTransition.translationX).toDouble()
        ).toFloat()
    }

    fun setParallaxViewTranslation(parallaxTransition: ParallaxTransition?) {
        parallaxTransition?.let { this.parallaxTransition = parallaxTransition }
    }

}


data class ParallaxTransition(
        var translationX: Float = 0f
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readFloat())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(translationX)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ParallaxTransition> {
        override fun createFromParcel(parcel: Parcel): ParallaxTransition {
            return ParallaxTransition(parcel)
        }

        override fun newArray(size: Int): Array<ParallaxTransition?> {
            return arrayOfNulls(size)
        }
    }
}
