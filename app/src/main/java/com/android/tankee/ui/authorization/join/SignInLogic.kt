package com.android.tankee.ui.authorization.join

import android.util.Log
import com.android.tankee.database.AppDatabase
import com.android.tankee.database.kws.models.KwsUserIdModel
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.ui.authorization.sign_in.SignInViewModel
import com.android.tankee.ui.base.BaseView

object SignInLogic {

    const val KWS_GET_USER_TOKEN_HEADER = "Basic dGFua2VlOmo4TWpSMWhZR1NrOVJ3WjVJQ2FxMjNYcWdCeU9iSFFs"


}

data class SignInDataModel(
    val userName: String,
    val password: String
)