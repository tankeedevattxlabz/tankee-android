package com.android.tankee.ui.index_page

data class LazyLoadingModel(
        var page: Int = DEFAULT_START_PAGE,
        var tag: String = "",
        var isFinished: Boolean = false
) {
    fun reset() {
        page = DEFAULT_START_PAGE
        tag = ""
        isFinished = false
    }

    companion object {
        const val DEFAULT_START_PAGE = 1
        const val DEFAULT_LAZY_LOADING_ITEMS = 36
    }
}