package com.android.tankee.ui.authorization.join.account_info


import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.rest.kws.responces.create_user.KwsCreateUserRequestBody
import com.android.tankee.ui.authorization.join.UserAccountInfoModel
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.base.BaseView
import java.util.*
import java.util.regex.Pattern


class AccountInfoViewModel(private val accountInfoView: AccountInfoView) {

    fun checkAccountInfo(accountInfoDataModel: UserAccountModel) {
        if (!checkIsValidField(accountInfoDataModel)) {
            AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Invalid account info fields.")
            return
        } else {
            AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Valid account info fields.")
        }

        checkUserName(accountInfoDataModel)
    }

    private fun checkIsValidField(userAccountModel: UserAccountModel): Boolean {

        var isFieldsValid: Boolean

        isFieldsValid = when {
            userAccountModel.userName.isEmpty() -> {
                accountInfoView.setUserNameEmptyError()
                false
            }
            userAccountModel.userName.first().isDigit() -> {
                accountInfoView.setUserNameFirstDigitalError()
                false
            }
            !isUserNameValid(userAccountModel.userName) -> {
                accountInfoView.setUserNameContainsInvalidCharactersError()
                false
            }
            else -> true
        }

        if (userAccountModel.password.isEmpty()) {
            accountInfoView.setPasswordEmptyError()
            isFieldsValid = false
        } else if (userAccountModel.password.length < MIN_PASSWORD_LENGTH) {
            accountInfoView.seaPasswordLengthError()
            isFieldsValid = false
        }

        return isFieldsValid
    }

    private fun isUserNameValid(userName: String): Boolean {
        val pattern = Pattern.compile(USER_NAME_PATTERN)
        val mather = pattern.matcher(userName)
        return mather.matches()
    }

    /**
     * Step_2
     * Check username field on KWS server.
     */
    private fun checkUserName(userAccountModel: UserAccountModel) {
        AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Check username field on KWS server.", 1, LogStatus.EXECUTING)

        accountInfoView.showLoading()
        KwsRest.api.checkUserName(userAccountModel.userName)
            .enqueue(accountInfoView.createCallBack(
                { response ->
                    AppLogger.logDebug(
                        TAG_SIGN_UP_FLOW,
                        "Check username field on KWS server.",
                        1,
                        LogStatus.SUCCESS
                    )

                    response.body()?.let { checkUserNameResponse ->
                        val userNameAvailable = checkUserNameResponse.available!!
                        AppLogger.logDebug(TAG_SIGN_UP_FLOW, "User name available:\t$userNameAvailable", 1)

                        accountInfoView.hideLoading()
                        if (userNameAvailable) {
                            AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Navigate to Parent Screen", 1)

                            saveAccountInfoData(userAccountModel)
                            accountInfoView.navigateToParentEmail()
                        } else {
                            accountInfoView.setInvalidCredentialsError()
                        }
                    }
                }, {
                    accountInfoView.hideLoading()

                    AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Check username field on KWS server.", 1, LogStatus.FAILED)
                })
            )
    }

    private fun saveAccountInfoData(userAccountModel: UserAccountModel) {
        AppLogger.logDebug(TAG_SIGN_UP_FLOW, "Save AccountInfoData", 2)

        UserAccountInfoModel.instance.kwsCreateUserRequestBody = KwsCreateUserRequestBody()
        UserAccountInfoModel.instance.kwsCreateUserRequestBody?.username = userAccountModel.userName
        UserAccountInfoModel.instance.kwsCreateUserRequestBody?.password = userAccountModel.password
        UserAccountInfoModel.instance.kwsCreateUserRequestBody?.dateOfBirth = DEFAULT_DATE
    }

    fun setTestData() {
        accountInfoView.setTestData(
            "testSergey${Random().nextInt(10000)}",
            "12345678"
        )
    }

    companion object {
        const val TAG_SIGN_UP_FLOW = "SignUpFlow"

        const val MIN_PASSWORD_LENGTH = 8
        const val DEFAULT_DATE = "2000-01-01"
        const val USER_NAME_PATTERN = "^[A-z][\\w-]{2,}\$"
    }

    interface AccountInfoView : BaseView {
        fun navigateToParentEmail()

        fun setUserNameEmptyError()

        fun setUserNameFirstDigitalError()

        fun setUserNameContainsInvalidCharactersError()

        fun setUserNameInvalidError()

        fun setPasswordEmptyError()

        fun seaPasswordLengthError()

        fun setInvalidCredentialsError()

        fun setTestData(createUserNameValue: String, createPasswordValue: String)
    }
}
