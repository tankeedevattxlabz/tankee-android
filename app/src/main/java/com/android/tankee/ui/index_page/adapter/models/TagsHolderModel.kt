package com.android.tankee.ui.index_page.adapter.models

import com.android.tankee.ui.index_page.tags.TagModel

data class TagsHolderModel(val tags: List<TagModel>, var selected: Int = 0) : IndexPageHolderModel