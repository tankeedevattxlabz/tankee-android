package com.android.tankee.ui.index_page.load_data_strategies.base

import com.android.tankee.ui.index_page.LazyLoadingModel

abstract class LazyLoadingStrategy : IndexPageLoadStrategy() {

    val lazyLoadingModel = LazyLoadingModel()
    private var isPageInitialized = false
    private var isLoadingItems = false

    protected abstract fun loadMoreItems()

    fun updateLazyLoadingIndexPage() {
        if (!isPageInitialized) {
            isPageInitialized = true
            updateMainInfo()
        } else {
            addMoreItems()
        }
        hideLoading()
    }

    fun checkIsLazyLoadingFinished(itemsSize: Int) {
        if (itemsSize < LazyLoadingModel.DEFAULT_LAZY_LOADING_ITEMS) {
            lazyLoadingModel.isFinished = true
        } else {
            lazyLoadingModel.page++
        }
    }

    private fun startLoadingItems() {
        isLoadingItems = true
    }

    protected fun finishLoadingItems() {
        isLoadingItems = false
        disableLoading()
    }

    fun loadItems() {
        // Not load more items if lazy loading wa finished
        if (lazyLoadingModel.isFinished) return
        // Not load items more if now item is loading
        if (isLoadingItems) return

        startLoadingItems()
        loadMoreItems()
    }

    private fun addMoreItems() {
        indexPageView.addMoreItems(indexPageBuilder.buildNewItems()!!)
    }

    fun selectNewTag(tagName: String) {
        if (!isLoadingItems) {
            checkNewTagLazyLoading(tagName)
            loadMoreItems()
        }
    }

    private fun checkNewTagLazyLoading(tag: String) {
        lazyLoadingModel.reset()
        lazyLoadingModel.tag = tag
        indexPageBuilder.resetIndexPageItemsModels()
    }
}