package com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers.choose_favorite_gamers


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import com.android.tankee.ui.my_stuff.pic.base.PickBaseFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.MarginItemDecoration
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import com.android.tankee.ui.parallax.ParallaxController
import kotlinx.android.synthetic.main.fragment_choose_favorite_items.*


class ChooseFavoriteGamersFragment : PickBaseFragment(), ChooseFavoriteGamersView {

    private lateinit var chooseFavoriteGamersPresenter: ChooseFavoriteGamersPresenter
    private var parallaxController: ParallaxController? = null
    private var favoriteGamersAdapter: FavoriteGamersAdapter? = null
    var saveFavoriteUnfavorableGames: (() -> Unit)? = null

    override fun onSaveInstanceState(outState: Bundle) {
        parallaxController?.parallaxTransition?.translationX?.let { outState.putFloat("transitionX", it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        chooseFavoriteGamersPresenter = ChooseFavoriteGamersPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_choose_favorite_gamers_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        saveFavoriteUnfavorableGames = {
            favoriteGamersAdapter?.items?.let { chooseFavoriteGamersPresenter.updateGames() }
        }
        chooseFavoriteGamersPresenter.getGamers()

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("transitionX")) {
                parallaxController?.parallaxTransition?.translationX = savedInstanceState.getFloat("transitionX")
            }
        }
    }

    override fun setGamers(gamers: List<Gamer>) {
        favoriteGamesRecycler.layoutManager = createHorizontalLayoutManager()
        favoriteGamesRecycler.adapter = getFavoriteGamesAdapter(gamers)

        setUpParallaxWithRecyclerView()
        setMarginItemDecorator()
    }

    private fun getFavoriteGamesAdapter(gamers: List<Gamer>): FavoriteGamersAdapter? {
        favoriteGamersAdapter = FavoriteGamersAdapter()
        favoriteGamersAdapter?.items = gamers.toMutableList()
        favoriteGamersAdapter?.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<Gamer> {
            override fun onItemClicked(item: Gamer) {
                chooseFavoriteGamersPresenter.addGamerToStore(item)
                favoriteGamersAdapter?.setFavoriteOrUnfavorableGame(item)
            }
        }
        return favoriteGamersAdapter
    }

    private fun setUpParallaxWithRecyclerView() {
        parallaxController = ParallaxController(favoriteGamesRecycler, favGamesParallaxBackground)
    }

    private fun setMarginItemDecorator() {
        val marginItemDecoration = MarginItemDecoration(resources.getDimension(R.dimen.pic_avatar_list_margin).toInt())
        favoriteGamesRecycler.addItemDecoration(marginItemDecoration)
        parallaxController?.itemDecoratorSize = resources.getDimension(R.dimen.pic_avatar_list_margin).toInt()
    }

    override fun onGamersUpdatesNavigation() {
        arguments?.let {
            when (it.getString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY)) {
                PicAvatarNavigation.MySettingsFlow.value -> navigateToMySettingsScreen()
                PicAvatarNavigation.RegistrationFlow.value -> navigateToHomeScreen()
            }
        }
    }

    private fun navigateToHomeScreen() {
        val navOption = NavOptions.Builder().setPopUpTo(R.id.homeListFragment, true)
        findNavController().navigate(R.id.homeListFragment, null, navOption.build())
    }

    companion object {
        var TAG: String = ChooseFavoriteGamersFragment::class.java.simpleName
    }
}
