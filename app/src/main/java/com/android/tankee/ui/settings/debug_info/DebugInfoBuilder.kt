package com.android.tankee.ui.settings.debug_info


import com.android.tankee.cookies.SessionDataHolder

/**
 * Created by Sergey Kulyk on 2019-09-11.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
class DebugInfoBuilder {

    private val debugInfoList = mutableListOf<DebugInfoModel>()
    private val userModel = SessionDataHolder.user

    fun build(): MutableList<DebugInfoModel> {
        debugInfoList.add(DebugInfoModel("id", userModel?.id))
        debugInfoList.add(DebugInfoModel("uid", userModel?.uid))
        debugInfoList.add(DebugInfoModel("user_name", userModel?.userName))
        debugInfoList.add(DebugInfoModel("avatar", userModel?.avatar))
        debugInfoList.add(DebugInfoModel("token", userModel?.token))
        debugInfoList.add(DebugInfoModel("timezone", userModel?.timezone))
        debugInfoList.add(DebugInfoModel("fcm_token", "${userModel?.fcmToken}"))
        debugInfoList.add(DebugInfoModel("is_anonymous", "${userModel?.isAnonymous}"))

        return debugInfoList
    }
}