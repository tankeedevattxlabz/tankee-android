package com.android.tankee.ui.video_player.stickers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.extentions.isNotNull
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.item_sticker_layout.view.*
import org.jetbrains.anko.imageResource

class StickerAdapter(val click: (Sticker, View) -> Unit) : RecyclerView.Adapter<StickerAdapter.StickerViewHolder>() {
    private val stickers = listOf(
            Sticker(0, "sticker0"),
            Sticker(0, "sticker1"),
            Sticker(0, "sticker2"),
            Sticker(0, "sticker3"),
            Sticker(0, "sticker4"),
            Sticker(0, "sticker5"),
            Sticker(0, "sticker6")
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StickerViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sticker_layout, parent, false)
        return StickerViewHolder(itemView)
    }

    override fun getItemCount(): Int = stickers.size

    override fun onBindViewHolder(holder: StickerViewHolder, position: Int) {
        holder.onBind(stickers[position])
    }

    inner class StickerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(sticker: Sticker) {
            if (itemView.isNotNull()) {
                val name = sticker.name
                itemView.imageview.imageResource = Sticker.stickerNameToResId(name)
                itemView.setOnClickListener {
                    click.invoke(stickers[position], itemView.imageview)
                }
            }
        }
    }

    class Sticker(
            @SerializedName("time") var time: Long,
            @SerializedName("name") var name: String,
            @SerializedName("user") var user: Map<String, Any>? = null
    ) {

        override fun toString(): String {
            return "Sticker(time=$time, name='$name', user=$user)"
        }

        companion object {
            fun stickerNameToResId(name: String): Int {
                return when (name) {
                    "like" -> R.drawable.sticker_like
                    "sticker0" -> R.drawable.sticker_like
                    "heart" -> R.drawable.sticker_heart
                    "sticker1" -> R.drawable.sticker_heart
                    "lol" -> R.drawable.sticker_lol
                    "sticker2" -> R.drawable.sticker_lol
                    "saywhat" -> R.drawable.sticker_saywhat
                    "sticker3" -> R.drawable.sticker_saywhat
                    "smile" -> R.drawable.sticker_smile
                    "sticker4" -> R.drawable.sticker_smile
                    "sad" -> R.drawable.sticker_sad
                    "sticker5" -> R.drawable.sticker_sad
                    else -> R.drawable.sticcker_angry
                }
            }
        }
    }
}