package com.android.tankee.ui.notifications


import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.notifications.model.BaseNotificationModel
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.notifications.model.NotificationPlaceHolderModel
import com.android.tankee.repo.notificatios.ViewNotificationRepo
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.base.BaseView


class NotificationsPresenter(private val notificationsView: NotificationsView) {

    private val notifications = mutableListOf<BaseNotificationModel>()

    fun getNotifications() {
        if (notifications.isEmpty() || SessionDataHolder.notificationModel != null) {
            SessionDataHolder.notificationModel = null
            notifications.clear()
            loadNotifications()
        } else {
            notificationsView.setNotifications(notifications)
        }
    }

    fun addNewNotification(notificationModel: NotificationModel) {
        notifications.add(0, notificationModel)
        val viewNotificationRepo = ViewNotificationRepo(notificationsView)
        notificationModel.id?.let { viewNotificationRepo.viewNotification(it) }
    }

    private fun loadNotifications() {
        val placeHolderModel = buildPlaceHolderModel()

        notificationsView.showLoading()
        TankeeRest.api.getNotifications().enqueue(
                notificationsView.createCallBack(
                        { response ->
                            notificationsView.hideLoading()

                            val notificationBaseModels = mutableListOf<BaseNotificationModel>()
                            val notificationModels = response.body()?.items ?: listOf()

                            notificationBaseModels.addAll(notificationModels)

                            notificationBaseModels.map { model: BaseNotificationModel ->
                                if (model is NotificationModel) {
                                    model.bold?.trim()
                                    model.text?.trim()
                                }
                            }

                            notificationBaseModels.add(placeHolderModel)
                            notifications.addAll(notificationBaseModels)
                            notificationsView.setNotifications(notificationBaseModels)

                            updateViewedForUnreadNotifications(notificationModels)
                        },
                        {
                            notificationsView.hideLoading()
                            notifications.add(placeHolderModel)
                            notificationsView.setNotifications(mutableListOf(placeHolderModel))
                        }
                )
        )
    }

    private fun buildPlaceHolderModel(): BaseNotificationModel {
        return NotificationPlaceHolderModel(R.drawable.all_caught_up)
    }

    private fun updateViewedForUnreadNotifications(list: List<NotificationModel>) {
        if (list.isNotEmpty()) {
            val unreadNotifications = getUnreadNotifications(list)

            if (unreadNotifications.isNotEmpty()) {
                viewNotifications(unreadNotifications)
            }
        }
    }

    private fun getUnreadNotifications(list: List<NotificationModel>): List<NotificationModel> {
        return list.filter { model -> model.isViewed == false }
    }

    private fun viewNotifications(list: List<NotificationModel>) {
        val viewNotificationRepo = ViewNotificationRepo(notificationsView)
        viewNotificationRepo.viewNotifications(list)
    }

    companion object {
        var TAG: String = NotificationsPresenter::class.java.simpleName
    }

}


interface NotificationsView : BaseView {

    fun setNotifications(notifications: MutableList<BaseNotificationModel>)

}