package com.android.tankee.ui.my_stuff.pic.pic_avatar

data class AvatarModel(
        val id: String,
        val image: Int,
        var isSelected: Boolean = false
) {
    fun select() {
        isSelected = true
    }

    fun unselected() {
        isSelected = false
    }
}