package com.android.tankee.ui.my_stuff.pic.pic_favorites_games


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.android.tankee.R
import com.android.tankee.event_bus.OnFavoriteGamesSelectedAction
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.ui.my_stuff.pic.base.PickBaseFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.AvatarModel
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import com.android.tankee.ui.my_stuff.pic.pic_favorites_games.choose_favorite_games.ChooseFavoriteGamesFragment
import com.android.tankee.ui.my_stuff.pic.selected_avatar.SelectedAvatarPresenter
import com.android.tankee.ui.my_stuff.pic.selected_avatar.SelectedAvatarView
import kotlinx.android.synthetic.main.fragment_pic_favotite_games.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class PickFavoriteGamesFragment : PickBaseFragment(), PickFavoriteGameView, SelectedAvatarView {

    private lateinit var picFavoriteGamePresenter: PicFavoriteGamePresenter
    private lateinit var selectedAvatarPresenter: SelectedAvatarPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        picFavoriteGamePresenter = PicFavoriteGamePresenter(this)
        selectedAvatarPresenter = SelectedAvatarPresenter(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pic_favotite_games, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pickFavoriteGamesBack.setOnClickListener { navigateToMySettingsScreen() }
        selectedAvatarPresenter.initSelectedAvatar()

        initChooseFavoriteGamesFragment()
        initActionButton()
    }

    override fun onResume() {
        super.onResume()
        if (isRegistrationFlow()) {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (isRegistrationFlow()) {
            if (EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().unregister(this)
            }
        }
    }

    private fun initChooseFavoriteGamesFragment() {
        val chooseFavoriteGamesFragment = ChooseFavoriteGamesFragment()
        chooseFavoriteGamesFragment.arguments = arguments
        fragmentManager
                ?.beginTransaction()
                ?.add(
                        R.id.chooseFavGamesContainer,
                        chooseFavoriteGamesFragment,
                        ChooseFavoriteGamesFragment.TAG
                )
                ?.commit()
    }

    private fun initActionButton() {
        arguments?.let {
            when (it.getString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY)) {
                PicAvatarNavigation.MySettingsFlow.value -> {
                    doneButton.onClickAction = { onDoneClick() }

                    doneButton.show()
                    nextButton.gone()
                    skipButton.gone()
                }
                PicAvatarNavigation.RegistrationFlow.value -> {
                    nextButton.onClickAction = { onNextClick() }
                    skipButton.onClickAction = { onSkipClick() }

                    doneButton.gone()
                    nextButton.gone()
                    skipButton.show()
                }
            }
        }
    }

    private fun onDoneClick() {
        (fragmentManager?.findFragmentByTag(
                ChooseFavoriteGamesFragment.TAG
        ) as ChooseFavoriteGamesFragment).saveFavoriteUnfavorableGames?.invoke()
    }

    private fun onNextClick() {
        (fragmentManager?.findFragmentByTag(
                ChooseFavoriteGamesFragment.TAG
        ) as ChooseFavoriteGamesFragment).saveFavoriteUnfavorableGames?.invoke()
    }

    private fun onSkipClick() {
        findNavController().navigate(R.id.action_picFavotiteGamesFragment_to_picFavoriteGamersFragment, arguments)
    }

    private fun showSkipHideNextButton() {
        skipButton.show()
        nextButton.gone()
    }

    private fun hideSkipShowNextButton() {
        skipButton.gone()
        nextButton.show()
    }

    override fun setSelectedAvatar(avatarModel: AvatarModel) {
        selectedAvatar.setImageResource(avatarModel.image)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: OnFavoriteGamesSelectedAction) {
        if (event.hasSelectedGames) {
            hideSkipShowNextButton()
        } else {
            showSkipHideNextButton()
        }
    }

}
