package com.android.tankee.ui.ads.kidoz


import android.app.Activity
import com.android.tankee.EMPTY_STRING
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.crashlytics.CrashlyticsLogSender
import com.kidoz.sdk.api.KidozSDK
import com.kidoz.sdk.api.ui_views.kidoz_banner.KidozBannerListener
import com.kidoz.sdk.api.ui_views.new_kidoz_banner.KidozBannerView


/**
 * Created by Sergey Kulyk on 2019-10-24.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
class KidozAdsHelper(private val activity: Activity) {
    fun initializeKidoz() {
        val publisherId = SessionDataHolder.videoPlayerBannerModel?.kidozModel?.publisherId ?: EMPTY_STRING
        val securityToken = SessionDataHolder.videoPlayerBannerModel?.kidozModel?.securityToken ?: EMPTY_STRING

        KidozSDK.initialize(activity, publisherId, securityToken)
    }
    fun showBanner(kidozBannerView: KidozBannerView) {
        try {
            kidozBannerView.setKidozBannerListener(object : KidozBannerListener {
                override fun onBannerReady() {
                    kidozBannerView.show()
                }

                override fun onBannerViewAdded() {
                }

                override fun onBannerNoOffers() {
                }

                override fun onBannerError(p0: String?) {
                    kidozBannerView.hide()
                }

                override fun onBannerClose() {
                }
            })
            kidozBannerView.load()
        } catch (e: RuntimeException) {
            e.message?.let { CrashlyticsLogSender.sendLog(it) }
        }

    }

}