package com.android.tankee.ui.authorization.navigation

import com.android.tankee.R

class FragmentAnimationStore {

    companion object {
        val leftToRightAnimation = FragmentAnimationModel(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
        )

        val donToUpAnimation = FragmentAnimationModel(
                R.anim.slide_in_up,
                R.anim.slide_out_up,
                R.anim.slide_out_up,
                R.anim.slide_in_up
        )
    }
}