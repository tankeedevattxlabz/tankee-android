package com.android.tankee.ui.settings


import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.android.tankee.BuildConfig
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.dynamic_links.DynamicLinks
import com.android.tankee.extentions.show
import com.android.tankee.ui.GeneratedUserNamePresenter
import com.android.tankee.ui.GeneratedUserNameView
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createVerticalLayoutManager
import com.android.tankee.ui.authorization.AuthorizationDialog
import com.android.tankee.ui.authorization.delete_account.DeleteAccountDialog
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.isNotTable
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : BaseFragment(), SettingsPresenter.SettingsView, GeneratedUserNameView {

    private lateinit var settingsPresenter: SettingsPresenter
    private lateinit var generatedUserNamePresenter: GeneratedUserNamePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        settingsPresenter = SettingsPresenter(this)
        generatedUserNamePresenter = GeneratedUserNamePresenter(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onResume() {
        super.onResume()
        val activity = activity as? MainActivity
        activity?.showToolbar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        settingsPresenter.initSettings()
        if (isNotTable()) {
            generatedUserNamePresenter.initGeneratedUserName()
        }

        getDynamicLink()
    }

    override fun setSettings(setting: List<SettingModel>) {
        settingsRecycler.layoutManager = createVerticalLayoutManager()
        settingsRecycler.adapter = getSettingsAdapter(setting)
    }

    override fun setGeneratedUserName(generatedUserNameValue: String) {
        val str = SpannableStringBuilder("${ResUtils.getString(R.string.logged_in_as)} $generatedUserNameValue")
        str.setSpan(
                StyleSpan(Typeface.BOLD),
                0,
                ResUtils.getString(R.string.logged_in_as).length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        generatedUserName.text = str
    }


    private fun getSettingsAdapter(setting: List<SettingModel>): SettingsAdapter {
        val settingsAdapter = SettingsAdapter()
        settingsAdapter.items = setting.toMutableList()
        settingsAdapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<SettingModel> {
            override fun onItemClicked(item: SettingModel) {
                navigateToSelectedMenuItem(item)
            }
        }

        return settingsAdapter
    }

    private fun navigateToSelectedMenuItem(settingModel: SettingModel) {
        when (settingModel.id) {
            SettingsId.About -> openAboutScreen()
            SettingsId.SwitchUser, SettingsId.JoinOrSignIn -> {
                AnalyticsManager.tankeeHamburgerMenuSwitchUserButtonEvent()
                AuthorizationDialog.openAuthorizationDialog(fragmentManager, R.id.homeListFragment)
            }
            SettingsId.DeleteAccount -> {
                AnalyticsManager.tankeeHamburgerMenuDeleteUserButtonEvent()
                DeleteAccountDialog.openDeleteAccountDialog(fragmentManager)
            }
            SettingsId.DebugInfo -> openDebugInfoBottomSheet()
        }
    }

    private fun openDebugInfoBottomSheet() {
        findNavController().navigate(R.id.debugInfoFragment)
    }

    private fun getDynamicLink() {
        val linkPath = SessionDataHolder.dynamicLink?.getPath()
        linkPath.let {
            when (it) {
                DynamicLinks.TankeeAboutPage.path -> {
                    openAboutScreen()
                    SessionDataHolder.clearDynamicLink()
                }
            }
        }
    }

    private fun openAboutScreen() {
        findNavController().navigate(R.id.aboutFragment)
    }

}
