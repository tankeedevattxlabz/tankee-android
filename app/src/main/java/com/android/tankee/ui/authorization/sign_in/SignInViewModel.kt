package com.android.tankee.ui.authorization.sign_in


import com.android.tankee.logs.AppLogger
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.authorization.sign_in.controller.SignInStepsHandler
import com.android.tankee.ui.authorization.sign_in.controller.SignInFlow
import com.android.tankee.ui.base.BaseView


class SignInViewModel(private val signInView: SignInView) {

    fun sigIn(signInUserModel: UserAccountModel) {
        if (!checkIsValidField(signInUserModel)) {
            AppLogger.logInfo(TAG_SIG_IN_FLOW, "Invalid sign in fields.")
            return
        } else {
            AppLogger.logInfo(TAG_SIG_IN_FLOW, "Valid sign in fields.")
        }

        executeSigInFlow(signInUserModel)
    }

    private fun checkIsValidField(signInUserModel: UserAccountModel): Boolean {
        var isFieldsValid = true

        if (signInUserModel.userName.isEmpty()) {
            signInView.setUserNameEmptyError()
            isFieldsValid = false
        } else if (signInUserModel.userName.first().isDigit()) {
            signInView.setUserNameFirstDigitalError()
            isFieldsValid = false
        }

        if (signInUserModel.password.isEmpty()) {
            signInView.setPasswordEmptyError()
            isFieldsValid = false
        }

        return isFieldsValid
    }

    private fun executeSigInFlow(signInUserModel: UserAccountModel) {
        val errorController = getSignInErrorController()

        val signInFlow = SignInFlow(signInView, errorController)
        signInFlow.getKwsUserToken(signInUserModel)
    }

    private fun getSignInErrorController(): SignInStepsHandler {
        val signInErrorController = SignInStepsHandler()

        signInErrorController.onStartSignIn = {
            signInView.showLoading()
        }
        signInErrorController.onError = {
            signInView.hideLoading()
        }
        signInErrorController.onSuccessSignIn = {
            signInView.navigateToHomeScreen()
            signInView.hideLoading()
        }
        signInErrorController.onFailedSignIn = {
            signInView.setUserAccountError()
            signInView.hideLoading()
        }

        return signInErrorController
    }

    fun setTestData() {
        signInView.setTestSignInData("test30", "12345678")
    }

    companion object {
        var TAG: String = SignInViewModel::class.java.simpleName
        var TAG_SIG_IN_FLOW: String = "SigInFlow"
    }

    interface SignInView : BaseView {
        fun navigateToHomeScreen()

        fun setUserNameEmptyError()

        fun setUserNameFirstDigitalError()

        fun setPasswordEmptyError()

        fun setUserAccountError()

        fun setTestSignInData(testUserName: String, testPassword: String)
    }

}
