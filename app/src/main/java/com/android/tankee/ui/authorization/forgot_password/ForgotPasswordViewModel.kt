package com.android.tankee.ui.authorization.forgot_password


import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.ForgotPasswordRequestBody
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.ui.base.BaseView


class ForgotPasswordViewModel(private val forgotPasswordView: ForgotPasswordView) {

    fun resetPassword(userName: String) {
        if (!checkIsValidField(userName)) {
            AppLogger.logDebug(FORGOT_PASSWORD_FLOW, "Invalid username field.")
            return
        } else {
            AppLogger.logDebug(FORGOT_PASSWORD_FLOW, "Valid username field.")
        }

        reset(userName)
    }

    private fun checkIsValidField(userName: String): Boolean {

        if (userName.isEmpty()) {
            forgotPasswordView.setUsernameEmptyError()
            return false
        }

        return true
    }

    private fun reset(parentEmailOrUserName: String) {
        AppLogger.logDebug(FORGOT_PASSWORD_FLOW, "Forgot password was sent.", logStatus = LogStatus.EXECUTING)

        forgotPasswordView.showLoading()

        val forgotPasswordRequestBody = ForgotPasswordRequestBody
            .Builder
            .buildForgotPasswordRequestBody(parentEmailOrUserName)

        KwsRest.api.forgotPassword(forgotPasswordRequestBody).enqueue(
            forgotPasswordView.createCallBack(
                { response ->
                    AppLogger.logDebug(FORGOT_PASSWORD_FLOW, "Forgot password was sent.", logStatus = LogStatus.SUCCESS)

                    forgotPasswordView.hideLoading()
                    if (response.code() == 204) {
                        forgotPasswordView.setSuccessfulForgotPasswordText()
                    }
                },
                {
                    forgotPasswordView.hideLoading()

                    AppLogger.logDebug(FORGOT_PASSWORD_FLOW, "Forgot password was not sent.", logStatus = LogStatus.FAILED)
                }
            )
        )
    }

    companion object {
        const val FORGOT_PASSWORD_FLOW = "ForgotPasswordFlow"
    }

    interface ForgotPasswordView : BaseView {
        fun setSuccessfulForgotPasswordText()

        fun setUsernameEmptyError()
    }
}