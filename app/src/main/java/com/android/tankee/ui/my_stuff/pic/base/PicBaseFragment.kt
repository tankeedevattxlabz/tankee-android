package com.android.tankee.ui.my_stuff.pic.base


import androidx.navigation.fragment.findNavController
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation


open class PickBaseFragment : BaseFragment(), PickBaseView {

    override fun navigateToMySettingsScreen() {
        findNavController().popBackStack()
    }

    protected fun isRegistrationFlow(): Boolean {
        return arguments != null &&
                arguments?.getString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY) == PicAvatarNavigation.RegistrationFlow.value
    }

}


interface PickBaseView : BaseView {
    fun navigateToMySettingsScreen()
}