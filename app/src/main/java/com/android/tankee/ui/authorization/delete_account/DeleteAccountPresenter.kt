package com.android.tankee.ui.authorization.delete_account


import com.android.tankee.repo.TankeeCleaner
import com.android.tankee.rest.repository.DeleteAccount
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.base.EventHandler


class DeleteAccountPresenter(private val deleteAccountView: DeleteAccountView) {

    fun deleteAccount(userAccountModel: UserAccountModel) {
        if (!checkIsValidField(userAccountModel)) {
            return
        }

        val eventHandler = buildDeleteAccountEventHandler()
        val deleteAccount = DeleteAccount(deleteAccountView, eventHandler, userAccountModel)
        deleteAccount.deleteAccount()
    }

    private fun checkIsValidField(userAccountModel: UserAccountModel): Boolean {
        var isValidFields = true

        if (userAccountModel.userName.isEmpty()) {
            deleteAccountView.setUsernameEmptyError()
            isValidFields = false
        }

        if (userAccountModel.password.isEmpty()) {
            deleteAccountView.setPasswordEmptyError()
            isValidFields = false
        }

        return isValidFields
    }
    private fun buildDeleteAccountEventHandler(): EventHandler {
        val eventHandler = EventHandler()

        eventHandler.onStart = { deleteAccountView.showLoading() }
        eventHandler.onError = { deleteAccountView.hideLoading() }
        eventHandler.onFailed = {
            deleteAccountView.hideLoading()
            deleteAccountView.setInvalidCredentialsError()
        }
        eventHandler.onFinish = { deleteAccountView.hideLoading() }
        eventHandler.onSuccess = {
            TankeeCleaner.cleanAllData()
            deleteAccountView.hideLoading()
            deleteAccountView.navigateHomeScreen()
        }

        return eventHandler
    }

    interface DeleteAccountView : BaseView {
        fun navigateHomeScreen()

        fun setInvalidCredentialsError()

        fun setUsernameEmptyError()

        fun setPasswordEmptyError()

    }
}