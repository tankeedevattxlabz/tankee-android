package com.android.tankee.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.request_options.gamerRequestOptions
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_gamer.view.*

class GamersAdapter : SectionAdapter() {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSectionViewHolder {
        var layoutId = R.layout.item_gamer

        if (viewType == 0) {
            layoutId = R.layout.item_see_all
        }

        val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

        return when (viewType) {
            1 -> GamersItemsViewHolder(itemView)
            else -> SeeAllViewHolder(itemView)
        }
    }

    inner class GamersItemsViewHolder(itemView: View) : SectionViewHolder(itemView) {
        override fun onBind(item: SectionItemMarker) {
            if (item is Gamer) {

                Glide.with(getAppContext())
                        .load(buildUrl(item.imageUrl?.url))
                        .apply(RequestOptions.circleCropTransform())
                        .apply(gamerRequestOptions)
                        .into(itemView.influencerImage)
                item.name?.let { itemView.influencerName.text = it }
                listener?.let { itemView.setOnClickListener { iv -> it.onClick(item) } }
            }
        }
    }
}