package com.android.tankee.ui.video_player.stickers

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.tankee.R
import com.android.tankee.TankeeApp
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.rest.BaseResponse
import com.android.tankee.rest.TankeeCallback
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.analytics.AnalyticsEvent
import com.android.tankee.analytics.AnalyticsParameter
import com.android.tankee.analytics.EventSender
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.utils.dp
import com.android.tankee.utils.getScreenSize
import com.android.tankee.utils.getWindowPositionX
import com.android.tankee.utils.log
import kotlinx.android.synthetic.main.fragment_video_player.view.*
import kotlinx.android.synthetic.main.item_sticker_layout.view.*
import org.jetbrains.anko.*
import retrofit2.Response
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread
import kotlin.random.Random

class StickersPresenter(
    private var rootView: ConstraintLayout?,
    val video: Video,
    private var getCurrentPlayTime: (() -> Long)? = null,
    private var baseView: BaseView? = null
) {
    private val STEP_1_DURATION = 1000
    private val STEP_2_DURATION = 5000
    private var lastClickedTime = 0L
    private val CLICK_DEBOUNCE_DELAY = 200
    private var stickerAdapter: StickerAdapter? = StickerAdapter { s, v ->
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - lastClickedTime > CLICK_DEBOUNCE_DELAY) {
            animateOnClick(cloneAndDecorateStickerView(v))
            clickLogic(s)
            lastClickedTime = currentTimeMillis
        }
    }
    private var handler: Handler?
    private var mainHandler : Handler?= Handler(Looper.getMainLooper())
    private val running = AtomicBoolean(false)
    private var initialStickerList: List<StickerAdapter.Sticker>? = null
    private var currentStickerList: MutableList<StickerAdapter.Sticker>? = null
    private var quee = Collections.synchronizedList(mutableListOf<StickerAdapter.Sticker>())
    init {
        rootView?.sticker_recycler?.apply {
            adapter = stickerAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
        video.id?.let { id->
            val onDone: (Response<BaseResponse<StickerAdapter.Sticker>>) -> Unit = {
                val body = it.body()
                body?.log()
                initialStickerList = body?.items
                resetStickerList()
            }
            val tankeeCallback =
                if (baseView != null) baseView?.createCallBack(onDone) else TankeeApp.app.createCallBack(onDone)
            tankeeCallback?.let {
                TankeeRest.api.getStickersForVideo(id).enqueue(tankeeCallback)
            }
        }
        val handlerThread = HandlerThread("currentStickerList")
        handlerThread.start()
        handler = Handler(handlerThread.looper)
    }




    fun showStickersForTime(position: Long, fullScreen: Boolean = false) {
        val filter = currentStickerList?.filter { it.time == position }
        filter?.toTypedArray()?.let { currentStickerList?.removeAll(it) }
        filter?.toTypedArray()?.let { quee?.addAll(it) }
        if (!fullScreen) {
            startShow()
        } else if (running.get()) {
            resetStickerList()
        }
        return
    }


    private fun startShow() {
        if (running.get()) {
            return
        }
        running.set(true)
        val screenSize = getScreenSize()
        thread {
            while (running.get()) {
                val delay = 100 * Random.nextInt(1, 5)
                try {
                    val last = quee?.lastOrNull()
                    last?.let {
                        quee?.remove(last)
                        Thread.sleep(delay.toLong())
                        mainHandler?.postDelayed({
                            val drawable = ContextCompat.getDrawable(
                                TankeeApp.app, StickerAdapter.Sticker.stickerNameToResId(
                                    last.name
                                )
                            )
                            val x = screenSize?.widthPixels ?: 0
                            val y = screenSize?.heightPixels ?: 0
                            if (running.get()) {
                                val isMine = it.user?.get("is_current_user") as? Boolean
                                animateToTop(
                                    createSticker(
                                        drawable,
                                        intArrayOf(x - (50 * Random.nextInt(1, 4)).dp(), y), isMine ?: false
                                    )
                                    //                                intArrayOf(x - (50 * Random.nextInt(1,4)).dp() , y))
                                )
                            }
                        }, 0)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun resetStickerList() {
        currentStickerList = initialStickerList?.toMutableList()
        quee?.clear()
        mainHandler?.removeCallbacksAndMessages(null)
        handler?.removeCallbacksAndMessages(null)
        running.set(false)
    }

    private fun clickLogic(sticker: StickerAdapter.Sticker) {
        video.id?.let {
            getCurrentPlayTime?.let {
                sticker.time = it()
            }
            ApptentiveManager.addSticker()
            TankeeRest.api.addSticker(it, sticker).enqueue(TankeeCallback<Any>())
        }
        EventSender.logEvent(
            AnalyticsEvent.ADD_STICKER, androidx.core.os.bundleOf(
                AnalyticsParameter.USER_NAME.paramName to (SessionDataHolder.user?.fullName ?: "NONAME"),
                AnalyticsParameter.DEVICE_ID.paramName to (SessionDataHolder.user?.uid ?: "NODEVICE"),
                AnalyticsParameter.GAME_TYPE.paramName to (video.gameTypes ?: "NO_GAME_TYPE"),
                AnalyticsParameter.STICKER_NAME.paramName to (sticker.name),
                AnalyticsParameter.VIDEO_NAME.paramName to (video.title ?: "NONAME"),
                AnalyticsParameter.VIDEO_SOURCE.paramName to (video.jwKey ?: "NOKEY"),
                AnalyticsParameter.VIDEO_TIME.paramName to sticker.time,
                AnalyticsParameter.USER_ID.paramName to (SessionDataHolder.user?.id ?: "NOID")
            )
        )
    }

    private fun animateOnClick(stickerview: View?) {
        stickerview?.let {
            it.postDelayed({
                animateToTop(it)
            }, STEP_1_DURATION.toLong())
            it.postDelayed({
                moveClickedSmileToStartPosition(it, duration = STEP_1_DURATION.toLong())
            }, 0)
        }
    }

    private fun animateToTop(stickerview: View?) {
        val animator = ObjectAnimator.ofFloat(stickerview, "translationY", -500f)
        animator.duration = STEP_2_DURATION.toLong()
        animator.setAutoCancel(true)
        val x = stickerview?.getWindowPositionX() ?: 0
        val pulseAnimator = ObjectAnimator.ofFloat(stickerview, "translationX", x - 50f)
        pulseAnimator.repeatCount = 100
        pulseAnimator.repeatMode = ObjectAnimator.REVERSE
        pulseAnimator.duration = STEP_2_DURATION.toLong() / (5 * Random.nextInt(2, 4))
        val fadeoutAnimator = ObjectAnimator.ofFloat(stickerview, "alpha", 0f)
        fadeoutAnimator.duration = STEP_2_DURATION.toLong()
        val animatorSet = AnimatorSet()
        val weakReference = WeakReference(stickerview)
        animatorSet.apply {
            play(animator).with(pulseAnimator).with(fadeoutAnimator)
            doOnEnd {
                val view = weakReference.get()
                view?.let {
                    rootView?.removeView(it)
                }
            }
            start()
        }
    }

    private fun cloneAndDecorateStickerView(stickerview: View?): View? {
        val array = IntArray(2) { 0 }
        stickerview?.getLocationInWindow(array)
        val image = stickerview?.imageview?.image
        val fl: FrameLayout? = createSticker(image, array)
        fl?.elevation = 12f
        return fl
    }

    private fun createSticker(
        image: Drawable?,
        array: IntArray,
        isMine: Boolean = true
    ): FrameLayout? {
        try {
            val constraintSet = ConstraintSet()
            constraintSet.clone(rootView)
            var fl: FrameLayout? = null
            var sticker: ImageView? = null
            var stripes: ImageView? = null

            rootView?.apply {
                val size = resources.getDimensionPixelSize(R.dimen.video_sticker_size)
                val offset = resources.getDimensionPixelOffset(R.dimen.sticker_stripe_top_margin)
    //            val size = 48.dp()
                fl = frameLayout {
                    id = View.generateViewId()

                    if (isMine) {
                        stripes = imageView {
                            id = View.generateViewId()
                            imageResource = R.drawable.sticker_lines
                            padding = 8.dp()
                        }

                        stripes?.lparams {
                            height = size
                            width = size
                            gravity = Gravity.CENTER
                            setMargins(0, offset, 0, 0)
                        }
                    }
                    sticker = imageView {
                        id = View.generateViewId()
                        setImageDrawable(image)
                        padding = 4.dp()
                    }
                    sticker?.lparams {
                        height = size
                        width = size
    //                    setMargins(0, 0, 0, offset/2)
                    }
                    if (isMine) {
                        imageView {
                            imageResource = R.drawable.mask
                            padding = 4.dp()
                        }.lparams {
                            height = size
                            width = size
    //                        setMargins(0, 0, 0, offset/2)
                        }
                    }
                }
                if (fl != null) {
                    val fid = fl!!.id
                    constraintSet.constrainWidth(fid, size)
                    constraintSet.constrainHeight(fid, size + offset)

                    constraintSet.setTranslationX(fid, array[0].toFloat())
                    constraintSet.setTranslationY(fid, array[1].toFloat())
                    constraintSet.applyTo(this)
                }
            }
            return fl
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    private fun moveClickedSmileToStartPosition(v: View, duration: Long) {
        val sw = getScreenSize()?.widthPixels ?: 0
        val sh = getScreenSize()?.heightPixels ?: 0
        val i = 50 * Random.nextInt(1, 4)
        val x = sw - i.dp()
        val array = IntArray(2) { 0 }
        v.getLocationInWindow(array)
        val diff = x - array[0]
        val setCompat = AnimatorSet()
        val animatorX = ObjectAnimator.ofFloat(v, "translationX", x.toFloat())
        val animatorY = ObjectAnimator.ofFloat(v, "translationY", (sh - 150.dp()).toFloat())
        animatorX.duration = duration
        animatorY.duration = duration
        setCompat.apply {
            play(animatorX).with(animatorY)
            start()
        }
    }

    fun destroy(){
        baseView = null
        rootView = null
        stickerAdapter = null
        handler = null
        mainHandler = null
        getCurrentPlayTime = null
    }
}