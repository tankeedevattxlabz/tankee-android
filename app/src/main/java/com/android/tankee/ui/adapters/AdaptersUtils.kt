package com.android.tankee.ui.adapters

import androidx.recyclerview.widget.LinearLayoutManager
import com.android.tankee.getAppContext


fun createHorizontalLayoutManager() = LinearLayoutManager(getAppContext(), LinearLayoutManager.HORIZONTAL, false)

fun createVerticalLayoutManager() = LinearLayoutManager(getAppContext())