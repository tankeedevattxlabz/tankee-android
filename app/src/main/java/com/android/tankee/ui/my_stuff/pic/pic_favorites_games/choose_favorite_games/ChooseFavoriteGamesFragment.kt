package com.android.tankee.ui.my_stuff.pic.pic_favorites_games.choose_favorite_games


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import com.android.tankee.ui.my_stuff.pic.base.PickBaseFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.MarginItemDecoration
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers.choose_favorite_gamers.ChooseFavoriteGamersFragment
import com.android.tankee.ui.parallax.ParallaxController
import kotlinx.android.synthetic.main.fragment_choose_favorite_items.*
import kotlinx.android.synthetic.main.fragment_pic_avatar.*


class ChooseFavoriteGamesFragment : PickBaseFragment(), ChooseFavoriteGamesView {

    private lateinit var chooseFavoriteGamesPresenter: ChooseFavoriteGamesPresenter
    private var parallaxController: ParallaxController? = null
    private var favoriteGamersAdapter: FavoriteGamesAdapter? = null
    var saveFavoriteUnfavorableGames: (() -> Unit)? = null

    override fun onSaveInstanceState(outState: Bundle) {
        parallaxController?.parallaxTransition?.translationX?.let { outState.putFloat("transitionX", it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        chooseFavoriteGamesPresenter = ChooseFavoriteGamesPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_choose_favorite_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        saveFavoriteUnfavorableGames = {
            favoriteGamersAdapter?.items?.let {
                chooseFavoriteGamesPresenter.updateGames()
            }
        }
        chooseFavoriteGamesPresenter.getGames()


        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("transitionX")) {
                parallaxController?.parallaxTransition?.translationX = savedInstanceState.getFloat("transitionX")
            }
        }
    }

    override fun setGames(games: List<Game>) {
        favoriteGamesRecycler.layoutManager = createHorizontalLayoutManager()
        favoriteGamesRecycler.adapter = getFavoriteGamesAdapter(games)

        setUpParallaxWithRecyclerView()
        setMarginItemDecorator()
    }

    private fun getFavoriteGamesAdapter(games: List<Game>): FavoriteGamesAdapter? {
        favoriteGamersAdapter = FavoriteGamesAdapter()
        favoriteGamersAdapter?.items = games.toMutableList()
        favoriteGamersAdapter?.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<Game> {
            override fun onItemClicked(item: Game) {
                chooseFavoriteGamesPresenter.addGameToStore(item)
                favoriteGamersAdapter?.setFavoriteOrUnfavorableGame(item)
            }
        }
        return favoriteGamersAdapter
    }

    private fun setUpParallaxWithRecyclerView() {
        parallaxController = ParallaxController(favoriteGamesRecycler, favGamesParallaxBackground)
    }

    private fun setMarginItemDecorator() {
        val marginItemDecoration = MarginItemDecoration(resources.getDimension(R.dimen.pic_avatar_list_margin).toInt())
        favoriteGamesRecycler.addItemDecoration(marginItemDecoration)
        parallaxController?.itemDecoratorSize = resources.getDimension(R.dimen.pic_avatar_list_margin).toInt()
    }

    override fun onGamesUpdatesNavigation() {
        arguments?.let {
            when (it.getString(PicAvatarFragment.PIC_AVATAR_NAVIGATION_KEY)) {
                PicAvatarNavigation.MySettingsFlow.value -> navigateToMySettingsScreen()
                PicAvatarNavigation.RegistrationFlow.value -> navigateToPickFavoriteGamersScreen()
            }
        }
    }

    private fun navigateToPickFavoriteGamersScreen() {
        findNavController().navigate(R.id.action_picFavotiteGamesFragment_to_picFavoriteGamersFragment, arguments)
    }

    companion object {
        var TAG: String = ChooseFavoriteGamersFragment::class.java.simpleName
    }
}
