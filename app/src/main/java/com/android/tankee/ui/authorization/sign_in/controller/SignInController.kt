package com.android.tankee.ui.authorization.sign_in.controller

import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.database.AppDatabase
import com.android.tankee.database.TankeeDbHelper
import com.android.tankee.database.kws.helper.KwsDbHelper
import com.android.tankee.database.kws.models.KwsUserIdModel
import com.android.tankee.event_bus.ChangeToolbarAvatar
import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.UpdateGeneratedUserNameEvent
import com.android.tankee.jwt.JWTDecoder
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.notifications.TankeeMessagingService.Companion.FCM_TAG
import com.android.tankee.repo.user.FcmTokenRepo
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.rest.kws.KwsRestUtils
import com.android.tankee.rest.responses.users.RequestUserBody
import com.android.tankee.rest.responses.users.UserModel
import com.android.tankee.rest.tankee.UserRequestBodyHelper
import com.android.tankee.rest.utils.RequestBodyConverter
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.base.BaseView
import okhttp3.RequestBody

class SignInFlow(
    private val baseView: BaseView,
    private val onSignInStepsHandler: SignInStepsHandler? = null
) {

    /**
     * Step_1
     */
    fun getKwsUserToken(signInUserModel: UserAccountModel) {
        onSignInStepsHandler?.onStartSignIn?.invoke()

        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get KWS user token.", 1, LogStatus.EXECUTING)

        val kwsGetUserTokenHeader = "Basic dGFua2VlOmo4TWpSMWhZR1NrOVJ3WjVJQ2FxMjNYcWdCeU9iSFFs"

        KwsRest.api.getToken(kwsGetUserTokenHeader, signInUserModel.userName, signInUserModel.password)
            .enqueue(baseView.createCallBack(
                { response ->
                    AppLogger.logDebug(
                        TAG_SIG_IN_FLOW,
                        "Get KWS user token.",
                        1,
                        LogStatus.SUCCESS
                    )
                    AppLogger.logDebug(
                        TAG_SIG_IN_FLOW,
                        "Get KWS user token:\t${response.body()?.accessToken}",
                        1
                    )

                    val kwsUserId = response.body()?.accessToken?.let { JWTDecoder.decodeAccessToken(it) }

                    AppLogger.logDebug(
                        TAG_SIG_IN_FLOW,
                        "User id:\t$kwsUserId",
                        1
                    )

                    KwsDbHelper.insertKwsUserId(KwsUserIdModel(kwsUserId!!))
                    response.body()?.accessToken?.let { getUser(response.body()?.accessToken, kwsUserId) }

                }, {
                    onSignInStepsHandler?.onFailedSignIn?.invoke()

                    AppLogger.logDebug(TAG_SIG_IN_FLOW, "tGet KWS user token.", 1, LogStatus.FAILED)
                })
            )
    }

    /**
     * Step_2
     * Get user model from Tankee server.
     */
    private fun getUser(kwsToken: String?, userId: String) {
        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Getting user model from Tankee server.", 2, LogStatus.EXECUTING)

        val requestUserBody = buildRequestUserBody(userId)
        val requestBody = RequestBodyConverter.convertToRequestBody(requestUserBody)

        TankeeRest.api.getUser(requestBody)
            .enqueue(baseView.createCallBack(
                { response ->
                    response.body()?.let {
                        it.items?.let { items ->
                            if (items.isNotEmpty()) {
                                AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get Tankee user.", 2, LogStatus.SUCCESS)

                                val userModel = items[0]
                                AppLogger.logDebug(TAG_SIG_IN_FLOW, "Tankee user -> $userModel", 2)

                                TankeeDbHelper.insertUserModel(userModel)
                                deleteFccTokenForPreviousUser(kwsToken!!, userModel)
                            }
                        }
                    }
                },
                {
                    onSignInStepsHandler?.onError?.invoke()

                    AppLogger.logDebug(TAG_SIG_IN_FLOW, "Tankee user was not loaded.", 2, LogStatus.FAILED)
                }
            ))
    }


    /**
     * Step_3
     * Delete fcm token for previous user and vendor user.
     */
    private fun deleteFccTokenForPreviousUser(kwsToken: String, userModel: UserModel) {
        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Delete FCM Token for previous user.", 4, LogStatus.EXECUTING)

        val fcmTokenRepo = FcmTokenRepo(baseView)
        fcmTokenRepo.onUpdateAction = { getKwsUserModel(kwsToken, userModel) }
        fcmTokenRepo.updateFcmTokenAndTimeZone()
    }

    /**
     * Step_4
     * Get KWS user model.
     */
    private fun getKwsUserModel(kwsToken: String, userModel: UserModel) {
        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get Kws user model.", 4, LogStatus.EXECUTING)

        val kwsGetUserHeader = KwsRestUtils.buildBearerHeader(kwsToken)
        KwsRest.api.getUser(kwsGetUserHeader, userModel.uid!!)
            .enqueue(baseView.createCallBack(
                { response ->
                    AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get Kws user model.", 4, LogStatus.SUCCESS)

                    baseView.hideLoading()
                    response.body()?.let { kwsUserModel ->
                        AppLogger.logDebug(
                            TAG_SIG_IN_FLOW, "\t$kwsUserModel.", 4
                        )
                        KwsDbHelper.insertKwsUserModel(kwsUserModel)

                        TankeePrefs.token = userModel.token.toString()
                        TankeePrefs.userUid = userModel.uid.toString()

                        val user = UserModel()
                        user.token = TankeePrefs.token
                        user.uid = TankeePrefs.userUid
                        SessionDataHolder.user = user
                        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Session_Token", TankeePrefs.token)

                        updateUserName(
                            UserRequestBodyHelper.getUpdateUserNameRequestBody(kwsUserModel.username!!),
                            userModel
                        )
                    }
                },
                {
                    onSignInStepsHandler?.onError?.invoke()

                    AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get Kws user model.", 4, LogStatus.FAILED)
                }
            ))
    }

    /**
     * Step_5
     * Update user name for sync app with server.
     */
    private fun updateUserName(requestBody: RequestBody, userModel: UserModel) {

        AppLogger.logDebug(
            TAG_SIG_IN_FLOW,
            "Update user name.",
            5, LogStatus.EXECUTING
        )

        TankeeRest.api.updateUserInfo(requestBody).enqueue(
            baseView.createCallBack(
                { responces ->
                    AppLogger.logDebug(
                        TAG_SIG_IN_FLOW,
                        "User name was updated.",
                        5,
                        LogStatus.SUCCESS
                    )
                    responces.body()?.items?.let { items ->
                        if (items.isEmpty()) {
                            SessionDataHolder.user?.userName = items[0].userName
                        }
                    }
                    if (userModel.isNotNew()) {
                        getUserInfo()
                    } else {
                        // Update toolbar user name for tablet
                        updateToolbarAvatarIconAndGeneratedUserName()
                        updateFcmToken()

                        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Successful SignIn.", 5)
                    }
                },
                {
                    onSignInStepsHandler?.onError?.invoke()

                    AppLogger.logDebug(
                        TAG_SIG_IN_FLOW,
                        "User name was not updated.",
                        5, LogStatus.FAILED
                    )
                }
            )
        )
    }

    /**
     * Step_5
     * Get UserModel from Tankee server if new user.
     */
    private fun getUserInfo() {
        AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get user info.", 6, LogStatus.EXECUTING)

        TankeeRest.api.getUserSelf().enqueue(
            baseView.createCallBack(
                { response ->
                    AppLogger.logDebug(TAG_SIG_IN_FLOW, "Get user info.", 6, LogStatus.SUCCESS)

                    baseView.hideLoading()
                    response.body()?.let {
                        it.items?.let { items ->
                            if (items.isNotEmpty()) {
                                val userModel = items[0]
                                AppDatabase.getAppDatabase()
                                    .userDao()
                                    .updateUser(userModel)
                                SessionDataHolder.user = userModel

                                AppLogger.logDebug(TAG_SIG_IN_FLOW, "\t$userModel", 6)
                                AppLogger.logDebug(TAG_SIG_IN_FLOW, "Successful SignIn.", 6)

                                updateToolbarAvatarIconAndGeneratedUserName()
                                updateFcmToken()
                            }
                        }
                    }
                },
                {
                    onSignInStepsHandler?.onError?.invoke()

                    AppLogger.logInfo(TAG_SIG_IN_FLOW, "Get user info.", 6, LogStatus.FAILED)
                }
            )
        )
    }

    /**
     * Step_6
     * Get UserModel from Tankee server if new user.
     */
    private fun updateFcmToken() {
        AppLogger.logInfo(TAG_SIG_IN_FLOW, "Update $FCM_TAG Token.", 6, LogStatus.EXECUTING)

        val fcmTokenRepo = FcmTokenRepo(baseView)
        fcmTokenRepo.onUpdateAction = { onSignInStepsHandler?.onSuccessSignIn?.invoke() }
        fcmTokenRepo.updateFcmTokenAndTimeZone()
    }

    // TODO Create observable for it. Delete for sign in flow.
    private fun updateToolbarAvatarIconAndGeneratedUserName() {
        EventPoster.postEvent(ChangeToolbarAvatar())
        EventPoster.postEvent(UpdateGeneratedUserNameEvent())
    }

    private fun buildRequestUserBody(kwsUserId: String?): RequestUserBody {
        val requestUserBody = RequestUserBody()

        requestUserBody.uid = kwsUserId
        requestUserBody.isAnonymous = true

        return requestUserBody
    }

    companion object {
        var TAG_SIG_IN_FLOW: String = "SigInFlow"
    }

}

class SignInStepsHandler {

    var onStartSignIn: (() -> Unit)? = null

    var onError: (() -> Unit)? = null

    var onSuccessSignIn: (() -> Unit)? = null

    var onFailedSignIn: (() -> Unit)? = null

}