package com.android.tankee.ui.settings.debug_info


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createVerticalLayoutManager
import com.android.tankee.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_debug_info.*


class DebugInfoFragment : BaseFragment(), DebugInfoView {

    private lateinit var debugInfoPresenter: DebugInfoPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        debugInfoPresenter = DebugInfoPresenter(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_debug_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        debugInfoBackButton.setOnClickListener { findNavController().popBackStack() }
        debugInfoPresenter.initDebugInfo()
    }

    override fun setDebugInfo(list: List<DebugInfoModel>) {
        debugInfoRecycler.layoutManager = createVerticalLayoutManager()
        debugInfoRecycler.adapter = buildAdapter(list)
    }

    private fun buildAdapter(list: List<DebugInfoModel>): DebugInfoAdapter {
        val debugInfoAdapter = DebugInfoAdapter()
        debugInfoAdapter.items = list.toMutableList()
        debugInfoAdapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<DebugInfoModel> {
            override fun onItemClicked(item: DebugInfoModel) {
                openBottomSheetDialog(item)
            }
        }
        return debugInfoAdapter
    }

    private fun openBottomSheetDialog(model: DebugInfoModel) {
        val debugInfoBottomSheet = context?.let { DebugInfoBottomSheet.newInstance(it, model) }
        debugInfoBottomSheet?.show()
    }

}
