package com.android.tankee.ui.home.toolbar.toolbar_item.toolbar_notification_item


import android.content.Context
import android.util.AttributeSet
import androidx.fragment.app.DialogFragment
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.extentions.visibility
import com.android.tankee.ui.home.toolbar.toolbar_item.ToolbarUtilItem
import kotlinx.android.synthetic.main.toolbar_notification_item.view.*


class ToolbarNotificationItem(context: Context, attrs: AttributeSet?) : ToolbarUtilItem(context, attrs), ToolbarNotificationItemView {

    private val presenter = ToolbarNotificationMarkerPresenter(this)

    init {
        presenter.showOrHideMarker()
    }

    override fun getLayoutId(): Int {
        return R.layout.toolbar_notification_item
    }

    override fun setHighlighted(selected: Boolean) {
        super.setHighlighted(selected)

        if (selected) {
            hideMarker()
        }
    }

    override fun setMarkerVisibility(visibility: Boolean) {
        notificationsMarker.visibility(visibility)
    }

    override fun showPopUp(p: DialogFragment, tag: String) {
        // No action
    }

    override fun showLoading() {
        // No action
    }

    override fun hideLoading() {
        // No action
    }

    override fun showLoading(showLoading: Boolean) {
        // No action
    }

    fun showMarker() {
        notificationsMarker.show()
    }

    private fun hideMarker() {
        notificationsMarker.gone()
    }

}