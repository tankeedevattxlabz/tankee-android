package com.android.tankee.ui.views.see_all

data class SeeAllItemVideosSection(
    val sectionId: Int,
    val sectionName: String,
    var color: Int,
    val icon: Int,
    val type: SeeAllType = SeeAllType.SECTION
) : SeeAllItem(color, icon, type)