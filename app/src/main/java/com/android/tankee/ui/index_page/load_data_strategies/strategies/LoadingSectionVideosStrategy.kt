package com.android.tankee.ui.index_page.load_data_strategies.strategies

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.load_data_strategies.base.LazyLoadingStrategy

class LoadingSectionVideosStrategy(private val sectionId: Int) : LazyLoadingStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getSectionById(sectionId)
            .enqueue(indexPageView.createCallBack(
                { response ->
                    response.body()?.let { baseResponse ->
                        baseResponse.items?.let { sections ->
                            sections.isNotEmpty().let {
                                val section = sections[0]
                                section.title?.let { indexPageBuilder.title = it }
                                section.tags?.let { indexPageBuilder.tags = it }
                                loadItems()
                            }
                        }
                    }
                }, {
                    hideLoading()
                    indexPageView.onErrorLoading()
                })
            )
    }

    override fun loadMoreItems() {
        TankeeRest.api.getSectionByIdVideos(sectionId, lazyLoadingModel.page, lazyLoadingModel.tag)
            .enqueue(indexPageView.createCallBack(
                { response ->
                    response.body()?.let { baseResponse ->
                        baseResponse.items?.let { items ->
                            finishLoadingItems()
                            checkIsLazyLoadingFinished(items.size)

                            items.isNotEmpty().let {
                                indexPageBuilder.newItems = items.toMutableList()
                                updateLazyLoadingIndexPage()
                            }.run {
                                hideLoading()
                            }
                        }
                    }
                }, {
                    hideLoading()
                    finishLoadingItems()
                })
            )
    }
}