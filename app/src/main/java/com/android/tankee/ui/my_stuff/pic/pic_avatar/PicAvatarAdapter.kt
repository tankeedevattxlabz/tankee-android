package com.android.tankee.ui.my_stuff.pic.pic_avatar


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.getAppContext
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.OnRecyclerItemClickListener
import com.android.tankee.ui.request_options.avatarsRequestOptions
import com.android.tankee.utils.dp
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_avatar.view.*


class PicAvatarAdapter : RecyclerView.Adapter<PicAvatarAdapter.AvatarViewHolder>() {

    var items = mutableListOf<AvatarModel>()
    var listener: ((AvatarModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AvatarViewHolder {
        return AvatarViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_avatar, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AvatarViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    fun selectItem(item: AvatarModel) {
        for (i in 0 until itemCount) {
            if (items[i].id == item.id) {
                items[i].select()
                notifyItemChanged(i)
            } else if (items[i].isSelected) {
                items[i].unselected()
                notifyItemChanged(i)
            }
        }
    }

    fun getSelectedItem(): AvatarModel? {
        for (item in items) {
            if (item.isSelected) return item
        }
        return when {
            items.isEmpty() -> items[0]
            else -> null
        }
    }

    fun getSelectedItemPosition(): Int {
        for (i in 0 until itemCount) {
            if (items[i].isSelected) return i
        }
        return 0
    }

    inner class AvatarViewHolder(
            itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        fun onBind(item: AvatarModel) {
            Glide.with(getAppContext())
                    .load(item.image)
                    .apply(avatarsRequestOptions)
                    .into(itemView.avatarImage)

            TransitionManager.beginDelayedTransition(itemView.avatarItemRoot)
            if (item.isSelected) {
                itemView.avatarSelector.show()
            } else {
                itemView.avatarSelector.gone()
            }
            itemView.setOnClickListener { listener?.invoke(item) }
        }
    }
}