package com.android.tankee.ui.settings.debug_info


import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_debug_info.*


/**
 * Created by Sergey Kulyk on 2019-09-11.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
class DebugInfoBottomSheet(context: Context) : BottomSheetDialog(context) {

    lateinit var debugInfoModel: DebugInfoModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutInflater.inflate(R.layout.bottom_sheet_debug_info, null))

        debugInfoName.text = debugInfoModel.name
        copyButton.setOnClickListener { copyDebugInfo() }
        shareButton.setOnClickListener { shareDebugInfo() }
    }

    private fun copyDebugInfo() {
        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText(debugInfoModel.name, debugInfoModel.value ?: EMPTY_STRING)
        clipboard?.setPrimaryClip(clip)

        Toast.makeText(context, "${debugInfoModel.name} copied.", Toast.LENGTH_SHORT).show()

        dismiss()
    }

    private fun shareDebugInfo() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, debugInfoModel.toString())
        intent.type = "text/plain"
        context.startActivity(Intent.createChooser(intent, "Send your ${debugInfoModel.name}"))

        dismiss()
    }

    companion object {
        fun newInstance(context: Context, debugInfoModel: DebugInfoModel): DebugInfoBottomSheet {
            val debugInfoBottomSheet = DebugInfoBottomSheet(context)
            debugInfoBottomSheet.debugInfoModel = debugInfoModel
            return debugInfoBottomSheet
        }
    }
}