package com.android.tankee.ui.my_stuff.my_settings


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.dynamic_links.DynamicLinks
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarFragment.Companion.PIC_AVATAR_NAVIGATION_KEY
import com.android.tankee.ui.my_stuff.pic.pic_avatar.PicAvatarNavigation
import kotlinx.android.synthetic.main.fragment_my_settings.*


class MySettingsFragment : BaseFragment(), MySettingsView {

    private lateinit var mySettingsPresenter: MySettingsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mySettingsPresenter = MySettingsPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showToolbar()
        mySettingsPresenter.initAvatar()
        initEditButtonsNavigation()
//        view.postDelayed({
        getDynamicLink()
//        }, 500)
    }

    override fun setAvatar(avatarDrawerId: Int) {
        selectedAvatar.setImageResource(avatarDrawerId)
    }

    private fun initEditButtonsNavigation() {
        picBtn.onClickAction = { navigateToPicAvatar() }
        favoriteGamesBtn.onClickAction = { navigateToFavoriteGames() }
        favoriteGamersBtn.onClickAction = { navigateToFavoriteGamers() }
    }

    private fun navigateToPicAvatar() {
        val extras = getMySettingsPickNavigationExtras()
        val args = getMySettingsPickNavigationArgs()
        findNavController().navigate(R.id.picAvatarFragment, args, null, extras)
        hideToolbar()
    }

    private fun navigateToFavoriteGames() {
        val extras = getMySettingsPickNavigationExtras()
        val args = getMySettingsPickNavigationArgs()
        findNavController().navigate(R.id.picFavotiteGamesFragment, args, null, extras)
        hideToolbar()
    }

    private fun navigateToFavoriteGamers() {
        val extras = getMySettingsPickNavigationExtras()
        val args = getMySettingsPickNavigationArgs()
        findNavController().navigate(R.id.picFavoriteGamersFragment, args, null, extras)
        hideToolbar()
    }

    private fun getMySettingsPickNavigationArgs(): Bundle {
        return Bundle().apply {
            putString(PIC_AVATAR_NAVIGATION_KEY, PicAvatarNavigation.MySettingsFlow.value)
        }
    }

    private fun getMySettingsPickNavigationExtras(): FragmentNavigator.Extras {
        return FragmentNavigatorExtras(
            selectedAvatar to "avatar"
        )
    }

    private fun getDynamicLink() {
        if (AnonymousUserRepo().isNotUserAnonymous()) {
            val linkPath = SessionDataHolder.dynamicLink?.getPath()

            linkPath.let {
                when (it) {
                    DynamicLinks.AvatarSelectionPage.path -> navigateToPicAvatar()
                    DynamicLinks.FavoriteGamesPage.path -> navigateToFavoriteGames()
                    DynamicLinks.FavoriteGamersPage.path -> navigateToFavoriteGamers()
                    else -> return
                }
                SessionDataHolder.clearDynamicLink()
            }
        }
    }

}
