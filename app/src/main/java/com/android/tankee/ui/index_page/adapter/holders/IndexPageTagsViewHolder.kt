package com.android.tankee.ui.index_page.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.createHorizontalLayoutManager
import com.android.tankee.ui.index_page.adapter.IndexPageAdapter
import com.android.tankee.ui.index_page.adapter.models.TagsHolderModel
import com.android.tankee.ui.index_page.tags.TagModel
import com.android.tankee.ui.index_page.tags.TagsAdapter
import kotlinx.android.synthetic.main.item_index_page_tags.view.*

class IndexPageTagsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var indexTags = false

    fun onBind(tagsHolderModel: TagsHolderModel, loadTagItemListener: IndexPageAdapter.LoadTagItemsListener?) {
        if (!indexTags) {
            itemView.tagsRecycler.layoutManager = createHorizontalLayoutManager()

            val tagsAdapter = TagsAdapter(tagsHolderModel.selected)
            itemView.tagsRecycler.adapter = tagsAdapter
            tagsAdapter.items = tagsHolderModel.tags.toMutableList()
            itemView.tagsRecycler.scrollToPosition(tagsHolderModel.selected)
            tagsAdapter.listener = object : BaseRecyclerAdapter.OnRecyclerItemClickListener<TagModel> {
                override fun onItemClicked(item: TagModel) {
                    tagsHolderModel.selected = tagsHolderModel.tags.indexOf(item)
                    loadTagItemListener?.loadTagItems(item)
                }
            }

            indexTags = true
        }
    }
}