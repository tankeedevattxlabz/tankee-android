package com.android.tankee.ui.index_page.favorite_button


import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.index_page.GameContent
import com.android.tankee.ui.index_page.GamerContent
import com.android.tankee.ui.index_page.IndexPageContent


class IndexPageFavoriteButtonPresenter(
    private val favoriteButtonView: FavoriteButtonView) {

    fun getGameInfo(gameId: Int) {
        TankeeRest.api.getGame(gameId).enqueue(
                favoriteButtonView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    items[0].like?.let { favoriteButtonView.setFavoriteButtonState(it) }
                                }
                            }
                        },
                        {
                        }
                )
        )
    }

    fun getGamerInfo(gamerId: Int) {
        TankeeRest.api.getGamer(gamerId).enqueue(
                favoriteButtonView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    items[0].like?.let { favoriteButtonView.setFavoriteButtonState(it) }
                                }
                            }
                        },
                        {
                        }
                )
        )
    }

    fun setFavoriteGame(gameId: Int) {
        TankeeRest.api.addLikeToGame(gameId).enqueue(
            favoriteButtonView.createCallBack(
                {

                },
                {
                    favoriteButtonView.setFavoriteButtonState(false)
                }
            )
        )
    }

    fun deleteFavoriteGame(gameId: Int) {
        TankeeRest.api.deleteLikeToGame(gameId).enqueue(
            favoriteButtonView.createCallBack(
                {
                },
                {
                    favoriteButtonView.setFavoriteButtonState(true)
                }
            )
        )
    }

    fun setFavoriteGamer(gamerId: Int) {
        TankeeRest.api.addLikeToGamer(gamerId).enqueue(
            favoriteButtonView.createCallBack(
                {

                },
                {
                    favoriteButtonView.setFavoriteButtonState(false)
                }
            )
        )
    }

    fun deleteFavoriteGamer(gamerId: Int) {
        TankeeRest.api.deleteLikeToGamer(gamerId).enqueue(
            favoriteButtonView.createCallBack(
                {

                },
                {
                    favoriteButtonView.setFavoriteButtonState(true)
                }
            )
        )
    }

}

interface FavoriteButtonView : BaseView {

    fun setFavoriteButtonState(isFavorite: Boolean)

}

data class FavoriteStateModel(val id: Int, val isFavorite: Boolean = false)

