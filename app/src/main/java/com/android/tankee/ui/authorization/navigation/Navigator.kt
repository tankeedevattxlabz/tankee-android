package com.android.tankee.ui.authorization.navigation


import androidx.fragment.app.FragmentManager
import com.android.tankee.ui.base.BaseFragment


class Navigator private constructor() {

    var countId: Int = 0
    var fragmentManager: FragmentManager? = null
    private var lastFragment: BaseFragment? = null

    companion object {
        @JvmStatic
        val instance = Navigator()
    }

    fun replaceFragment(
            fragment: BaseFragment,
            tag: String?,
            fragmentAnimationModel: FragmentAnimationModel? = null
    ) {
        try {
            val userFragment: BaseFragment = fragment

            val ft = fragmentManager?.beginTransaction()

            fragmentAnimationModel?.let {
                ft?.setCustomAnimations(
                        it.enterAnim,
                        it.enterAnim2,
                        it.exitAnim,
                        it.exitAnim2
                )
            }

            ft?.replace(countId, userFragment, tag)
            ft?.addToBackStack(null)
            ft?.commit()

            lastFragment = fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun replaceFragment(
            fragment: BaseFragment,
            tag: String?) {

        replaceFragment(fragment, tag, FragmentAnimationStore.leftToRightAnimation)
    }

    fun replaceStartFragment(
            fragment: BaseFragment,
            tag: String?) {

        replaceFragment(fragment, tag, FragmentAnimationStore.donToUpAnimation)
    }

    fun removeFromBackStack() {
        fragmentManager?.popBackStackImmediate()
    }
}
