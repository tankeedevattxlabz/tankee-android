package com.android.tankee.ui.index_page.tags

import com.android.tankee.ui.index_page.adapter.models.TagsHolderModel
import java.util.*
import kotlin.Comparator

class TagsBuilder {

    fun buildTags(tags: List<String>): TagsHolderModel? {
        Collections.sort(tags, TagsComparator())

        val sortedTags = tags.toMutableList()
        sortedTags.forEach { it.trim() }
        sortedTags.add(0, TAG_ALL_VIDEOS)

        return buildTagModels(sortedTags)
    }

    private fun buildTagModels(tags: List<String>): TagsHolderModel? {
        val res = mutableListOf<TagModel>()
        if (tags.isEmpty()) return null

        for (i in tags) {
            res.add(TagModel(i, false))
        }
        res[0].selected = true

        return TagsHolderModel(res, 0)
    }

    companion object {
        const val TAG_ALL_VIDEOS = "All Videos"
    }
}

class TagsComparator : Comparator<String> {
    override fun compare(o1: String?, o2: String?): Int {
        return o1!!.toLowerCase().compareTo(o2!!.toLowerCase())
    }
}