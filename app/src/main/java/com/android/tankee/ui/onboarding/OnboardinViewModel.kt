package com.android.tankee.ui.onboarding


import android.text.TextUtils
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.database.TankeeDbHelper
import com.android.tankee.extentions.isNull
import com.android.tankee.logs.AppLogger
import com.android.tankee.notifications.TankeeMessagingService.Companion.FCM_TAG
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.repo.user.FcmTokenRepo
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.users.UserModel
import com.android.tankee.rest.user.GetUserPostBody
import com.android.tankee.ui.base.BaseView
import com.google.firebase.iid.FirebaseInstanceId
import java.util.*


class OnboardinViewModel(val view: BaseView) {

    fun checkLogin(onUserExist: () -> Unit, onNeedLogin: () -> Unit) {
//        val empty = true
        val userUid = TankeePrefs.userUid
        val empty = TextUtils.isEmpty(userUid)
        if (empty) {
            onNeedLogin()
        } else {
            fetchExistingUser {
                onUserExist()
            }
        }
    }

    private fun fetchExistingUser(onSuccess: () -> Unit) {
        TankeeRest.api.getUserSelf().enqueue(view.createCallBack({
            val user = it.body()?.items?.get(0)
            if (AnonymousUserRepo().isNotUserAnonymous()) {
                user?.let { userModel ->
                    TankeeDbHelper.insertUserModel(userModel)
                }
            }

            updateFcmTokenAndTimeZone(user)

            if (user != null && !TextUtils.isEmpty(user.token)) {
                TankeePrefs.token = user.token ?: ""
                SessionDataHolder.user = user
                initPlayerConfigurations { onSuccess() }
            } else {
                TankeePrefs.token = ""
                TankeePrefs.userUid = ""
            }
        }, {
            TankeePrefs.token = ""
            TankeePrefs.userUid = ""
        }))
    }

    private fun updateFcmTokenAndTimeZone(user: UserModel?) {
        val userFcmToken = user?.fcmToken
        val firebaseFcmToken = FirebaseInstanceId.getInstance().token
        AppLogger.logDebug(TAG, "User $FCM_TAG Token (on Tankee backend)", userFcmToken)
        AppLogger.logDebug(TAG, "Current $FCM_TAG Token (on phone)", userFcmToken)

        val userTimeZone = user?.timezone
        val currentTimeZone = Calendar.getInstance().timeZone.toString()
        AppLogger.logDebug(TAG, "TimeZone", userTimeZone.toString())
        AppLogger.logDebug(TAG, "TimeZone", currentTimeZone)

        if (
                userFcmToken.isNull() || userTimeZone.isNull()
                || userFcmToken != firebaseFcmToken
                || userTimeZone != currentTimeZone
        ) {
            firebaseFcmToken?.let { FcmTokenRepo(view).updateFcmTokenAndTimeZone() }
        }
    }

    fun loginAndSaveUserCreds(onSuccess: () -> Unit) {
        TankeeRest.api.createUser(GetUserPostBody(getUserUid(), true))
                .enqueue(view.createCallBack({ response ->
                    val user = response.body()?.items?.get(0)
                    if (user != null && !TextUtils.isEmpty(user.token)) {
                        TankeePrefs.token = user.token ?: ""
                        SessionDataHolder.user = user
                        initPlayerConfigurations { onSuccess() }
                    }
                }, {}))
    }

    private fun getUserUid(): String {
        val savedUid = TankeePrefs.userUid
        return if (TextUtils.isEmpty(savedUid)) {
            val randomUUID = UUID.randomUUID().toString()
            TankeePrefs.userUid = randomUUID
            randomUUID
        } else {
            savedUid
        }
    }

    private fun initPlayerConfigurations(onSuccess: () -> Unit) {
        TankeeRest.api.getConfigurationsByVersion().enqueue(
                view.createCallBack(
                        { response ->
                            response.body()?.let { body ->
                                body.items?.isNotEmpty()?.let { isNotEmpty ->
                                    if (isNotEmpty) {
                                        TankeePrefs.isNativePlayer = body.items!![0].isAndroidNative!!
                                    }
                                }
                            }
                            onSuccess()
                        },
                        {
                            onSuccess()
                        }
                )
        )
    }

    companion object {
        var TAG = OnboardinViewModel::class.java.simpleName
    }

}