package com.android.tankee.ui.home.toolbar.toolbar_item.toolbar_notification_item


import com.android.tankee.logs.AppLogger
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.base.BaseView


class ToolbarNotificationMarkerPresenter(private val notificationItemView: ToolbarNotificationItemView) {

    fun showOrHideMarker() {
        TankeeRest.api.getNotifications().enqueue(
            notificationItemView.createCallBack(
                { response ->
                    val visibility = hasUnViewedNotification(
                        response.body()?.items ?: listOf()
                    )
                    AppLogger.logInfo("ToolbarNotificationMarkerPresenter", visibility.toString())
                    notificationItemView.setMarkerVisibility(
                        visibility
                    )
                }
            )
        )
    }

    private fun hasUnViewedNotification(list: List<NotificationModel>): Boolean {
        return list.any { model -> model.isViewed == false }
    }

}


interface ToolbarNotificationItemView : BaseView {

    fun setMarkerVisibility(visibility: Boolean)

}
