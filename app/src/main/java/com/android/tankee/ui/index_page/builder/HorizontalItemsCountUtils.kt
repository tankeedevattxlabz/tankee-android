package com.android.tankee.ui.index_page.builder

import com.android.tankee.R
import com.android.tankee.TankeeApp
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.isTable
import com.android.tankee.utils.px

class HorizontalItemsCountUtils {

    fun getHorizontalItemsCount(itemMarker: SectionItemMarker): Int {
        return if (itemMarker is Game) {
            if (isTable()) {
                GAMES_COUNT_TABLET
            } else {
                getGamesItemsHorizontalCountPhone(GAMES_COUNT_TABLET)
            }
        } else if (itemMarker is Gamer) {
            if (isTable()) {
                GAMERS_COUNT_TABLET
            } else {
                getGamersItemsHorizontalCountPhone(GAMERS_COUNT_TABLET)
            }
        } else if (itemMarker is Video) {
            if (isTable()) {
                VIDEOS_COUNT_TABLET
            } else {
                if (getVideoItemsHorizontalCountPhone(VIDEOS_COUNT_PHONE_MEDIUM)) {
                    VIDEOS_COUNT_PHONE_MEDIUM
                } else {
                    VIDEOS_COUNT_PHONE_SMALL
                }
            }
        } else {
            DEFAULT_COUNT
        }
    }

    // TODO refact
    private fun getVideoItemsHorizontalCountPhone(horizontalItemsCount: Int): Boolean {
        return TankeeApp.display.width.px() >=
                (getAppContext().resources.getDimension(R.dimen.index_page_items_margin_start).toInt().px() * 2
                        + getAppContext().resources.getDimension(R.dimen.video_item_margin).toInt().px() * 2 * horizontalItemsCount
                        + getAppContext().resources.getDimension(R.dimen.section_item_with).toInt().px() * horizontalItemsCount)
    }

    // TODO refact
    private fun getGamesItemsHorizontalCountPhone(horizontalItemsCount: Int): Int {
        val containsItems = (TankeeApp.display.width.px() - 70) >=
                (getAppContext().resources.getDimension(R.dimen.index_page_items_margin_start).toInt().px() * 2
                        + getAppContext().resources.getDimension(R.dimen.game_start_margin).toInt().px() * 2 * horizontalItemsCount
                        + getAppContext().resources.getDimension(R.dimen.game_image_width).toInt().px() * horizontalItemsCount)

        return if (containsItems) {
            GAMES_COUNT_TABLET
        } else {
            GAMES_COUNT_PHONE
        }
    }

    // TODO refact
    private fun getGamersItemsHorizontalCountPhone(horizontalItemsCount: Int): Int {
        val i = (getAppContext().resources.getDimension(R.dimen.index_page_items_margin_start).toInt().px() * 2
                + getAppContext().resources.getDimension(
                R.dimen.game_start_margin
        ).toInt().px() * 2 * horizontalItemsCount
                + getAppContext().resources.getDimension(R.dimen.game_image_width).toInt().px() * horizontalItemsCount)
        val containsItems = (TankeeApp.display.width.px() - 70) >=
                i

        return if (containsItems) {
            GAMERS_COUNT_TABLET
        } else {
            GAMERS_COUNT_PHONE
        }
    }

    companion object {
        const val DEFAULT_COUNT = 4

        const val GAMES_COUNT_PHONE = 5
        const val GAMES_COUNT_TABLET = 6

        const val GAMERS_COUNT_PHONE = 5
        const val GAMERS_COUNT_TABLET = 6

        const val VIDEOS_COUNT_PHONE_SMALL = 2
        const val VIDEOS_COUNT_PHONE_MEDIUM = 3
        const val VIDEOS_COUNT_TABLET = 4
    }
}