package com.android.tankee.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView

/**
 * Base generic RecyclerView adapter.
 * Handles base logic such as adding/removing items,
 * setting listener, binding ViewHolders.
 * Extend adapter for appropriate use case.
 *
 * @param <T> type of item, which will be used in the adapter's.
 * @param <L> click listener {@link BaseRecyclerListener}
 * @para <VH> ViewHolder {@link BaseViewHolder}
 */
abstract class BaseRecyclerAdapter<T, L : BaseRecyclerAdapter.BaseRecyclerListener, VH : BaseRecyclerAdapter.BaseVideoHolder<T, L>>
    : RecyclerView.Adapter<VH>() {

    /**
     * Set items to adapter and notifies data that set has been changed.
     * If value is null items = listOf().
     */
    var items: MutableList<T> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun clear() {
        items = mutableListOf()
        notifyDataSetChanged()
    }

    var listener: L? = null

    /**
     * To be implemented in a specific adapter
     *
     * @param parent The ViewGroup into with the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of the given view type.
     */
    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    /**
     * Called by RecyclerView to display the data at the specified position. this method should.
     * This method should update the contents of the itemView to reflect the item at the given position.
     *
     * @param holder    The ViewHolder with should be updated to represent contents of the item at
     *                  the given position in the data set
     * @param position  The position of the item within th adapter's data set.
     */
    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = items[position]
        holder.onBind(item, listener)
    }

    override fun getItemCount(): Int = items.size

    /**
     * Inflate a view.
     */
    protected fun inflate(@LayoutRes layout: Int, parent: ViewGroup?, attachToRoot: Boolean): View {
        return LayoutInflater.from(parent?.context).inflate(layout, parent, attachToRoot)
    }

    abstract class BaseVideoHolder<T, L : BaseRecyclerListener>(itemView: View) : RecyclerView.ViewHolder(itemView) {

        /**
         * Bind data to the item and set listener if needed.
         *
         * @param itemView associated with item.
         * @param listener {@link BaseRecyclerListener} witch has to be set at the item (if not `null`).
         */
        abstract fun onBind(item: T, @Nullable listener: L?)
    }

    interface BaseRecyclerListener

    interface OnRecyclerItemClickListener<T> : BaseRecyclerListener {

        /**
         * Game has been clicked.
         *
         * @param item associated with th clicked item
         */
        fun onItemClicked(item: T)
    }

    interface OnRecyclerClickListener : BaseRecyclerListener {

        /**
         * RecyclerView item has been clicked
         *
         * @params id item id
         */
        fun onItemClicked(id: Int)
    }
}