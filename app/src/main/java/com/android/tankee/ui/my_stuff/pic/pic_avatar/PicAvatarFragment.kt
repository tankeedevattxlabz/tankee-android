package com.android.tankee.ui.my_stuff.pic.pic_avatar


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.ui.my_stuff.pic.base.PickBaseFragment
import kotlinx.android.synthetic.main.fragment_pic_avatar.*


class PicAvatarFragment : PickBaseFragment(), PicAvatarView {

    private lateinit var picAvatarPresenter: PicAvatarPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        picAvatarPresenter = PicAvatarPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pic_avatar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (isRegistrationFlow()) {
            picAvatarBack.gone()
        } else {
            picAvatarBack.setOnClickListener { onBackButtonClick() }
        }

        val initAction = {
            picAvatarPresenter.initAvatars()
            picAvatarPresenter.initSelectedAvatar()
        }
        if (isRegistrationFlow()) {
            view.postDelayed(initAction, 10)
        } else {
            initAction()
        }

        initActionButton()
    }

    override fun onBackPressed(): Boolean {
        showToolbar()
        return super.onBackPressed()
    }

    override fun onBackButtonClick() {
        showToolbar()
        findNavController().popBackStack()
    }

    override fun setSelectedAvatar(avatarModel: AvatarModel) {
        selectedAvatar.setImageResource(avatarModel.image)
        picAvatarParallaxView.onAvatarSelected?.invoke(avatarModel)
        picAvatarParallaxView.setSelectedAvatar(avatarModel)
        picAvatarParallaxView.scrollToSelectedItem()
    }

    override fun setAvatars(avatars: List<AvatarModel>) {
        picAvatarParallaxView.initParallaxItems(avatars)
        picAvatarParallaxView.onAvatarSelected = { avatarModel ->
            selectedAvatar.setImageResource(avatarModel.image)
        }
    }

    private fun initActionButton() {
        arguments?.let {
            when (it.getString(PIC_AVATAR_NAVIGATION_KEY)) {
                PicAvatarNavigation.MySettingsFlow.value -> {
                    doneButton.onClickAction = { onDoneClick() }
                    doneButton.show()
                    nextButton.gone()
                }
                PicAvatarNavigation.RegistrationFlow.value -> {
                    nextButton.onClickAction = { onNextClick() }
                    doneButton.gone()
                    nextButton.show()
                }
            }
        }
    }

    private fun onDoneClick() {
        picAvatarParallaxView.getSelectedAvatar()
            ?.let { picAvatarPresenter.saveSelectedAvatar(it) { onBackButtonClick() } }
    }

    private fun onNextClick() {
        picAvatarParallaxView.getSelectedAvatar()
            ?.let { picAvatarPresenter.saveSelectedAvatar(it) { navigateToPickFavoriteGamesScreen() } }
    }

    private fun navigateToPickFavoriteGamesScreen() {
        findNavController().navigate(R.id.action_picAvatarFragment_to_picFavotiteGamesFragment, arguments)
    }

    companion object {
        const val PIC_AVATAR_NAVIGATION_KEY = "pic_avatar_navigation_key"
    }
}

enum class PicAvatarNavigation(val value: String) {
    MySettingsFlow("my_settings_flow"),
    RegistrationFlow("reg_flow"),
}
