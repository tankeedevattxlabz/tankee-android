package com.android.tankee.ui.my_stuff.pic.pic_favorite_gamers.choose_favorite_gamers


import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.OnFavoriteGamersSelectedAction
import com.android.tankee.event_bus.OnFavoriteGamesSelectedAction
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.ui.my_stuff.pic.base.PickBaseView


class ChooseFavoriteGamersPresenter(private val chooseFavoriteGamersView: ChooseFavoriteGamersFragment) {

    private val favoriteUnfavorableGamers = mutableListOf<Gamer>()

    fun getGamers() {
        chooseFavoriteGamersView.showLoading()
        TankeeRest.api.getGamers(false)
                .enqueue(chooseFavoriteGamersView.createCallBack(
                        { response ->
                            chooseFavoriteGamersView.hideLoading()
                            response.body()?.items?.let { items ->
                                val favoriteItems = items.filter { gamer -> gamer.isFavoriteGamer() }

                                EventPoster.postEvent(
                                        OnFavoriteGamesSelectedAction(favoriteItems.isNotEmpty())
                                )
                                chooseFavoriteGamersView.setGamers(items)
                            }
                        },
                        {
                            chooseFavoriteGamersView.hideLoading()
                        }
                ))
    }

    fun addGamerToStore(gamer: Gamer) {
        if (isGameNotChanged(gamer)) {
            favoriteUnfavorableGamers.add(gamer)
        } else {
            favoriteUnfavorableGamers.remove(gamer)
        }

        EventPoster.postEvent(
                OnFavoriteGamersSelectedAction(favoriteUnfavorableGamers.isNotEmpty())
        )
    }

    private fun isGameNotChanged(gamer: Gamer): Boolean {
        return favoriteUnfavorableGamers.find { changedGamer -> gamer.id == changedGamer.id } == null
    }

    fun updateGames() {
        chooseFavoriteGamersView.showLoading()

        favoriteUnfavorableGamers.forEach { gamer ->
            gamer.like?.let { liked ->
                if (liked) {
                    addLikeToGame(gamer)
                } else {
                    deleteLikeToGame(gamer)
                }
            }
        }
        chooseFavoriteGamersView.hideLoading()
        chooseFavoriteGamersView.onGamersUpdatesNavigation()
    }

    private fun addLikeToGame(gamer: Gamer) {
        gamer.id?.let {
            TankeeRest.api.addLikeToGamer(it)
                    .enqueue(chooseFavoriteGamersView.createCallBack(
                            {
                            },
                            {
                            }
                    )
                    )
        }
    }

    private fun deleteLikeToGame(gamer: Gamer) {
        gamer.id?.let {
            TankeeRest.api.deleteLikeToGamer(it)
                    .enqueue(chooseFavoriteGamersView.createCallBack(
                            {
                            },
                            {
                            }
                    )
                    )
        }
    }
}


interface ChooseFavoriteGamersView : PickBaseView {

    fun setGamers(gamers: List<Gamer>)

    fun onGamersUpdatesNavigation()

}