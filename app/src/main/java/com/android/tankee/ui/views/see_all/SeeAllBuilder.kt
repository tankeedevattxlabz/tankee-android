package com.android.tankee.ui.views.see_all

import com.android.tankee.R

class SeeAllBuilder {

    private val colorBlue = R.color.colorBlue
    private val colorOrange = R.color.colorOrange
    private val colorRed = R.color.colorRed

    private val drawableBlue = R.drawable.ic_see_all_blue
    private val drawableOrange = R.drawable.ic_see_all_orange
    private val drawableRed = R.drawable.ic_see_all_red

    private var seeAllStyle: SeeAllStyle = SeeAllStyle.BLUE
    private var seeAllType: SeeAllType = SeeAllType.SECTION
    private var sectionId: Int = -1
    private var sectionName: String = ""

    fun setStyle(style: SeeAllStyle): SeeAllBuilder {
        seeAllStyle = style
        return this
    }

    fun setType(seeAllType: SeeAllType): SeeAllBuilder {
        this.seeAllType = seeAllType
        return this
    }

    fun setSectionId(sectionId: Int): SeeAllBuilder {
        this.sectionId = sectionId
        return this
    }

    fun setSectionName(sectionName: String?): SeeAllBuilder {
        sectionName?.let { this.sectionName = it }
        return this
    }

    fun build(): SeeAllItem {
        val seeAll = if (sectionId == -1) {
            buildSimple()
        } else {
            buildWithSectionId()
        }

        seeAll.seeAllType = seeAllType
        return seeAll
    }

    private fun buildSimple(): SeeAllItem {
        return when (seeAllStyle) {
            SeeAllStyle.BLUE -> buildBlue()
            SeeAllStyle.ORANGE -> buildOrange()
            SeeAllStyle.RED -> buildRed()
        }
    }

    private fun buildWithSectionId(): SeeAllItem {
        return when (seeAllStyle) {
            SeeAllStyle.BLUE -> buildBlue(sectionId)
            SeeAllStyle.ORANGE -> buildOrange(sectionId)
            SeeAllStyle.RED -> buildRed(sectionId)
        }
    }

    private fun buildBlue() = build(colorBlue, drawableBlue)

    private fun buildOrange() = build(colorOrange, drawableOrange)

    private fun buildRed() = build(colorRed, drawableRed)

    private fun buildBlue(sectionId: Int) = build(colorBlue, drawableBlue, sectionId)

    private fun buildOrange(sectionId: Int) = build(colorOrange, drawableOrange, sectionId)

    private fun buildRed(sectionId: Int) = build(colorRed, drawableRed, sectionId)

    private fun build(colorId: Int, drawerId: Int, sectionId: Int = -1) = if (sectionId == -1) {
        SeeAllItem(colorId, drawerId)
    } else {
        SeeAllItemVideosSection(sectionId, sectionName, colorId, drawerId)
    }

    fun reset() {
        sectionId = -1
    }

}

enum class SeeAllStyle {
    BLUE,
    ORANGE,
    RED
}