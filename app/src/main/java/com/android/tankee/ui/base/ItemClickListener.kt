package com.android.tankee.ui.base

import com.android.tankee.ui.views.see_all.SectionItemMarker

interface ItemClickListener {
    fun onClick(item: SectionItemMarker)
}