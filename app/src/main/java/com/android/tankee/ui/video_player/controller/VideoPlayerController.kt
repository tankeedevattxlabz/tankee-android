package com.android.tankee.ui.video_player.controller

import androidx.navigation.NavController
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.video_player.VIdeoPlayerFragment

object VideoPlayerController {

    /**
     * @param simplePlayerType  is responsible for existing ui configuration for player screen
     */
    fun openVideoPlayer(video: Video, navController: NavController, simplePlayerType: Boolean = false) {
        VIdeoPlayerFragment.showVideo(video, navController, hideMoreVideoBase = simplePlayerType)
    }

    /**
     * @param simplePlayerType  is responsible for existing ui configuration for player screen
     */
    fun openVideoPlayer(videoId: Int, navController: NavController, simplePlayerType: Boolean = false) {
        VIdeoPlayerFragment.showVideo(videoId, navController, hideMoreVideoBase = simplePlayerType)
    }

    fun isExoPlayer() = TankeePrefs.isNativePlayer

    fun isJWPlayer() = !isExoPlayer()

}