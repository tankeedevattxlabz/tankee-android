package com.android.tankee.ui.authorization.join.parent_email


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.TANKEE_PRICACY
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.extentions.isNotNull
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.join.generate_name.GenerateNameFragment
import com.android.tankee.ui.authorization.navigation.Navigator
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.custom_tabs.CustomTabsFactory
import kotlinx.android.synthetic.main.fragment_parent_email.*
import kotlinx.android.synthetic.main.view_header_authorization.*


class ParentEmailFragment : BaseFragment(), ParentEmailViewModel.ParentEmailView {

    private lateinit var parentEmailViewModel: ParentEmailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        parentEmailViewModel = ParentEmailViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_parent_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authorizationBack.setOnClickListener { Navigator.instance.removeFromBackStack() }
        authorizationClose.setOnClickListener { AuthoEventHelper.closeEvent() }

        parentEmail.updateRootContainer = {
            startRootAnimation()
        }

        nextButton.onClickAction = {
            AnalyticsManager.tankeeJoinParentEmailNextButtonEvent()
            executeCheckParentEmail()
        }
        privacyPolicy.setOnClickListener { openPrivacyPolicy() }

//        if(BuildConfig.DEBUG)
//        parentEmailViewModel.setTestData()
//        parentEmailViewModel.setTestData()
    }

    override fun navigateToGenerateDisplayName() {
        Navigator.instance.replaceFragment(GenerateNameFragment(), GenerateNameFragment.TAG)
    }

    override fun setParentEmailEmptyError() {
        startRootAnimation()
        parentEmail?.setErrorText(getString(R.string.error_parent_email_required))
    }

    override fun setIncorrectEmailFormatError() {
        startRootAnimation()
        parentEmail?.setErrorText(getString(R.string.error_incorrect_email_format))
    }

    override fun setTestData(parentEmailValue: String) {
        parentEmail?.setText(parentEmailValue)
    }

    private fun executeCheckParentEmail() {
        if (parentEmail.isNotNull()) {
            parentEmailViewModel.checkParentEmail(parentEmail.getText())
        }
    }

    private fun startRootAnimation() {
        parentEmailRoot?.let { TransitionManager.beginDelayedTransition(it) }
    }

    private fun openPrivacyPolicy() {
        CustomTabsFactory.openUrl(context!!, TANKEE_PRICACY)
    }

    companion object {
        var TAG: String = ParentEmailFragment::class.java.simpleName
    }
}
