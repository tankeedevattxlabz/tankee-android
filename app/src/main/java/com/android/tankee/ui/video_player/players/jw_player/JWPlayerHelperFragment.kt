package com.android.tankee.ui.video_player.players.jw_player


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.ui.video_player.VIDEO_ARG
import com.android.tankee.ui.video_player.advertising.AdsController
import com.android.tankee.ui.video_player.players.BasePlayerFragment
import com.android.tankee.ui.video_player.players.jw_player.handlers.JWEventHandler
import com.android.tankee.ui.video_player.players.jw_player.handlers.JWPlayerFullscreenHandler
import com.android.tankee.ui.video_player.players.jw_player.handlers.KeepScreenOnHandler
import com.android.tankee.ui.video_player.video_utils.VideoUrlBuilder
import com.longtailvideo.jwplayer.JWPlayerSupportFragment
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.configuration.PlayerConfig
import com.longtailvideo.jwplayer.events.FullscreenEvent
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem
import kotlinx.android.synthetic.main.fragment_jwplayer_helper.*


class JWPlayerHelperFragment : BasePlayerFragment {

    constructor() : super() {
        // Add empty constructor to prevent Fragment$InstantiationException
    }
    
    override fun setControls() {
        mPlayerView?.let {
            mPlayerView?.controls = showControls
        }
    }

    override fun initPlayer() {

    }

    /**
     * A reference to the [JWPlayerSupportFragment].
     */
    private var playerSupportFragment: JWPlayerSupportFragment? = null

    /**
     * A reference to the [JWPlayerView] used by the JWPlayerSupportFragment.
     */
//    private var mPlayerView: JWPlayerView? = null
    private var mEventHandler: JWEventHandler? = null

    private fun initVideoFromArguments() {
        video = arguments?.getParcelable(VIDEO_ARG)
    }

    override fun moveToPosition(position: Int) {
        playbackPosition = position.toLong()
        mPlayerView?.seek(playbackPosition.toDouble())
    }

    override fun onStart() {
        super.onStart()
        mPlayerView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mPlayerView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mPlayerView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mPlayerView?.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPlayerView?.onDestroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerView?.onDestroy()

        playerSupportFragment = null
//        mPlayerView = null

        mEventHandler = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVideoFromArguments()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_jwplayer_helper, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val jwKey = video?.jwKey
        addPlayerFragment(jwKey)

        configurePlayer()

        if (video is ClipVideo) {
            forceToPosition(view)
        }
    }

    private fun forceToPosition(view: View) {
        if (playbackPosition > 0 && getPlayerPosition() < playbackPosition)
            view.postDelayed({
                mPlayerView?.seek(playbackPosition.toDouble())
                forceToPosition(view)
            }, 150)
    }

    override fun startPlayer() {
        mPlayerView?.onStart()
    }

    override fun resumePlayer() {
        mPlayerView?.onResume()
    }

    override fun stopPlayer() {
        mPlayerView?.onStop()
    }

    override fun pausePlayer() {
        mPlayerView?.pause()
    }

    override fun destroyPlayer() {
        mPlayerView?.onDestroy()
    }

    override fun getPlayerPosition(): Long {
        val position = mPlayerView?.position ?: 0.0
        return position.toLong()
    }

    override fun getPlayerView(): View? {
        return mPlayerView
    }

    override fun setFullscreen() {
        mPlayerView?.setFullscreen(false, false)
    }

    override fun seekToPosition(position: Int, onSeekListener: (() -> Unit)?) {
        if (video !is ClipVideo) {
            mPlayerView?.seek(position.toDouble())
            mPlayerView?.addOnSeekListener { onSeekListener?.invoke() }
        }
    }

    private fun addPlayerFragment(jwKey: String?) {
        val playerConfig = buildConfig(jwKey)
        mPlayerView.setup(playerConfig)
//        playerSupportFragment = JWPlayerSupportFragment.newInstance(playerConfig)

        // Attach the Fragment to our layout
//        val fm = childFragmentManager
//        if (fm.findFragmentByTag("subPlayer") == null) {
//            val ft = fm.beginTransaction()
//            ft.add(R.id.playerFragmentContainer, playerSupportFragment!!, "subPlayer")
//            ft.commit()
//            // Make sure all the pending fragment transactions have been completed, otherwise
//            // playerSupportFragment.getPlayer() may return null
//            fm.executePendingTransactions()
//        }
    }

    private fun buildConfig(jwKey: String?): PlayerConfig? {
        val builder = PlayerConfig.Builder()
        val playlistItem = PlaylistItem(VideoUrlBuilder.buildUrlString(jwKey))

        if (video?.id != null && AdsController.showJwpVideoPlayerAds()) {
            AdsController(video).startVideoPlayerWithAdsIfNeeded(builder)
        }
        return builder
                .autostart(video !is ClipVideo)
                .playlist(listOf(playlistItem))
                .build()
    }

    private fun configurePlayer() {
        // Get a reference to the JWPlayerView from the fragment
//        mPlayerView = playerSupportFragment?.player
        video?.startedAt?.toDouble()?.let {
            mPlayerView?.postDelayed({
                mPlayerView?.seek(it)
            }, 500)
        }
        playerSupportFragment?.setFullscreenOnDeviceRotate(false)
        // Keep the screen on during playback
        mPlayerView?.let {
            KeepScreenOnHandler(
                    mPlayerView!!,
                    activity?.window!!
            )
        }

        // Instantiate the JW Player event handler class
        if (mPlayerView != null) {
            mEventHandler = object : JWEventHandler(mPlayerView!!) {
                override fun onFullscreen(fullscreen: FullscreenEvent) {
                    super.onFullscreen(fullscreen)
                    playerEventListener?.onFullScreen(fullscreen.fullscreen)
                }
            }
            mPlayerView?.setFullscreen(false, true)
            mPlayerView?.setFullscreenHandler(
                    JWPlayerFullscreenHandler(
                            activity!!,
                            mPlayerView!!
                    )
            )
            playerSupportFragment?.setFullscreenOnDeviceRotate(false)
            mPlayerView?.play()

            mEventHandler?.onTime = {
                playerEventListener?.onPlayerTime(getPlayerPosition())
            }
            mEventHandler?.resetState = {
                playerEventListener?.resetState()
            }
            mEventHandler?.onComplete = {
                playerEventListener?.onCompleteVideo()
            }
            mEventHandler?.onAdPlay = {
                playerEventListener?.onAdPlay()
            }
            mEventHandler?.onAdComplete = {
                playerEventListener?.onAdComplete()
            }
            mEventHandler?.onAdError = {
                playerEventListener?.onAdError()
            }
            mEventHandler?.onFullscreen = { fullscreen ->
                playerEventListener?.onFullscreen(fullscreen)
            }

            mEventHandler?.onControlBarVisibilityEvent = { isVisible ->
                playerEventListener?.onControlBarVisibilityEvent(isVisible)
                isVisibleControls = isVisible
            }
        }

        setControls()
    }
}
