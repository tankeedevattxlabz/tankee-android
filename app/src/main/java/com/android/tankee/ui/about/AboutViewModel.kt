package com.android.tankee.ui.about

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.utils.buildThumbnailUrl

class AboutViewModel(private var aboutView: AboutView) {

    fun initAboutData() {
        aboutView.showLoading()
        TankeeRest.api.getConfigurationsAll().enqueue(aboutView.createCallBack(
                { response ->
                    val aboutItem = response.body()?.items?.get(0)
                    val thumbnailAbout = buildThumbnailUrl(aboutItem?.thumbnailAbout?.url)
                    val video = Video()
                    video.jwKey = aboutItem?.jwKey
                    video.title = aboutItem?.titleAbout
                    aboutView.setAbout(aboutItem?.titleAbout, thumbnailAbout, aboutItem?.aboutText, video)
                    aboutView.hideLoading()
                }, {})
        )
    }

    interface AboutView : BaseView {
        fun setAbout(titleAboutText: String?, thumbnailAboutUrl: String?, textAboutText: String?, video: Video?)
    }
}