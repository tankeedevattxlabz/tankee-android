package com.android.tankee.ui.video_player.players.exo_player


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.ui.video_player.players.BasePlayerFragment
import com.android.tankee.ui.video_player.video_utils.VideoUrlBuilder
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.hls.DefaultHlsExtractorFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.fragment_tmp_player.*
import java.util.concurrent.TimeUnit

/**
 * Class for handle all ExoPlayer configuration for playing video,
 * also this class has different events for executing player states.
 */
class ExoPlayerHelperFragment : BasePlayerFragment() {

    override fun seekToPosition(position: Int, onSeekListener: (() -> Unit)?) {
        exoPlayerInstance?.seekTo(position.toLong())
    }

    private var exoPlayerInstance: SimpleExoPlayer? = null
    private var currentWindow = 0


    override fun setControls(){
        playerView?.let {
           it.useController = showControls

            if(showControls) {
                it.showController()
            }
            else {
                it.hideController()
            }
        }
    }
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tmp_player, container, false)
    }

    override fun onStart() {
        super.onStart()
        if (exoPlayerInstance != null) {
            restartPlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        restartPlayer()
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            stopPlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            stopPlayer()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        releasePlayer()
    }

    override fun startPlayer() {
        //No action
    }

    override fun resumePlayer() {
        // No action
    }

    override fun pausePlayer() {
        stopPlayer()
        // No action
    }

    override fun destroyPlayer() {
        // No action
    }

    override fun getPlayerPosition(): Long {
        return exoPlayerInstance?.currentPosition ?: 0
    }

    override fun getPlayerView(): View? {
        return playerView
    }

    override fun setFullscreen() {
        // No action
    }

    private fun restartPlayer() {
        playerView?.let {
            playerView.player = exoPlayerInstance
            exoPlayerInstance?.playWhenReady = true
            exoPlayerInstance?.seekTo(currentWindow, playbackPosition)
        }
    }

    override fun stopPlayer() {
        exoPlayerInstance?.currentPosition?.let { playbackPosition = it }
        exoPlayerInstance?.currentWindowIndex?.let { currentWindow = it }

        exoPlayerInstance?.playWhenReady = false
        playerView.onResume()
    }

    override fun moveToPosition(position: Int) {
        playbackPosition = position.toLong() * TimeUnit.SECONDS.toMillis(1)
        exoPlayerInstance?.seekTo(playbackPosition)
    }

    private fun releasePlayer() {
        exoPlayerInstance?.stop()
        exoPlayerInstance?.release()
        exoPlayerInstance = null
    }

    override fun initPlayer() {
        val trackSelector = DefaultTrackSelector()
        exoPlayerInstance = ExoPlayerFactory.newSimpleInstance(context, trackSelector)

        val videoURI = VideoUrlBuilder.buildUrl(video?.jwKey)
        val dataSourceFactory = DefaultHttpDataSourceFactory("exoplayer_video")
        val defaultHlsExtractorFactory = DefaultHlsExtractorFactory()

        val mediaSource = HlsMediaSource
                .Factory(dataSourceFactory)
                .setExtractorFactory(defaultHlsExtractorFactory)
                .createMediaSource(videoURI)
        exoPlayerInstance?.prepare(mediaSource)

        playerView.player = exoPlayerInstance
        playerView.setControllerVisibilityListener { visibility ->
            playerEventListener?.onControlBarVisibilityEvent(visibility == View.VISIBLE)
        }


        exoPlayerInstance?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

                when (playbackState) {
                    Player.STATE_ENDED -> run {
                        playerEventListener?.onCompleteVideo()
                        exoPlayerInstance?.seekTo(0)
                    }
                }
            }
        })

        exoPlayerInstance?.playWhenReady = true
        setControls()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        forceToPosition(view)
    }

    private fun forceToPosition(view: View) {
        if (playbackPosition > 0 && getPlayerPosition() < playbackPosition) {
            view.postDelayed({
                exoPlayerInstance?.seekTo(playbackPosition)
                forceToPosition(view)
            }, 150)
        } else
            checkForCurrentPosition()
    }

    private fun checkForCurrentPosition() {
        val callback = Runnable {
            exoPlayerInstance?.let {
                if (it.playbackState == Player.STATE_IDLE || it.playbackState == Player.STATE_READY) {
                    playerEventListener?.onPlayerTime(getPlayerPosition() / TimeUnit.SECONDS.toMillis(1))
                }
            }
            checkForCurrentPosition()
        }


        playerView?.postDelayed(callback, TimeUnit.SECONDS.toMillis(1))
    }
}
