package com.android.tankee.ui.views.see_all

interface CheckableItem{
    fun getChecked(): Boolean
    fun setChecked(checked: Boolean)
}