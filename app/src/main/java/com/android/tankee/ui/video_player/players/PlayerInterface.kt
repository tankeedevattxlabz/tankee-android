package com.android.tankee.ui.video_player.players


import android.view.View


interface PlayerInterface {

    fun startPlayer()

    fun resumePlayer()

    fun pausePlayer()

    fun stopPlayer()

    fun destroyPlayer()

    fun getPlayerPosition(): Long

    fun getPlayerView(): View?

    fun setFullscreen()

    fun moveToPosition(position: Int)
}