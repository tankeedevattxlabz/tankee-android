package com.android.tankee.ui.animation

import android.view.View
import com.github.florent37.viewanimator.ViewAnimator


object AnimatorManager {

    fun animateScale(
            view: View,
            onStartAction: (() -> Unit)? = null,
            onStopAction: (() -> Unit)? = null,
            startScale: Float = 1f,
            mediumScale: Float = 0.9f,
            endScale: Float = 1f
    ) {
        ViewAnimator
                .animate(view)
                .scale(startScale, mediumScale, endScale)
                .duration(DEFAULT_ANIMATION_DELAY)
                .onStart { onStartAction?.invoke() }
                .onStop { onStopAction?.invoke() }
                .start()
    }

    const val DEFAULT_ANIMATION_DELAY = 500L
}