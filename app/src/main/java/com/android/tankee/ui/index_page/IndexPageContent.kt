package com.android.tankee.ui.index_page

import java.io.Serializable

interface IndexPageContent : Serializable

class GamesContent : IndexPageContent

class GameContent(val gameId: Int, val isFavorite: Boolean = false) : IndexPageContent

class GamersContent : IndexPageContent

class GamerContent(val gamerId: Int, val isFavorite: Boolean = false) : IndexPageContent

class SectionContent(val sectionId: Int) : IndexPageContent

class GameTypeContent(val gameType: String) : IndexPageContent

class ClipsContent : IndexPageContent

class FavsContent : IndexPageContent