package com.android.tankee.ui.my_stuff.my_video_content.base


import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.findNavController
import com.android.tankee.R
import com.android.tankee.extentions.show
import com.android.tankee.rest.NUM_RECORDS
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.home.videos.IndexPageBundleFactory
import com.android.tankee.ui.request_options.GlideRequestOptions
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllItem
import com.android.tankee.utils.ResUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_my_stuff_section.*
import org.jetbrains.anko.image


/**
 * Class @{BaseMyStuffSectionFragment} has methods for base inflating view for @{BaseMyStuffSectionFragment}
 * and videos @{SavedClipsFragment}.
 * Also here is base view for @{BaseMyStuffSectionView} for setting data from presenter.
 */
abstract class BaseMyStuffSectionFragment : BaseFragment(), BaseMyStuffSectionView {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_stuff_section, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTitle()
        setSectionBackground()
        loadVideos()
    }

    private fun setTitle() {
        titleImage.image = getTitleImage()
        sectionTitle.text = getTitle()
    }

    protected abstract fun getTitle(): String

    protected abstract fun getTitleImage(): Drawable?

    /**
     * Get drawable id for setting it into sectionBackground.
     *  See method setSectionBackground().
     */
    protected abstract fun getBackgroundDrawableId(): Int

    protected abstract fun loadVideos()

    protected abstract fun buildSeeAllButton(): SeeAllItem

    protected abstract fun getPlaceHolderModel(): MyStuffSectionPlaceHolderModel

    private fun setSectionBackground() {
        context?.let {
            Glide.with(it)
                    .load(getBackgroundDrawableId())
                    .apply(GlideRequestOptions.getSectionBackgroundRequestOptions())
                    .into(sectionBackground)
        }
    }

    protected fun navigateToIndexPage(seeAllItem: SeeAllItem) {
        val indexPageBundle = IndexPageBundleFactory().createBundle(seeAllItem)
        findNavController().navigate(R.id.indexPageFragment, indexPageBundle)
    }

    protected fun buildVideoItems(videoItems: MutableList<SectionItemMarker>): MutableList<SectionItemMarker> {
        val resItems = mutableListOf<SectionItemMarker>()

        return if (videoItems.size >= NUM_RECORDS) {
            // Add first NUM_RECORDS items for videoItems list
            resItems.addAll(videoItems.subList(0, NUM_RECORDS))

            val seeAllItem = buildSeeAllButton()
            // Add SEE ALL button
            resItems.add(seeAllItem)

            resItems
        } else {
//            val seeAllItem = buildSeeAllButton()
//            // Add SEE ALL button
//            videoItems.add(seeAllItem)

            videoItems
        }
    }

    /**
     * For showing placeholder need to get model with
     */
    override fun showNoVideosPlaceHolder() {
        val placeHolderModel = getPlaceHolderModel()
        PlaceHolderInflater(
                noVideosPlaceHolder,
                placeHolderModel
        ).inflate()
        noVideosPlaceHolder.show()
    }

    private inner class PlaceHolderInflater(
            private val rootView: View,
            private val placeHolderModel: MyStuffSectionPlaceHolderModel) {

        fun inflate() {
            // Background
            rootView.findViewById<ConstraintLayout>(R.id.noVideosPlaceHolder)
                    .setBackgroundColor(ResUtils.getColor(placeHolderModel.backgroundColor))
            // Title Image
            rootView.findViewById<ImageView>(R.id.placeHolderTitleImage).image = placeHolderModel.titleImage
            // Title
            rootView.findViewById<TextView>(R.id.placeHolderTitle).text = placeHolderModel.title
            // Details text
            rootView.findViewById<TextView>(R.id.placeHolderDetailText).text = placeHolderModel.detailsText
        }
    }
}


/**
 * Model for saving placeholder ui elements and texts for inflating.
 */
data class MyStuffSectionPlaceHolderModel(
        val backgroundColor: Int,
        val titleImage: Drawable?,
        val title: String,
        val detailsText: String
)