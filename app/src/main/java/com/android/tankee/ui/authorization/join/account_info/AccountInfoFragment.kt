package com.android.tankee.ui.authorization.join.account_info


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.BuildConfig
import com.android.tankee.R
import com.android.tankee.TANKEE_PRICACY
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.isNotNull
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.join.parent_email.ParentEmailFragment
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.authorization.navigation.FragmentAnimationStore
import com.android.tankee.ui.authorization.navigation.Navigator
import com.android.tankee.ui.authorization.sign_in.SignInFragment
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.custom_tabs.CustomTabsFactory
import com.android.tankee.ui.views.TankeeButton
import com.android.tankee.utils.isTable
import kotlinx.android.synthetic.main.fragment_account_info.*
import kotlinx.android.synthetic.main.view_header_authorization.*


class AccountInfoFragment : BaseFragment(), AccountInfoViewModel.AccountInfoView {

    private lateinit var accountInfoViewModel: AccountInfoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        accountInfoViewModel = AccountInfoViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_account_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authorizationBack.gone()
        authorizationClose.setOnClickListener { AuthoEventHelper.closeEvent() }

        nextButton.onClickAction = {
            AnalyticsManager.tankeeJoinNextButtonEvent()
            executeCheckAccountInfo()
        }

        createUserName.updateRootContainer = {
            startRootAnimation()
        }
        createPassword.updateRootContainer = {
            startRootAnimation()
        }

        if (isTable()) {
            (signInHereButton as TankeeButton).onClickAction = {
                navigateToSignIn()
            }
        } else {
            signInHereButton.setOnClickListener {
                navigateToSignIn()
            }
        }

        nextButton.setOnClickListener { executeCheckAccountInfo() }
        privacyPolicy.setOnClickListener { openPrivacyPolicy() }
//        if(BuildConfig.DEBUG)
//        accountInfoViewModel.setTestData()

//        accountInfoViewModel.setTestData()
    }

    override fun navigateToParentEmail() {
        Navigator.instance.replaceFragment(ParentEmailFragment(), ParentEmailFragment.TAG)
    }

    override fun setUserNameEmptyError() {
        startRootAnimation()
        createUserName?.setErrorText(getString(R.string.error_user_name_required))
    }

    override fun setUserNameFirstDigitalError() {
        startRootAnimation()
        createUserName?.setErrorText(getString(R.string.error_user_first_digital))
    }

    override fun setUserNameContainsInvalidCharactersError() {
        startRootAnimation()
        createUserName?.setErrorText(getString(R.string.error_user_name_contains_invalid_characters))
    }


    override fun setUserNameInvalidError() {
        startRootAnimation()
        createUserName?.setErrorText(getString(R.string.error_user_invalid))
    }

    override fun setPasswordEmptyError() {
        startRootAnimation()
        createPassword?.setErrorText(getString(R.string.password_error_required))
    }

    override fun seaPasswordLengthError() {
        startRootAnimation()
        createPassword?.setErrorText(getString(R.string.error_password_lenght))
    }

    override fun setInvalidCredentialsError() {
        startRootAnimation()
        createPassword?.setErrorText(getString(R.string.error_invalid_credentials))
    }

    override fun setTestData(createUserNameValue: String, createPasswordValue: String) {
        createUserName?.setText(createUserNameValue)
        createPassword?.setText(createPasswordValue)
    }

    private fun navigateToSignIn() {
        Navigator.instance.replaceFragment(
            SignInFragment(),
            SignInFragment.TAG,
            FragmentAnimationStore.donToUpAnimation
        )
    }

    private fun executeCheckAccountInfo() {
        if (createUserName.isNotNull() && createPassword.isNotNull()) {
            accountInfoViewModel.checkAccountInfo(buildAccountInfoDataModel())
        }
    }

    private fun buildAccountInfoDataModel(): UserAccountModel {
        return UserAccountModel(
            createUserName.getText(),
            createPassword.getText()
        )
    }

    private fun startRootAnimation() {
        accountInfoRoot?.let { TransitionManager.beginDelayedTransition(it) }
    }

    private fun openPrivacyPolicy() {
        CustomTabsFactory.openUrl(context!!, TANKEE_PRICACY)
    }

    companion object {
        var TAG: String = AccountInfoFragment::class.java.simpleName
    }
}
