package com.android.tankee.ui.authorization.join

import com.android.tankee.rest.kws.responces.create_user.KwsCreateUserRequestBody

class UserAccountInfoModel private constructor() {

    var kwsCreateUserRequestBody: KwsCreateUserRequestBody? = null

    companion object {
        var instance = UserAccountInfoModel()
    }
}
