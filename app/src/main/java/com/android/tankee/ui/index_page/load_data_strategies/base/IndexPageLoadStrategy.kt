package com.android.tankee.ui.index_page.load_data_strategies.base

import com.android.tankee.ui.index_page.IndexPageViewModel
import com.android.tankee.ui.index_page.builder.IndexPageBuilder

abstract class IndexPageLoadStrategy {
    
    val indexPageBuilder = IndexPageBuilder()
    lateinit var indexPageView: IndexPageViewModel.IndexPageView

    abstract fun loadMainInfo()

    fun updateMainInfo() {
        hideLoading()
        indexPageView.updateMainInfo(indexPageBuilder.buildIndexPageInfo())
    }

    fun showLoading() {
        indexPageView.showLoading()
    }

    fun hideLoading() {
        indexPageView.hideLoading()
    }

    fun disableLoading() {
        indexPageView.disableLoading()
    }
}