package com.android.tankee.ui.video_player.players.jw_player.handlers

import android.util.Log
import com.android.tankee.utils.log
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.events.*
import com.longtailvideo.jwplayer.events.listeners.AdvertisingEvents
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents

/**
 * Outputs all JW Player Events to logging, with the exception of time events.
 */
open class JWEventHandler(jwPlayerView: JWPlayerView) :
    VideoPlayerEvents.OnControlBarVisibilityListener, VideoPlayerEvents.OnSetupErrorListener,
    VideoPlayerEvents.OnPlaylistListener, VideoPlayerEvents.OnPlaylistItemListener, VideoPlayerEvents.OnPlayListener,
    VideoPlayerEvents.OnPauseListener, VideoPlayerEvents.OnBufferListener, VideoPlayerEvents.OnIdleListener,
    VideoPlayerEvents.OnErrorListener, VideoPlayerEvents.OnSeekListener, VideoPlayerEvents.OnTimeListener,
    VideoPlayerEvents.OnFullscreenListener, VideoPlayerEvents.OnAudioTracksListener,
    VideoPlayerEvents.OnAudioTrackChangedListener, VideoPlayerEvents.OnCaptionsListListener,
    VideoPlayerEvents.OnMetaListener, VideoPlayerEvents.OnPlaylistCompleteListener,
    VideoPlayerEvents.OnCompleteListener, VideoPlayerEvents.OnLevelsChangedListener, VideoPlayerEvents.OnLevelsListener,
    VideoPlayerEvents.OnCaptionsChangedListener, VideoPlayerEvents.OnControlsListener,
    VideoPlayerEvents.OnDisplayClickListener, VideoPlayerEvents.OnMuteListener, VideoPlayerEvents.OnSeekedListener,
    VideoPlayerEvents.OnVisualQualityListener, VideoPlayerEvents.OnFirstFrameListener,
    AdvertisingEvents.OnAdClickListener, AdvertisingEvents.OnAdCompleteListener, AdvertisingEvents.OnAdSkippedListener,
    AdvertisingEvents.OnAdErrorListener, AdvertisingEvents.OnAdImpressionListener, AdvertisingEvents.OnAdTimeListener,
    AdvertisingEvents.OnAdPauseListener, AdvertisingEvents.OnAdPlayListener, AdvertisingEvents.OnAdScheduleListener,
    AdvertisingEvents.OnBeforePlayListener, AdvertisingEvents.OnBeforeCompleteListener {

    var onTime: ((Long) -> Unit)? = null
    var resetState: (() -> Unit)? = null
    var onComplete: (() -> Unit)? = null
    var onAdPlay: (() -> Unit)? = null
    var onAdComplete: (() -> Unit)? = null
    var onAdError: (() -> Unit)? = null
    var onControlBarVisibilityEvent: ((visible: Boolean) -> Unit)? = null
    var onFullscreen: ((visible: Boolean) -> Unit)? = null

    init {
        // Subscribe to all JW Player events

        jwPlayerView.addOnFirstFrameListener(this)
        jwPlayerView.addOnSetupErrorListener(this)
        jwPlayerView.addOnPlaylistListener(this)
        jwPlayerView.addOnPlaylistItemListener(this)
        jwPlayerView.addOnPlayListener(this)
        jwPlayerView.addOnPauseListener(this)
        jwPlayerView.addOnBufferListener(this)
        jwPlayerView.addOnIdleListener(this)
        jwPlayerView.addOnErrorListener(this)
        jwPlayerView.addOnSeekListener(this)
        jwPlayerView.addOnTimeListener(this)
        jwPlayerView.addOnFullscreenListener(this)
        jwPlayerView.addOnLevelsChangedListener(this)
        jwPlayerView.addOnLevelsListener(this)
        jwPlayerView.addOnCaptionsListListener(this)
        jwPlayerView.addOnCaptionsChangedListener(this)
        jwPlayerView.addOnControlBarVisibilityListener(this)
        //  jwPlayerView.addOnRelatedCloseListener(this);
        //  jwPlayerView.addOnRelatedOpenListener(this);
        //  jwPlayerView.addOnRelatedPlayListener(this);
        jwPlayerView.addOnControlsListener(this)
        jwPlayerView.addOnDisplayClickListener(this)
        jwPlayerView.addOnMuteListener(this)
        jwPlayerView.addOnVisualQualityListener(this)
        jwPlayerView.addOnSeekedListener(this)
        jwPlayerView.addOnAdClickListener(this)
        jwPlayerView.addOnAdCompleteListener(this)
        jwPlayerView.addOnAdSkippedListener(this)
        jwPlayerView.addOnAdErrorListener(this)
        jwPlayerView.addOnAdImpressionListener(this)
        jwPlayerView.addOnAdTimeListener(this)
        jwPlayerView.addOnAdPauseListener(this)
        jwPlayerView.addOnAdPlayListener(this)
        jwPlayerView.addOnMetaListener(this)
        jwPlayerView.addOnPlaylistCompleteListener(this)
        jwPlayerView.addOnCompleteListener(this)
        jwPlayerView.addOnBeforePlayListener(this)
        jwPlayerView.addOnBeforeCompleteListener(this)
        jwPlayerView.addOnAdScheduleListener(this)
    }

    private fun updateOutput(output: String) {
        output.log()
    }

    override fun onControlBarVisibilityChanged(p0: ControlBarVisibilityEvent?) {
        Log.d(TAG, "ControlBarVisibility ${p0?.isVisible}")
        p0?.let { onControlBarVisibilityEvent?.invoke(it.isVisible) }
    }

    /**
     * Regular playback events below here
     */
    override fun onAudioTracks(audioTracksEvent: AudioTracksEvent) {
        Log.d(TAG, "onAudioTracks")
        updateOutput("onAudioTracks(List<AudioTrack>)")
    }

    override fun onBeforeComplete(beforeCompleteEvent: BeforeCompleteEvent) {
        Log.d(TAG, "onBeforeComplete")
        updateOutput("onBeforeComplete()")
    }

    override fun onBeforePlay(
        beforePlayEvent: BeforePlayEvent
    ) {
        Log.d(TAG, "onBeforePlay")
        updateOutput("onBeforePlay()")
    }

    override fun onBuffer(bufferEvent: BufferEvent) {
        Log.d(TAG, "onBuffer")
        updateOutput("onBuffer()")
    }

    override fun onCaptionsList(captionsListEvent: CaptionsListEvent) {
        Log.d(TAG, "onCaptionsList")
        updateOutput("onCaptionsList(List<Caption>)")
    }

    override fun onComplete(completeEvent: CompleteEvent) {
        Log.d(TAG, "onComplete")
        updateOutput("onComplete()")
        resetState?.invoke()
        onComplete?.invoke()
    }

    override fun onFullscreen(fullscreen: FullscreenEvent) {
        Log.d(TAG, "onFullscreen")
        updateOutput("onFullscreen(" + fullscreen.fullscreen + ")")
        onFullscreen?.invoke(fullscreen.fullscreen)
    }

    override fun onIdle(idleEvent: IdleEvent) {
        Log.d(TAG, "onIdle")
        updateOutput("onIdle()")
        resetState?.invoke()
    }

    override fun onMeta(metaEvent: MetaEvent) {
        Log.d(TAG, "onMeta")
        updateOutput("onMeta()")
    }

    override fun onPause(pauseEvent: PauseEvent) {
        Log.d(TAG, "onPause")
        updateOutput("onPause()")
        resetState?.invoke()
    }

    override fun onPlay(playEvent: PlayEvent) {
        Log.d(TAG, "onPlay")
        updateOutput("onPlay()")
        resetState?.invoke()
    }

    override fun onPlaylistComplete(playlistCompleteEvent: PlaylistCompleteEvent) {
        Log.d(TAG, "onPlaylistComplete")
        updateOutput("onPlaylistComplete()")
    }

    override fun onPlaylistItem(playlistItemEvent: PlaylistItemEvent) {
        Log.d(TAG, "onPlaylistItem")
        updateOutput("onPlaylistItem()")
        resetState?.invoke()
        onControlBarVisibilityEvent?.invoke(true)
    }

    override fun onPlaylist(playlistEvent: PlaylistEvent) {
        Log.d(TAG, "onPlaylist")
        updateOutput("onPlaylist()")
    }

    override fun onSeek(seekEvent: SeekEvent) {
        Log.d(TAG, "onSeek")
        updateOutput("onSeek(" + seekEvent.position + ", " + seekEvent.offset + ")")
        resetState?.invoke()
    }

    override fun onSetupError(setupErrorEvent: SetupErrorEvent) {
        Log.d("onSetupError: ", setupErrorEvent.message)
        updateOutput("onSetupError(\"" + setupErrorEvent.message + "\")")
    }

    override fun onTime(timeEvent: TimeEvent) {
        Log.d(TAG, timeEvent.duration.toString() + "  ***  ")
        onTime?.invoke(timeEvent.position.toLong())
    }

    override fun onAdError(adErrorEvent: AdErrorEvent) {
        updateOutput("onAdError(\"" + "\", \"" + adErrorEvent.message + "\")")
        Log.d(TAG, "onAdError tagName : " + adErrorEvent.tag)
        Log.d(TAG, "onAdError msg : " + adErrorEvent.message)

        onAdError?.invoke()
    }

    override fun onError(errorEvent: ErrorEvent) {
        Log.d(TAG, "onError : $errorEvent")
        Log.d(TAG, "onError : " + errorEvent.message)
        updateOutput("onError(\"" + errorEvent.message + "\")")
    }

    override fun onLevelsChanged(levelsChangedEvent: LevelsChangedEvent) {
        Log.d(TAG, "onLevelsChanged")
        updateOutput("onLevelsChange(" + levelsChangedEvent.currentQuality + ")")
    }

    override fun onLevels(levelsEvent: LevelsEvent) {
        Log.d(TAG, "onLevels")
        updateOutput("onLevels(List<QualityLevel>)")
    }

    override fun onAudioTrackChanged(audioTrackChangedEvent: AudioTrackChangedEvent) {
        Log.d(TAG, "onAudioTrackChanged")
        updateOutput("onAudioTrackChanged(" + audioTrackChangedEvent.currentTrack + ")")
    }

    override fun onCaptionsChanged(list: CaptionsChangedEvent) {
        Log.d(TAG, "onCaptionsChanged")
        updateOutput("onCaptionsChanged(" + list.currentTrack + ", List<Caption>)")
    }

    override fun onAdClick(adClickEvent: AdClickEvent) {
        Log.d(TAG, "onAdClick")
        updateOutput("onAdClick(\"" + adClickEvent.tag + "\")")
    }

    override fun onAdComplete(adCompleteEvent: AdCompleteEvent) {
        Log.d(TAG, "onAdComplete")
        updateOutput("onAdComplete(\"" + adCompleteEvent.tag + "\")")

        onAdComplete?.invoke()
    }

    override fun onAdSkipped(adSkippedEvent: AdSkippedEvent) {
        Log.d(TAG, "onAdSkipped")
        updateOutput("onAdSkipped(\"" + adSkippedEvent.tag + "\")")
    }

    override fun onAdImpression(adImpressionEvent: AdImpressionEvent) {
        Log.d(TAG, "onAdImpression")
        updateOutput("onAdImpression(\"" + adImpressionEvent.tag + "\", \"" + adImpressionEvent.creativeType + "\", \"" + adImpressionEvent.adPosition.name + "\")")
    }

    override fun onAdTime(adTimeEvent: AdTimeEvent) {
        Log.d(TAG, "onAdTime")
        // Do nothing - this fires several times per second
    }

    override fun onAdPause(adPauseEvent: AdPauseEvent) {
        Log.d(TAG, "onAdPause")
        updateOutput("onAdPause(\"" + adPauseEvent.tag + "\", \"" + adPauseEvent.oldState + "\")")
    }

    override fun onAdPlay(adPlayEvent: AdPlayEvent) {
        Log.d(TAG, "onAdPlay")
        updateOutput("onAdPlay(\"" + adPlayEvent.tag + "\", \"" + adPlayEvent.oldState + "\")")

        onAdPlay?.invoke()
    }

    override fun onSeeked(seekedEvent: SeekedEvent) {
        Log.d(TAG, "onSeeked")
        updateOutput("onSeeked(\"" + "seeked" + "\")")
        resetState?.invoke()
    }

    override fun onControls(controlsEvent: ControlsEvent) {
        Log.d(TAG, "onControls")
        updateOutput("onControls(\"" + controlsEvent.controls + "\")")
    }

    override fun onDisplayClick(displayClickEvent: DisplayClickEvent) {
        Log.d(TAG, "onDisplayClick")
        updateOutput("onDisplayClick()")
    }

    override fun onVisualQuality(visualQuality: VisualQualityEvent) {
        Log.d(TAG, "onVisualQuality")
        updateOutput("onVisualQuality(\"" + "\")")
    }

    override fun onMute(muteEvent: MuteEvent) {
        Log.d(TAG, "onMute")
        updateOutput("onMute()")
    }

    override fun onFirstFrame(firstFrameEvent: FirstFrameEvent) {
        Log.d(TAG, "firstFrameEvent")
        updateOutput("onFirstFrame()")
    }

    override fun onAdSchedule(adScheduleEvent: AdScheduleEvent) {
        Log.d(TAG, "onAdSchedule")
        updateOutput("onAdSchedule()")
    }

    companion object {
        private val TAG: String = JWEventHandler::class.java.name
    }
}
