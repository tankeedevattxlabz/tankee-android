package com.android.tankee.ui.base

class EventHandler {

    var onStart: (() -> Unit)? = null

    var onFinish: (() -> Unit)? = null

    var onSuccess: (() -> Unit)? = null

    var onFailed: (() -> Unit)? = null

    var onError: (() -> Unit)? = null

}