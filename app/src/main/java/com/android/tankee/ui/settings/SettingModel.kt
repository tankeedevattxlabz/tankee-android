package com.android.tankee.ui.settings

data class SettingModel(
        val id: SettingsId,
        val name: Int,
        val icon: Int
)