package com.android.tankee.ui.authorization.delete_account


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.android.tankee.R
import com.android.tankee.database.AppDatabase
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.onboarding.OnboardinViewModel
import kotlinx.android.synthetic.main.fragment_delete_account.*


class DeleteAccountFragment : BaseFragment(), DeleteAccountPresenter.DeleteAccountView {

    private lateinit var deleteAccountPresenter: DeleteAccountPresenter
    private lateinit var onboardinViewModel: OnboardinViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        deleteAccountPresenter = DeleteAccountPresenter(this)
        onboardinViewModel = OnboardinViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_delete_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usernameDelete.updateRootContainer = { startRootAnimation() }
        passwordDelete.updateRootContainer = { startRootAnimation() }

        deleteAccountClose.setOnClickListener { AuthoEventHelper.closeDeleteDialogEvent() }
        cancelButton.setOnClickListener { AuthoEventHelper.closeDeleteDialogEvent() }
        confirmButton.setOnClickListener { deleteAccountPresenter.deleteAccount(buildUserAccountModel()) }

//        setTestData()
    }

    private fun setTestData() {
        AppDatabase.getAppDatabase().kwsUserDao().get()?.username?.let { usernameDelete.setText(it) }
        passwordDelete.setText("12345678")
    }

    override fun navigateHomeScreen() {
        onboardinViewModel.checkLogin(onUserExist = {
            val navBuilder = NavOptions.Builder()
                .setPopUpTo(R.id.homeScreen, false)
            hideToolbar()
            Navigation.findNavController(activity!!, R.id.nav_host_fragment)
                .navigate(R.id.homeScreen, null, navBuilder.build())
            AuthoEventHelper.closeDeleteDialogEvent()
        },
            onNeedLogin = {
                onboardinViewModel.loginAndSaveUserCreds {
                    val navBuilder = NavOptions.Builder()
                        .setPopUpTo(R.id.homeScreen, false)
                    hideToolbar()
                    Navigation.findNavController(activity!!, R.id.nav_host_fragment)
                        .navigate(R.id.homeScreen, null, navBuilder.build())
                    AuthoEventHelper.closeDeleteDialogEvent()
                }
            })
    }

    override fun setUsernameEmptyError() {
        startRootAnimation()
        usernameDelete.setErrorText(getString(R.string.error_user_name_required))
    }

    override fun setPasswordEmptyError() {
        startRootAnimation()
        passwordDelete.setErrorText(getString(R.string.password_error_required))
    }

    private fun startRootAnimation() {
        TransitionManager.beginDelayedTransition(deleteAccountRoot)
    }


    private fun buildUserAccountModel(): UserAccountModel {
        return UserAccountModel(usernameDelete.getText().trim(), passwordDelete.getText().trim())
    }

    override fun setInvalidCredentialsError() {
        passwordDelete.setErrorText(resources.getString(R.string.error_invalid_credentials))
    }

    companion object {
        val TAG: String = DeleteAccountFragment::class.java.simpleName
    }
}
