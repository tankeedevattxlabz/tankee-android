package com.android.tankee.ui.base

/**
 * Created by Sergey Kulyk on 2019-09-22.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */
interface OpenIntent {

    fun openBrowser(url: String)

}