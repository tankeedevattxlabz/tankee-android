package com.android.tankee.ui.authorization.events


import com.android.tankee.event_bus.*

object AuthoEventHelper {

    fun onHomeScreenNavigationEvent() {
        EventPoster.postEvent(OnHomeScreenNavigationEvent())
    }

    fun onRegistrationSettingsNavigationEvent() {
        EventPoster.postEvent(OnRegistrationSettingsNavigationEvent())
    }

    fun closeEvent() {
        EventPoster.postEvent(CloseEvent())
    }

    fun closeDeleteDialogEvent() {
        EventPoster.postEvent(CloseDeleteDialogEvent())
    }

}