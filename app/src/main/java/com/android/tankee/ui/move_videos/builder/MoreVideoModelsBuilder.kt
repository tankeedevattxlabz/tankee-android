package com.android.tankee.ui.move_videos.builder

import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.move_videos.model.MoreVideoModel
import com.android.tankee.utils.millisToMinutes

class MoreVideoModelsBuilder {

    var video: Video? = null
    var videos: List<Video> = listOf()

    fun build(): MoreVideoModel? {
        return build(video)
    }

    fun build(video: Video?): MoreVideoModel? {
        val moreVideoModel = MoreVideoModel()

        moreVideoModel.thumbnailUrl = video?.thumbnailUrl
        moreVideoModel.title = video?.title
        video?.duration?.let { moreVideoModel.duration = millisToMinutes(it.toFloat().toInt()) }

        return moreVideoModel
    }

    fun buildList(): List<MoreVideoModel> {
        val list = mutableListOf<MoreVideoModel>()

        videos.forEach {
            val moreVideoModel = build(video)
            moreVideoModel?.let { moreVideoModel -> list.add(moreVideoModel) }
        }

        return list
    }
}