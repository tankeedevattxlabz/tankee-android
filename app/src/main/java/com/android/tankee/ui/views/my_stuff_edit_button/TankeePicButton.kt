package com.android.tankee.ui.views.my_stuff_edit_button


import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.android.tankee.R
import com.android.tankee.extentions.disable
import com.android.tankee.extentions.enable
import com.android.tankee.ui.animation.AnimatorManager
import kotlinx.android.synthetic.main.view_tankee_pic_button.view.*


class TankeePicButton(context: Context, private val attrs: AttributeSet) : FrameLayout(context, attrs) {

    var onClickAction: (() -> Unit)? = null
    var onPreAnimationAction: (() -> Unit)? = null

    init {
        inflate(context, R.layout.view_tankee_pic_button, this)
        getAttrs()

        tankeeEditButtonRoot.setOnClickListener {
            tankeeEditButtonRoot.disable()
            tankeeEditButtonRoot.postDelayed({ tankeeEditButtonRoot.enable() }, AnimatorManager.DEFAULT_ANIMATION_DELAY)

            AnimatorManager.animateScale(it, onClickAction)
        }
    }

    @SuppressLint("CustomViewStyleable", "Recycle")
    private fun getAttrs() {
        val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.TankeeEditButtonOptions,
                0,
                0
        )

        val tbTextId = typedArray.getResourceId(R.styleable.TankeeEditButtonOptions_btn_text, 0)
        setButtonText(tbTextId)
    }

    private fun setButtonText(tbTextId: Int) {
        buttonText.setText(tbTextId)
    }

}