package com.android.tankee.ui.notifications


import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.ui.base.dialog.BaseDialog
import com.android.tankee.ui.home.videos.IndexPageBundleFactory
import com.android.tankee.ui.notifications.notification_text.NotificationTextFragment
import com.android.tankee.ui.video_player.controller.VideoPlayerController


class NotificationNavigator(private val navController: NavController) {

    fun navigateToIndexPage(model: NotificationModel) {
        val args = IndexPageBundleFactory().createBundleNotification(model)
        navController.navigate(R.id.indexPageFragment, args)
    }

    fun navigateToVideoPlayer(model: NotificationModel) {
        model.itemId?.let { VideoPlayerController.openVideoPlayer(it, navController) }
    }

    fun openTextNotificationPopup(model: NotificationModel, fragmentManager: FragmentManager) {
        val notificationTextFragment = NotificationTextFragment
                .newInstance(model.title ?: EMPTY_STRING, model.text ?: EMPTY_STRING)
        BaseDialog.openDialog(fragmentManager, notificationTextFragment)
    }

}