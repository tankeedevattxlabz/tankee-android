package com.android.tankee.ui.sections

import com.android.tankee.ui.views.see_all.SectionItemMarker

open class SectionModel(
    open val title: String,
    open val background: Int,
    open val items: List<SectionItemMarker>,
    open val totalItemCount: Int,
    open val sectionType: SectionType
) : SectionInterface {
    fun getItem(): SectionItemMarker? {
        return if (items.isNotEmpty()) {
            items[0]
        } else {
            null
        }
    }
}