package com.android.tankee.ui.home.videos


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.android.tankee.R
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.analytics.AnalyticsPageName
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.dynamic_links.DynamicLinks.*
import com.android.tankee.extentions.isNotNull
import com.android.tankee.extentions.isNull
import com.android.tankee.notifications.model.NotificationType
import com.android.tankee.repo.notificatios.ViewNotificationRepo
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.adapters.SectionsAdapter
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.notifications.NotificationNavigator
import com.android.tankee.ui.sections.SectionModel
import com.android.tankee.ui.video_player.controller.VideoPlayerController
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.ui.views.see_all.SeeAllItem
import com.android.tankee.ui.views.see_all.SeeAllItemVideosSection
import com.android.tankee.utils.log
import com.android.tankee.utils.screenHeight
import kotlinx.android.synthetic.main.fragment_home_list.*


class HomeListFragment : BaseFragment(), VideosViewModel.HomeSectionsView {

    private var isToolBarHide: Boolean = false
    private var videosViewModel: VideosViewModel? = null

    private var list = mutableListOf<SectionModel>()
    override fun onDestroy() {
        super.onDestroy()
        clearDeps()
    }

    private fun clearDeps() {
        videosViewModel = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.log()
        videosViewModel = VideosViewModel(this)
    }

    private fun createListener(): SectionsAdapter.SectionItemClickListener {
        return object : SectionsAdapter.SectionItemClickListener {
            override fun onClick(item: SectionItemMarker, title: String?) {
                if (item is Video) {
                    navigateToVideoPlayer(item)
                } else if (item is SeeAllItemVideosSection ||
                        item is SeeAllItem ||
                        item is Game ||
                        item is Gamer ||
                        item is GameType
                ) {
                    val bundle = IndexPageBundleFactory().createBundle(item)
                    findNavController().navigate(R.id.action_homeListFragment_to_indexPageFragment, bundle)

                    sendOpenAnalyticsEvent(item)
                }
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_list, container, false)
    }

    private fun navigateToVideoPlayer(item: Video) {
        VideoPlayerController.openVideoPlayer(item, findNavController())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sectionsRecycler.layoutManager = CenterLayoutManager(context!!)
        (sectionsRecycler.adapter) = SectionsAdapter()
        (sectionsRecycler.adapter as? SectionsAdapter)?.sectionItemClickListener = createListener()
        (sectionsRecycler.adapter as? SectionsAdapter)?.scrollToGamesSectionListener =
                object : SectionsAdapter.ScrollToGamesSectionListener {
                    override fun scroll(positionY: Int) {
                        Handler().postDelayed({
                            positionY + (screenHeight) / 2
                            sectionsRecycler?.smoothScrollToPosition(1)
                        }, 100)
                    }
                }
        (sectionsRecycler.adapter as? SectionsAdapter)?.sections = list

        sectionsRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                try {
                    if (dy > 50) {
                        if (!isToolBarHide) {
                            (activity as? MainActivity)?.hideToolbar()
                            isToolBarHide = !isToolBarHide
                        }
                    } else if (dy < 0) {
                        if (isToolBarHide) {
                            (activity as? MainActivity)?.showToolbar()
                            isToolBarHide = !isToolBarHide
                        }
                    }
                } catch (e: Exception) {
                }
            }
        })

        if (SessionDataHolder.notificationModel.isNotNull()) {
            navigateToScreenWithNotification()
        } else {
            videosViewModel?.loadHomeSections()
        }
        showToolbar()

        getDynamicLink()
    }

    private fun getDynamicLink() {
        SessionDataHolder.dynamicLink?.let {
            val linkPath = SessionDataHolder.dynamicLink?.getPath()

            linkPath.let { path ->
                var indexPageBundle: Bundle?

                when (path) {
                    GamesIndexPageAllGames.path,
                    GamersIndexPageAllGamers.path -> {
                        indexPageBundle = IndexPageBundleFactory().createBundleDynamicLink(path)
                        indexPageBundle.let {
                            findNavController().navigate(R.id.action_homeListFragment_to_indexPageFragment, indexPageBundle)
                        }
                    }
                    DirectVideoLinks.path -> {
                        hideLoading()
                        val query = SessionDataHolder.dynamicLink?.getQueryId()
                        query?.toInt()?.let { videoId -> VideoPlayerController.openVideoPlayer(videoId, findNavController()) }
                    }
                    GamesIndexPageIndividualGames.path,
                    GamersIndexPageIndividualGamers.path,
                    SectionIndexPageSeeAll.path,
                    GameTypeIndexPage.path -> {

                        val query = SessionDataHolder.dynamicLink?.getQueryId()
                        query?.let {
                            hideLoading()

                            indexPageBundle = IndexPageBundleFactory().createBundleDynamicLink(path, it)
                            indexPageBundle.let {
                                findNavController().navigate(
                                        R.id.action_homeListFragment_to_indexPageFragment,
                                        indexPageBundle
                                )
                            }

//                            videosViewModel?.checkIsExist(path, query, onExist)
                        }
                    }
                }
                SessionDataHolder.clearDynamicLink()
            }
        }

    }

    private fun navigateToScreenWithNotification() {
        SessionDataHolder.notificationModel?.let { model ->
            val notificationNavigator = NotificationNavigator(findNavController())

            when (model.getNotificationType()) {
                NotificationType.GAME,
                NotificationType.GAMER -> {
                    hideLoading()
                    notificationNavigator.navigateToIndexPage(model)
                }
                NotificationType.VIDEO -> {
                    notificationNavigator.navigateToVideoPlayer(model)
                    hideLoading()
                }
                else -> fragmentManager?.let {
                    notificationNavigator.openTextNotificationPopup(model, it)
                    videosViewModel?.loadHomeSections()
                }
            }
            model.itemId?.let { ViewNotificationRepo(this).viewNotification(id) }
            SessionDataHolder.clearNotificationModel()
        }
    }

    override fun updateSections(sections: List<SectionModel>) {
        list = mutableListOf()
        list.addAll(sections)
        sectionsRecycler?.let {
            (it.adapter as? SectionsAdapter)?.sections = list
            (it.adapter as? SectionsAdapter)?.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        view?.postDelayed({
            showToolbar()
        }, 500)
    }

    private fun sendOpenAnalyticsEvent(item: SectionItemMarker) {
        when (item) {
            is Game -> {
                ApptentiveManager.openGameFromVideoScreen()
                AnalyticsManager.openGameEvent(item, AnalyticsPageName.VIDEOS)
            }
            is Gamer -> {
                ApptentiveManager.openGamerFromVideoScreen()
                AnalyticsManager.openGamerEvent(item, AnalyticsPageName.VIDEOS)
            }
            is GameType -> {
                ApptentiveManager.openGameTypes()
                AnalyticsManager.openGameTypeEvent(item, AnalyticsPageName.VIDEOS)
            }
            is SeeAllItemVideosSection -> {
                AnalyticsManager.seeAllEvent(item.sectionName)
                ApptentiveManager.seeAllAnotherSection()
            }
            is SeeAllItem -> AnalyticsManager.seeAllEvent(item.seeAllType.name)
        }
    }
}

class CenterLayoutManager(context: Context) : LinearLayoutManager(context) {

    var millisecondsPerInch = 80f

    override fun smoothScrollToPosition(recyclerView: RecyclerView, state: RecyclerView.State?, position: Int) {
        val smoothScroller = CenterSmoothScroller(recyclerView.context)
        smoothScroller.targetPosition = position
        startSmoothScroll(smoothScroller)
    }

    private inner class CenterSmoothScroller internal constructor(context: Context) : LinearSmoothScroller(context) {

        override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
            return millisecondsPerInch / displayMetrics.densityDpi
        }

        override fun calculateDtToFit(
                viewStart: Int,
                viewEnd: Int,
                boxStart: Int,
                boxEnd: Int,
                snapPreference: Int
        ): Int {
            return boxStart + (boxEnd - boxStart) / 2 - (viewStart + (viewEnd - viewStart) / 2)
        }
    }
}


