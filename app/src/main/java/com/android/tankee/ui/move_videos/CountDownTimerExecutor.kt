package com.android.tankee.ui.move_videos

import com.android.tankee.utils.millisToSeconds

class CountDownTimerExecutor {

    private lateinit var countDownTimerTimer: CustomCountDownTimer
    private var timeLeftIntMillis = START_TIME_IN_MILLIS
    private var isTimerRunning: Boolean = false
    private var isTimerStoppedBeforeBackground: Boolean = false

    var onUpdateTimerText: ((String) -> Unit)? = null
    var onUpdateTimerState: ((Boolean) -> Unit)? = null
    var onOpenNextVideo: (() -> Unit)? = null

    fun executeTimer() {
        if (isTimerRunning) {
            pauseTimer()
        } else {
            startTimer()
        }
    }

    fun startTimer() {
        if (!isTimerRunning) {
            countDownTimerTimer = object : CustomCountDownTimer(timeLeftIntMillis, COUNT_DOWN_INTERVAL) {
                override fun onFinish() {
                    isTimerRunning = false

                    timeLeftIntMillis = 0
                    updateTimerText()
                    onOpenNextVideo?.invoke()
                }

                override fun onTick(millisUntilFinished: Long) {
                    timeLeftIntMillis = millisUntilFinished
                    updateTimerText()
                }
            }.start()
            isTimerRunning = true
            updateTimerState()
        }
    }

    fun pauseTimer() {
        if (isTimerRunning) {
            countDownTimerTimer.cancel()
            isTimerRunning = false
            updateTimerState()
        }
    }

    fun resetTimer() {
        timeLeftIntMillis = START_TIME_IN_MILLIS
        updateTimerText()
    }

    fun updateTimerText() {
        val timerText = millisToSeconds(timeLeftIntMillis).toString()
        onUpdateTimerText?.invoke(timerText)
    }

    private fun updateTimerState() {
        onUpdateTimerState?.invoke(isTimerRunning)
    }

    fun goBackground() {
        isTimerStoppedBeforeBackground = isTimerRunning

        pauseTimer()
    }

    fun comeBackFromBackground() {
        if (isTimerStoppedBeforeBackground) {
            startTimer()
        }
    }

    companion object {
        const val START_TIME_IN_MILLIS: Long = 15000L
        const val COUNT_DOWN_INTERVAL: Long = 1000L
    }
}