package com.android.tankee.ui


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.dynamic_links.DynamicLink
import com.android.tankee.logs.AppLogger
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.home.HomeScreen
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.SHOW_HIDE_TOOLBAR_ACTION
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.SHOW_TOOLBAR_ARG
import com.android.tankee.ui.home.toolbar.TankeeToolbar.Companion.UPDATE_TOOLBAR_ARG
import com.android.tankee.utils.log
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.tenjin.android.TenjinSDK


const val ARG_NOTIFICATION_MODEL = "NOTIFICATION_MODEL_ARG"

class MainActivity : AppCompatActivity() {
    private var toolbarShowing: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideSystemUI()
        setContentView(R.layout.activity_main)

        Log.d("Session_Token", TankeePrefs.token)

//        TankeePrefs.clearAll()
        ApptentiveManager.openApp()
//        initSearch()

        disableScreenLock()
        getDynamicLink()
        integrateTenjin()
        getNotificationModel()

    }

    fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                // Set the content to appear under the system bars so that the
//                // content doesn't resize when the system bars gone and show.
                        or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    private fun disableScreenLock() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun showToolbar(force: Boolean = false) {
        if (!force && toolbarShowing) {
            return
        }
        toolbarShowing = true
        val intent = Intent(SHOW_HIDE_TOOLBAR_ACTION).apply {
            putExtra(SHOW_TOOLBAR_ARG, true)
        }
        sendBroadcast(intent)
    }

    fun hideToolbar(force: Boolean = false) {
        if (!force && !toolbarShowing) {
            return
        }
        toolbarShowing = false
        val intent = Intent(SHOW_HIDE_TOOLBAR_ACTION).apply {
            putExtra(SHOW_TOOLBAR_ARG, false)
        }
        sendBroadcast(intent)
    }

    override fun onBackPressed() {
        var interceptedByFragment = false
        supportFragmentManager.fragments.forEach {
            interceptedByFragment = checkFragmentInterceptBackPress(it) || interceptedByFragment
        }
        if (interceptedByFragment) {
            return
        }
        try {
            val findNavController = findNavController(R.id.home_nav_host_fragment)
            val navigateUp = findNavController.navigateUp()
            if (!navigateUp) {
                finish()
            }
        } catch (e: Exception) {
            super.onBackPressed()
        }
        updateToolbar()
    }

    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    // This method is to handle case when app in background
    //  and user open deep link or open push notification.
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.setClass(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun updateToolbar() {
        val intent = Intent(SHOW_HIDE_TOOLBAR_ACTION).apply {
            putExtra(UPDATE_TOOLBAR_ARG, true)
        }
        sendBroadcast(intent)
    }

    private fun checkFragmentInterceptBackPress(fragment: Fragment): Boolean {
        var interceptedByFragment = false
        interceptedByFragment = (fragment as? BaseFragment)?.onBackPressed() ?: false || interceptedByFragment
        fragment.log("checkFragmentInterceptBackPress $interceptedByFragment")
        fragment.childFragmentManager.fragments.forEach {
            interceptedByFragment = checkFragmentInterceptBackPress(it) || interceptedByFragment
        }
        return interceptedByFragment
    }

    private fun getDynamicLink() {
        intent?.let {
            FirebaseDynamicLinks.getInstance()
                .getDynamicLink(it)
                .addOnSuccessListener(this) { pendingDynamicLinkData ->
                    // Get deep link from result (may be null if no link is found)
                    var deepLink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.link
                    }

                    SessionDataHolder.dynamicLink = DynamicLink(deepLink)
                    AppLogger.logDebug("DynamicLink", deepLink.toString())
                    // Handle the deep link. For example, open the linked
                    // content, or apply promotional credit to the user's
                    // account.
                    // ...

                    // ...
                }
                .addOnFailureListener(this) { e -> Log.w(HomeScreen.TAG, "getDynamicLink:onFailure", e) }
        }
    }

    private fun integrateTenjin() {
        //Integrate TenjinSDK connect call
        val apiKey = getString(R.string.tenjin_api_key)
        val instance = TenjinSDK.getInstance(this, apiKey)

        if (SessionDataHolder.dynamicLink?.link != null) {
            instance.connect(SessionDataHolder.dynamicLink?.link.toString())
        } else {
            instance.connect()
        }
    }

    private fun getNotificationModel() {
        intent?.extras?.let {
            if (it.containsKey(ARG_NOTIFICATION_MODEL)) {
                val notificationModel = it.getParcelable<NotificationModel>(ARG_NOTIFICATION_MODEL)
                notificationModel?.toString()?.let { model -> AppLogger.logDebug(TAG, model) }
                SessionDataHolder.notificationModel = notificationModel
            }
        }
    }


    companion object {

        var TAG: String = MainActivity::class.java.simpleName

    }
}
