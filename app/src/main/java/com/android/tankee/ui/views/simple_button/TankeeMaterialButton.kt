package com.android.tankee.ui.views.simple_button


import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.android.tankee.R
import kotlinx.android.synthetic.main.view_tankee_matrial_button.view.*


class TankeeMaterialButton(context: Context, private val attrs: AttributeSet) : FrameLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_tankee_matrial_button, this)

        initAttrs()
    }

    @SuppressLint("Recycle", "CustomViewStyleable")
    private fun initAttrs() {
        val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.TankeeMaterialButtonOptions,
                0,
                0
        )

        val buttonType = typedArray.getString(R.styleable.TankeeMaterialButtonOptions_tmb_type)
        val buttonTextId = typedArray.getResourceId(R.styleable.TankeeMaterialButtonOptions_tmb_text, 0)

        setButtonType(buttonType!!)
        tankeeMaterialButton.text = context.getString(buttonTextId)
    }

    private fun setButtonType(buttonType: String) {
        try {
            when (buttonType) {
                TankeeMaterialButtonType.Positive.value -> setPositiveStyle()
                TankeeMaterialButtonType.Negative.value -> setNegativeStyle()
                else -> {
                    setNeutralStyle()
                }
            }
        } catch (ignore: Exception) {
        }
    }

    private fun setPositiveStyle() {
        tankeeMaterialButton.setBackgroundResource(R.drawable.bg_tankee_material_button_green)
        tankeeMaterialButton.setTextColor(ContextCompat.getColor(context, R.color.colorBayOfMany))
    }

    private fun setNegativeStyle() {
        tankeeMaterialButton.setBackgroundResource(R.drawable.bg_tankee_material_button_red)
        tankeeMaterialButton.setTextColor(ContextCompat.getColor(context, R.color.colorRed))
    }

    private fun setNeutralStyle() {
        tankeeMaterialButton.setBackgroundResource(R.drawable.bg_tankee_material_button_green)
        tankeeMaterialButton.setTextColor(ContextCompat.getColor(context, R.color.colorBayOfMany))
    }
}

private enum class TankeeMaterialButtonType(val value: String) {

    Positive("positive"),
    Negative("negative"),
    Neutral("neutral")

}