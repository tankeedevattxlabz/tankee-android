package com.android.tankee.ui.ads.adbutler

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import com.android.tankee.EMPTY_STRING
import com.android.tankee.cookies.SessionDataHolder
import com.facebook.stetho.inspector.helper.IntegerFormatter
import com.sparklit.adbutler.*
import com.squareup.imagelib.Picasso
import kotlinx.android.synthetic.main.fragment_video_player.*

class AdButlerHelper {

    fun showAdbutlerBanner(context: Context?, adButlerBannerView: ImageView?) {
        val accountId = SessionDataHolder.videoPlayerBannerModel?.adbutlerModel?.accountId ?: EMPTY_STRING
        val zoneId = SessionDataHolder.videoPlayerBannerModel?.adbutlerModel?.zoneId ?: EMPTY_STRING
        if(TextUtils.isEmpty(accountId) || TextUtils.isEmpty(zoneId))
            return
        var config = PlacementRequestConfig.Builder(Integer.parseInt(accountId), Integer.parseInt(zoneId) , 320, 50).build()
        val adbutler = AdButler()
        adbutler.requestPlacement(config, object : PlacementResponseListener {
            override fun error(p0: Throwable?) {

            }

            override fun success(p0: PlacementResponse?) {
                if (p0 == null || adButlerBannerView == null || context == null)
                    return;

                if (p0.placements.size > 0) {
                    val placement: Placement = p0.placements[0]
                    adButlerBannerView.visibility = View.VISIBLE
                    adButlerBannerView.setOnClickListener {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(placement.redirectUrl)
                        context.startActivity(intent)
                    }
                    Picasso.with(context)
                        .load(placement.imageUrl)
                        .resize(placement.width, placement.height)
                        .into(adButlerBannerView)
                    placement.recordImpression()
                }
            }
        })
    }
}