package com.android.tankee.ui.settings

import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.*
import com.android.tankee.utils.getColor
import kotlinx.android.synthetic.main.item_setting.view.*

class SettingsAdapter : BaseRecyclerAdapter<
        SettingModel,
        OnRecyclerItemClickListener<SettingModel>,
        SettingsAdapter.SettingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingViewHolder {
        return SettingViewHolder(inflate(R.layout.item_setting, parent, false))
    }

    inner class SettingViewHolder(itemView: View)
        : BaseRecyclerAdapter.BaseVideoHolder<SettingModel, OnRecyclerItemClickListener<SettingModel>>(itemView) {

        override fun onBind(
                item: SettingModel,
                listener: OnRecyclerItemClickListener<SettingModel>?
        ) {
            itemView.settingIcon.setImageResource(item.icon)
            itemView.settingName.text = getAppContext().getString(item.name)

            if (item.id == SettingsId.DebugInfo) {
                itemView.settingName.setTextColor(getColor(R.color.colorDebug))
            }
            listener?.let { itemView.setOnClickListener { listener.onItemClicked(item) } }
        }

    }

}