package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_games

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.adapter.holders.AvatarCornersType
import com.android.tankee.ui.index_page.load_data_strategies.base.LazyLoadingStrategy
import com.android.tankee.utils.buildUrl

class LoadingGameVideosStrategy(private val gameId: Int) : LazyLoadingStrategy() {
    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getGame(gameId)
            .enqueue(indexPageView.createCallBack({ response ->
                response.body()?.let { baseResponse ->
                    baseResponse.items?.let { items ->
                        items.isNotEmpty().let {
                            val game = items[0]
                            game.name?.let { indexPageBuilder.title = it }
                            val image: String? = if (game.imageUrl != null && game.imageUrl!!.isNotEmpty()) {
                                game.imageUrl
                            } else {
                                buildUrl(game.avatar?.url.toString())
                            }
                            indexPageBuilder.image = image
                            indexPageBuilder.avatarCornersType = AvatarCornersType.GAME_AVATAR
                            game.tags?.let { indexPageBuilder.tags = it }

                            loadItems()
                        }
                    }
                }
            }, { hideLoading()
                indexPageView.onErrorLoading()
            }))
    }

    override fun loadMoreItems() {
        TankeeRest.api.getGameVideos(gameId, lazyLoadingModel.page, lazyLoadingModel.tag)
            .enqueue(indexPageView.createCallBack(
                { response ->
                    response.body()?.let { baseResponse ->
                        baseResponse.items?.let { items ->
                            finishLoadingItems()
                            checkIsLazyLoadingFinished(items.size)

                            items.isNotEmpty().let {
                                indexPageBuilder.newItems = items.toMutableList()
                                updateLazyLoadingIndexPage()
                            }.run {
                                hideLoading()
                            }
                        }
                    }
                }, {
                    hideLoading()
                    finishLoadingItems()
                })
            )
    }
}