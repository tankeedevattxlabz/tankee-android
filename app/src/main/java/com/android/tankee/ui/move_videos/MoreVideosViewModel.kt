package com.android.tankee.ui.move_videos


import android.util.Log
import com.android.tankee.EMPTY_STRING
import com.android.tankee.extentions.isNull
import com.android.tankee.rest.MORE_VIDEOS_NUM_RECORDS
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.base.lazy_loading.LazyLoadingBaseModel
import com.android.tankee.ui.move_videos.builder.MoreVideoModelsBuilder
import com.android.tankee.ui.move_videos.model.MoreVideoModel
import com.android.tankee.utils.getLastItem
import com.android.tankee.utils.withoutLastItem


class MoreVideosViewModel(private val moreVideosView: MoreVideosView) {

    var video: Video? = null
    var nextVideo: Video? = null
    var lazyLoadingModel: LazyLoadingBaseModel = LazyLoadingBaseModel()

    fun loadMoreItems() {
        video?.let {
            val playlistKey = getPlaylistKey(video!!)
            moreVideosView.showLoading(lazyLoadingModel.isDefaultPage())
            TankeeRest.api.getRecommendedVideosById(video!!.id!!, playlistKey!!, lazyLoadingModel.page)
                .enqueue(moreVideosView.createCallBack({ response ->
                    response.body()?.let { baseResponse ->
                        moreVideosView.hideLoading()

                        var nextVideoModel = baseResponse.lastVideo
                        baseResponse.items?.let { items ->
                            if (nextVideoModel == null) {
                                nextVideoModel = items.getLastItem()
                            }
                            nextVideoModel?.let { nextVideo ->
                                buildNextMoreVideoModel(nextVideo)?.let { moreVideoModel ->
                                    if (lazyLoadingModel.isDefaultPage()) {
                                        moreVideosView.setNextVideo(moreVideoModel)
                                    }
                                }
                            }

                            nextVideo = nextVideoModel
                            val withLastItem = if (baseResponse.lastVideo.isNull()) {
                                lazyLoadingModel.isDefaultPage()
                            } else {
                                true
                            }
                            val moreVideos = getMoreVideosVideo(items, withLastItem)
                            moreVideos.isNotEmpty().let { moreVideosView.addMoreVideos(moreVideos) }

                            if (items.size < MORE_VIDEOS_NUM_RECORDS) {
                                lazyLoadingModel.finishLazyLoading()
                                return@createCallBack
                            }
                            lazyLoadingModel.incrementPage()

                        }
                        lazyLoadingModel.stopLoading()
                    }
                }, {
                    moreVideosView.hideLoading()
                    lazyLoadingModel.stopLoading()
                }))
        }
    }

    fun addReviewToVideo(video: Video?, onFinishAddReviewToVideo: () -> Unit) {
        video?.id?.let {
            TankeeRest.api.addReviewsToVideo(it, 0)
                .enqueue(moreVideosView.createCallBack(
                    {
                        Log.d("add_review_to_video", "0")
                        onFinishAddReviewToVideo()
                    },
                    {
                        onFinishAddReviewToVideo()
                    }
                ))
        }
    }

    private fun getMoreVideosVideo(videos: List<Video>, withLastItem: Boolean = true): List<Video> {
        return if (videos.isNotEmpty()) {
            if (withLastItem) {
                videos
            } else {
                videos.withoutLastItem()
            }
        } else {
            listOf()
        }
    }

    private fun buildNextMoreVideoModel(video: Video): MoreVideoModel? {
        val moreVideoModelsBuilder = MoreVideoModelsBuilder()
        moreVideoModelsBuilder.video = video
        return moreVideoModelsBuilder.build()
    }

    private fun getPlaylistKey(video: Video): String? {
        return if (video.playlistKey != null) {
            video.playlistKey
        } else {
            EMPTY_STRING
        }
    }

    interface MoreVideosView : BaseView {
        fun setNextVideo(nextVideo: MoreVideoModel)

        fun addMoreVideos(moreVideos: List<Video>)
    }

}