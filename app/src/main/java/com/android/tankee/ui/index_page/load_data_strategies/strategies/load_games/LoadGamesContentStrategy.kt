package com.android.tankee.ui.index_page.load_data_strategies.strategies.load_games

import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.ui.index_page.load_data_strategies.base.IndexPageLoadStrategy

class LoadGamesContentStrategy : IndexPageLoadStrategy() {

    override fun loadMainInfo() {
        showLoading()
        TankeeRest.api.getGames()
            .enqueue(indexPageView.createCallBack(
                { response ->
                    response.body()?.let {
                        it.sectionTitle?.let { indexPageBuilder.title = it }
                        it.items?.let { indexPageBuilder.newItems = it.toMutableList() }

                        updateMainInfo()
                    }
                }, { hideLoading() })
            )
    }
}