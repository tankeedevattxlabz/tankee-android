package com.android.tankee.ui.base

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.android.tankee.HTTP
import com.android.tankee.HTTPS
import com.android.tankee.R
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.popups.BasePopup
import com.android.tankee.ui.views.LoadingDialog
import com.android.tankee.utils.isNetworkAvailable

open class BaseFragment : Fragment(), BaseView, OpenIntent {

    private var loadingDialog: LoadingDialog? = null
    private var internetConnectionPopup: BasePopup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = LoadingDialog(activity!!)
    }

    override fun onResume() {
        super.onResume()
        val mainActivity = activity as?MainActivity
        view?.postDelayed({ mainActivity?.hideSystemUI() }, 600)
    }

    override fun showLoading() {
        loadingDialog?.show()
    }

    override fun hideLoading() {
        loadingDialog?.hide()
    }

    override fun showLoading(showLoading: Boolean) {
        if (showLoading) {
            showLoading()
        } else {
            hideLoading()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        if (loadingDialog != null)
            loadingDialog?.hide()
    }

    override fun showPopUp(p: DialogFragment, tag: String) {
        if (childFragmentManager.findFragmentByTag(tag) == null)
            p.show(childFragmentManager, tag)
    }

    //Should return true if intercepted back event!!!
    open fun onBackPressed(): Boolean {
        return false
    }

    override fun openBrowser(url: String) {
        var webPage = Uri.parse(url)

        if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
            webPage = Uri.parse(HTTP + url)
        }

        val intent = Intent(Intent.ACTION_VIEW, webPage)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val packageManager = activity?.packageManager

        packageManager?.let {
            if (intent.resolveActivity(it) != null) {
                activity?.startActivity(intent)
            }
        }
    }

    fun showConnectionErrorPopup(tryAgainAction: (() -> Unit)? = null) {

        internetConnectionPopup = BasePopup.Builder()
                .title(getString(R.string.no_internet_connection))
                .image(R.drawable.ic_wifi_off)
                .message(getString(R.string.check_internet_connection))
                .buttons(mapOf(getString(R.string.try_again) to { p ->
                    if (isNetworkAvailable()) {
                        tryAgainAction?.invoke()
                        try {
                            p.dismiss()
                        } catch (ignore: Exception) {
                        }
                    }
                }))
                .cancelable(false)
                .create()

        showPopUp(internetConnectionPopup!!, "NO_INTERNET_POPUP")
    }

    fun hideConnectionPopup() {
        internetConnectionPopup?.let {
            try {
                if (it.isAdded) {
                    it.dismiss()
                }
            } catch (ignore: Exception) {
            }
        }
    }

    protected fun showToolbar() {
        (activity as? MainActivity)?.showToolbar(true)
    }

    protected fun hideToolbar() {
        (activity as? MainActivity)?.hideToolbar(true)
    }
}