package com.android.tankee.ui.my_stuff.my_video_content.base


import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView


interface BaseMyStuffSectionView : BaseView {

    fun <T : Video> setVideos(videos: List<T>)

    fun showNoVideosPlaceHolder()

}