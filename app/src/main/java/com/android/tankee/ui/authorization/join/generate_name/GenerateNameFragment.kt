package com.android.tankee.ui.authorization.join.generate_name


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.TANKEE_PRICACY
import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.isNotNull
import com.android.tankee.ui.authorization.events.AuthoEventHelper
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.custom_tabs.CustomTabsFactory
import kotlinx.android.synthetic.main.fragment_generate_name.*
import kotlinx.android.synthetic.main.view_header_authorization.*


class GenerateNameFragment : BaseFragment(), GenerateNameViewModel.GenerateNameView {

    private lateinit var generateNameViewModel: GenerateNameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        generateNameViewModel = GenerateNameViewModel(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_generate_name, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authorizationBack.gone()
        authorizationClose.setOnClickListener { AuthoEventHelper.closeEvent() }

        joinButton.onClickAction = {
            AnalyticsManager.tankeeJoinGenerateNameButtonEvent()
            executeCheckAccountInfo()
        }

        generatedName.updateRootContainer = {
            startRootAnimation()
        }
        generatedName.regenerateNameAction = {
            generateNameViewModel.generateDisplayName()
        }

        joinButton.setOnClickListener { executeCheckAccountInfo() }
        privacyPolicy.setOnClickListener { openPrivacyPolicy() }
        generateNameViewModel.generateDisplayName()
    }

    override fun navigateToHomeScreen() {
        AuthoEventHelper.onRegistrationSettingsNavigationEvent()
        AuthoEventHelper.closeEvent()
    }

    override fun setGeneratedNameEmptyError() {
        startRootAnimation()
        generatedName?.setError(getString(R.string.generate_name_error_required))
    }

    override fun setGeneratedDisplayName(generatedDisplayName: String) {
        generatedName?.setText(generatedDisplayName)
        generatedName?.hideError()
    }

    private fun executeCheckAccountInfo() {
        if (generatedName.isNotNull()) {
            generateNameViewModel.checkGeneratedName(generatedName.getText())
        }
    }

    private fun startRootAnimation() {
        generatedNameRoot?.let { TransitionManager.beginDelayedTransition(it) }
    }

    private fun openPrivacyPolicy() {
        CustomTabsFactory.openUrl(context!!, TANKEE_PRICACY)
    }

    companion object {
        var TAG: String = GenerateNameFragment::class.java.simpleName
    }
}
