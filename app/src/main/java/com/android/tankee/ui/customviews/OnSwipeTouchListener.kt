package com.android.tankee.ui.customviews

import android.content.Context
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener

open class OnSwipeTouchListener(ctx: Context) : OnTouchListener {
    companion object {
        private const val SWIPE_THRESHOLD = 10
        private const val SWIPE_VELOCITY_THRESHOLD = 10
    }

    private val gestureDetector: GestureDetector
    var videoView : View? = null

    init {
        gestureDetector = GestureDetector(ctx, GestureListener())
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
//        "$v onTouch $event".log()
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
//            "onDown $e".log()
            return true
        }


        override fun onSingleTapUp(e: MotionEvent?): Boolean {
//            "onSingleTapUp".log()
            return false
        }

        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
//            "onSingleTapConfirmed".log()
            e?.action = MotionEvent.ACTION_DOWN
            videoView?.dispatchTouchEvent(e)
            e?.action = MotionEvent.ACTION_UP
            videoView?.dispatchTouchEvent(e)
            return false
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
//            "onFling $e1 $e2".log()
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight()
                        } else {
                            onSwipeLeft()
                        }
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    result = if (diffY > 0) {
                        onSwipeBottom()
                        true
                    } else {
                        onSwipeTop()
                        true
                    }
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            return result
        }
    }

    open fun onSwipeRight() {}

    open fun onSwipeLeft() {}

    open fun onSwipeTop() {}

    open fun onSwipeBottom() {}
}