package com.android.tankee.ui.search

import android.view.View
import android.view.ViewGroup
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.adapters.BaseRecyclerAdapter
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.BaseVideoHolder
import com.android.tankee.ui.adapters.BaseRecyclerAdapter.OnRecyclerItemClickListener
import com.android.tankee.utils.setSpanColor
import kotlinx.android.synthetic.main.item_search_suggestion.view.*
import org.jetbrains.anko.textColor

class SuggestionsAdapter : BaseRecyclerAdapter<
        Video,
        OnRecyclerItemClickListener<Video>,
        BaseVideoHolder<Video, OnRecyclerItemClickListener<Video>>>() {

    var query: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVideoHolder<Video, OnRecyclerItemClickListener<Video>> {
        return SuggestionViewHolder(inflate(R.layout.item_search_suggestion, parent, false))
    }

    inner class SuggestionViewHolder(itemView: View
    ) : BaseVideoHolder<Video, OnRecyclerItemClickListener<Video>>(itemView) {
        override fun onBind(item: Video, listener: OnRecyclerItemClickListener<Video>?) {
            itemView.suggestion.textColor = getAppContext().resources.getColor(if (query.isEmpty()) {
                R.color.colorBlack
            } else {
                R.color.colorGray
            })

            item.title?.let {

                setSpanColor(itemView.suggestion, query, it)
            }

            itemView.setOnClickListener { listener?.onItemClicked(item) }
        }
    }
}