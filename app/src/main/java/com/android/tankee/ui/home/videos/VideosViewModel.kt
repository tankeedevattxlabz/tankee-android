package com.android.tankee.ui.home.videos

import com.android.tankee.GAME_TYPES_COUNT
import com.android.tankee.R
import com.android.tankee.dynamic_links.DynamicLinks.*
import com.android.tankee.getAppContext
import com.android.tankee.repo.avatars.UserBackgroundRepo
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.rest.*
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.games.GameSectionResponse
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Section
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.popups.BasePopup
import com.android.tankee.ui.sections.SectionModel
import com.android.tankee.ui.sections.SectionType
import com.android.tankee.ui.sections.VideoSectionModel
import com.android.tankee.utils.isNetworkAvailable
import retrofit2.Call

class VideosViewModel(private val homeSectionsView: HomeSectionsView) {

    private var networkCallsCount = 0
    private var networkCallsFailureCount = 0
    private var loadGamesSection = false
    private var loadTrendingSection = false
    private var loadGamersSection = false
    private var loadAnotherSections = false
    private val homeSections = mutableListOf<SectionModel>()
    private var justForYouSection: SectionModel? = null
    private var gamesSection: SectionModel? = null
    private var gamersSection: SectionModel? = null
    private var otherSections = mutableListOf<SectionModel>()
    private lateinit var gameTypesSection: SectionModel
    private val userBackgroundRepo = UserBackgroundRepo()
    private var count = 0

    fun loadHomeSections() {
        if (networkCallsCount < 4) {
            networkCallsCount = 0
            loadGamesSection()
            loadTrendingSection()
            loadGamersSection()
            loadAnotherSections()
            initGamesType()
            homeSectionsView.showLoading()
        } else {
            checkFinishLoadSections()
        }
    }


    private fun loadTrendingSection() {
        TankeeRest.api.getTopVideos()
                .enqueue(homeSectionsView.createCallBackWithBadConnection({ response ->
                    networkCallsCount++
                    var justForYouSection: SectionModel? = null
                    response.body()?.items?.let {
                        val sectionTitle = if (AnonymousUserRepo().isUserAnonymous()) {
                            getAppContext().getString(R.string.section_trending)
                        } else {
                            getAppContext().getString(R.string.section_featured)
                        }

                        justForYouSection = SectionModel(
                                sectionTitle,
                                userBackgroundRepo.getBackground(), // Get user background for trending section.
                                it,
                                it.size,
                                SectionType.TOP
                        )
                    }

                    this@VideosViewModel.justForYouSection = justForYouSection
                    checkFinishLoadSections()
                }, onBadConnect = { b: Boolean, call: Call<BaseResponse<Video>> ->
                    loadTrendingSection = true
                    networkCallsFailureCount++
                    failureListener()
                }
                ))
    }


    private fun initGamesType() {
        val gameTypes = mutableListOf<GameType>()

        val gameTypeAction =
                GameType(getAppContext().getString(R.string.game_type_action), R.drawable.ic_game_type_action)
        val gameTypeAdventure =
                GameType(getAppContext().getString(R.string.game_type_adventure), R.drawable.ic_game_type_adventure)
        val gameTypeCreative =
                GameType(getAppContext().getString(R.string.game_type_creative), R.drawable.ic_game_type_creative)
        val gameTypeRacing =
                GameType(getAppContext().getString(R.string.game_type_racing), R.drawable.ic_game_type_racing)
        val gameTypeSports =
                GameType(getAppContext().getString(R.string.game_type_sports), R.drawable.sports)

        gameTypes.add(gameTypeAction)
        gameTypes.add(gameTypeAdventure)
        gameTypes.add(gameTypeCreative)
        gameTypes.add(gameTypeRacing)
        gameTypes.add(gameTypeSports)

        gameTypesSection =
                SectionModel(
                        getAppContext().getString(R.string.section_game_type),
                        R.drawable.scroll_section_blue,
                        gameTypes,
                        GAME_TYPES_COUNT,
                        SectionType.GAME_TYPES
                )
    }

    private fun failureListener() {


        if (networkCallsFailureCount == 1) {

            homeSectionsView.showPopUp(
                    BasePopup.Builder()
                            .title("No internet connection")
                            .image(R.drawable.ic_wifi_off)
                            .message("Check your internet connection")
                            .buttons(mapOf("Try again" to { p ->

                                if (isNetworkAvailable()) {
                                    callApis()
                                    p.dismissAllowingStateLoss()
                                }
                            }))
                            .cancelable(false)
                            .create()
                    , "NO_INTERNET_POPUP"
            )

        }


    }

    private fun callApis() {
        networkCallsCount = 0;
        networkCallsFailureCount = 0;
        if (loadGamesSection)
            loadGamesSection()
        if (loadTrendingSection)
            loadTrendingSection()
        if (loadGamersSection)
            loadGamersSection()
        if (loadAnotherSections)
            loadAnotherSections()
    }


    private fun loadGamersSection() {
        TankeeRest.api.getInfluencers()
                .enqueue(homeSectionsView.createCallBackWithBadConnection({ response ->
                    networkCallsCount++
                    var gamersSection: SectionModel? = null
                    response.body()?.let {
                        val gamers = mutableListOf<Gamer>()
                        it.items?.let { list ->
                            if (list.size > 10) {
                                for (i in 0..NUM_RECORDS) {
                                    gamers.add(list[i])
                                }
                            } else {
                                gamers.addAll(list)
                            }
                        }

                        gamersSection = SectionModel(
                                it.sectionTitle!!,
                                R.drawable.scroll_section_yellow,
                                gamers,
                                it.items?.size!!,
                                SectionType.GAMERS
                        )
                    }

                    this@VideosViewModel.gamersSection = gamersSection
                    checkFinishLoadSections()
                }, onBadConnect = { b: Boolean, call: Call<BaseSectionResponse<Gamer>> ->
                    loadGamersSection = true

                    networkCallsFailureCount++
                    failureListener()
                }
                ))
    }


    private fun loadAnotherSections() {
        TankeeRest.api.getSections()
                .enqueue(homeSectionsView.createCallBackWithBadConnection({ response ->
                    networkCallsCount++
                    val otherSections = mutableListOf<SectionModel>()

                    response.body()?.let {
                        it.forEach { section ->
                            run {
                                if (section.videos != null && section.videosCount != null && section.title != null) {
                                    val videoSection =
                                            VideoSectionModel(
                                                    section.id!!,
                                                    section.title!!,
                                                    section.background?.url!!,
                                                    section.videos!!,
                                                    section.videosCount!!,
                                                    SectionType.ANOTHER
                                            )
                                    otherSections.add(videoSection)
                                }
                            }
                        }
                    }

                    this@VideosViewModel.otherSections = otherSections
                    checkFinishLoadSections()
                }, onBadConnect = { b: Boolean, call: Call<List<Section>> ->
                    loadAnotherSections = true

                    networkCallsFailureCount++
                    failureListener()
                }
                ))
    }


    private fun loadGamesSection() {
        val callback = homeSectionsView.createCallBackWithBadConnection<GameSectionResponse>({ response ->
            var gamesSection: SectionModel? = null
            response.body()?.let {
                val list = it.items!!
                val sectionItems = mutableListOf<Game>()
                if (list.size > 10) {
                    for (i in 0 until NUM_RECORDS) {
                        sectionItems.add(list[i])
                    }
                } else {
                    sectionItems.addAll(list)
                }
                gamesSection =
                        SectionModel(
                                it.sectionTitle!!,
                                R.drawable.scroll_section_blue,
                                sectionItems,
                                list.size,
                                SectionType.GAMES
                        )
            }

            this@VideosViewModel.gamesSection = gamesSection
            if (gamesSection != null) {
                networkCallsCount++
                checkFinishLoadSections()
            }
        }, onBadConnect = { b: Boolean, call: Call<GameSectionResponse> ->
            loadGamesSection = true
            networkCallsFailureCount++
            failureListener()
        }

        )
        TankeeRest.api.getGames().enqueue(callback)
    }


    private fun checkFinishLoadSections() {

        if (networkCallsCount >= 4) {
            buildSections()
            homeSectionsView.updateSections(homeSections)
            homeSectionsView.hideLoading()
            //NetworkcallCounts = 0;
        }
    }

    private fun buildSections() {
        homeSections.clear()

        justForYouSection?.let { homeSections.add(it) }
        gamesSection?.let { homeSections.add(it) }
        gamersSection?.let { homeSections.add(it) }
        homeSections.addAll(otherSections)
        homeSections.add(gameTypesSection)
    }

    private fun isGameExist(id: Int, onExist: (() -> Unit)) {
        TankeeRest.api.getGame(id).enqueue(
                homeSectionsView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    onExist()
                                }
                            }
                        }
                )
        )
    }

    private fun isGamerExist(id: Int, onExist: (() -> Unit)) {
        TankeeRest.api.getGamer(id).enqueue(
                homeSectionsView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    onExist()
                                }
                            }
                        }
                )
        )
    }

    private fun isSectionExist(id: Int, onExist: (() -> Unit)) {
        TankeeRest.api.getSectionById(id).enqueue(
                homeSectionsView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    onExist()
                                }
                            }
                        }
                )
        )
    }

    private fun isGameTypeExist(id: String, onExist: (() -> Unit)) {
        TankeeRest.api.getGameType(id).enqueue(
                homeSectionsView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    onExist()
                                }
                            }
                        }
                )
        )
    }

    private fun isVideoExist(id: Int, onExist: (() -> Unit)) {
        TankeeRest.api.getVideoInfo(id).enqueue(
                homeSectionsView.createCallBack(
                        { response ->
                            response.body()?.items?.let { items ->
                                if (items.isNotEmpty()) {
                                    onExist()
                                }
                            }
                        }
                )
        )
    }

    fun checkIsExist(path: String, id: String, onExist: () -> Unit) {
        when (path) {
            DirectVideoLinks.path -> isVideoExist(id.toInt(), onExist)
            GamesIndexPageIndividualGames.path -> isGameExist(id.toInt(), onExist)
            GamersIndexPageIndividualGamers.path -> isGamerExist(id.toInt(), onExist)
            SectionIndexPageSeeAll.path -> isSectionExist(id.toInt(), onExist)
            GameTypeIndexPage.path -> isGameTypeExist(id, onExist)
        }
    }

    interface HomeSectionsView : BaseView {
        fun updateSections(sections: List<SectionModel>)
    }
}