package com.android.tankee.ui.authorization.navigation

data class FragmentAnimationModel(var enterAnim: Int = -1,
                                  var enterAnim2: Int = -1,
                                  var exitAnim: Int = -1,
                                  var exitAnim2: Int = -1
)