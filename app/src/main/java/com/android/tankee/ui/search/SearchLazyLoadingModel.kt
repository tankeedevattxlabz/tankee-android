package com.android.tankee.ui.search

import com.android.tankee.EMPTY_STRING
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.base.lazy_loading.LazyLoadingBaseModel

class SearchLazyLoadingModel(
        var title: String = EMPTY_STRING,
        var items: MutableList<Video> = mutableListOf()
) : LazyLoadingBaseModel() {
    var totalResult: Int = 0

    override fun reset() {
        super.reset()
        title = EMPTY_STRING
        items.clear()
        totalResult = 0
    }
}