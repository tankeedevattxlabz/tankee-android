package com.android.tankee.ui.home.toolbar.toolbar_item


import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.android.tankee.R
import com.android.tankee.extentions.gone
import com.android.tankee.extentions.show
import com.android.tankee.ui.home.toolbar.toolbar_item.BaseToolbarItem
import com.android.tankee.utils.ResUtils
import com.android.tankee.utils.dp
import kotlinx.android.synthetic.main.toolbar_selectable_item_enabled.view.*


class ToolbarSelectableItem(context: Context, attrs: AttributeSet?) : BaseToolbarItem(context, attrs) {

    override fun onItemClick() {
    }

    override fun getLayoutId(): Int {
        return R.layout.toolbar_selectable_item_enabled
    }

    override fun setBodyImage() {
        if (disabledImageResourceId > 0) {
            itemImage.setImageResource(disabledImageResourceId)
        } else if (imageResourceId > 0) {
            itemImage.setImageResource(imageResourceId)
        }
    }

    override fun setBodyImagePadding(padding: Int) {
    }

    override fun setText(textResourceId: Int) {
        if (textResourceId >
                0) {
            title.setText(textResourceId)
        }
    }

    override fun initConstraintSets() {
    }

    override fun setHighlighted(selected: Boolean) {
        if (selected) {
            itemImage.setImageResource(imageResourceId)
        }
        if (checkIsSelectedItem(selected)) {
            return
        }

        setItemState(selected)
    }

    private fun setItemState(selected: Boolean) {
        if (selected) {
            setEnableState()
            itemImage.setImageResource(imageResourceId)
        } else {
            setDisableState()
        }
        this.itemSelected = selected
    }

    private fun checkIsSelectedItem(selected: Boolean) =
            this.itemSelected == selected || !animationsEnabled

    private fun setEnableState() {
        title.show()
        itemImage.setImageResource(imageResourceId)
        title.animate().alpha(1f).setListener(null)

        val layoutParams = ConstraintLayout.LayoutParams(
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_enable).toInt(),
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_enable).toInt()
        )
        layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.bottomToTop = R.id.title
        itemImage.layoutParams = layoutParams

        right_indicator.animate().alpha(1f)
        left_indicator.animate().alpha(1f)

        right_indicator.animate().translationX(0f)
        left_indicator.animate().translationX(0f)
    }

    private fun setDisableState() {
        title.gone()
        title.animate().alpha(0f).setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                itemImage.setImageResource(disabledImageResourceId)
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }

        })

        val layoutParams = ConstraintLayout.LayoutParams(
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_disable).toInt(),
                ResUtils.getDimension(R.dimen.toolbar_image_item_size_disable).toInt()
        )
        layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
        layoutParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
        itemImage.layoutParams = layoutParams

        right_indicator.animate().alpha(0f)
        left_indicator.animate().alpha(0f)

        right_indicator.animate().translationX(-45f)
        left_indicator.animate().translationX(45f)
    }
}