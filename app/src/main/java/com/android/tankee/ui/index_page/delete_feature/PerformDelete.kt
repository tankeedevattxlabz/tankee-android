package com.android.tankee.ui.index_page.delete_feature

import com.android.tankee.analytics.AnalyticsManager
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.responses.sections.ClipVideo
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.index_page.adapter.IndexPageAdapter
import com.android.tankee.ui.views.see_all.SectionItemMarker

fun performDelete(
    items: List<SectionItemMarker>,
    baseView: BaseView?,
    listener: IndexPageAdapter.DeleteClickListener?
) {

    val isVideoFilter: (SectionItemMarker) -> Boolean = { it is ClipVideo }

    val clipVideo = items.filter(isVideoFilter)

    if (clipVideo.isNotEmpty()) {
        val list = ArrayList<Int>()

        for (item in clipVideo) {
            val clip = item as ClipVideo
            clip.clipInfo?.let {
                list.add(it.id!!)
                AnalyticsManager.deleteClipVideo()
            }
        }
        TankeeRest.api.deleteClips(list).enqueue(baseView!!.createCallBack(
            { response ->
                if (response.isSuccessful) {
                    listener?.let {
                        it.onDeleteFinished()
                    }
                }

            },
            { t ->
                t.printStackTrace()
            }
        )
        )

    }


}