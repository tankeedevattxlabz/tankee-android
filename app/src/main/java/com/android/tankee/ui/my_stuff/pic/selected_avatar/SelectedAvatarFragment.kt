package com.android.tankee.ui.my_stuff.pic.selected_avatar


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.transition.TransitionInflater
import com.android.tankee.R
import com.android.tankee.ui.base.BaseFragment
import com.android.tankee.ui.my_stuff.pic.pic_avatar.AvatarModel
import kotlinx.android.synthetic.main.fragment_pic_avatar.*


class SelectedAvatarFragment : BaseFragment(), SelectedAvatarView {

    private lateinit var selectedAvatarPresenter: SelectedAvatarPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        selectedAvatarPresenter = SelectedAvatarPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_selected_avatar, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectedAvatarPresenter.initSelectedAvatar()
    }

    override fun setSelectedAvatar(avatarModel: AvatarModel) {
        selectedAvatar.setImageResource(avatarModel.image)
    }

}
