package com.android.tankee.js


import android.webkit.JavascriptInterface
import com.android.tankee.getAppContext
import com.android.tankee.logs.AppLogger
import org.jetbrains.anko.runOnUiThread


abstract class ReceiveJavaScriptInterface {

    @JavascriptInterface
    fun processHTML(html: String) {
        AppLogger.logDebug(TAG, "HTML\t$html")
        receiveAction(html)
    }

    abstract fun receiveAction(html: String)

    companion object {
        private var TAG: String = ReceiveJavaScriptInterface::class.java.simpleName
        const val HTML_OUT = "HTML_OUT"
    }
}
