package com.android.tankee.cookies


import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.android.tankee.EMPTY_STRING
import com.android.tankee.R
import com.android.tankee.getAppContext
import com.android.tankee.utils.getLong


object TankeePrefs {

    private val prefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(getAppContext())
    }

    var userUid: String
        set(value) {
            prefs.edit().putString(PrefsKeys.USER_UID, value).apply()
        }
        get() = prefs.getString(PrefsKeys.USER_UID, EMPTY_STRING) ?: EMPTY_STRING

    var token: String
        set(value) {
            prefs.edit().putString(PrefsKeys.TOKEN, value).apply()
        }
        get() = prefs.getString(PrefsKeys.TOKEN, EMPTY_STRING) ?: EMPTY_STRING

    var isNativePlayer: Boolean
        set(value) {
            prefs.edit().putBoolean(PrefsKeys.IS_NATIVE_PLAYER, value).apply()
        }
        get() = prefs.getBoolean(PrefsKeys.IS_NATIVE_PLAYER, true)

    var isAdMob: Boolean
        set(value) {
            prefs.edit().putBoolean(PrefsKeys.IS_AD_MOB, value).apply()
        }
        get() = prefs.getBoolean(PrefsKeys.IS_AD_MOB, true)

    var updateAdInterval: Long
        set(value) {
            prefs.edit().putLong(PrefsKeys.UPDATE_AD_INTERVAL, value).apply()
        }
        get() = prefs.getLong(PrefsKeys.UPDATE_AD_INTERVAL, getLong(R.integer.update_banner_ad_interval))

    var numberOfVideosWatched: Int
        set(value) {
            prefs.edit().putInt(PrefsKeys.NUMBER_OF_VIDEOS_WATCHED, value).apply()
        }
        get() = prefs.getInt(PrefsKeys.NUMBER_OF_VIDEOS_WATCHED, 0) ?: 0

    var videoWatchTime: Int
        set(value) {
            prefs.edit().putInt(PrefsKeys.VIDEO_WATCH_TIME, value).apply()
        }
        get() = prefs.getInt(PrefsKeys.VIDEO_WATCH_TIME, 0) ?: 0

    fun clearAll() {
        prefs.edit()
            .remove("com.android.tankee.database.userUid")
            .remove("com.android.tankee.database.token")
            .remove("com.android.tankee.database.numberOfVideosWatched")
            .remove("com.android.tankee.database.videoWatchTime").
            apply()
    }

    var TAG = TankeePrefs::class.java.simpleName


}

object PrefsKeys {

    const val USER_UID = "com.android.tankee.database.userUid"
    const val TOKEN = "com.android.tankee.database.token"
    const val IS_NATIVE_PLAYER = "com.android.tankee.database.is_native_player"
    const val IS_AD_MOB= "com.android.tankee.database.is_ad_mob"
    const val UPDATE_AD_INTERVAL = "com.android.tankee.database.update_ad_interval"
    const val NUMBER_OF_VIDEOS_WATCHED = "number_of_videos";
    const val VIDEO_WATCH_TIME = "ten_minute_time";
}