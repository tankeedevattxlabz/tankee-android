package com.android.tankee.cookies


import com.android.tankee.dynamic_links.DynamicLink
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.rest.responses.ad.VideoPlayerBannerModel
import com.android.tankee.rest.responses.users.UserModel
import com.android.tankee.rest.responses.video_ads_configurations.VideoAdsConfigurationsModel


object SessionDataHolder {

    var user: UserModel? = null
    var dynamicLink: DynamicLink? = null
    var videoPlayerBannerModel: VideoPlayerBannerModel? = null
    var videoAdsConfigurations: List<VideoAdsConfigurationsModel>? = null
    var fcmToken: String? = null
    var notificationModel: NotificationModel? = null
    var kidozInitialized : Boolean = false;
    fun clearDynamicLink() {
        dynamicLink = null
    }

    fun clearNotificationModel() {
        notificationModel = null
    }
}

