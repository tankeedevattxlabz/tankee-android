package com.android.tankee.utils

import com.android.tankee.R

object ResWrapper {

    fun getHeartImageDelay(): Long {
        return ResUtils.getInteger(R.integer.heart_image_delay).toLong()
    }

    fun getHeartImageScaleDuration(): Long {
        return ResUtils.getInteger(R.integer.heart_image_scale_duration).toLong()
    }
}