package com.android.tankee.utils

import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.android.tankee.TankeeApp
import com.android.tankee.getAppContext
import android.util.DisplayMetrics
import android.widget.TextView
import androidx.annotation.DrawableRes


val screenWidth = TankeeApp.display.width
val screenHeight = TankeeApp.display.height

fun String.makeClickable(onClick: () -> Unit): SpannableString {
    val span = object : ClickableSpan() {
        override fun onClick(p0: View) {
            onClick()
        }
    }
    val spannableString = SpannableString(this)
    spannableString.setSpan(span, 0, this.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
    return spannableString
}

fun SpannableString.color(@ColorRes color: Int): SpannableString {
    val colorSpan = ForegroundColorSpan(getAppContext().resources.getColor(color))
    this.setSpan(colorSpan, 0, this.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
    return this
}

fun setVisibilityInto(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

fun showView(view: View?) {
    view?.let { it.visibility = View.VISIBLE }
}

fun hideView(view: View?) {
    view?.let { it.visibility = View.GONE }
}

fun String.bold(): Spannable {
    val sb = SpannableStringBuilder(this)
    val bss = StyleSpan(android.graphics.Typeface.BOLD)
    sb.setSpan(bss, 0, this.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
    return sb
}

fun getColor(colorId: Int) = ContextCompat.getColor(getAppContext(), colorId)

fun Int.dp(): Int {
    val r = TankeeApp.app.resources
    val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this.toFloat(),
            r.displayMetrics
    )
    return px.toInt()
}

fun Int.px(): Int {
    val r = TankeeApp.app.resources
    val pd = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX,
            this.toFloat(),
            r.displayMetrics
    )
    return pd.toInt()
}

fun getScreenSize(): DisplayMetrics? {
    return TankeeApp.app.resources.displayMetrics
}

fun View.getWindowPositionX(): Long {
    val array = IntArray(2) { 0 }
    this.getLocationInWindow(array)
    return array[0].toLong()
}

fun View.getWindowPositionY(): Long {
    val array = IntArray(2) { 0 }
    this.getLocationInWindow(array)
    return array[1].toLong()
}

fun getDrawerFromResources(drawerId: String): Drawable? {
    return getAppContext().getDrawable(
            getAppContext().resources.getIdentifier(
                    drawerId,
                    "drawable",
                    getAppContext().packageName
            )
    )
}
