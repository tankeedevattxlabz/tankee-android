package com.android.tankee.utils


import android.graphics.drawable.Drawable
import com.android.tankee.EMPTY_STRING
import com.android.tankee.getAppContext
import com.android.tankee.getAppRecourse


object ResUtils {

    fun getString(stringId: Int?): String {
        if (stringId == null) return EMPTY_STRING
        if (stringId <= 0) return EMPTY_STRING
        return getAppContext().resources.getString(stringId)
    }

    fun getDrawableIdByString(variableName: String): Int {
        return getRecourseIdByString(variableName, "drawable", getAppContext().packageName)
    }

    fun getDrawable(drawableId: Int): Drawable? {
        return getAppRecourse().getDrawable(drawableId, null)
    }

    private fun getRecourseIdByString(variableName: String, resourceName: String, packageName: String): Int {
        return try {
            getAppContext().resources.getIdentifier(variableName, resourceName, packageName)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }

    fun getColor(colorId: Int): Int {
        return getAppContext().resources.getColor(colorId)
    }

    fun getInteger(integerId: Int): Int {
        return getAppContext().resources.getInteger(integerId)
    }

    fun getDimension(dimenId: Int): Float {
        return getAppContext().resources.getDimension(dimenId)
    }

    fun getBoolean(booleanId: Int): Boolean {
        return getAppContext().resources.getBoolean(booleanId)
    }

}

fun getStringArray(stringArrayId: Int?): List<String> {
    if (stringArrayId == null) return listOf()
    if (stringArrayId <= 0) return listOf()
    return getAppRecourse().getStringArray(stringArrayId).toList()
}

fun getString(stringId: Int?): String {
    if (stringId == null) return EMPTY_STRING
    if (stringId <= 0) return EMPTY_STRING
    return getAppContext().resources.getString(stringId)
}

fun getLong(longId: Int?): Long {
    if (longId == null) return 0
    if (longId <= 0) return 0
    return getAppContext().resources.getInteger(longId).toLong()
}