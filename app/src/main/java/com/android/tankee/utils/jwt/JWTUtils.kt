package com.android.tankee.utils.jwt

import android.util.Base64
import android.util.Log
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class JWTUtils {
    companion object {

        @JvmStatic
        fun decode(jwtEncoded: String) {
            try {
                val split = jwtEncoded.split("\\.")
                Log.d(JWT_DECODE, "Header: ${getJson(split[0])}")
                Log.d(JWT_DECODE, "Body: ${getJson(split[1])}")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        }

        @JvmStatic
        private fun getJson(stringEncoded: String): String {
            val decodedBytes = Base64.decode(stringEncoded, Base64.URL_SAFE)
            return String(decodedBytes, Charset.defaultCharset())
        }

        private const val JWT_DECODE = "JWT_DECODE"
        private const val UTF_8 = "UTF_8"
    }
}