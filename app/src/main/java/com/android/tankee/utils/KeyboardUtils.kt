package com.android.tankee.utils


import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.android.tankee.getAppContext


fun showKeyboard(view: View) {
    val imm = getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}

fun hideKeyboard(view: View) {
    val imm = getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.hideSoftInputFromWindow(view.windowToken, 0)
}