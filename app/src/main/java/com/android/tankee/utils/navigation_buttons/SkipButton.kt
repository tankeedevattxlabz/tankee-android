package com.android.tankee.utils.navigation_buttons


import android.content.Context
import android.util.AttributeSet
import com.android.tankee.R
import com.android.tankee.extentions.disable
import com.android.tankee.extentions.enable
import com.github.florent37.viewanimator.ViewAnimator
import kotlinx.android.synthetic.main.view_tankee_button.view.*


class SkipButton(context: Context, attributeSet: AttributeSet) : BaseNavigationButton(context, attributeSet) {

    var onClickAction: (() -> Unit)? = null

    init {
        inflate(context, R.layout.view_skip_button, this)

        tankeeButtonRoot.setOnClickListener {
            tankeeButtonRoot.disable()
            tankeeButtonRoot.postDelayed({ tankeeButtonRoot.enable() }, 500)

            ViewAnimator
                    .animate(it)
                    .scale(1f, 0.9f, 1f)
                    .duration(500)
                    .onStop {
                        onClickAction?.invoke()
                    }
                    .start()
        }
    }

}