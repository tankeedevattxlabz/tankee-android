package com.android.tankee.utils


import android.content.Context
import android.net.ConnectivityManager
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.android.tankee.*
import com.android.tankee.extentions.isNull
import java.util.regex.Pattern


fun isTable(): Boolean {
    return getAppContext().resources.getBoolean(R.bool.isTablet)
}

fun isNotTable() = !isTable()


fun getInteger(integerId: Int): Int {
    return getAppContext().resources.getInteger(integerId)
}

fun getDimen(dimenId: Int): Float {
    return getAppContext().resources.getDimension(dimenId)
}

fun buildThumbnailUrl(url: String?): String? {
    if (url == null) return url
    if (url.contains(HTTP) || url.contains(HTTPS)) return url
    return BASE_THUMBNAIL_URL_IMAGE + url
}

fun setSpanColor(span: SpannableStringBuilder, string: String, text: String, color: Int) {
    val start = text.indexOf(string)
    val end = start + string.length
    try {
        span.setSpan(ForegroundColorSpan(color), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    } catch (ignore: IndexOutOfBoundsException) {
        ignore.printStackTrace()
    }
}

fun setSpanColor(textView: TextView, string: String, text: String, color: Int = 0) {
    val colorValue = if (color == 0) {
        getAppContext().resources.getColor(R.color.colorBlack)
    } else {
        getAppContext().resources.getColor(color)
    }

    val start = text.toLowerCase().indexOf(string.toLowerCase())
    val end = start + string.length
    val span = SpannableStringBuilder(text)

    try {
        span.setSpan(ForegroundColorSpan(colorValue), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.text = span
    } catch (ignore: IndexOutOfBoundsException) {
        ignore.printStackTrace()
        textView.text = span
    }
}

fun setSpanStringBold(span: SpannableStringBuilder, string: String, text: String) {
    val start = text.indexOf(string)
    val end = start + string.length
    val font = ResourcesCompat.getFont(getAppContext(), R.font.pt_sans_web_bold)

    try {
        span.setSpan(CustomTypefaceSpan("", font!!), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
    } catch (ignore: IndexOutOfBoundsException) {
        ignore.printStackTrace()
    }
}

fun buildUrl(url: String?): String? {
    if (url == null) return url
    return BASE_THUMBNAIL_URL_IMAGE + url
}

fun millisToMinutes(millis: Int): String {
    return if (millis < 3600) {
        String.format("%d:%02d", millis / 60, millis % 60)
    } else {
        val minutes = millis % 3600 / 60
        String.format("%d%02d:%02d", millis / 3600, minutes, minutes % 60)
    }
}

fun <T> Sequence<T>.batch(n: Int): Sequence<List<T>> {
    return BatchingSequence(this, n)
}

private class BatchingSequence<T>(val source: Sequence<T>, val batchSize: Int) : Sequence<List<T>> {
    override fun iterator(): Iterator<List<T>> = object : AbstractIterator<List<T>>() {
        val iterate = if (batchSize > 0) source.iterator() else emptyList<T>().iterator()
        override fun computeNext() {
            if (iterate.hasNext()) setNext(iterate.asSequence().take(batchSize).toList())
            else done()
        }
    }
}

fun <T> chopped(list: List<T>, L: Int): List<List<T>> {
    val parts = ArrayList<List<T>>()
    val listSize = list.size
    var i = 0
    while (i < listSize) {
        parts.add(ArrayList(list.subList(i, Math.min(listSize, i + L))))
        i += L
    }
    return parts
}

fun isNetworkAvailable(): Boolean {
    return TankeeApp.connection.isConnection
}

fun <T> List<T>.getLastIndex(): Int {
    return size - 1
}

fun <T> List<T>.getLastItem(): T? {
    return if (isNotEmpty()) {
        get(size - 1)
    } else {
        null
    }
}

fun <T> List<T>.withoutLastItem(): List<T> {
    return if (isNotEmpty()) {
        subList(0, getLastIndex())
    } else {
        listOf()
    }
}


fun millisToSeconds(millis: Long): Long {
    return millis / 1000
}

fun containtAtSign(string: String): Boolean {
    return string.contains(AT_SIGN)
}
/**
 * method is used for checking valid email id format.
 *
 * @param email
 * @return boolean true for valid false for invalid
 */
fun isEmailValid(email: String): Boolean {
    val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
    val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(email)
    return matcher.matches()
}

fun String?.isNumber(): Boolean {
    if (isNull()) return false
    if (this!!.isEmpty()) return false
    return this.matches("\\d+".toRegex())
}



