package com.android.tankee.utils

import android.util.Log

fun Any.log(tag : String = "",msg : String = ""){
    Log.w("DEBUGGING_$tag","$msg : ${this}")
}