package com.android.tankee.utils

import android.os.Bundle
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.video_player.VIDEO_ARG

object FragmentArgumentsUtils {

    fun getVideoFromArgs(arguments: Bundle?): Video? {
        return if (arguments != null && arguments.containsKey(VIDEO_ARG)) {
            arguments.getParcelable(VIDEO_ARG) as Video?
        } else {
            null
        }
    }
}