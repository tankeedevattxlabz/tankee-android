package com.android.tankee.global

import com.android.tankee.EMPTY_STRING
import com.android.tankee.extentions.isNull
import com.android.tankee.notifications.model.NotificationType

enum class TankeeContent(val type: String) {

    GAME("game"),
    GAMER("gamer"),
    VIDEO("video"),
    SECTION("section"),
    GAME_TYPE("game_type"),
    UNKNOWN("unknown");

    companion object {

        fun isGame(content: String): Boolean {
            return content == GAME.type
        }

        fun isGamer(content: String): Boolean {
            return content == GAMER.type
        }

        fun isVideo(content: String): Boolean {
            return content == VIDEO.type
        }

        fun isSection(content: String): Boolean {
            return content == SECTION.type
        }

        fun isGameType(content: String): Boolean {
            return content == GAME_TYPE.type
        }

        fun buildContent(content: String?): TankeeContent {
            return if (content != null) {
                when {
                    isGame(content) -> GAME
                    isGamer(content) -> GAMER
                    isVideo(content) -> VIDEO
                    isSection(content) -> SECTION
                    isGameType(content) -> GAME_TYPE
                    else -> UNKNOWN
                }
            } else {
                UNKNOWN
            }
        }
    }

}