package com.android.tankee.analytics


import android.os.Bundle
import android.util.Log
import androidx.core.os.bundleOf
import com.android.tankee.BuildConfig
import com.android.tankee.TankeeApp
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.video_player.VIdeoPlayerFragment
import com.android.tankee.utils.log
import com.bumptech.glide.load.resource.bitmap.VideoDecoder
import com.google.firebase.analytics.FirebaseAnalytics
import kotlin.properties.Delegates


object EventSender {
    private val mFirebaseAnalytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(TankeeApp.app)

    fun logEvent(event: AnalyticsEvent, paramsBundle: Bundle) {
//        paramsBundle.putString(AnalyticsParameter.FIREBASE_EVENT_ORIGIN.paramName,"android_app")

        if (BuildConfig.DEBUG) {
            event.params.forEach {
                paramsBundle.get(it.paramName)
                    ?: throw IllegalStateException("Sending ${event.eventName} requires param: ${it.paramName}")
            }
        }

        if (!BuildConfig.DEBUG) {
            mFirebaseAnalytics.logEvent(event.eventName, paramsBundle)
        } else {
            val builder = StringBuilder("       ${event.eventName} : ")
            event.params.forEach {
                val get = paramsBundle.get(it.paramName)
                builder.append("\n              ")
                builder.append(get)
            }
            builder.toString().log("TankeeAnalytics")
        }
    }

    fun logOpenVideoEvent(video: Video, sectionName: String?, pageName: String) {
        logEvent(
            AnalyticsEvent.VIDEO_OPEN_EVENT, bundleOf(
                AnalyticsParameter.USER_NAME.paramName to (SessionDataHolder.user?.fullName ?: "NO_NAME"),
                AnalyticsParameter.USER_ID.paramName to (SessionDataHolder.user?.id ?: "NO_USER_ID"),
                AnalyticsParameter.DEVICE_ID.paramName to (SessionDataHolder.user?.uid ?: "NO_USER_UID"),
                AnalyticsParameter.VIDEO_NAME.paramName to (video.title),
                AnalyticsParameter.SECTION_NAME.paramName to (sectionName ?: "NO_SECTION_NAME"),
                AnalyticsParameter.PAGE_NAME.paramName to (pageName)
            )
        )
    }


    fun logVideoTimeTenMin(video: Video){
        logEvent(
                AnalyticsEvent.VIDEO_TIME_10MIN, bundleOf(
                AnalyticsParameter.VIDEO_NAME.paramName to (video.title),
                AnalyticsParameter.USER_NAME.paramName to (SessionDataHolder.user?.fullName ?: "NO_NAME"),
                AnalyticsParameter.USER_ID.paramName to (SessionDataHolder.user?.id ?: "NO_USER_ID"),
                AnalyticsParameter.DEVICE_ID.paramName to (SessionDataHolder.user?.uid ?: "NO_USER_UID")
        )
        )
    }

    fun logFiveVideosOpen(){
     logEvent(
             AnalyticsEvent.OPEN_5VIDEOS_EVENT, bundleOf(
             AnalyticsParameter.USER_NAME.paramName to (SessionDataHolder.user?.fullName ?: "NO_NAME"),
             AnalyticsParameter.USER_ID.paramName to (SessionDataHolder.user?.id ?: "NO_USER_ID"),
             AnalyticsParameter.DEVICE_ID.paramName to (SessionDataHolder.user?.uid ?: "NO_USER_UID"))
     )

    }
}

//Comment non-mandatory params
enum class AnalyticsEvent(val eventName: String, val params: List<AnalyticsParameter>) {
    APP_UPDATE(
        "app_update",
        listOf()
    ),
    BROWSE_ACTION(
        "browse_action",
        listOf(
            AnalyticsParameter.DEVICE_ID
        )
    ),
    ADD_STICKER(
        "add_sticker_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.GAME_TYPE,
            AnalyticsParameter.STICKER_NAME,
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.VIDEO_SOURCE,
            AnalyticsParameter.VIDEO_TIME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    OPEN_GAME(
        "open_game_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.GAME_NAME,
            AnalyticsParameter.PAGE_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    FIRST_GO_TO_TANKEE_BUTTON(
        "first_go_to_tankee_button_event",
        listOf(
            AnalyticsParameter.DEVICE_ID
        )
    ),
    FIRST_LEARM_MORE_BUTTON(
        "first_learm_more_button_event",
        listOf(
            AnalyticsParameter.DEVICE_ID
        )
    ),
    OPEN_GAMER(
        "open_gamer_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.GAMER_NAME,
            AnalyticsParameter.PAGE_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    OPEN_GAME_TYPE(
        "open_game_type",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.GAME_TYPE,
            AnalyticsParameter.PAGE_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    SEE_ALL_EVENT(
        "see_all_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.SECTION_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    NOTIFICATION_OPEN_EVENT(
        "notification_open",
        listOf(
            AnalyticsParameter.FIREBASE_EVENT_ORIGIN
        )
    ),
    NOTIFICATION_CLICK(
        "click_notification",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.ITEM_ID,
            AnalyticsParameter.ITEM_NAME,
            AnalyticsParameter.TYPE
        )
    ),
    TOP_BAR_ITEM_EVENT(
        "top_bar_item_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.ITEM_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    VIDEO_OPEN_EVENT(
        "open_video_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
//            AnalyticsParameter.FIREBASE_CONVERSION,
//            AnalyticsParameter.FIREBASE_ERROR,
//            AnalyticsParameter.FIREBASE_EVENT_ORIGIN,
            AnalyticsParameter.PAGE_NAME,
            AnalyticsParameter.SECTION_NAME,
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    VIDEO_TIME_10MIN("video_time_10min", listOf(
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
       )
    )
    ,
    OPEN_5VIDEOS_EVENT("open_5videos_event", listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    )
    ,
    VIDEO_OPEN_TOOLBAR(
        "video_open_toolbar_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.TIME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    VIDEO_CLOSE_TOOLBAR(
        "video_close_toolbar_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.TIME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    VIDEO_REPLAY_EVENT(
        "replay_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    VIDEO_PLAYER_BACK_BUTTON(
        "video_player_back_button_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.TIME_ON_SCREEN,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    SKIP_EVENT(
        "skip_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    WATCH_FOR_PARENT(
        "watch_for_parent_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.USER_NAME
        )
    ),
    SPLASH_PAGE_JOIN_OR_SIGN_IN_BUTTON(
        "Splash_Page_Join_or_Sign-In_Button",
        listOf()
    ),
    TANKEE_SIGN_IN_FORGOT_PASSWORD_BUTTON(
        "Tankee_Sign-in_Forgot_Password_Button",
        listOf()
    ),
    TANKEE_SIGN_IN_BUTTON(
        "Tankee_Sign-in_Sign-in_Button",
        listOf()
    ),
    TANKEE_SIGN_IN_JOIN_NEXT_BUTTON(
        "Tankee_Sign-in_Join_Button",
        listOf()
    ),
    TANKEE_JOIN_NEXT_BUTTON(
        "Tankee_Join_Next_Button",
        listOf()
    ),
    TANKEE_JOIN_PARENT_EMAIL_NEXT_BUTTON(
        "Tankee_Join_Parent’s_Email_Next_Button",
        listOf()
    ),
    TANKEE_JOIN_NAME_GENERATOR_JOIN_BUTTON(
        "Tankee_Join_Name_Generator_Join_Button",
        listOf()
    ),
    HAMBURGER_MENU_SWITCH_USER_BUTTON(
        "Hamburger_Menu_Switch_User_Button",
        listOf()
    ),
    HAMBURGER_MENU_DELETE_USER_BUTTON(
        "Hamburger_Menu_Delete_User_Button",
        listOf()
    ),
    PICK_AVATAR_EVENT(
         "pick_avatar_event",
         listOf(
                 AnalyticsParameter.DEVICE_ID,
                 AnalyticsParameter.USER_ID,
                 AnalyticsParameter.USER_NAME,
                 AnalyticsParameter.AVATAR_NAME
         )
    ),
    ADD_FAVORITE_VIDEO(
        "favorite_video_event",
        listOf(
            AnalyticsParameter.VIDEO_NAME,
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    ADD_CLIP_VIDEO(
        "video_create_clip_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
    DELETE_FAVORITE_VIDEO(
        "unfavorite_video_button_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),

    DELETE_CLIP_VIDEO(
        "delete_clip_event",
        listOf(
            AnalyticsParameter.DEVICE_ID,
            AnalyticsParameter.USER_ID,
            AnalyticsParameter.USER_NAME
        )
    ),
}

enum class AnalyticsParameter(val paramName: String) {
    DEVICE_ID("device_id"),
    FIREBASE_EVENT_ORIGIN("firebase_event_origin"),
    GAME_NAME("game_name"),
    GAMER_NAME("gamer_name"),
    GAME_TYPE("game_type"),
    STICKER_NAME("sticker_name"),
    USER_ID("user_id"),
    USER_NAME("user_name"),
    VIDEO_NAME("video_name"),
    VIDEO_SOURCE("video_source"),
    VIDEO_TIME("video_time"),
    ITEM_ID("item_id"),
    ITEM_NAME("item_name"),
    TYPE("type"),
    FIREBASE_CONVERSION("firebase_conversion"),
    FIREBASE_ERROR("firebase_error"),
    PAGE_NAME("page_name"),
    SECTION_NAME("section_name"),
    TIME("time"),
    TIME_ON_SCREEN("time_on_screen"),
    PLAYER_SCREEN("player"),
    MORE_VIDEOS_SCREEN("more_videos"),
    AVATAR_NAME("avatar_name"),
}

fun Bundle.put(param: AnalyticsParameter, value: Any) {
    this.putString(param.paramName, value.toString())
}