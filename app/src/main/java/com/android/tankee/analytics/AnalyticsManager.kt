package com.android.tankee.analytics


import android.os.Bundle
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.repo.user.AnonymousUserRepo
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.time.TimeUtils
import com.android.tankee.ui.home.videos.GameType
import java.util.*


object AnalyticsManager {

    /**
     * Game
     */
    fun openGameEvent(game: Game, pageName: AnalyticsPageName) {
        EventSender.logEvent(
            AnalyticsEvent.OPEN_GAME,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putGameName(game.name)
                .putPageName(pageName)
                .build()
        )
    }

    /**
     * Gamer
     */
    fun openGamerEvent(gamer: Gamer, pageName: AnalyticsPageName) {
        EventSender.logEvent(
            AnalyticsEvent.OPEN_GAMER,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putGamerName(gamer.name)
                .putPageName(pageName)
                .build()
        )
    }

    fun openGameTypeEvent(gameType: GameType, pageName: AnalyticsPageName) {
        EventSender.logEvent(
            AnalyticsEvent.OPEN_GAME_TYPE,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putGameType(gameType.name)
                .putPageName(pageName)
                .build()
        )
    }

    fun seeAllEvent(sectionName: String?) {
        EventSender.logEvent(
            AnalyticsEvent.SEE_ALL_EVENT,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putSectionName(sectionName)
                .build()
        )
    }

    /**
     * More Videos
     */
    fun replayEvent(video: Video) {
        EventSender.logEvent(
            AnalyticsEvent.VIDEO_REPLAY_EVENT,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putVideoName(video.title)
                .build()
        )
    }

    fun toolbarItemEvent(itemName: String) {
        EventSender.logEvent(
            AnalyticsEvent.TOP_BAR_ITEM_EVENT,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putItemName(itemName)
                .build()
        )
    }

    fun videoOpenToolbarEvent() {
        EventSender.logEvent(
            AnalyticsEvent.VIDEO_OPEN_TOOLBAR,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putTime()
                .build()
        )
    }

    fun videoCloseToolbarEvent() {
        EventSender.logEvent(
            AnalyticsEvent.VIDEO_CLOSE_TOOLBAR,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putTime()
                .build()
        )
    }

    fun watchForParentEvent(video: Video) {
        EventSender.logEvent(
            AnalyticsEvent.WATCH_FOR_PARENT,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putVideoName(video.title)
                .build()
        )
    }

    fun tankeeSignInForgotPasswordButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_SIGN_IN_FORGOT_PASSWORD_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeSignInButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_SIGN_IN_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeSignInJoinButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_SIGN_IN_JOIN_NEXT_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeJoinNextButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_JOIN_NEXT_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeJoinParentEmailNextButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_JOIN_PARENT_EMAIL_NEXT_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeJoinGenerateNameButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.TANKEE_JOIN_NAME_GENERATOR_JOIN_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeHamburgerMenuSwitchUserButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.HAMBURGER_MENU_SWITCH_USER_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun tankeeHamburgerMenuDeleteUserButtonEvent() {
        EventSender.logEvent(
            AnalyticsEvent.HAMBURGER_MENU_DELETE_USER_BUTTON,
            AnalyticsParamsBuilder()
                .build()
        )
    }

    fun pickAvatarEvent(avatarName: String) {
        EventSender.logEvent(
                AnalyticsEvent.PICK_AVATAR_EVENT,
                AnalyticsParamsBuilder()
                        .putBaseParams()
                        .putAvatarName(avatarName)
                        .build()
        )
    }

    fun addFavoriteVideo(itemName: String) {
        EventSender.logEvent(
            AnalyticsEvent.ADD_FAVORITE_VIDEO,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .putVideoName(itemName)
                .build()
        )
    }
    fun deleteFavoriteVideo() {
        EventSender.logEvent(
            AnalyticsEvent.DELETE_FAVORITE_VIDEO,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .build()
        )
    }
    fun addClipVideo() {
        EventSender.logEvent(
            AnalyticsEvent.ADD_CLIP_VIDEO,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .build()
        )
    }
    fun deleteClipVideo() {
        EventSender.logEvent(
            AnalyticsEvent.DELETE_CLIP_VIDEO,
            AnalyticsParamsBuilder()
                .putBaseParams()
                .build()
        )
    }
}

class AnalyticsParamsBuilder {

    private val bundle = Bundle()

    fun putBaseParams(): AnalyticsParamsBuilder {
        putDeviceId()
        putUserId()
        putUserName()

        return this
    }

    fun putDeviceId(): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.DEVICE_ID, SessionDataHolder.user?.uid ?: AnalyticsConstants.NO_DEVICE)
        return this
    }

    /**
     * User
     */
    private fun putUserId(): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.USER_ID, SessionDataHolder.user?.id ?: AnalyticsConstants.NO_ID)
        return this
    }

    private fun putUserName(): AnalyticsParamsBuilder {
        var userName = SessionDataHolder.user?.userName
        if (SessionDataHolder.user?.fullName == null) {
            if (AnonymousUserRepo().isNotUserAnonymous()) {
                userName = SessionDataHolder.user?.userName
            }
        }
        bundle.put(AnalyticsParameter.USER_NAME,userName ?: AnalyticsConstants.NO_NAME)
        return this
    }

    /**
     * Game
     */
    fun putGameName(gameName: String?): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.GAME_NAME, gameName ?: AnalyticsConstants.NO_NAME)
        return this
    }

    /**
     * Gamer
     */
    fun putGamerName(gamerName: String?): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.GAMER_NAME, gamerName ?: AnalyticsConstants.NO_NAME)
        return this
    }

    fun putSectionName(sectionName: String?): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.SECTION_NAME, sectionName ?: AnalyticsConstants.NO_NAME)
        return this
    }

    fun putPageName(pageName: AnalyticsPageName): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.PAGE_NAME, pageName.value)
        return this
    }

    fun putVideoName(videoName: String?): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.VIDEO_NAME, videoName ?: AnalyticsConstants.NO_NAME)
        return this
    }

    fun putItemName(itemName: String): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.ITEM_NAME, itemName)
        return this
    }

    fun putTime(): AnalyticsParamsBuilder {
        TimeUtils.convertTimeToUTC(Calendar.getInstance().timeInMillis)?.let { bundle.put(AnalyticsParameter.TIME, it) }
        return this
    }

    fun putGameType(gameType: String): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.GAME_TYPE, gameType)
        return this
    }

    fun putAvatarName(avatarName: String): AnalyticsParamsBuilder {
        bundle.put(AnalyticsParameter.AVATAR_NAME, avatarName)
        return this
    }

    fun build(): Bundle {
        return bundle
    }
}


object AnalyticsConstants {

    const val NO_DEVICE = "NO_DEVICE"
    const val NO_NAME = "NO_NAME"
    const val NO_ID = "NO_ID"

}

enum class AnalyticsPageName(val value: String) {

    VIDEOS("videos"),
    SEARCH("search"),
    MENU("menu"),
    ABOUT("about"),
    MY_STUFF("my_stuff")

}