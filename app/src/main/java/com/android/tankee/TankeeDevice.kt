package com.android.tankee

import com.android.tankee.utils.ResUtils

object TankeeDevice {

    fun isTablet(): Boolean {
        return ResUtils.getBoolean(R.bool.isTablet)
    }

    fun isPhone(): Boolean {
        return !isTablet()
    }
}