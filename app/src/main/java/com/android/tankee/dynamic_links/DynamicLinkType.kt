package com.android.tankee.dynamic_links


interface DynamicLinkType


data class VideoDynamicLink(var id: Int) : DynamicLinkType

data class GameDynamicLink(var id: Int) : DynamicLinkType

data class GamerDynamicLink(var id: Int) : DynamicLinkType

data class SectionDynamicLink(var id: Int) : DynamicLinkType

data class GameTypeDynamicLink(var id: String) : DynamicLinkType