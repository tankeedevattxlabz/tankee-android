package com.android.tankee.dynamic_links


import android.net.Uri
import com.android.tankee.EMPTY_STRING
import com.android.tankee.logs.AppLogger


data class DynamicLink(var link: Uri?) {

    fun getPath(): String? {
        return link?.path?.removeRange(0, 1)
    }

    fun getQueryId(): String? {
        val dynaLinkQuery = DynamicLinkQueryFactory(getPath() ?: EMPTY_STRING).getQuery()
        val queryParameter = link?.getQueryParameter(dynaLinkQuery?.query)
        AppLogger.logDebug(TAG, "DynamicLinkQuery\t$queryParameter")
        return queryParameter
    }

    companion object {

        private var TAG: String = DynamicLink::class.java.simpleName

    }

}