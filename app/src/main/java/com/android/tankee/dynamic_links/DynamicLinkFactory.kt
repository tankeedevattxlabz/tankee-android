package com.android.tankee.dynamic_links


import com.android.tankee.dynamic_links.DynamicLinks.*


class DynamicLinkFactory(val path: String) {

    fun createDynamicLink(id: String): DynamicLinkType? {
        return when (path) {
            DirectVideoLinks.path -> VideoDynamicLink(id.toInt())
            GamesIndexPageIndividualGames.path -> GameDynamicLink(id.toInt())
            GamersIndexPageIndividualGamers.path -> GamerDynamicLink(id.toInt())
            SectionIndexPageSeeAll.path -> SectionDynamicLink(id.toInt())
            GameTypeIndexPage.path -> GameTypeDynamicLink(id)
            else -> null
        }
    }
}