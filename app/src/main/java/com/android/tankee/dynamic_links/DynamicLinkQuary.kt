package com.android.tankee.dynamic_links


import com.android.tankee.dynamic_links.DynamicLinks.*


class DynamicLinkQueryFactory(private val path: String) {
    
    fun getQuery(): DynamicLinkQuery? {
       return when (path) {
           DirectVideoLinks.path -> DynamicLinkQuery.VIDEO_ID
            GamesIndexPageIndividualGames.path -> DynamicLinkQuery.GAME_ID
            GamersIndexPageIndividualGamers.path -> DynamicLinkQuery.GAMER_ID
            SectionIndexPageSeeAll.path -> DynamicLinkQuery.SECTION_ID
            GameTypeIndexPage.path -> DynamicLinkQuery.GAME_TYPE_ID
           else -> {
               null
           }
       }
    }
}


enum class DynamicLinkQuery(val query: String) {
    VIDEO_ID("video_id"),
    GAME_ID("game_id"),
    GAMER_ID("gamer_id"),
    SECTION_ID("section_id"),
    GAME_TYPE_ID("game_type"),
}