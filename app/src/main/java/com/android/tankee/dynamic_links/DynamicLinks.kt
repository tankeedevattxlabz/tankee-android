package com.android.tankee.dynamic_links


/**
 * This enum represents all dynamic links.
 *
 * Include generated dynamic links like video, game, gamer etc and it's id.
 * Include static dynamic link only with path.
 *
 * Use link under value for testing.
 */
enum class DynamicLinks(val path: String) {

    // Generated path

    DirectVideoLinks("DirectVideoLinks"),

    GamesIndexPageIndividualGames("GamesIndexPageIndividualGames"),

    GamersIndexPageIndividualGamers("GamersIndexPageIndividualGamers"),

    SectionIndexPageSeeAll("SectionIndexPageSeeAll"),

    GameTypeIndexPage("GameTypeIndexPage"),


    // Static path


    VideosTab("VideosTab"),

    GamesIndexPageAllGames("GamesIndexPageAllGames"),

    GamersIndexPageAllGamers("GamersIndexPageAllGamers"),

    // Need to change link on home screen
    HeroPassSubscriptionPage("HeroPassSubscriptionPage"),

    AvatarSelectionPage("AvatarSelectionPage"),

    TankeeAboutPage("TankeeAboutPage"),

    // Need to change link on home screen
    DownloadsPage("DownloadsPage"),

    NotificationsPage("NotificationsPage"),

    SigncaseupLoginScreen("SigncaseupLoginScreen"),

    MyStuffPage("MyStuffPage"),

    FavoriteGamesPage("FavoriteGamesPage"),

    FavoriteGamersPage("FavoriteGamersPage"),

    FavoriteVideosPage("FavoriteVideosPage"),
}


