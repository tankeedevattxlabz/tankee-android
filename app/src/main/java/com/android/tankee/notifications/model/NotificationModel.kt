package com.android.tankee.notifications.model


import android.os.Parcel
import android.os.Parcelable
import com.android.tankee.EMPTY_STRING
import com.android.tankee.extentions.isNotNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


interface BaseNotificationModel


class NotificationModel() : BaseNotificationModel, Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("item_id")
    @Expose
    var itemId: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("bold")
    @Expose
    var bold: String? = null
    @SerializedName("image_url")
    @Expose
    var imageUrl: String? = null
    @SerializedName("is_viewed")
    @Expose
    var isViewed: Boolean? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        itemId = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        text = parcel.readString()
        type = parcel.readString()
        bold = parcel.readString()
        imageUrl = parcel.readString()
        isViewed = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    }

    fun getNotificationType(): NotificationType {
        return NotificationType.getType(
            type
                ?: EMPTY_STRING
        )
    }

    fun isValid(): Boolean {
        return itemId.isNotNull()
    }

    fun isNotValid(): Boolean {
        return !isValid()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(itemId)
        parcel.writeString(title)
        parcel.writeString(text)
        parcel.writeString(type)
        parcel.writeString(bold)
        parcel.writeString(imageUrl)
        parcel.writeValue(isViewed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationModel> {
        override fun createFromParcel(parcel: Parcel): NotificationModel {
            return NotificationModel(parcel)
        }

        override fun newArray(size: Int): Array<NotificationModel?> {
            return arrayOfNulls(size)
        }
    }

}


data class NotificationPlaceHolderModel(val imageId: Int) : BaseNotificationModel