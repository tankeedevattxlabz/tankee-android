package com.android.tankee.notifications.model


enum class NotificationType(val type: String) {

    GAME("game"),
    GAMER("influencer"),
    VIDEO("video"),
    TEXT("text");

    companion object {

        fun getType(value: String): NotificationType {
            return when {
                isGame(value) -> GAME
                isGamer(value) -> GAMER
                isVideo(value) -> VIDEO
                else -> TEXT
            }
        }

        private fun isGame(value: String): Boolean {
            return value == GAME.type
        }

        private fun isGamer(value: String): Boolean {
            return value == GAMER.type
        }

        private fun isVideo(value: String): Boolean {
            return value == VIDEO.type
        }
    }
}