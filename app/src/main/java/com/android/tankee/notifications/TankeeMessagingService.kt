package com.android.tankee.notifications


import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.android.tankee.R
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.event_bus.EventPoster
import com.android.tankee.event_bus.NewNotificationEvent
import com.android.tankee.event_bus.ShowNotificationsMarkerEvent
import com.android.tankee.extentions.isNotNull
import com.android.tankee.logs.AppLogger
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.notifications.model.NotificationParams
import com.android.tankee.notifications.model.NotificationType
import com.android.tankee.services.apptentive.ApptentiveManager
import com.android.tankee.ui.ARG_NOTIFICATION_MODEL
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.request_options.GlideRequestOptions
import com.android.tankee.utils.buildThumbnailUrl
import com.android.tankee.utils.log
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*


class TankeeMessagingService : FirebaseMessagingService() {

    private var notificationManager: NotificationManager? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        ApptentiveManager.openNotificationItem()

        val id = remoteMessage?.data?.get(NotificationParams.Id.param)
        val contentItemId = remoteMessage?.data?.get(NotificationParams.ContentItemId.param)
        val title = remoteMessage?.notification?.title?.trim()
        val text = remoteMessage?.notification?.body?.trim()
        val type = remoteMessage?.data?.get(NotificationParams.Type.param)
        val attachmentUrl = remoteMessage?.data?.get(NotificationParams.AttachmentUrl.param)
        val imageUrl = remoteMessage?.notification?.imageUrl

        AppLogger.logInfo(FCM_TAG, "id:\t$id")
        AppLogger.logInfo(FCM_TAG, "contentItemId:\t$contentItemId")
        AppLogger.logInfo(FCM_TAG, "title:\t$title")
        AppLogger.logInfo(FCM_TAG, "text:\t$text")
        AppLogger.logInfo(FCM_TAG, "type:\t$type")
        AppLogger.logInfo(FCM_TAG, "attachmentUrl:\t$attachmentUrl")
        AppLogger.logInfo(FCM_TAG, "imageUrl:\t$imageUrl")
        AppLogger.logInfo(FCM_TAG, "Remove\t${remoteMessage.toString()}")

        val notification = remoteMessage?.notification
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )

        val notificationModel: NotificationModel?
        notificationModel = buildNotificationModel(
            id,
            contentItemId,
            title,
            text,
            type,
            imageUrl
        )
        val bundle = Bundle()
        bundle.putParcelable(ARG_NOTIFICATION_MODEL, notificationModel)
        intent.putExtras(bundle)
        SessionDataHolder.notificationModel = notificationModel

        val i = Random().nextInt()
        val pendingIntent = PendingIntent.getActivity(
                this, i, intent,
                0
        )

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val bigTextStyle = NotificationCompat.BigTextStyle()

        val largeImage = getLargeIcon(
            notificationModel.getNotificationType(),
            imageUrl
        )

        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setLargeIcon(largeImage)
            .setDefaults(NotificationCompat.DEFAULT_SOUND or NotificationCompat.DEFAULT_VIBRATE)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setWhen(System.currentTimeMillis())
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentTitle(notification?.title)
            .setContentText(notification?.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setStyle(bigTextStyle)

        createNotificationChannel()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager?.notify(i, notificationBuilder.build())

        showNotificationsMarkerEvent()
        newNotificationEvent(notificationModel)
    }

    private fun getLargeIcon(type: NotificationType, imageUrl: Uri?): Bitmap? {
        val requestOptions = GlideRequestOptions.getNotificationRequestOptions(type)
        if (imageUrl != null) {
            return Glide
                .with(this)
                .asBitmap()
                .load(buildThumbnailUrl(imageUrl.toString()))
                .apply(requestOptions)
                .submit()
                .get()
        } else {
            return Glide
                .with(this)
                .asBitmap()
                .load(R.mipmap.ic_launcher)
                .apply(requestOptions)
                .submit()
                .get()
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                getString(R.string.notification_channel_id),
                getString(R.string.notification_channel_name),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(notificationChannel)
            notificationManager?.createNotificationChannelGroup(
                NotificationChannelGroup("group_id_1", getString(R.string.notification_group_name))
            )
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        ("Refreshed token: $token").log("FCM")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token)
    }

    private fun buildNotificationModel(
        id: String?,
        itemId: String?,
        title: String?,
        text: String?,
        type: String?,
        imageUrl: Uri?
    ): NotificationModel {
        val model = NotificationModel()

        /*
         *  Use this parameter for correct work.
         *  id = itemId
         *  itemId = id
         *  This is because on server and IOS not good realization.
         */
        model.id = if (itemId.isNotNull()) {
            itemId!!.toInt()
        } else {
            0
        }
        model.itemId = if (id.isNotNull()) {
            id!!.toInt()
        } else {
            0
        }
        model.title = title
        model.text = text
        model.type = type
        model.imageUrl = imageUrl?.toString()

        return model
    }

    /**
     * To resolve this event find class {@TankeeToolbar} and method {updateNewNotificationsMarker}.
     */
    private fun showNotificationsMarkerEvent() {
        EventPoster.postEvent(ShowNotificationsMarkerEvent())
    }

    private fun newNotificationEvent(model: NotificationModel) {
        EventPoster.postEvent(NewNotificationEvent(model))
    }

    companion object {
        const val FCM_TAG = "FCM"
    }

}
