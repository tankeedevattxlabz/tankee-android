package com.android.tankee.notifications.model

enum class NotificationParams(val param: String) {
    Id("id"),
    ItemId("item_id"),
    ContentItemId("content_item_id"),
    Title("title"),
    Text("text"),
    Type("type"),
    AttachmentUrl("attachment-url")
}

