package com.android.tankee

import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.view.Display
import android.view.WindowManager
import com.android.tankee.services.apptentive.ApptentiveController
import com.android.tankee.system.Connection
import com.android.tankee.ui.video_player.advertising.AdsController
import com.android.tankee.utils.log
import com.facebook.stetho.Stetho
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.iid.FirebaseInstanceId


//    _    _                  _______ _     _         _         _______          _                 __  __
//   | |  | |                |__   __| |   (_)       (_)       |__   __|        | |               |  \/  |
//   | |__| | ___ _   _         | |  | |__  _ ___     _ ___       | | __ _ _ __ | | _____  ___    | \  / | __ _ _ __
//   |  __  |/ _ \ | | |        | |  | '_ \| / __|   | / __|      | |/ _` | '_ \| |/ / _ \/ _ \   | |\/| |/ _` | '_ \
//   | |  | |  __/ |_| |_       | |  | | | | \__ \   | \__ \      | | (_| | | | |   <  __/  __/   | |  | | (_| | | | |
//   |_|  |_|\___|\__, ( )      |_|  |_| |_|_|___/   |_|___/      |_|\__,_|_| |_|_|\_\___|\___|   |_|  |_|\__,_|_| |_|
//                 __/ |/
//                |___/
//
//    ____           _    _                                  __
//   |  _ \         | |  | |                            _    \ \
//   | |_) | ___    | |__| | __ _ _ __  _ __  _   _    (_)    | |
//   |  _ < / _ \   |  __  |/ _` | '_ \| '_ \| | | |          | |
//   | |_) |  __/   | |  | | (_| | |_) | |_) | |_| |    _     | |
//   |____/ \___|   |_|  |_|\__,_| .__/| .__/ \__, |   (_)    | |
//                               | |   | |     __/ |         /_/
//                               |_|   |_|    |___/


class TankeeApp : Application() {
    companion object {
        internal lateinit var app: TankeeApp

        lateinit var connection: Connection
        lateinit var display: Display
    }

    init {
        app = this

    }

    override fun onCreate() {
        super.onCreate()
        //Turn on for leak detection
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
        connection = Connection(this)
        val wm = baseContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        display = wm.defaultDisplay
        if (!BuildConfig.DEBUG) {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    ("getInstanceId failed  " + task.exception).log("FCM")
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                ("TOKEN: $token").log("FCM")
            })
        AdsController.retrieveAndSaveAdsConfig()
        AdsController.getVideoPlayerBanner()
        AdsController.getVideosAdsConfigurations()

        ApptentiveController.registerApptentive(this)
    }
}

fun getAppContext(): TankeeApp {
    return TankeeApp.app
}

fun getAppRecourse(): Resources {
    return TankeeApp.app.resources
}