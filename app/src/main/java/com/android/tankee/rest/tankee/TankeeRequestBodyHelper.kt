package com.android.tankee.rest.tankee


import com.android.tankee.rest.user.UpdateAvatarRequestBody
import com.android.tankee.rest.user.UpdateUserNameRequestBody
import com.android.tankee.rest.utils.RequestBodyConverter
import okhttp3.RequestBody


object TankeeRequestBodyHelper {


}

object UserRequestBodyHelper {

    fun getUpdateAvatarRequestBody(avatar: String): RequestBody {
        val requestBody = UpdateAvatarRequestBody(avatar)
        return RequestBodyConverter.convertToRequestBody(requestBody)
    }

    fun getUpdateUserNameRequestBody(userName: String): RequestBody {
        val requestBody = UpdateUserNameRequestBody(userName)
        return RequestBodyConverter.convertToRequestBody(requestBody)
    }
}