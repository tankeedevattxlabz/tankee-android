package com.android.tankee.rest

import com.android.tankee.BuildConfig
import com.android.tankee.TankeeApp
import com.android.tankee.cookies.SessionDataHolder
import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.rest.tankee.api.TankeeApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object TankeeRest {
        private const val BASE_URL = BuildConfig.BASE_URL
    private const val TEST_SERGIO = "http://192.168.88.50:3000/v4/"
//    private val BASE_URL = TEST_SERGIO
    private val LOGGING_LEVEL = HttpLoggingInterceptor.Level.BODY
    private val SHOULD_ADD_INTERCEPTOR = BuildConfig.DEBUG

    val api: TankeeApi by lazy {
        return@lazy buildRetrofit().create(TankeeApi::class.java)
    }

    private fun buildRetrofit(): Retrofit {
        return Retrofit.Builder()
            .client(buildOkHttpClient())
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun buildOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.eventListener(TankeeApp.connection.connectionStateListener)
        if (SHOULD_ADD_INTERCEPTOR) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = LOGGING_LEVEL
            builder.addInterceptor(interceptor)
        }
        if (BuildConfig.DEBUG) {
            builder.callTimeout(60, TimeUnit.SECONDS)
            builder.writeTimeout(60, TimeUnit.SECONDS)
            builder.readTimeout(60, TimeUnit.SECONDS)
            builder.connectTimeout(60, TimeUnit.SECONDS)
        }
        builder.addNetworkInterceptor {
            val request = it.request()
            val newBuilder = request.newBuilder()
//            newBuilder.addHeader("token","077becd4cb2c15691d827bcbce591aded6cb9035")
            val token = SessionDataHolder.user?.token
            val token1 = TankeePrefs.token
            newBuilder.addHeader("token", token ?: token1)
            it.proceed(newBuilder.build())

        }
        return builder.build()
    }
}