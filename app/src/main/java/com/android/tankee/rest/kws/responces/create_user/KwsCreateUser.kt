package com.android.tankee.rest.kws.responces.create_user


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class KwsCreateUserResponse(
    @SerializedName("id")
    @Expose
    var id: Int
)

data class KwsCreateUserRequestBody(
    var username: String? = null,
    var password: String? = null,
    var parentEmail: String? = null,
    var dateOfBirth: String? = null
)