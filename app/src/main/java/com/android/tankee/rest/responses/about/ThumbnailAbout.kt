package com.android.tankee.rest.responses.about

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ThumbnailAbout {

    @SerializedName("url")
    @Expose
    var url: String? = null

}
