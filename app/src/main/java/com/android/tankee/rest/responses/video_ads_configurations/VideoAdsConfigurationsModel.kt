package com.android.tankee.rest.responses.video_ads_configurations

/**
 * Created by Sergey Kulyk on 2019-10-11.
 *
 * Mail sergey.kulyk@practicallogix.com.
 */


import com.android.tankee.cookies.SessionDataHolder
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Class {@VideoAdsConfigurationsModel} need for implement JWP video ads configurations.
 *
 * @property videoLengthSecondsFrom         is minimum video duration for example 1 min.
 * @property videoLengthSecondsTo           is maximum video duration for example 5 min.
 *
 * @property prerollBackToBack              count of prepoll video ads (can be 0, 1, 2...n).
 *
 * Each video can contain 'n' midroll video ads blocks.
 * Each ads block can contains 'm' video ads which play one by one.
 * @property midrollBackToBack              count of midroll ads block (can be 0, 1, 2...n).
 * @property numberOfMidrolls               count of midroll video ads in 1 midroll block.
 * @property midrollStartAt   time of start time the first midroll in percent.
 *
 * @property postrollBackToBack             count of postroll video ads (can be 0, 1, 2...n).
 *
 */
class VideoAdsConfigurationsModel {

    @SerializedName("video_length_seconds_from")
    @Expose
    var videoLengthSecondsFrom: Int? = null
    @SerializedName("video_length_seconds_to")
    @Expose
    var videoLengthSecondsTo: Int? = null
    @SerializedName("preroll_back_to_back")
    @Expose
    var prerollBackToBack: Int? = null
    @SerializedName("midroll_back_to_back")
    @Expose
    var midrollBackToBack: Int? = null
    @SerializedName("number_of_midrolls")
    @Expose
    var numberOfMidrolls: Int? = null
    @SerializedName("midroll_start_at")
    @Expose
    var midrollStartAt: Int? = null
    @SerializedName("postroll_back_to_back")
    @Expose
    var postrollBackToBack: Int? = null

}


/**
 * Factory for getting video ads configuration
 * by video duration.
 */
class VideosAdsConfigFactory(private val videoDuration: Int) {

    fun buildVideoAds(): VideoAdsConfigurationsModel? {

        SessionDataHolder.videoAdsConfigurations?.let {
            for (videoConfigModel in it) {
                val minDuration = videoConfigModel.videoLengthSecondsFrom ?: 0
                val maxDuration = videoConfigModel.videoLengthSecondsTo ?: 0

                if (videoDuration in (minDuration + 1)..maxDuration) {
                    return videoConfigModel
                }
            }
        }

        return null
    }
}