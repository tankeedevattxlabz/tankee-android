package com.android.tankee.rest.responses.sections

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Section {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("background")
    @Expose
    var background: Background? = null
    @SerializedName("playlist_id")
    @Expose
    var playlistId: Int? = null
    @SerializedName("jw_key")
    @Expose
    var jwKey: String? = null
    @SerializedName("is_active")
    @Expose
    var isActive: Boolean? = null
    @SerializedName("tags_string")
    @Expose
    var tagsString: String? = null
    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null
    @SerializedName("iphone_background")
    @Expose
    var iphoneBackground: IphoneBackground? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("videos")
    @Expose
    var videos: List<Video>? = null
    @SerializedName("videos_count")
    @Expose
    var videosCount: Int? = null

}
