package com.android.tankee.rest.responses.influencer

import com.android.tankee.rest.BaseResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GamerSectionResponse : BaseResponse<Gamer>() {

    @SerializedName("section_title")
    @Expose
    var sectionTitle: String? = null

}
