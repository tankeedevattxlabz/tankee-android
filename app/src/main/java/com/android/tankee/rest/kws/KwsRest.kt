package com.android.tankee.rest.kws


import com.android.tankee.BuildConfig
import com.android.tankee.rest.ApiFactory
import com.android.tankee.rest.kws.api.KwsApi


object KwsRest {

    private const val KWS_URL = BuildConfig.KWS_URL

    private val apiFactory: ApiFactory = ApiFactory()

    val api: KwsApi by lazy {
        return@lazy apiFactory.buildRetrofit(KWS_URL).create(
            KwsApi::class.java)
    }

    val retrofit = apiFactory.buildRetrofit(KWS_URL)
}