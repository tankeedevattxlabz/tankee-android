package com.android.tankee.rest.utils


import com.android.tankee.rest.APPLICATION_JSON
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.RequestBody


object RequestBodyConverter {

    fun <T> convertToRequestBody(requestBody: T): RequestBody {
        val gson = Gson()
        val requestBodyJson = gson.toJson(requestBody)
        return RequestBody.create(MediaType.parse(APPLICATION_JSON), requestBodyJson)
    }
}