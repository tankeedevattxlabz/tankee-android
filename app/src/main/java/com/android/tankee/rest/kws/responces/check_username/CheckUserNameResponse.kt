package com.android.tankee.rest.kws.responces.check_username


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class CheckUserNameResponse(
    @SerializedName("username")
    @Expose
    var username: String? = null,
    @SerializedName("available")
    @Expose
    var available: Boolean? = null
)
