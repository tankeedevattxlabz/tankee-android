package com.android.tankee.rest.responses.sections

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IphoneBackground {

    @SerializedName("url")
    @Expose
    var url: Any? = null

}
