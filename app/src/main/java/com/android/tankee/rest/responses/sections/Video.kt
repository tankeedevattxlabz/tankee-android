package com.android.tankee.rest.responses.sections

import android.os.Parcel
import android.os.Parcelable
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildThumbnailUrl
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.lang.NumberFormatException

open class Video() : SectionItemMarker, Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("like")
    @Expose
    var like: Boolean? = null
    @SerializedName("started_at")
    @Expose
    var startedAt: Int? = null
    @SerializedName("jw_key")
    @Expose
    var jwKey: String? = null
    @SerializedName("thumbnail_url")
    @Expose
    var thumbnailUrl: String? = null
    @SerializedName("duration")
    @Expose
    var duration: String? = null
    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null
    @SerializedName("influencer_name")
    @Expose
    var influencerName: String? = null
    @SerializedName("influencer_image")
    @Expose
    var influencerImage: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("disable_ads")
    @Expose
    var disableAds: Boolean? = null
    @SerializedName("can_be_downloaded")
    @Expose
    var canBeDownloaded: Boolean? = null
    @SerializedName("devise_videos")
    @Expose
    var deviseVideos: List<Any>? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("playlist_key")
    @Expose
    var playlistKey: String? = null
    @SerializedName("game_types_string")
    @Expose
    var gameTypes: String? = null
    @SerializedName("hide_banner_ads")
    @Expose
    var hideBannerAds: Boolean? = false
    var isVideoCompleted: Boolean = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        like = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        startedAt = parcel.readValue(Int::class.java.classLoader) as? Int
        jwKey = parcel.readString()
        thumbnailUrl = parcel.readString()
        duration = parcel.readString()
        tags = parcel.createStringArrayList()
        influencerName = parcel.readString()
        influencerImage = parcel.readString()
        createdAt = parcel.readString()
        disableAds = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        canBeDownloaded = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        class_ = parcel.readString()
        playlistKey = parcel.readString()
        gameTypes = parcel.readString()
    }

    fun isOriginal(): Boolean {
        return if (tags != null) {
            tags!!.contains(ORIGINAL_TAG)
        } else {
            false
        }
    }

    fun buildGamerImage(): String? {
        return buildThumbnailUrl(influencerImage)
    }

    fun getVideoDurationInMillis(): Long {
        return try {
            duration?.toDouble()?.times(1000L)?.toLong() ?: 0
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            0
        }
    }

    fun getVideoDurationInSeconds(): Long {
        return try {
            duration?.toDouble()?.toLong() ?: 0
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            0
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(title)
        parcel.writeValue(like)
        parcel.writeValue(startedAt)
        parcel.writeString(jwKey)
        parcel.writeString(thumbnailUrl)
        parcel.writeString(duration)
        parcel.writeStringList(tags)
        parcel.writeString(influencerName)
        parcel.writeString(influencerImage)
        parcel.writeString(createdAt)
        parcel.writeValue(disableAds)
        parcel.writeValue(canBeDownloaded)
        parcel.writeString(class_)
        parcel.writeString(playlistKey)
        parcel.writeString(gameTypes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Video> {
        override fun createFromParcel(parcel: Parcel): Video {
            return Video(parcel)
        }

        override fun newArray(size: Int): Array<Video?> {
            return arrayOfNulls(size)
        }
    }

}

const val ORIGINAL_TAG: String = "Tankee Original"
