package com.android.tankee.rest.kws


import com.android.tankee.rest.kws.responces.errors.KwsErrorModel
import retrofit2.Response
import java.io.IOException


object KwsErrorUtils {

    fun parseError(response: Response<*>): KwsErrorModel {
        val converter = KwsRest.retrofit.responseBodyConverter<KwsErrorModel>(KwsErrorModel::class.java, arrayOfNulls<Annotation>(0))

        val error: KwsErrorModel

        try {
            error = converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            return KwsErrorModel()
        }

        return error
    }
}