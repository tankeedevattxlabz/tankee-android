package com.android.tankee.rest.responses.ad


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.android.tankee.rest.responses.ad.BannerAdSource.*;

class VideoPlayerBannerModel {

    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("super_awesome_url")
    @Expose
    var url: String? = null
    @SerializedName("adbutler")
    @Expose
    var adbutlerModel: AdButlerModel? = null
    @SerializedName("refresh_interval")
    @Expose
    var seconds: Int? = null
    @SerializedName("source")
    @Expose
    var source: String? = null
    @SerializedName("kidoz")
    @Expose
    var kidozModel: KidozModel? = null

    fun getSource(): BannerAdSource {
        return when (source) {
            AdMob.source -> AdMob
            Kidoz.source -> Kidoz
            AdButler.source -> AdButler
            else -> SuperAwesome
        }
    }

    companion object {
        const val DEFAULT_REFRESH_AD_SECONDS: Int = 30
    }

}


enum class BannerAdSource(val source: String) {

    AdMob("ad_mob"),
    Kidoz("kidoz"),
    AdButler("adbutler"),
    SuperAwesome("super_awesome");

}


class KidozModel {

    @SerializedName("security_token")
    @Expose
    var securityToken: String? = null
    @SerializedName("publisher_id")
    @Expose
    var publisherId: String? = null

}

class AdButlerModel {

    @SerializedName("account_id")
    @Expose
    var accountId: String? = null
    @SerializedName("zone_id")
    @Expose
    var zoneId: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null

}