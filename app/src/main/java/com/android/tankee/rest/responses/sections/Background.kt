package com.android.tankee.rest.responses.sections

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Background {

    @SerializedName("url")
    @Expose
    var url: String? = null

}
