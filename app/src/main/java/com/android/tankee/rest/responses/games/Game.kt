package com.android.tankee.rest.responses.games

import android.os.Parcel
import android.os.Parcelable
import com.android.tankee.EMPTY_STRING
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildThumbnailUrl
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Game() : SectionItemMarker, Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("image_url")
    @Expose
    var imageUrl: String? = null
    @SerializedName("publisher")
    @Expose
    var publisher: String? = null
    @SerializedName("cost")
    @Expose
    var cost: String? = null
    @SerializedName("age")
    @Expose
    var age: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("common_sense_rating")
    @Expose
    var commonSenseRating: String? = null
    @SerializedName("app_store_rating")
    @Expose
    var appStoreRating: String? = null
    @SerializedName("has_purchases")
    @Expose
    var hasPurchases: Boolean? = null
    @SerializedName("rating")
    @Expose
    var rating: Any? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: Avatar? = null
    @SerializedName("priority")
    @Expose
    var priority: Int? = null
    @SerializedName("full_description")
    @Expose
    var fullDescription: String? = null
    @SerializedName("short_description")
    @Expose
    var shortDescription: String? = null
    @SerializedName("video_id")
    @Expose
    var videoId: Int? = null
    @SerializedName("about")
    @Expose
    var about: String? = null
    @SerializedName("playlist_key")
    @Expose
    var playlistKey: String? = null
    @SerializedName("game_types_string")
    @Expose
    var gameTypesString: String? = null
    @SerializedName("platforms_string")
    @Expose
    var platformsString: String? = null
    @SerializedName("tags_string")
    @Expose
    var tagsString: String? = null
    @SerializedName("playlist_id")
    @Expose
    var playlistId: Any? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("types")
    @Expose
    var types: String? = null
    @SerializedName("platforms")
    @Expose
    var platforms: String? = null
    @SerializedName("like")
    @Expose
    var like: Boolean? = null
    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    fun getValidImageUrl(): String? {
        if (imageUrl != null && imageUrl!!.isNotEmpty()) {
            return buildThumbnailUrl(imageUrl!!)
        }

        if (avatar?.url != null && avatar?.url!!.isNotEmpty()) {
            return buildThumbnailUrl(avatar?.url)
        }

        return EMPTY_STRING
    }

    fun isFavoriteGame(): Boolean {
        return if (like != null) {
            like!!
        } else {
            false
        }
    }

    fun isNotFavoriteGame(): Boolean {
        return if (like != null) {
            !like!!
        } else {
            false
        }
    }

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        imageUrl = parcel.readString()
        publisher = parcel.readString()
        cost = parcel.readString()
        age = parcel.readString()
        description = parcel.readString()
        commonSenseRating = parcel.readString()
        appStoreRating = parcel.readString()
        hasPurchases = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
        url = parcel.readString()
        priority = parcel.readValue(Int::class.java.classLoader) as? Int
        fullDescription = parcel.readString()
        shortDescription = parcel.readString()
        videoId = parcel.readValue(Int::class.java.classLoader) as? Int
        about = parcel.readString()
        playlistKey = parcel.readString()
        gameTypesString = parcel.readString()
        platformsString = parcel.readString()
        tagsString = parcel.readString()
        class_ = parcel.readString()
        types = parcel.readString()
        platforms = parcel.readString()
        like = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        tags = parcel.createStringArrayList()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(imageUrl)
        parcel.writeString(publisher)
        parcel.writeString(cost)
        parcel.writeString(age)
        parcel.writeString(description)
        parcel.writeString(commonSenseRating)
        parcel.writeString(appStoreRating)
        parcel.writeValue(hasPurchases)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(url)
        parcel.writeValue(priority)
        parcel.writeString(fullDescription)
        parcel.writeString(shortDescription)
        parcel.writeValue(videoId)
        parcel.writeString(about)
        parcel.writeString(playlistKey)
        parcel.writeString(gameTypesString)
        parcel.writeString(platformsString)
        parcel.writeString(tagsString)
        parcel.writeString(class_)
        parcel.writeString(types)
        parcel.writeString(platforms)
        parcel.writeValue(like)
        parcel.writeStringList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Game> {
        override fun createFromParcel(parcel: Parcel): Game {
            return Game(parcel)
        }

        override fun newArray(size: Int): Array<Game?> {
            return arrayOfNulls(size)
        }
    }
}
