package com.android.tankee.rest.kws


object KwsRestUtils {

    fun buildBearerHeader(accessToken: String): String {
        return "$KWS_HEADER_BEARER $accessToken"
    }

    fun buildBasicHeader(authorization: String): String {
        return "$KWS_HEADER_BASIC $authorization"
    }


}