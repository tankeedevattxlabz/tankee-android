package com.android.tankee.rest.responses.about

import com.android.tankee.rest.BaseResponse

class AboutResponse : BaseResponse<AboutItem>()
