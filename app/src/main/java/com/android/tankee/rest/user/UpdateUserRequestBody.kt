package com.android.tankee.rest.user


data class UpdateAvatarRequestBody(var avatar: String)

data class UpdateUserNameRequestBody(var user_name: String)