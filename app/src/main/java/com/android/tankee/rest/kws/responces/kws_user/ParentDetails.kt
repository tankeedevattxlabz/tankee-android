package com.android.tankee.rest.kws.responces.kws_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ParentDetails {

    @SerializedName("oauthProvider")
    @Expose
    var oauthProvider: String? = null
    @SerializedName("usedVerificationMethodName")
    @Expose
    var usedVerificationMethodName: String? = null

}
