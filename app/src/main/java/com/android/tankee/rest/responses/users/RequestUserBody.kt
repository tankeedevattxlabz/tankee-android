package com.android.tankee.rest.responses.users

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RequestUserBody {

    @SerializedName("uid")
    @Expose
    var uid: String? = null
    @SerializedName("is_anonymous")
    @Expose
    var isAnonymous: Boolean? = false

    override fun toString(): String {
        return "{uid=$uid, isAnonymous=$isAnonymous)"
    }

}