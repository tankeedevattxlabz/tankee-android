package com.android.tankee.rest.tankee.api

import com.android.tankee.BuildConfig
import com.android.tankee.OS_ANDROID
import com.android.tankee.notifications.model.NotificationModel
import com.android.tankee.rest.*
import com.android.tankee.rest.responses.about.AboutItem
import com.android.tankee.rest.responses.ad.VideoPlayerBannerModel
import com.android.tankee.rest.responses.clip.ClipInfo
import com.android.tankee.rest.responses.clip.FavsInfo
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.rest.responses.games.GameSectionResponse
import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.influencer.GamerSectionResponse
import com.android.tankee.rest.responses.sections.Section
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.rest.responses.users.UserModel
import com.android.tankee.rest.responses.video_ads_configurations.VideoAdsConfigurationsModel
import com.android.tankee.rest.user.GetUserPostBody
import com.android.tankee.rest.user.User
import com.android.tankee.ui.video_player.advertising.AdsConfigResponse
import com.android.tankee.ui.video_player.stickers.StickerAdapter
import com.android.tankee.ui.videoplayer.mystaff.SaveClipPresenter
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface TankeeApi {


    /**
     * UserModel
     */
    @POST("users")
    fun createUser(@Body body: GetUserPostBody): Call<BaseResponse<UserModel>>

    @GET("users/self")
    fun getUserExist(): Call<BaseResponse<User>>

    // Default configurations call.
    @GET("configurations/v1")
    fun getConfigurationsByVersion(): Call<BaseResponse<AboutItem>>

    @GET("configurations")
    fun getConfigurationsAll(): Call<BaseResponse<AboutItem>>

    /**
     * Sections
     */
    @GET("sections")
    fun getSections(
//        @Header("token") token: String
    ): Call<List<Section>>

    @GET("sections/{sectionId}")
    fun getSectionById(
        @Path(value = "sectionId") sectionId: Int
    ): Call<BaseResponse<Section>>

    @GET("sections/{id}/videos")
    fun getSectionByIdVideos(
        @Path(value = "id") sectionId: Int,
        @Query("page") page: Int,
        @Query("tag") tag: String
    ): Call<BaseResponse<Video>>

    @GET("videos/recommended")
    fun getTopVideos(): Call<BaseResponse<Video>>

    @POST("videos/{videoId}/reviews")
    fun addReviewsToVideo(
        @Path("videoId") videoId: Int,
        @Query("time") time: Int
    ): Call<BaseResponse<Video>>

    /**
     * Video
     */
    @GET("videos/{videoId}")
    fun getVideoInfo(@Path("videoId") videoId: Int): Call<BaseResponse<Video>>

    /**
     * Games
     */
    @GET("games")
    fun getGames(
        @Query("hasVideo") hasVideo: Boolean = true,
        @Query("numRecords") numRecords: Int = NUM_RECORDS
    ): Call<GameSectionResponse>

    @GET("games/{id}")
    fun getGame(@Path("id") id: Int): Call<BaseResponse<Game>>

    @GET("games/{id}/videos")
    fun getGameVideos(
        @Path("id") sectionId: Int,
        @Query("page") page: Int,
        @Query("tag") tag: String
    ): Call<BaseResponse<Video>>

    @GET("games/favorites")
    fun getFavoritesGames(): Call<BaseResponse<Game>>

    @POST("games/{gameId}/like")
    fun addLikeToGame(@Path("gameId") gamerId: Int): Call<BaseResponse<Game>>

    @DELETE("games/{gameId}/like")
    fun deleteLikeToGame(@Path("gameId") gamerId: Int): Call<BaseResponse<Game>>

    /**
     * Game types.
     */
    @GET("game_types/get_by_name")
    fun getGameType(@Query("name") gameType: String): Call<GameSectionResponse>

    @GET("game_types/videos")
    fun getGameTypeVideos(
        @Query("name") name: String,
        @Query("page") page: Int,
        @Query("tag") tag: String = ""
    ): Call<BaseResponse<Video>>

    /**
     * Gamers
     */
    @GET("influencers")
    fun getGamers(@Query("hasVideo") hasVideo: Boolean = true): Call<GamerSectionResponse>

    @GET("influencers/{id}")
    fun getGamer(@Path("id") gamerId: Int): Call<BaseResponse<Gamer>>

    @GET("influencers/{id}/videos")
    fun getGamerVideos(
        @Path("id") gamerId: Int,
        @Query("page") page: Int,
        @Query("tag") tag: String
    ): Call<BaseResponse<Video>>

    @GET("influencers")
    fun getInfluencers(
//            @Header("token") token: String,
        @Query("hasVideo") hasVideo: Boolean = true,
        @Query("ranking") ranking: Int = NUM_RECORDS
    ): Call<BaseSectionResponse<Gamer>>

    @POST("influencers/{gamerId}/like")
    fun addLikeToGamer(@Path("gamerId") gamerId: Int): Call<BaseResponse<Game>>

    @DELETE("influencers/{gamerId}/like")
    fun deleteLikeToGamer(@Path("gamerId") gamerId: Int): Call<BaseResponse<Game>>

    /**
     * Strikers
     */
    @POST("videos/{videoId}/stickers")
    fun addSticker(@Path("videoId") videoId: Int, @Body sticker: StickerAdapter.Sticker): Call<Any>

    @GET("videos/{videoId}/stickers")
    fun getStickersForVideo(@Path("videoId") videoId: Int): Call<BaseResponse<StickerAdapter.Sticker>>


    /**
     * Advertising
     */
    @GET("advertisements")
    fun getAdsConfig(): Call<AdsConfigResponse>

    @GET("advertisements/video_player_banner")
    fun getVideoPlayerBanner(
        @Query("OSType") osType: String = OS_ANDROID,
        @Query("buildNumber") buildNumber: String = BuildConfig.VERSION_NAME
    ): Call<BaseResponse<VideoPlayerBannerModel>>

    @GET("video_ads_configurations")
    fun getVideoAdsConfigurations(): Call<BaseResponse<VideoAdsConfigurationsModel>>

    /**
     * More videos
     */
    @GET("videos/{videoId}/recommendations")
    fun getRecommendedVideosById(
        @Path("videoId") videoId: Int,
        @Query("playlist_key") playlistKey: String,
        @Query("page") page: Int): Call<BaseMoreVideosResponse<Video>>

    /**
     * Search
     */
    @GET("search/videos")
    fun getSearchVideos(
        @Query("title") title: String,
        @Query("page") page: Int
    ): Call<BaseResponse<Video>>

    @GET("search/video_suggestions")
    fun getSearchVideosSuggestions(
        @Query("title") title: String
    ): Call<BaseResponse<Video>>

    /**
     * Users
     */
    @POST("users")
    fun getUser(@Body requestUserBody: RequestBody): Call<BaseResponse<UserModel>>

    @GET("users/self")
    fun getUserSelf(): Call<BaseResponse<UserModel>>

    @PUT("users")
    fun updateUserInfo(@Body requestUserBody: RequestBody): Call<BaseResponse<UserModel>>

    @PUT("users")
    @FormUrlEncoded
    fun updateFcmTokenAndTimeZone(
        @Field("fcm_token") fcmToken: String,
        @Field("time_zone") timezone: String
    ): Call<BaseResponse<UserModel>>

    @DELETE("users/self")
    fun deleteUser(): Call<BaseResponse<UserModel>>

    /**
     * ClipInfo
     */
    @POST("clips")
    fun saveClip(@Body saveClip: SaveClipPresenter.SaveClip): Call<BaseResponse<ClipInfo>>

    @GET("clips")
    fun getClips(): Call<BaseResponse<ClipInfo>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "clips/batch_delete", hasBody = true)
    fun deleteClips(@Field("ids[]") clipIds: ArrayList<Int>): Call<Any>

    @DELETE("clips/{clipId}")
    fun deleteClip(@Path(value = "clipId") clipId: Int): Call<Any>

    /**
     * Favorites
     */
    @GET("videos/favorites")
    fun getFavoriteVideos(): Call<BaseResponse<Video>>

    @POST("videos/{videoId}/like")
    fun addVideoToFavorites(@Path("videoId") videoId: Int): Call<FavsInfo>

    @DELETE("videos/{videoId}/like")
    fun deleteVideoFromFavorites(@Path("videoId") videoId: Int): Call<Any>


    /**
     * Notifications
     */
    @GET("notifications")
    fun getNotifications(): Call<BaseResponse<NotificationModel>>

    @POST("notifications/{id}/view")
    fun viewNotification(@Path("id") itemId: Int): Call<Any>


}