package com.android.tankee.rest.kws.responces.kws_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Permissions {

    @SerializedName("sendPushNotification")
    @Expose
    var sendPushNotification: Boolean? = null

}
