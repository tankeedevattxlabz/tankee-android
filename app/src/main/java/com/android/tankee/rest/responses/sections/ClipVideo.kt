package com.android.tankee.rest.responses.sections

import com.android.tankee.rest.responses.clip.ClipInfo
import com.android.tankee.ui.views.see_all.CheckableItem

class ClipVideo : Video(), CheckableItem {
    private var isChecked: Boolean = false
    override fun getChecked(): Boolean {
        return isChecked
    }

    override fun setChecked(checked: Boolean) {
        isChecked = checked
    }

    var clipInfo: ClipInfo? = null
    val clipStartPosition: Int?
        get() {
            return startedAt
        }
    val clipDuration: Int?
        get() {
            return startedAt?.plus(30)
        }
}

