package com.android.tankee.rest.responses.about

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AboutItem {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("version")
    @Expose
    var version: String? = null
    @SerializedName("title_about")
    @Expose
    var titleAbout: String? = null
    @SerializedName("thumbnail_about")
    @Expose
    var thumbnailAbout: ThumbnailAbout? = null
    @SerializedName("jw_key")
    @Expose
    var jwKey: String? = null
    @SerializedName("about_text")
    @Expose
    var aboutText: String? = null
    @SerializedName("welcome_title")
    @Expose
    var welcomeTitle: String? = null
    @SerializedName("welcome_link")
    @Expose
    var welcomeLink: String? = null
    @SerializedName("welcome_text")
    @Expose
    var welcomeText: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("is_default")
    @Expose
    var isDefault: Boolean? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("is_android_native")
    val isAndroidNative: Boolean? = null

}
