package com.android.tankee.rest

const val NUM_RECORDS = 10

const val MORE_VIDEOS_NUM_RECORDS = 10

const val NUM_RECORDS_TOP_VIDEOS = 8

const val APPLICATION_JSON = "application/json"