package com.android.tankee.rest.responses.clip

import com.android.tankee.rest.responses.influencer.Gamer
import com.android.tankee.rest.responses.sections.Video
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FavsInfo : SectionItemMarker {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("time")
    @Expose
    var time: Int? = null
    @SerializedName("video")
    @Expose
    var video: Video? = null
    @SerializedName("influencer")
    @Expose
    var influencer: Gamer? = null

    override fun toString(): String {
        return "{id=$id, time=$time, video=$video, influencer=$influencer}"
    }
}