package com.android.tankee.rest.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class GetUserPostBody(
    @SerializedName("uid")
    @Expose
    val uid : String,
    @SerializedName("is_anonymous")
    @Expose
    val is_anonymous : Boolean)

class User{
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("provider")
    @Expose
    var provider: String? = null
    @SerializedName("uid")
    @Expose
    var uid: String? = null
    @SerializedName("encrypted_password")
    @Expose
    var encryptedPassword: String? = null
    @SerializedName("reset_password_token")
    @Expose
    var resetPasswordToken: Any? = null
    @SerializedName("reset_password_sent_at")
    @Expose
    var resetPasswordSentAt: Any? = null
    @SerializedName("full_name")
    @Expose
    var fullName: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("token")
    @Expose
    var token: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("timezone")
    @Expose
    var timezone: Any? = null
    @SerializedName("is_anonymous")
    @Expose
    var isAnonymous: Boolean? = null
    @SerializedName("is_active")
    @Expose
    var isActive: Boolean? = null
    @SerializedName("fcm_token")
    @Expose
    var fcmToken: Any? = null
    @SerializedName("time_zone")
    @Expose
    var timeZone: Any? = null
    @SerializedName("login_remind_at")
    @Expose
    var loginRemindAt: Any? = null
    @SerializedName("class")
    @Expose
    var _class: String? = null
    @SerializedName("is_new")
    @Expose
    var isNew: Boolean? = null

    override fun toString(): String {
        return "UserModel(id=$id, uid=$uid, fullName=$fullName, avatar=$avatar, email=$email, token=$token)"
    }


}