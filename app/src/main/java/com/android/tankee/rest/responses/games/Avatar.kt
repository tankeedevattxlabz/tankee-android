package com.android.tankee.rest.responses.games

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Avatar {

    @SerializedName("url")
    @Expose
    var url: String? = null

}
