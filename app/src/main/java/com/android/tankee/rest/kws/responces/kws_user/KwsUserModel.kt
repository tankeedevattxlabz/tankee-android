package com.android.tankee.rest.kws.responces.kws_user


import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.android.tankee.database.KWS_USERS_TABLE
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Entity(tableName = KWS_USERS_TABLE)
class  KwsUserModel {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("email")
    @Expose
    @Ignore
    var email: String? = null

    @SerializedName("firstName")
    @Expose
    @Ignore
    var firstName: String? = null

    @SerializedName("lastName")
    @Expose
    @Ignore
    var lastName: String? = null

    @SerializedName("city")
    @Expose
    @Ignore
    var city: String? = null

    @SerializedName("postalCode")
    @Expose
    @Ignore
    var postalCode: String? = null

    @SerializedName("streetAddress")
    @Expose
    @Ignore
    var streetAddress: String? = null

    @SerializedName("country")
    @Expose
    @Ignore
    var country: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    @Ignore
    var dateOfBirth: String? = null

    @SerializedName("language")
    @Expose
    @Ignore
    var language: String? = null

    @SerializedName("permissions")
    @Expose
    @Ignore
    var permissions: Permissions? = null

    @SerializedName("username")
    @Expose
    var username: String? = null

    @SerializedName("appData")
    @Expose
    @Ignore
    var appData: List<Any>? = null

    @SerializedName("activationCreatedAt")
    @Expose
    @Ignore
    var activationCreatedAt: String? = null

    @SerializedName("createdAt")
    @Expose
    @Ignore
    var createdAt: String? = null

    @SerializedName("consentAgeForCountry")
    @Expose
    @Ignore
    var consentAgeForCountry: Int? = null

    @SerializedName("isMinor")
    @Expose
    @Ignore
    var isMinor: Boolean? = null

    @SerializedName("parentEmail")
    @Expose
    @Ignore
    var parentEmail: String? = null

    @SerializedName("parentState")
    @Expose
    @Ignore
    var parentState: ParentState? = null

    @SerializedName("parentDetails")
    @Expose
    @Ignore
    var parentDetails: ParentDetails? = null

    override fun toString(): String {
        return "KwsUserModel(id=$id, email=$email, firstName=$firstName, lastName=$lastName, city=$city, postalCode=$postalCode, streetAddress=$streetAddress, country=$country, dateOfBirth=$dateOfBirth, language=$language, permissions=$permissions, username=$username, appData=$appData, activationCreatedAt=$activationCreatedAt, createdAt=$createdAt, consentAgeForCountry=$consentAgeForCountry, isMinor=$isMinor, parentEmail=$parentEmail, parentState=$parentState, parentDetails=$parentDetails)"
    }

}
