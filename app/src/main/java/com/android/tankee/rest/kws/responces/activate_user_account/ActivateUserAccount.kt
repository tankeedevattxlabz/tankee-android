package com.android.tankee.rest.kws.responces.activate_user_account

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ActivateUserAccountResponse {
}

data class ActivateUserAccountRequestBody(
    @SerializedName("username")
    @Expose
    var userName: String? = null,
    var appName: String? = null,
    var parentEmail: String? = null,
    var dateOfBirth: String? = null,
    var permissions: MutableList<String>? = null
)