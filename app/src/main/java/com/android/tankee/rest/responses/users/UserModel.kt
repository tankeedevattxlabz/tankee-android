package com.android.tankee.rest.responses.users


import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.tankee.database.USERS_TABLE_NAME
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Entity(tableName = USERS_TABLE_NAME)
class UserModel {

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("provider")
    @Expose
    var provider: String? = null
    @SerializedName("uid")
    @Expose
    var uid: String? = null
    @SerializedName("encrypted_password")
    @Expose
    var encryptedPassword: String? = null
    @SerializedName("reset_password_token")
    @Expose
    var resetPasswordToken: String? = null
    @SerializedName("reset_password_sent_at")
    @Expose
    var resetPasswordSentAt: String? = null
    @SerializedName("full_name")
    @Expose
    var fullName: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("token")
    @Expose
    var token: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("time_zone")
    @Expose
    var timezone: String? = null
    @SerializedName("is_anonymous")
    @Expose
    var isAnonymous: Boolean? = null
    @SerializedName("is_active")
    @Expose
    var isActive: Boolean? = null
    @SerializedName("fcm_token")
    @Expose
    var fcmToken: String? = null
    @SerializedName("login_remind_at")
    @Expose
    var loginRemindAt: String? = null
    @SerializedName("premium_expiration_date")
    @Expose
    var premiumExpirationDate: String? = null
    @SerializedName("kws_notification_status")
    @Expose
    var kwsNotificationStatus: Boolean? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("premium_available")
    @Expose
    var premiumAvailable: Boolean? = null
    @SerializedName("is_new")
    @Expose
    var isNew: Boolean? = null
    @SerializedName("user_name")
    @Expose
    var userName: String? = null

    fun isNotNew(): Boolean {
        return if (isNew != null) {
            !isNew!!
        } else {
            true
        }
    }

    override fun toString(): String {
        return "UserModel(id=$id, provider=$provider, uid=$uid, encryptedPassword=$encryptedPassword, resetPasswordToken=$resetPasswordToken, resetPasswordSentAt=$resetPasswordSentAt, fullName=$fullName, avatar=$avatar, email=$email, token=$token, createdAt=$createdAt, updatedAt=$updatedAt, timezone=$timezone, isAnonymous=$isAnonymous, isActive=$isActive, fcmToken=$fcmToken, loginRemindAt=$loginRemindAt, premiumExpirationDate=$premiumExpirationDate, kwsNotificationStatus=$kwsNotificationStatus, class_=$class_, premiumAvailable=$premiumAvailable, isNew=$isNew, userName=$userName)"
    }

}