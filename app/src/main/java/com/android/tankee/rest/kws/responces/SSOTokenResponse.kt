package com.android.tankee.rest.kws.responces

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SSOTokenResponse {

    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null
    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null

}