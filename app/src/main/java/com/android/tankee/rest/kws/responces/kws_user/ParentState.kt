package com.android.tankee.rest.kws.responces.kws_user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ParentState {

    @SerializedName("verified")
    @Expose
    var verified: Boolean? = null
    @SerializedName("expired")
    @Expose
    var expired: Boolean? = null
    @SerializedName("rejected")
    @Expose
    var rejected: Boolean? = null
    @SerializedName("idVerified")
    @Expose
    var idVerified: Boolean? = null
    @SerializedName("deleted")
    @Expose
    var deleted: Boolean? = null

}
