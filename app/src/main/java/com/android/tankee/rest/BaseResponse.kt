package com.android.tankee.rest

import com.android.tankee.rest.responses.sections.Video
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse<T> {
    @SerializedName("items")
    @Expose
    var items: List<T>? = null
    @SerializedName("hasData")
    @Expose
    var hasData: Boolean? = null
    @SerializedName("totalResults")
    @Expose
    var totalResults: Int? = null
}

open class BaseMoreVideosResponse<T> {
    @SerializedName("items")
    @Expose
    var items: List<T>? = null
    @SerializedName("hasData")
    @Expose
    var hasData: Boolean? = null
    @SerializedName("totalResults")
    @Expose
    var totalResults: Int? = null
    @SerializedName("last_video")
    @Expose
    var lastVideo: Video? = null
}