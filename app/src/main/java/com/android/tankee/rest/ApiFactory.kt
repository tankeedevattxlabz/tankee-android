package com.android.tankee.rest


import android.util.Log
import com.android.tankee.BuildConfig
import com.android.tankee.TankeeApp
import com.android.tankee.extentions.isNull
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiFactory {

    private var builder: OkHttpClient.Builder? = null

    private fun getClient(): OkHttpClient {
        if (builder.isNull()) {
            builder = OkHttpClient.Builder()

            builder?.eventListener(TankeeApp.connection.connectionStateListener)
            builder?.callTimeout(CALL_TIMEOUT, TimeUnit.SECONDS)
            builder?.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            builder?.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            builder?.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)

            builder?.addInterceptor { chain ->
                Log.d(TAG, "addInterceptor")

                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .addHeader("platform", "android")
                    .method(original.method(), original.body())

                val request = requestBuilder
                    .cacheControl(CacheControl.Builder().noCache().build())
                    .build()

                chain.proceed(request)
            }
        }

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            builder?.addInterceptor(logging)
        }

        return builder!!.build()
    }

    fun buildRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    companion object {
        var TAG: String = ApiFactory::class.java.simpleName

        private const val DEFAULT_TIMEOUT = 60L

        const val CALL_TIMEOUT = DEFAULT_TIMEOUT
        const val CONNECT_TIMEOUT = DEFAULT_TIMEOUT
        const val WRITE_TIMEOUT = DEFAULT_TIMEOUT
        const val READ_TIMEOUT = DEFAULT_TIMEOUT
    }
}