package com.android.tankee.rest

import android.content.Context
import android.util.Log
import com.android.tankee.BuildConfig
import com.android.tankee.R
import com.android.tankee.rest.responses.games.Game
import com.android.tankee.ui.MainActivity
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.home.videos.GameType
import com.android.tankee.ui.home.videos.VideosViewModel
import com.android.tankee.ui.index_page.GameContent
import com.android.tankee.ui.popups.BasePopup
import com.android.tankee.utils.isNetworkAvailable
import com.android.tankee.utils.log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class TankeeCallback<T> : Callback<T> {

    override fun onFailure(call: Call<T>, t: Throwable) {
        try {
            t.printStackTrace()
            if (!BuildConfig.DEBUG) {
                val crashlytics = FirebaseCrashlytics.getInstance()
                t.message?.let { crashlytics.log(it) }
            }
            if (
                t is UnknownHostException ||
                t is SocketTimeoutException ||
                t is ConnectException
            ) {
                onBadConnection(call)
            } else {
                onOtherError(call, t)
            }
        } catch (ignore: java.lang.Exception) {
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            onSuccess(response)
        } else {
            onOtherError(call, Exception(response.message()))
        }
    }

    open fun onSuccess(response: Response<T>) {}

    open fun onBadConnection(call: Call<T>) {

    }

    open fun onOtherError(call: Call<T>, t: Throwable) {}
    fun onNotAuthorized() {}
}

fun <T> BaseView.createCallBack(
    onDone: (Response<T>) -> Unit,
    onFail: ((Throwable) -> Unit)? = null
): TankeeCallback<T> {
    return object : TankeeCallback<T>() {
        override fun onSuccess(response: Response<T>) {
            try {
                onDone(response)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onOtherError(call: Call<T>, t: Throwable) {
            super.onOtherError(call, t)
            onFail?.invoke(t)
        }


        fun showPopup(call: Call<T>) {
            this@createCallBack.showPopUp(
                BasePopup.Builder()
                    .title("No internet connection")
                    .image(R.drawable.ic_wifi_off)
                    .message("Check your internet connection")
                    .buttons(mapOf("Try again" to { p ->

                        if (isNetworkAvailable()) {
                            call.clone().enqueue(this)
                            p.dismissAllowingStateLoss()
                        }
                    }))
                    .cancelable(false)
                    .create()
                , "NO_INTERNET_POPUP"
            )
        }

        override fun onBadConnection(call: Call<T>) {
            showPopup(call)

        }
    }

}


fun <T> BaseView.createCallBackWithBadConnection(
    onDone: (Response<T>) -> Unit,
    onBadConnect: (Boolean, Call<T>) -> Unit,
    onFail: ((Throwable) -> Unit)? = null
): TankeeCallback<T> {
    return object : TankeeCallback<T>() {
        override fun onSuccess(response: Response<T>) {
            try {
                onDone(response)
            } catch (e: Exception) {


                e.printStackTrace()
            }
        }

        override fun onOtherError(call: Call<T>, t: Throwable) {
            super.onOtherError(call, t)
            onBadConnect(false, call);
            onFail?.invoke(t)
        }

        override fun onBadConnection(call: Call<T>) {

            onBadConnect(false, call);


        }


    }

}


fun <T> Context.createCallBack(onDone: (Response<T>) -> Unit): TankeeCallback<T> {
    return object : TankeeCallback<T>() {
        override fun onSuccess(response: Response<T>) {
            onDone(response)
        }

        override fun onBadConnection(call: Call<T>) {
            this@createCallBack.toast("Bad connection").show()
        }
    }
}