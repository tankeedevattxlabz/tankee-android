package com.android.tankee.rest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BaseSectionResponse<T> : BaseResponse<T>() {

    @SerializedName("section_title")
    @Expose
    var sectionTitle: String? = null

}
