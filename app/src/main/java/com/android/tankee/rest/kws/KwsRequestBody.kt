package com.android.tankee.rest.kws


import com.android.tankee.TANKEE
import com.android.tankee.rest.utils.RequestBodyConverter
import okhttp3.RequestBody


data class ForgotPasswordRequestBody(
    val username: String,
    val appName: String = TANKEE
) {

    object Builder {

        fun buildForgotPasswordRequestBody(parentEmailOrUsername: String): RequestBody {
            val forgotPasswordRequestBody = ForgotPasswordRequestBody(
                parentEmailOrUsername
            )

            return RequestBodyConverter.convertToRequestBody(forgotPasswordRequestBody)
        }

    }

}

data class DeleteAccountRequestBody(
    val password: String
) {

    object Builder {

        fun buildBody(password: String): RequestBody {
            val body = DeleteAccountRequestBody(password)
            return RequestBodyConverter.convertToRequestBody(body)
        }

    }

}