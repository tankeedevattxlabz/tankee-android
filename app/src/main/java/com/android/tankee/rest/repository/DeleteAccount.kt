package com.android.tankee.rest.repository


import com.android.tankee.cookies.TankeePrefs
import com.android.tankee.database.AppDatabase
import com.android.tankee.extentions.isNotNull
import com.android.tankee.extentions.isNull
import com.android.tankee.logs.AppLogger
import com.android.tankee.logs.LogStatus
import com.android.tankee.notifications.TankeeMessagingService.Companion.FCM_TAG
import com.android.tankee.repo.user.FcmTokenRepo
import com.android.tankee.rest.TankeeRest
import com.android.tankee.rest.createCallBack
import com.android.tankee.rest.kws.DeleteAccountRequestBody
import com.android.tankee.rest.kws.KwsRest
import com.android.tankee.rest.kws.KwsRestUtils
import com.android.tankee.ui.authorization.model.UserAccountModel
import com.android.tankee.ui.base.BaseView
import com.android.tankee.ui.base.EventHandler


class DeleteAccount(
    private val baseView: BaseView,
    private val eventHandler: EventHandler?,
    private val userAccountModel: UserAccountModel?
) {

    fun deleteAccount() {
        eventHandler?.onStart?.invoke()
        getSSOToken()
    }

    /**
     * Step 1
     */
    private fun getSSOToken() {
        AppLogger.logDebug(DELETE_ACCOUNT_FLOW, "Get SSO Token from KWS server.", 1, LogStatus.EXECUTING)

        val authorization = buildGetSSOTokenHeader()

        if (userAccountModel.isNotNull()) {
            KwsRest.api.getSSOToken(authorization, userAccountModel!!.userName, userAccountModel.password).enqueue(
                baseView.createCallBack(
                    { response ->
                        AppLogger.logDebug(
                            DELETE_ACCOUNT_FLOW,
                            "Get SSO Token from KWS server.",
                            1,
                            LogStatus.SUCCESS
                        )
                        AppLogger.logDebug(
                            DELETE_ACCOUNT_FLOW,
                            "SSO Token:\t${response.body()?.accessToken}",
                            1,
                            LogStatus.SUCCESS
                        )
                        response.body()?.accessToken?.let { deleteAccount(it) }
                    },
                    {
                        eventHandler?.onFailed?.invoke()
                        eventHandler?.onError?.invoke()

                        AppLogger.logDebug(
                            DELETE_ACCOUNT_FLOW,
                            "SSO Token was not gated.",
                            1,
                            LogStatus.FAILED
                        )
                    }
                )
            )
        } else {
            if (userAccountModel != null) {
                if (userAccountModel.userName.isNull()) {
                    AppLogger.logDebug(
                        DELETE_ACCOUNT_FLOW,
                        "SSO Token was not gated.\tUserName is null.",
                        2,
                        LogStatus.FAILED
                    )
                }

                if (userAccountModel.password.isNull()) {
                    AppLogger.logDebug(
                        DELETE_ACCOUNT_FLOW,
                        "SSO Token was not gated.\tPassword is null.",
                        2,
                        LogStatus.FAILED
                    )
                }
            }
        }
    }

    private fun buildGetSSOTokenHeader() =
        KwsRestUtils.buildBasicHeader("c3VwZXJhd2Vzb21lY2x1YjpzdXBlcmF3ZXNvbWVjbHVi")

    /**
     * Step 2
     */
    private fun deleteAccount(ssoToken: String) {
        AppLogger.logDebug(DELETE_ACCOUNT_FLOW, "Delete account on KWS server.", 2, LogStatus.EXECUTING)

        val authoHeader = KwsRestUtils.buildBearerHeader(ssoToken)
        val userId = AppDatabase.getAppDatabase().kwsUserDao().get()?.id

        if (userId.isNotNull() && userAccountModel?.password.isNotNull()) {
            KwsRest.api.deleteAccount(
                authoHeader,
                userId!!,
                DeleteAccountRequestBody.Builder.buildBody(userAccountModel!!.password)
            )
                .enqueue(baseView.createCallBack(
                    { response ->
                        AppLogger.logDebug(
                            DELETE_ACCOUNT_FLOW,
                            "Account was deleted on KWS server.",
                            2,
                            LogStatus.SUCCESS
                        )

                        if (response.code() == 204) {
                            clearFcmTokenAndTimeZone()
                        }
                    },
                    {
                        eventHandler?.onFailed?.invoke()
                        eventHandler?.onError?.invoke()

                        AppLogger.logDebug(
                            DELETE_ACCOUNT_FLOW,
                            "Account was not deleted on KWS server.",
                            2,
                            LogStatus.FAILED
                        )
                    }
                )
                )
        } else {
            if (userId.isNull()) {
                AppLogger.logDebug(
                    DELETE_ACCOUNT_FLOW,
                    "Delete account on KWS server.\tUserId is null.",
                    2,
                    LogStatus.FAILED
                )
            }

            if (userAccountModel?.password.isNull()) {
                AppLogger.logDebug(
                    DELETE_ACCOUNT_FLOW,
                    "Delete account on KWS server.\tPassword is null.",
                    2,
                    LogStatus.FAILED
                )
            }
        }
    }

    /**
     * Step 3
     */
    private fun clearFcmTokenAndTimeZone() {
        AppLogger.logDebug(DELETE_ACCOUNT_FLOW, "Clear $FCM_TAG token", 3, LogStatus.EXECUTING)

        val fcmTokenRepo = FcmTokenRepo(baseView)
        fcmTokenRepo.onUpdateAction = { deleteUserAccount() }
        fcmTokenRepo.onErrorAction = { eventHandler?.onError?.invoke() }
        fcmTokenRepo.clearFcmTokenAndTimeZone()
    }

    /**
     * Step 4
     */
    private fun deleteUserAccount() {
        AppLogger.logDebug(DELETE_ACCOUNT_FLOW, "Delete account on Tankee server.", 4, LogStatus.EXECUTING)
        AppLogger.logDebug(
            DELETE_ACCOUNT_FLOW,
            "${TankeePrefs.TAG} token:\t${TankeePrefs.token}",
            3,
            LogStatus.EXECUTING
        )

        TankeeRest.api.deleteUser()
            .enqueue(baseView.createCallBack(
                {
                    AppLogger.logDebug(
                        DELETE_ACCOUNT_FLOW,
                        "Account was deleted on Tankee server.",
                        4,
                        LogStatus.SUCCESS
                    )

                    eventHandler?.onSuccess?.invoke()

                },
                {
                    eventHandler?.onError?.invoke()

                    AppLogger.logDebug(
                        DELETE_ACCOUNT_FLOW,
                        "Account was not deleted on Tankee server.",
                        4,
                        LogStatus.FAILED
                    )
                }
            )
            )
    }


    companion object {
        private const val DELETE_ACCOUNT_FLOW = "DeleteAccountFlow"
    }
}

