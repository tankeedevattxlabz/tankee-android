package com.android.tankee.rest.kws.responces.errors

data class KwsErrorModel(
    var errorCode: String? = null,
    var error: String? = null
)