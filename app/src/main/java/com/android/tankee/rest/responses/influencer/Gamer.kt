package com.android.tankee.rest.responses.influencer

import com.android.tankee.EMPTY_STRING
import com.android.tankee.ui.views.see_all.SectionItemMarker
import com.android.tankee.utils.buildThumbnailUrl
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Gamer : SectionItemMarker {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("primary_game")
    @Expose
    var primaryGame: String? = null
    @SerializedName("secondary_game")
    @Expose
    var secondaryGame: String? = null
    @SerializedName("subscribers")
    @Expose
    var subscribers: Any? = null
    @SerializedName("video_count")
    @Expose
    var videoCount: Any? = null
    @SerializedName("game_type")
    @Expose
    var gameType: String? = null
    @SerializedName("primary_video_type")
    @Expose
    var primaryVideoType: String? = null
    @SerializedName("secondary_video_type")
    @Expose
    var secondaryVideoType: String? = null
    @SerializedName("tier")
    @Expose
    var tier: Any? = null
    @SerializedName("related_influencers")
    @Expose
    var relatedInfluencers: String? = null
    @SerializedName("pr13_content")
    @Expose
    var pr13Content: Boolean? = null
    @SerializedName("contact")
    @Expose
    var contact: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("image_url")
    @Expose
    var imageUrl: ImageUrl? = null
    @SerializedName("channel_name")
    @Expose
    var channelName: String? = null
    @SerializedName("priority")
    @Expose
    var priority: Int? = null
    @SerializedName("playlist_key")
    @Expose
    var playlistKey: String? = null
    @SerializedName("tags_string")
    @Expose
    var tagsString: String? = null
    @SerializedName("playlist_id")
    @Expose
    var playlistId: Any? = null
    @SerializedName("class")
    @Expose
    var class_: String? = null
    @SerializedName("like")
    @Expose
    var like: Boolean? = null
    @SerializedName("channel_id")
    @Expose
    var channelId: String? = null
    @SerializedName("channel_type")
    @Expose
    var channelType: String? = null
    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    fun getValidImageUrl(): String? {
        if (imageUrl != null && imageUrl!!.url!= null) {
            return buildThumbnailUrl(imageUrl!!.url)
        }

        if (url != null && url!!.isNotEmpty()) {
            return buildThumbnailUrl(url)
        }

        return EMPTY_STRING
    }

    fun isFavoriteGamer(): Boolean {
        return if (like != null) {
            like!!
        } else {
            true
        }
    }

    fun isNotFavoriteGamer(): Boolean {
        return if (like != null) {
            !like!!
        } else {
            true
        }
    }
}
