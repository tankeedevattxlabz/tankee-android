package com.android.tankee.rest.kws.api

import com.android.tankee.rest.kws.GRANT_TYPE
import com.android.tankee.rest.kws.responces.KwsTokenModel
import com.android.tankee.rest.kws.responces.SSOTokenResponse
import com.android.tankee.rest.kws.responces.activate_user_account.ActivateUserAccountRequestBody
import com.android.tankee.rest.kws.responces.check_username.CheckUserNameResponse
import com.android.tankee.rest.kws.responces.create_user.KwsCreateUserResponse
import com.android.tankee.rest.kws.responces.kws_user.KwsUserModel
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface KwsApi {

    /**
     * Login
     */
    @FormUrlEncoded
    @POST("oauth/token")
    fun getToken(
        @Header("Authorization") authorization: String,
        @Field("username") userName: String,
        @Field("password") password: String,
        @Field("grant_type") grandType: String = GRANT_TYPE
    ): Call<KwsTokenModel>

    @GET("v2/apps/2/users/{userId}")
    fun getUser(
        @Header("Authorization") authorization: String,
        @Path("userId") userId: String
    ): Call<KwsUserModel>

    @GET("v1/users/check-username")
    fun checkUserName(
        @Query("username") userName: String
    ): Call<CheckUserNameResponse>

    @POST("v2/users")
    fun createUser(
        @Body createUserRequestBody: RequestBody
    ): Call<KwsCreateUserResponse>

    @GET("v2/apps/2/randomDisplayName")
    fun generateDisplayName(): Call<String>

    @POST("v1/users/{userId}/apps")
    fun activateUserAccount(
        @Header("Authorization") authorization: String,
        @Path("userId") userId: String,
        @Body requestBody: RequestBody
    ): Call<ResponseBody>

    @POST("v1/users/forgotPassword")
    fun forgotPassword(
        @Body requestBody: RequestBody
    ): Call<ResponseBody>

    /**
     * Delete user
     */
    @FormUrlEncoded
    @POST("oauth/token")
    fun getSSOToken(
        @Header("Authorization") authorization: String,
        @Field("username") userName: String,
        @Field("password") password: String,
        @Field("grant_type") grandType: String = GRANT_TYPE
    ): Call<SSOTokenResponse>

    @POST("v1/users/{userId}/delete-account")
    fun deleteAccount(
        @Header("Authorization") authorization: String,
        @Path("userId") userId: String,
        @Body requestBody: RequestBody
    ): Call<ResponseBody>
}