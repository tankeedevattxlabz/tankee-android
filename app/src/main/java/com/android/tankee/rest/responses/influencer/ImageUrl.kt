package com.android.tankee.rest.responses.influencer

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageUrl {

    @SerializedName("url")
    @Expose
    var url: String? = null

}
