package com.android.tankee.time

import android.annotation.SuppressLint
import java.text.SimpleDateFormat

object TimeUtils {

    @SuppressLint("SimpleDateFormat")
    fun convertTimeToUTC(timeInMillis: Long): String? {
        val sdf = SimpleDateFormat("yyyy-MMM-dd HH:mm:ss")
        return sdf.format(timeInMillis)
    }
}