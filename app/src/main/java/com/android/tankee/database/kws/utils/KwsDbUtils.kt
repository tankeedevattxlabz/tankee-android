package com.android.tankee.database.kws.utils

import com.android.tankee.database.AppDatabase

object KwsDbUtils {
    fun deleteAll() {
        AppDatabase.getAppDatabase().kwsUserIdDao().deleteAll()
    }
}