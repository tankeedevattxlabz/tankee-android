package com.android.tankee.database.kws.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.tankee.database.KWS_USER_ID_TABLE

@Entity(tableName = KWS_USER_ID_TABLE)
data class KwsUserIdModel(
    @PrimaryKey(autoGenerate = false)
    var kwsUserId: String
) {
    companion object {
        var TAG: String = KwsUserIdModel::class.java.simpleName
    }
}