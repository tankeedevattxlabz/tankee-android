package com.android.tankee.database


import com.android.tankee.logs.AppLogger
import com.android.tankee.rest.responses.users.UserModel


object TankeeDbHelper {

    var TAG: String = TankeeDbHelper::class.java.simpleName

    fun insertUserModel(userModel: UserModel) {
        AppLogger.logDebug(TAG, "Inserting user model into db:\t$userModel")

        AppDatabase.getAppDatabase()
            .userDao()
            .insetUserModel(userModel)

        AppLogger.logDebug(TAG, "User model was inserted into db:\t$userModel")
    }


}