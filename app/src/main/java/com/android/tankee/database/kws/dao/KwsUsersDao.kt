package com.android.tankee.database.kws.dao


import androidx.room.*
import com.android.tankee.database.KWS_USERS_TABLE
import com.android.tankee.rest.kws.responces.kws_user.KwsUserModel


@Dao
interface KwsUsersDao {

    @Query("SELECT * FROM $KWS_USERS_TABLE LIMIT 1")
    fun get(): KwsUserModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userModel: KwsUserModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(kwsUserModel: KwsUserModel)

    @Query("DELETE FROM $KWS_USERS_TABLE")
    fun deleteAll()

    @Transaction
    fun insertKwsUserModel(kwsUserModel: KwsUserModel) {
        deleteAll()
        insert(kwsUserModel)
    }

}