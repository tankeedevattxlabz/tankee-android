package com.android.tankee.database

const val DB_NAME = "tankee_db"
const val DB_VERSION = 2

const val SUGGESTION_TABLE_NAME = "suggestions"
const val USERS_TABLE_NAME = "users"

/**
 * KWS
 */
const val KWS_USERS_TABLE = "kws_users"
const val KWS_USER_ID_TABLE = "kws_user_id"