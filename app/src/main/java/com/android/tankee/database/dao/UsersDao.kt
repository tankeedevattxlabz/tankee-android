package com.android.tankee.database.dao

import androidx.room.*
import com.android.tankee.database.USERS_TABLE_NAME
import com.android.tankee.rest.responses.users.UserModel

@Dao
interface UsersDao {

    @Query("SELECT * FROM $USERS_TABLE_NAME LIMIT 1")
    fun getUser(): UserModel?

    @Query("SELECT avatar FROM $USERS_TABLE_NAME")
    fun getUserAvatar(): String?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userModel: UserModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUser(userModel: UserModel)

    @Query("DELETE FROM $USERS_TABLE_NAME")
    fun deleteAll()

    @Transaction
    fun insetUserModel(userModel: UserModel) {
        deleteAll()
        insertUser(userModel)
    }

}