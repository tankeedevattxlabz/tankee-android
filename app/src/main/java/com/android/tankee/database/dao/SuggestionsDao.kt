package com.android.tankee.database.dao

import androidx.room.*
import com.android.tankee.database.SUGGESTION_TABLE_NAME
import com.android.tankee.database.models.SuggestionModel

@Dao
interface SuggestionsDao {

    @Query("SELECT * FROM $SUGGESTION_TABLE_NAME")
    fun getAll(): List<SuggestionModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(suggestionModel: SuggestionModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(suggestionModel: SuggestionModel)

    @Delete
    fun delete(lastItem: SuggestionModel?)

}