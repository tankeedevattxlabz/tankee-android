package com.android.tankee.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import com.android.tankee.database.converters.Converters
import com.android.tankee.database.dao.SuggestionsDao
import com.android.tankee.database.dao.UsersDao
import com.android.tankee.database.kws.dao.KwsUserIdDao
import com.android.tankee.database.kws.dao.KwsUsersDao
import com.android.tankee.database.kws.models.KwsUserIdModel
import com.android.tankee.database.models.SuggestionModel
import com.android.tankee.getAppContext
import com.android.tankee.rest.kws.responces.kws_user.KwsUserModel
import com.android.tankee.rest.responses.users.UserModel
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [
        SuggestionModel::class,
        KwsUserIdModel::class,
        UserModel::class,
        KwsUserModel::class
    ], version = DB_VERSION
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun suggestionsDao(): SuggestionsDao

    abstract fun userDao(): UsersDao

    /**
     * KWS
     */
    abstract fun kwsUserIdDao(): KwsUserIdDao

    abstract fun kwsUserDao(): KwsUsersDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(getAppContext(), AppDatabase::class.java, DB_NAME)
                    .allowMainThreadQueries()
                    .addMigrations(MIGRATION_1_2)
                    .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE users ADD COLUMN userName TEXT")
            }
        }
    }
}
