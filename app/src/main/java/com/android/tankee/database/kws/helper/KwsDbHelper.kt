package com.android.tankee.database.kws.helper


import com.android.tankee.database.AppDatabase
import com.android.tankee.database.kws.models.KwsUserIdModel
import com.android.tankee.logs.AppLogger
import com.android.tankee.rest.kws.responces.kws_user.KwsUserModel


object KwsDbHelper {

    var TAG: String = KwsDbHelper::class.java.simpleName

    fun insertKwsUserId(kwsUserIdModel: KwsUserIdModel) {
        AppLogger.logDebug(TAG, "Inserting Kws use id into db:\t${kwsUserIdModel.kwsUserId}")

        AppDatabase.getAppDatabase()
            .kwsUserIdDao()
            .insertKwsUserId(kwsUserIdModel)

        AppLogger.logDebug(TAG, "Kws user id inserted into db.")
    }

    fun insertKwsUserModel(kwsUserModel: KwsUserModel) {
        AppLogger.logDebug(TAG, "Inserting Kws use model into db:\t$kwsUserModel")

        AppDatabase.getAppDatabase()
            .kwsUserDao()
            .insertKwsUserModel(kwsUserModel)

        AppLogger.logDebug(TAG, "Kws user model inserted into db:\t$kwsUserModel")
    }

    fun getGeneratedName(): String? {
        return AppDatabase.getAppDatabase()
            .kwsUserDao()
            .get()
            ?.username
    }

    fun deleteAll() {
        AppDatabase.getAppDatabase().clearAllTables()
    }

}
