package com.android.tankee.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.tankee.database.SUGGESTION_TABLE_NAME

@Entity(tableName = SUGGESTION_TABLE_NAME)
data class SuggestionModel(
        @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
        var id: Int = 0,
        var title: String? = null,
        var jwKey: String? = null,
        var playlistKey: String? = null,
        var createdAt: Long = 0
)