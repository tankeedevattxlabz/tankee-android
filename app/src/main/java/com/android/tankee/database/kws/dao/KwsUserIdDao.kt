package com.android.tankee.database.kws.dao

import androidx.room.*
import com.android.tankee.database.KWS_USER_ID_TABLE
import com.android.tankee.database.kws.models.KwsUserIdModel

@Dao
interface KwsUserIdDao {

    @Query("SELECT * FROM $KWS_USER_ID_TABLE LIMIT 1")
    fun get(): String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(kwsUserId: KwsUserIdModel)

    @Query("DELETE FROM $KWS_USER_ID_TABLE")
    fun deleteAll()

    @Transaction
    fun insertKwsUserId(kwsUserId: KwsUserIdModel) {
        deleteAll()
        insert(kwsUserId)
    }
}